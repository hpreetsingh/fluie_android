package servicio;

import android.util.Log;

public class log_aplicacion {
	private static boolean depuracion = false;
	private static boolean informacion = false;
	private static boolean error = false;
	private static String TAG = "fluied";

	public static void log_depueracion(String msg) {
		if (depuracion) {
			Log.d(TAG, msg);
		}
	}

	public static void log_error(String msg) {
		if (error) {
			Log.e(TAG, "informacion fluie: " + msg);
		}
	}

	public static void log_informacion(String msg) {
		if (informacion) {
			Log.i(TAG, "informacion fluie: " + msg);
		}
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public void setDepuracion(boolean depuracion) {
		this.depuracion = depuracion;
	}

	public void setInformacion(boolean info) {
		this.informacion = informacion;
	}

}
