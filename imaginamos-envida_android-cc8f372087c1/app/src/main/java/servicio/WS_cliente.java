package servicio;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.zonesoftware.fluie.R;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import modelo.Imagen;

public class WS_cliente extends AsyncTask<String, Void, Void> {

	private Context myContext;
	private String servicio;
	public boolean Adjunto = false;

	private MultipartEntity reqEntity;

//	http://fluie.com/fluie/
//	http://fluie.com/fluie/http://mentesweb.com/fluie/programacion/
	public String url_servicio = "http://fluie.com/fluie/api";

	/******************** servicios fluie ************************/
	String url_imagen = "";
	public static String home = "home";
	public static String login = "login";
	public static String pais = "pais";
	public static String crear_usuario = "crear_usuario";
	public static String crear_ben_ver = "crear_ben_ver";
	public static String userRole = "userRole";
	public static String Update_Usuario = "Update_Usuario";
	public static String beneficiaryUser = "beneficiaryUser";
	public static String Asignacion_verificador = "Asignacion_verificador";
	public static String get_usuario = "get_usuario";

	public static String GET = "GET";
	public static String POST = "POST";
	public static String PUT = "PUT";
	public static String DELETE = "DELETE";

	String url_metodo = "";
	Handler puente_envio;
	Message msg;
	String Respuesta = "";

	// verificacion de internet
	PruebaInternet prueba_internet;
	boolean envio = false;
	boolean time_out = false;
	WS_cliente ws;
	boolean cancelado = false;
	String idioma = "";
	String tipo = "";
	String id_idioma = "";
	String autenticacion = "Basic YWRtaW46YWRtaW4=";
	String mimeType = "";
	MultipartUtility multipartUtil = null;
	MultipartEntityBuilder builder = null;
	Charset chars = Charset.forName("UTF-8");
	// mensaje

	String type_file_id = "";
	String type_message_id = "";
	String title = "";
	String date_view = "";
	String description = "";
	String file = "";
	String beneficiaries = "";
	ArrayList<Imagen> imagenes = null;
	String file_url = "";
	String idmessage = "";
	ContentType contentType;
	String id_consulta = "";

	public WS_cliente(Context myContext) throws JSONException, UnsupportedEncodingException {
		this.myContext = myContext;

		this.tipo = tipo;
		msg = new Message();
		ws = this;
		prueba_internet = new PruebaInternet(myContext);
		id_idioma = "1";
		idioma = Locale.getDefault().getLanguage();
		log_aplicacion.log_informacion(idioma);
		if (idioma.equals("es")) {
			id_idioma = "1";
		} else {
			id_idioma = "2";

		}
		chars = Charset.forName("UTF-8");
		contentType = ContentType.create(HTTP.PLAIN_TEXT_TYPE, HTTP.UTF_8);
	}
	public String get_locallanguage() {
		PackageManager packageManager = myContext.getPackageManager();
		Resources resources = null;
		boolean add=false;
		String lang="";
		try {
			resources = packageManager.getResourcesForApplication("android");
			add=true;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
			add=false;
		}
		if(add){
			lang=resources.getConfiguration().locale.getLanguage();
		}else {
			lang="en";
		}
		return lang;
	}
	public void get_home() {
		try {
			JSONObject json_idioma = new JSONObject();
			json_idioma.put("language_id", id_idioma);
			url_metodo = url_servicio + "/home?filter=" + URLEncoder.encode(json_idioma.toString(), "UTF-8");
			log_aplicacion.log_informacion(url_metodo);
			tipo = GET;
			servicio = home;
			Adjunto = false;
			reqEntity = new MultipartEntity();

		} catch (Exception e) {
			// TODO: handle exception

		}
	}

	public void getsplash() {
		try {
			JSONObject json_idioma = new JSONObject();
			json_idioma.put("language_id", id_idioma);

			url_metodo = "http://fluie.com/fluie/api/banner/?filter="
					+ URLEncoder.encode(json_idioma.toString(), "UTF-8");
			log_aplicacion.log_informacion(url_metodo);
			tipo = GET;
			servicio = "getsplash";
		} catch (Exception e) {
			// TODO: handle exception

		}

	}

	public void login(String email, String password) {
		try {
			Adjunto = false;
			reqEntity = new MultipartEntity();
			reqEntity.addPart("email", new StringBody(email));
			reqEntity.addPart("password", new StringBody(password));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
			url_metodo = url_servicio + "/user/login";
			servicio = login;
			tipo = POST;

		} catch (Exception e) {
			// TODO: handle exception

		}
	}

	public void get_usuario(String id) throws JSONException, UnsupportedEncodingException {
		JSONObject json_id = new JSONObject();

		json_id.put("iduser", id);

		url_metodo = url_servicio + "/user?filter=" + URLEncoder.encode(json_id.toString(), "UTF-8");
		tipo = GET;
		servicio = "get_usuario";
	}

	public void get_usuarios_lista(String id) throws JSONException, UnsupportedEncodingException {
		JSONObject json_id = new JSONObject();

		json_id.put("iduser", id);

		tipo = GET;
		servicio = "get_usuario_beneficiario";

		if (data_servicio.lista.equals("beneficiarios")) {
			url_metodo = url_servicio + "/user?filter=" + URLEncoder.encode(json_id.toString(), "UTF-8");
			servicio = "get_usuario_beneficiario";

		} else if (data_servicio.lista.equals("verificadores")) {
			url_metodo = url_servicio + "/user?filter=" + URLEncoder.encode(json_id.toString(), "UTF-8");
			servicio = "get_usuario_verificadores";
		}

	}

	public void get_lista_relacion() throws JSONException, UnsupportedEncodingException {
		if(get_locallanguage().equalsIgnoreCase("es")) {
			url_metodo = url_servicio + "/relationship?"+"language=1";
		}else{
			url_metodo = url_servicio + "/relationship?"+"language=2";
		}
		tipo = GET;
		servicio = "get_lista_relaciones";

	}

	public void get_lista_verificador(String id) throws JSONException, UnsupportedEncodingException {

		JSONObject json_id = new JSONObject();

		json_id.put("user_id", id);

		url_metodo = url_servicio + "/checkerUser?with=checker&filter="
				+ URLEncoder.encode(json_id.toString(), "UTF-8");
		tipo = GET;
		servicio = "get_lista_verificadores";

	}

	public void get_lista_beneficiario(String id) throws JSONException, UnsupportedEncodingException {
		JSONObject json_id = new JSONObject();

		json_id.put("user_id", id);

		url_metodo = url_servicio + "/beneficiaryUser?with=beneficiary&filter="
				+ URLEncoder.encode(json_id.toString(), "UTF-8");
		tipo = GET;
		servicio = "get_lista_beneficiarios";
	}

	public void set_beneficiario(String id) throws JSONException, UnsupportedEncodingException {
		JSONObject json_id = new JSONObject();

		json_id.put("user_id", id);
		if(get_locallanguage().equalsIgnoreCase("es")) {
			json_id.put("language", "1");
		}else{
			json_id.put("language", "2");
		}
		url_metodo = url_servicio + "/beneficiaryUser?filter=" + URLEncoder.encode(json_id.toString(), "UTF-8");
		tipo = POST;
		servicio = "set_beneficiarios";

		/*
		 * beneficiary_id , ￼ ￼user_id, ￼code , ￼relationship_id
		 * 
		 */

	}

	public void getcountry() {
		url_metodo = url_servicio + "/country?idcountry=1";
		tipo = GET;
		servicio = pais;
	}

	public void getplanes() throws JSONException, UnsupportedEncodingException {

		JSONObject json_idioma = new JSONObject();
		json_idioma.put("language_id", id_idioma);

		url_metodo = "http://fluie.com/fluie/api/space?filter="
				+ URLEncoder.encode(json_idioma.toString(), "UTF-8");
		tipo = GET;
		servicio = "getplanes";
	}

	public void crear_usuario(String email, String password, String name, String name_last, String identification,
			String country_id, String phone, String address, String image, String email_optional, String notification,
			String pass, String confirm_password, String terms, String type_identification_id) {
		try {

			reqEntity = new MultipartEntity();
			reqEntity.addPart("email", new StringBody(email));
			reqEntity.addPart("password", new StringBody(password));
			reqEntity.addPart("name", new StringBody(name, chars));
			reqEntity.addPart("name_last", new StringBody(name_last, chars));
			reqEntity.addPart("identification", new StringBody(identification));
			reqEntity.addPart("country_id", new StringBody(country_id));
			reqEntity.addPart("phone", new StringBody(phone));
			reqEntity.addPart("address", new StringBody(address, chars));
			reqEntity.addPart("type_identification_id", new StringBody(type_identification_id));

			try {

				File f = new File(image);
				if (f.exists()) {
					ContentBody contenedor = new FileBody(f, "image/png");
					reqEntity.addPart("image", new FileBody(f));
				}
			} catch (Exception ex) {
				log_aplicacion.log_error(ex.toString());
			}

			reqEntity.addPart("space", new StringBody("0"));
			reqEntity.addPart("email_optional", new StringBody(email_optional));

			reqEntity.addPart("notification", new StringBody(notification));

			reqEntity.addPart("pass", new StringBody(pass));
			reqEntity.addPart("confirm_password", new StringBody(confirm_password));
			reqEntity.addPart("terms", new StringBody(terms));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1"));
			}else{
				reqEntity.addPart("language", new StringBody("2"));
			}
			url_metodo = url_servicio + "/user";
			tipo = POST;

			servicio = crear_usuario;

		} catch (Exception e) {
			// TODO: handle exception

		}

	}

	public void crear_ben_ver(String id, String email, String name, String apellido, String relacion,
			String genero_tel) {
		try {

			reqEntity = new MultipartEntity();

			if (data_servicio.lista.equals("beneficiarios")) {

				url_metodo = "http://fluie.com/fluie/user/SaveBeneficiary";
				reqEntity.addPart("user_id", new StringBody(data_servicio.usuario.getUser_id()));
				reqEntity.addPart("b[name]", new StringBody(name, chars));
				if(get_locallanguage().equalsIgnoreCase("es")) {
					reqEntity.addPart("language", new StringBody("1"));
				}else{
					reqEntity.addPart("language", new StringBody("2"));
				}

				StringBody stb_apellido = new StringBody(apellido, chars);

				reqEntity.addPart("b[name_last]", stb_apellido);
				reqEntity.addPart("b[email]", new StringBody(email));
				reqEntity.addPart("b[gender_id]", new StringBody(genero_tel));

				reqEntity.addPart("b[relationship_id]", new StringBody(relacion));

				log_aplicacion.log_informacion("user_id" + data_servicio.usuario.getUser_id());
				log_aplicacion.log_informacion("b[name]" + name);
				log_aplicacion.log_informacion("b[email]" + email);
				log_aplicacion.log_informacion("b[gender_id]" + genero_tel);
				log_aplicacion.log_informacion("b[relationship_id]" + relacion);
			} else if (data_servicio.lista.equals("verificadores")) {
				url_metodo = "http://fluie.com/fluie/user/SaveChecker";

				reqEntity.addPart("user_id", new StringBody(id));
				reqEntity.addPart("v[name]", new StringBody(name, chars));
				reqEntity.addPart("v[name_last]", new StringBody(apellido, chars));
				reqEntity.addPart("v[email]", new StringBody(email));
				reqEntity.addPart("v[gender_id]", new StringBody(genero_tel));

				reqEntity.addPart("v[phone]", new StringBody(relacion));
				if(get_locallanguage().equalsIgnoreCase("es")) {
					reqEntity.addPart("language", new StringBody("1"));
				}else{
					reqEntity.addPart("language", new StringBody("2"));
				}

			}

			tipo = POST;

			servicio = crear_ben_ver;

		} catch (Exception e) {
			// TODO: handle exception

		}

	}

	public void getfreespace() {

		url_metodo = url_servicio + "/user/freeSpace/" + data_servicio.usuario.getUser_id();
		tipo = GET;
		servicio = "getfreespace";
	}

	public void getlistdocumento() {
		try {

			JSONObject json_idioma = new JSONObject();
			json_idioma.put("language_id", id_idioma);

			url_metodo = "http://fluie.com/fluie/api/typeIdentification";
			tipo = GET;
			servicio = "getlistdocumento";
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void getespacio_msg() {

		url_metodo = "http://fluie.com/fluie/api/user/sizeFiles/" + data_servicio.usuario.getUser_id();
		tipo = GET;
		servicio = "getespacio_msg";
	}

	public void getespacio_consejos(String id, String type_message) {

		url_metodo = "http://fluie.com/fluie/api/user/sizeFilesTypeMessage";
		tipo = POST;
		reqEntity = new MultipartEntity();
		try {
			reqEntity.addPart("id", new StringBody(id));
			reqEntity.addPart("type_message", new StringBody(type_message));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		servicio = "getespacio_consejos";
	}

	public void Update_ben_verif(String id, String idbeneficiaryuser, String nombre, String apellido, String email,
			String relacion, String genero_tel, String telefono) {

		servicio = "Update_Usuario";
		tipo = "POST";
		log_aplicacion.log_informacion("b[name]" + nombre);
		log_aplicacion.log_informacion("b[email]" + email);
		log_aplicacion.log_informacion("b[gender_id]" + genero_tel);
		log_aplicacion.log_informacion("b[relationship_id]" + relacion);
		log_aplicacion.log_informacion("b[idbeneficiaryuser]" + relacion);

		log_aplicacion.log_informacion("tipo " + data_servicio.lista.equals("beneficiarios"));
		builder = MultipartEntityBuilder.create();
		Charset chars = Charset.forName("UTF-8");
		builder.setCharset(chars);
		/* example for setting a HttpMultipartMode */
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

		try {

			if (data_servicio.lista.equals("beneficiarios")) {

				url_metodo = "http://fluie.com/fluie/user/EditBeneficiary";

				builder.addPart("b[iduser]", new StringBody(id, contentType));
				builder.addPart("b[name]", new StringBody(nombre, contentType));
				builder.addPart("b[name_last]", new StringBody(apellido, contentType));
				builder.addPart("b[relationship_id]", new StringBody(relacion, contentType));
				builder.addPart("b[idbeneficiaryuser]", new StringBody(idbeneficiaryuser, contentType));
				builder.addPart("b[email]", new StringBody(email, contentType));
				builder.addPart("b[gender_id]", new StringBody(genero_tel, contentType));
				log_aplicacion.log_informacion("relacion edit " + relacion);

			} else if (data_servicio.lista.equals("verificadores")) {
				url_metodo = "http://fluie.com/fluie/user/EditChecker";
				builder.addPart("v[iduser]", new StringBody(id, contentType));
				builder.addPart("v[name_last]", new StringBody(apellido, contentType));
				builder.addPart("v[name]", new StringBody(nombre, contentType));
				builder.addPart("v[email]", new StringBody(email, contentType));
				builder.addPart("v[phone]", new StringBody(telefono, contentType));
				builder.addPart("v[gender_id]", new StringBody(genero_tel, contentType));

			}
			if(get_locallanguage().equalsIgnoreCase("es")) {
				builder.addPart("language", new StringBody("1"));
			}else{
				builder.addPart("language", new StringBody("2"));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void delete_ben_ver(String id, String idbeneficiaryuser) {

		servicio = "delete_ben_ver";
		tipo = "POST";

		reqEntity = new MultipartEntity();
		try {

			if (data_servicio.lista.equals("beneficiarios")) {

				url_metodo = "http://fluie.com/fluie/user/DeleteBeneficiary";
				reqEntity = new MultipartEntity();

				reqEntity.addPart("user_id", new StringBody(id));
				reqEntity.addPart("beneficiary_id", new StringBody(idbeneficiaryuser));


			} else if (data_servicio.lista.equals("verificadores")) {
				url_metodo = "http://fluie.com/fluie/user/DeleteChecker";

				reqEntity.addPart("user_id", new StringBody(id));
				reqEntity.addPart("checker_id", new StringBody(idbeneficiaryuser));

			}
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void Update_Usuario(String id, String email, String email_opcional, String idenficacion, String name,
			String name_last, String country_id, String phone, String address, String image, String id_doc)
					throws UnsupportedEncodingException {

		reqEntity = new MultipartEntity();
		servicio = "Update_Usuario";

		tipo = "POST";

		reqEntity.addPart("iduser", new StringBody(data_servicio.usuario.getUser_id()));
		reqEntity.addPart("country_id", new StringBody(country_id));
		reqEntity.addPart("phone", new StringBody(phone));

		reqEntity.addPart("identification", new StringBody(idenficacion));
		reqEntity.addPart("terms", new StringBody("1"));
		reqEntity.addPart("email", new StringBody(email));

		reqEntity.addPart("email_optional", new StringBody(email_opcional));
		reqEntity.addPart("name", new StringBody(name));
		reqEntity.addPart("name_last", new StringBody(name_last, chars));
		reqEntity.addPart("address", new StringBody(address));
		reqEntity.addPart("type_identification_id", new StringBody(id_doc));
		reqEntity.addPart("image", new StringBody(""));
		if(get_locallanguage().equalsIgnoreCase("es")) {
			reqEntity.addPart("language", new StringBody("1"));
		}else{
			reqEntity.addPart("language", new StringBody("2"));
		}

		builder = MultipartEntityBuilder.create();

		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		Charset chars = Charset.forName("UTF-8");
		builder.setCharset(chars);

		builder.addPart("iduser", new StringBody(data_servicio.usuario.getUser_id(), contentType));
		builder.addPart("country_id", new StringBody(country_id, contentType));
		builder.addPart("phone", new StringBody(phone, contentType));
		builder.addPart("identification", new StringBody(idenficacion, contentType));
		builder.addPart("terms", new StringBody("1", contentType));
		builder.addPart("email", new StringBody(email, contentType));
		builder.addPart("email_optional", new StringBody(email_opcional, contentType));
		builder.addPart("name", new StringBody(name, contentType));
		builder.addPart("name_last", new StringBody(name_last, contentType));
		builder.addPart("address", new StringBody(address, contentType));
		builder.addPart("type_identification_id", new StringBody(id_doc, contentType));
		if(get_locallanguage().equalsIgnoreCase("es")) {
			builder.addPart("language", new StringBody("1",contentType));
		}else{
			builder.addPart("language", new StringBody("2",contentType));
		}
		url_metodo = "http://fluie.com/fluie/user/updateUser";
	}

	public void changePassword(String email, String password) {
		url_metodo = "http://fluie.com/fluie/api/user/changePassword";

		reqEntity = new MultipartEntity();
		try {
			reqEntity.addPart("email", new StringBody(email));
			reqEntity.addPart("password", new StringBody(password));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		servicio = "changePassword";
		tipo = POST;

	}

	public void recuperar_contrasena(String email) {
		url_metodo = "http://fluie.com/fluie/user/recover";

		reqEntity = new MultipartEntity();
		try {
			reqEntity.addPart("email", new StringBody(email));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		servicio = "recuperar_contrasena";
		tipo = POST;

	}

	public void comprar(String user_id, String price, String payments_platform_id, String space_id, String space,
			String reference, String confirm_buy) {
		url_metodo = "http://fluie.com/fluie/user/buy/";

		reqEntity = new MultipartEntity();
		try {
			/*
			 * 01-25 05:28:41.203: I/paymentExample(12486): { 01-25
			 * 05:28:41.203: I/paymentExample(12486): "response": { 01-25
			 * 05:28:41.203: I/paymentExample(12486): "state": "approved", 01-25
			 * 05:28:41.203: I/paymentExample(12486): "id":
			 * "PAY-6RV70583SB702805EKEYSZ6Y", 01-25 05:28:41.203:
			 * I/paymentExample(12486): "create_time": "2014-02-12T22:29:49Z",
			 * 01-25 05:28:41.203: I/paymentExample(12486): "intent": "sale"
			 * 01-25 05:28:41.203: I/paymentExample(12486): }, 01-25
			 * 05:28:41.203: I/paymentExample(12486): "client": { 01-25
			 * 05:28:41.203: I/paymentExample(12486): "platform": "Android",
			 * 01-25 05:28:41.203: I/paymentExample(12486):
			 * "paypal_sdk_version": "2.12.1", 01-25 05:28:41.203:
			 * I/paymentExample(12486): "product_name": "PayPal-Android-SDK",
			 * 01-25 05:28:41.203: I/paymentExample(12486): "environment":
			 * "mock" 01-25 05:28:41.203: I/paymentExample(12486): }, 01-25
			 * 05:28:41.203: I/paymentExample(12486): "response_type": "payment"
			 * 01-25 05:28:41.203: I/paymentExample(12486): }
			 * 
			 * 
			 */

			log_aplicacion.log_error("user_id" + user_id);
			log_aplicacion.log_error("price" + price);
			log_aplicacion.log_error("payments_platform_id" + "2");
			log_aplicacion.log_error("space_id" + space_id);
			log_aplicacion.log_error("space" + space);
			log_aplicacion.log_error("reference" + reference);
			log_aplicacion.log_error("user_id" + user_id);
			log_aplicacion.log_error("confirm_buy" + confirm_buy);

			reqEntity.addPart("user_id", new StringBody(user_id));
			reqEntity.addPart("price", new StringBody(price));
			reqEntity.addPart("payments_platform_id", new StringBody("2"));
			reqEntity.addPart("space_id", new StringBody(space_id));
			reqEntity.addPart("space", new StringBody(space));
			reqEntity.addPart("reference", new StringBody(reference));
			reqEntity.addPart("email", new StringBody(data_servicio.usuario.getEmail()));
			reqEntity.addPart("telefono", new StringBody(data_servicio.usuario.getMovil()));
			reqEntity.addPart("confirm_buy", new StringBody(confirm_buy));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		servicio = "comprar";
		tipo = POST;

	}

	public void set_role(String user_id, String role_id) {
		try {
			reqEntity = new MultipartEntity();
			reqEntity.addPart("user_id", new StringBody(user_id));
			reqEntity.addPart("role_id", new StringBody(role_id));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
			url_metodo = url_servicio + "/userRole";
			tipo = POST;
			servicio = "set_role";
		} catch (Exception e) {
			// TODO: handle exception

		}
	}

	public void getrelacion() {
		try {
			url_metodo = "http://fluie.com/fluie/api/relationship";
			tipo = GET;
			servicio = "get_relacion";
		} catch (Exception e) {

		}

	}

	// ********************************beneficiariousuario**************************************//

	public void getgenero() {
		try {
			url_metodo = url_servicio + "/gender/";
			tipo = GET;
			servicio = "get_genero";
		} catch (Exception e) {

		}

	}

	public void setbeneficiaryUser(String beneficiary_id, String user_id, String code, String relationship_id) {

		reqEntity = new MultipartEntity();
		try {

			reqEntity.addPart("beneficiary_id", new StringBody(beneficiary_id));
			reqEntity.addPart("user_id", new StringBody(user_id));
			reqEntity.addPart("code", new StringBody(code));
			reqEntity.addPart("relationship_id", new StringBody(relationship_id));
			reqEntity.addPart("relationship_id", new StringBody(relationship_id));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
			url_metodo = url_servicio + "/beneficiaryUser";
			tipo = POST;
			servicio = "setbeneficiaryUser";
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void Delete_beneficiario(String beneficiary_id) {

		try {

			url_metodo = url_servicio + "/beneficiaryUser/" + beneficiary_id;
			tipo = DELETE;
			servicio = "Delete_beneficiario";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void Delete_mensaje(String id) {

		try {

			url_metodo = "http://fluie.com/fluie/api/message/" + id;
			tipo = DELETE;
			servicio = "Delete_mensaje";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// verificador

	public void Asignacion_verificador(String checker_id, String user_id, String code) {

		try {
			reqEntity = new MultipartEntity();
			reqEntity.addPart("checker_id", new StringBody(checker_id));
			reqEntity.addPart("user_id", new StringBody(user_id));
			reqEntity.addPart("code", new StringBody(code));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
			url_metodo = url_servicio + "/checkerUser";
			tipo = POST;
			servicio = "Asignacion_verificador";
		} catch (Exception e) {
			// TODO: handle exception

		}

	}

	public void Delete_verificador(String user_id) {

		try {
			url_metodo = url_servicio + "/checkerUser/" + user_id;
			tipo = DELETE;
			servicio = "Delete_verificador";
		} catch (Exception e) {
		}
	}

	// *******************mensaje*************************//

	public void update_mensaje(String idmessage, String type_file_id, String type_message_id, String title,
			String date_view, String description, String file, String beneficiaries, ArrayList<Imagen> imagenes) {

		this.type_file_id = type_file_id;
		this.type_message_id = type_message_id;
		this.title = title;
		this.file = file;
		this.beneficiaries = beneficiaries;
		this.description = description;
		this.idmessage = idmessage;
		this.date_view = date_view;
		this.imagenes = (ArrayList<Imagen>) imagenes.clone();
		//
		url_metodo = "http://fluie.com/fluie/messageRestFull/updateMessage";
		//
		tipo = "upload_service";
		servicio = "update_mensaje";

	}

	public void crear_mensaje(String type_file_id, String type_message_id, String title, String date_view,
			String description, String file, String beneficiaries, ArrayList<Imagen> imagenes) {
		try {
			this.type_file_id = type_file_id;
			this.type_message_id = type_message_id;
			this.title = title;
			this.file = file;
			this.beneficiaries = beneficiaries;
			this.imagenes = (ArrayList<Imagen>) imagenes.clone();
			this.description = description;
			//
				url_metodo = "http://fluie.com/fluie/messageRestFull/createMessage";
			//
			tipo = "upload_service";
			servicio = "crear_mensaje";

		} catch (Exception e) {
		}

	}

	public void get_mensaje(String id) throws JSONException, UnsupportedEncodingException {
		JSONObject json_id = new JSONObject();
		json_id.put("user_id", data_servicio.usuario.getUser_id());

		json_id.put("idmessage", id);

		url_metodo = "http://fluie.com/fluie/api/message?with=messageFiles,messageBeneficiaries&filter="
				+ URLEncoder.encode(json_id.toString(), "UTF-8");
		tipo = GET;
		servicio = "get_mensaje";

		/*
		 * beneficiary_id , ￼ ￼user_id, ￼code , ￼relationship_id
		 * 
		 */

	}
	public void subir_imagen(String file) {

		try {
			this.file_url = file;
			builder = MultipartEntityBuilder.create();

			url_metodo = "http://fluie.com/fluie/user/SavePhotoUser";
			tipo = "subir imagen";
			servicio = "subir_imagen";
		} catch (Exception e) {
			// TODO: handle exception

		}

	}

	public void get_historial() {
		try {
			JSONObject json_id = new JSONObject();

			json_id.put("user_id", data_servicio.usuario.getUser_id());

			if (!data_servicio.tipo_msg.equals("")) {
				json_id.put("type_file_id", data_servicio.tipo_msg);
			}

			url_metodo = "http://fluie.com/fluie/api/message?with=messageFiles,messageBeneficiaries&filter="
					+ URLEncoder.encode(json_id.toString(), "UTF-8");
			tipo = GET;
			servicio = "get_historial";

		} catch (Exception e) {

		}

	}

	public void get_tipo_mensaje() {
		try {
			url_metodo = url_servicio + "/typeMessage";
			tipo = GET;
			servicio = "get_tipo_mensaje";
		} catch (Exception e) {
		}

	}

	public void get_tipoEvento() {
		try {
			url_metodo = url_servicio + "/typeEvent";
			tipo = GET;
			servicio = "get_tipoEvento";
		} catch (Exception e) {
		}
	}

	public void envio_mensaje() {
		try {
			url_metodo = url_servicio + "/typeEvent";
			tipo = GET;
			servicio = "get_tipoEvento";
		} catch (Exception e) {
		}

	}

	public void get_imagen_user(String id_user) {
		try {
			id_consulta = id_user;
			url_metodo = "http://fluie.com/fluie/api/user/getImage";
			tipo = POST;

			reqEntity = new MultipartEntity();
			reqEntity.addPart("iduser", new StringBody(id_user));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
			servicio = "get_imagen";

		} catch (Exception e) {

		}

	}

	public void get_imagenperfil(String id_user) {
		try {
			id_consulta = id_user;
			url_metodo = "http://fluie.com/fluie/api/user/getImage";
			tipo = POST;

			reqEntity = new MultipartEntity();
			reqEntity.addPart("iduser", new StringBody(id_user));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
			servicio = "get_imagenperfil";

		} catch (Exception e) {

		}

	}

	/*
	 * http://fluie.com/fluie/api/notification/getNotifications
	 * post user_id language_d
	 * 
	 * language_id
	 * 
	 * http://fluie.com/fluie/user/reportPrincipal POST
	 * idcheckeruser other_id
	 * 
	 * http://fluie.com/fluie/user/stateNotification POST
	 * idnotification
	 * http://fluie.com/fluie/api/banner/?filter=%5Blanguaje_id
	 * [22/02/16 9:53:47 p.m.] carlos andres robinson lara:
	 * http://fluie.com/fluie/api/typeIdentification/getList
	 * [22/02/16 9:55:05 p.m.] carlos andres robinson lara:
	 * type_identification_id (añadir al registrar) [22/02/16 9:57:06 p.m.]
	 * carlos andres robinson lara: agregar verificador
	 * http://fluie.com/fluie/api/checkerUser?with=checker&
	 * filter={user_id:87}
	 * 
	 */
	public void getnotificaciones() {

		try {

			url_metodo = "http://fluie.com/fluie/api/notification/getNotifications";
			tipo = POST;

			reqEntity = new MultipartEntity();
			reqEntity.addPart("user_id", new StringBody(data_servicio.usuario.getUser_id()));
			reqEntity.addPart("language_id", new StringBody(id_idioma));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
			servicio = "getnotificaciones";

		} catch (Exception e) {

		}
	}

	public void eliminarnotificaciones(String idnotification) {

		try {

			url_metodo = "http://fluie.com/fluie/api/notification/" + idnotification;
			tipo = DELETE;

			reqEntity = new MultipartEntity();
			reqEntity.addPart("idnotification", new StringBody(idnotification));
			servicio = "eliminarnotificaciones";

		} catch (Exception e) {

		}
	}

	public void setnotificaciones_vista(String idnotification) {

		try {
			url_metodo = "http://fluie.com/fluie/user/stateNotification";
			tipo = POST;
			reqEntity = new MultipartEntity();
			reqEntity.addPart("idnotification", new StringBody(idnotification));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
			servicio = "setnotificacion_vista";

		} catch (Exception e) {

		}
	}

	public void cancelarnotificaciones(String idcheckeruser) {

		try {
			url_metodo = "http://fluie.com/fluie/user/reportPrincipal";
			tipo = POST;

			reqEntity = new MultipartEntity();
			reqEntity.addPart("idcheckeruser", new StringBody(idcheckeruser));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				reqEntity.addPart("language", new StringBody("1",contentType));
			}else{
				reqEntity.addPart("language", new StringBody("2",contentType));
			}
			servicio = "cancelarnotificaciones";

		} catch (Exception e) {

		}
	}

	// *****************************************************//
	@Override
	protected Void doInBackground(String... arg0) {
		log_aplicacion.log_informacion("doInBackground");
		if (tipo.equals(GET)) {
			consultaGet();
		} else if (tipo.equals(POST)) {
			consultapost();
		} else if (tipo.equals(PUT)) {
			consultaPut();
		} else if (tipo.equals(DELETE)) {
			consultadelete();
		} else if (tipo.equals("descarga")) {
			descargar_imagen(url_imagen);
		} else if (tipo.equals("upload_service")) {
			if (servicio.equals("crear_mensaje")) {
				upload();
				consultapost();
			} else if (servicio.equals("update_mensaje")) {

				update();
				consultapost();
			}

		} else if (tipo.equals("subir imagen")) {

			uploadimagen();
			consultapost();
		}

		return null;
	}

	private void uploadimagen() {
		builder = MultipartEntityBuilder.create();

		/* example for setting a HttpMultipartMode */
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		Charset chars = Charset.forName("UTF-8");
		builder.setCharset(chars);
		builder.addPart("user_id", new StringBody(data_servicio.usuario.getUser_id(), contentType));

		File f = new File(file_url);
		if (f.exists()) {
			String charset = "UTF-8";
			log_aplicacion.log_informacion("archivo existe");
			mimeType = "image/png";
			// mimeType = URLConnection.guessContentTypeFromName(f.getName());

			FileBody fb = new FileBody(f);

			builder.addPart("image", fb);

		} else {
			log_aplicacion.log_informacion("archivo no existe");

		}

	}

	private void upload() {
		try {

			String charset = "UTF-8";
			log_aplicacion.log_informacion("type_file_id" + type_file_id);

			log_aplicacion.log_informacion("type_message_id" + type_message_id);
			log_aplicacion.log_informacion("title" + title);
			log_aplicacion.log_informacion("date_view" + date_view);
			log_aplicacion.log_informacion("description" + description);
			log_aplicacion.log_informacion("beneficiaries" + beneficiaries);
			log_aplicacion.log_informacion("file" + file);
			log_aplicacion.log_informacion("idmessage" + idmessage);

			builder = MultipartEntityBuilder.create();

			/* example for setting a HttpMultipartMode */
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			Charset chars = Charset.forName("UTF-8");
			builder.setCharset(chars);
			/* example for adding an image part */
			// image should be a String

			builder.addPart("user_id", new StringBody(data_servicio.usuario.getUser_id(), contentType));
			builder.addPart("m[type_file_id]", new StringBody(type_file_id, contentType));
			builder.addPart("m[type_message_id]", new StringBody(type_message_id, contentType));
			builder.addPart("m[title]", new StringBody(title, contentType));
			builder.addPart("m[date_view]", new StringBody(date_view, contentType));
			builder.addPart("m[description]", new StringBody(description, contentType));
			builder.addPart("m[beneficiaries]", new StringBody(beneficiaries, contentType));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				builder.addPart("language", new StringBody("1", contentType));
			}else{
				builder.addPart("language", new StringBody("2", contentType));
			}
			if (!idmessage.equals("")) {

				builder.addPart("m[idmessage]", new StringBody(idmessage, contentType));

			}

			if (!type_file_id.equals("4")) {
				try {
					log_aplicacion.log_informacion("archivo" + file);

					File f = new File(file);

					if (f.exists()) {
						log_aplicacion.log_informacion("archivo existe");
						FileBody fb = new FileBody(f);

						builder.addPart("m[file]", fb);

					} else {

						log_aplicacion.log_informacion("archivo no existe");
					}
				} catch (Exception ex) {
					log_aplicacion.log_error(ex.toString());
				}

			}

			if (type_file_id.equals("4")) {
				log_aplicacion.log_informacion("imagenes" + imagenes.size());

				for (int j = 0; j < imagenes.size(); j++) {
					log_aplicacion.log_informacion("leyendo imagen" + j);
					try {
						log_aplicacion.log_informacion("url imagen" + imagenes.get(j).getUrl());
						File f = new File(imagenes.get(j).getUrl());

						if (f.exists()) {
							log_aplicacion.log_informacion("archivo existe");

							mimeType = URLConnection.guessContentTypeFromName(f.getName());
							log_aplicacion.log_informacion("url imagen" + imagenes.get(j).getUrl());
							String url = imagenes.get(j).getUrl();
							// builder.addPart("m[file][]", fb);
							FileBody fb = new FileBody(f, ContentType.create(mimeType), charset);
							builder.addPart("m[file][" + j + "]", new FileBody(f));

							log_aplicacion.log_informacion("mime" + mimeType);

						} else {

							log_aplicacion.log_informacion("archivo no existe");
						}

					} catch (Exception e) {
						// TODO: handle exception
						log_aplicacion.log_informacion("error envio mensaje multipart" + e.toString());
					}
				}
			}
			// Respuesta = multipartUtil.finish(); // response from server.
			// log_aplicacion.log_informacion(Respuesta);

		} catch (Exception e) {
			log_aplicacion.log_informacion("error upload " + e.toString());
			// TODO: handle exception
		}

	}

	private void update() {
		try {

			String charset = "UTF-8";
			log_aplicacion.log_informacion("type_file_id" + type_file_id);

			log_aplicacion.log_informacion("type_message_id" + type_message_id);
			log_aplicacion.log_informacion("title" + title);
			log_aplicacion.log_informacion("date_view" + date_view);
			log_aplicacion.log_informacion("description" + description);
			log_aplicacion.log_informacion("beneficiaries" + beneficiaries);
			log_aplicacion.log_informacion("file" + file);
			log_aplicacion.log_informacion("idmessage" + idmessage);

			builder = MultipartEntityBuilder.create();

			/* example for setting a HttpMultipartMode */
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			Charset chars = Charset.forName("UTF-8");
			builder.setCharset(chars);
			/* example for adding an image part */
			// image should be a String
			builder.addPart("m[idmessage]", new StringBody(idmessage, contentType));

			builder.addPart("user_id", new StringBody(data_servicio.usuario.getUser_id(), contentType));
			builder.addPart("m[type_file_id]", new StringBody(type_file_id, contentType));
			builder.addPart("m[type_message_id]", new StringBody(type_message_id, contentType));
			builder.addPart("m[title]", new StringBody(title, contentType));
			builder.addPart("m[date_view]", new StringBody(date_view, contentType));
			builder.addPart("m[description]", new StringBody(description, contentType));
			builder.addPart("m[beneficiaries]", new StringBody(beneficiaries, contentType));
			if(get_locallanguage().equalsIgnoreCase("es")) {
				builder.addPart("language", new StringBody("1", contentType));
			}else{
				builder.addPart("language", new StringBody("2", contentType));
			}

			if (!type_file_id.equals("4")) {
				try {
					log_aplicacion.log_informacion("archivo" + file);

					File f = new File(file);

					if (f.exists()) {
						log_aplicacion.log_informacion("archivo existe");
						FileBody fb = new FileBody(f);

						builder.addPart("m[file]", fb);

					} else {

						log_aplicacion.log_informacion("archivo no existe");
					}
				} catch (Exception ex) {
					log_aplicacion.log_error(ex.toString());
				}

			}

			if (type_file_id.equals("4")) {
				log_aplicacion.log_informacion("imagenes" + imagenes.size());

				for (int j = 0; j < imagenes.size(); j++) {
					log_aplicacion.log_informacion("leyendo imagen" + j);
					try {
						log_aplicacion.log_informacion("url imagen" + imagenes.get(j).getUrl());
						File f = new File(imagenes.get(j).getUrl());

						if (f.exists()) {
							log_aplicacion.log_informacion("archivo existe");

							mimeType = URLConnection.guessContentTypeFromName(f.getName());
							log_aplicacion.log_informacion("url imagen" + imagenes.get(j).getUrl());
							String url = imagenes.get(j).getUrl();
							// builder.addPart("m[file][]", fb);
							FileBody fb = new FileBody(f, ContentType.create(mimeType), charset);
							builder.addPart("m[file][" + j + "]", new FileBody(f));

							log_aplicacion.log_informacion("mime" + mimeType);

						} else {

							log_aplicacion.log_informacion("archivo no existe");
						}

					} catch (Exception e) {
						// TODO: handle exception
						log_aplicacion.log_informacion("error envio mensaje multipart" + e.toString());
					}
				}
			}
			// Respuesta = multipartUtil.finish(); // response from server.
			// log_aplicacion.log_informacion(Respuesta);

		} catch (Exception e) {
			log_aplicacion.log_informacion("error upload " + e.toString());
			// TODO: handle exception
		}

	}

	private void consultaGet() {
		if (prueba_internet.estadoConexion()) {
			envio = true;
			log_aplicacion.log_informacion("metodo GET");
			HttpClient httpclient = new DefaultHttpClient();
			HttpUriRequest httpurirequest;

			try {
				httpurirequest = new HttpGet(url_metodo);
				// httpurirequest.addHeader( new
				// BasicNameValuePair("Authorization","Basic
				// YWRtaW46YWRtaW4="));
				httpurirequest.setHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httpurirequest);

				// According to the JAVA API, InputStream constructor do
				// nothing.
				// So we cant initialize InputStream although it is not an
				// interface
				InputStream inputStream = response.getEntity().getContent();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
				StringBuilder stringBuilder = new StringBuilder();
				String bufferedStrChunk = null;
				while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
					stringBuilder.append(bufferedStrChunk);
				}
				Respuesta = stringBuilder.toString();
				log_aplicacion.log_informacion(Respuesta);
			} catch (Exception e) {

				time_out = true;
				e.printStackTrace();
				log_aplicacion.log_informacion(e.toString());
			}
		} else {
			envio = false;

		}
	}

	private void consultapost() {
		if (prueba_internet.estadoConexion()) {
			envio = true;
			log_aplicacion.log_informacion(servicio + " " + url_metodo);

			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is
			// established.

			HttpConnectionParams.setConnectionTimeout(httpParameters, 60000);

			HttpConnectionParams.setSoTimeout(httpParameters, 60000);

			HttpClient httpclient = new DefaultHttpClient(httpParameters);
			log_aplicacion.log_informacion(servicio + " " + url_metodo);

			HttpPost httppost = new HttpPost(url_metodo);

			httppost.setParams(httpParameters);

			try {
				// Execute HTTP Post Request

				if (servicio.equals("crear_mensaje") || servicio.equals("subir imagen") || builder != null) {

					httppost.setEntity(builder.build());
					log_aplicacion.log_informacion("post ingresa build");
					String boundary = "-------------" + System.currentTimeMillis();
					// httppost.setHeader("Content-type", "multipart/form-data;
					// boundary=" + boundary);
					httppost.setHeader("Authorization", "Basic YWRtaW46YWRtaW4=");

				} else {

					httppost.setEntity(reqEntity);

					httppost.setHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
				}

				// httppost.setHeader("Content-Type", mimeType);

				log_aplicacion.log_informacion(httppost.getRequestLine().getMethod());
				log_aplicacion.log_informacion("envio" + httppost.getRequestLine());

				HttpResponse response = httpclient.execute(httppost);
				// According to the JAVA API, InputStream constructor do
				// nothing.
				// So we cant initialize InputStream although it is not an
				// interface
				InputStream inputStream = response.getEntity().getContent();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");

				// InputStreamReader inputStreamReader = new
				// InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				StringBuilder stringBuilder = new StringBuilder();
				String bufferedStrChunk = null;
				while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
					stringBuilder.append(bufferedStrChunk);
				}
				log_aplicacion.log_informacion("Entro al servicio");
				Respuesta = stringBuilder.toString();
				log_aplicacion.log_informacion("respuesta servicio " + Respuesta);
			} catch (ConnectTimeoutException e) {

				time_out = true;

			} catch (Exception e) {
				envio = false;
				e.printStackTrace();
				log_aplicacion.log_error("Error servicio " + servicio + "  " + e.toString());

			}
		} else {
			envio = false;

		}
	}

	private void consultadelete() {
		if (prueba_internet.estadoConexion()) {
			envio = true;
			log_aplicacion.log_informacion(servicio + " ");

			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is
			// established.

			HttpConnectionParams.setConnectionTimeout(httpParameters, 60000);

			HttpConnectionParams.setSoTimeout(httpParameters, 60000);
			HttpClient httpclient = new DefaultHttpClient(httpParameters);
			HttpDelete httpdelete = new HttpDelete(url_metodo);
			httpdelete.setHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
			httpdelete.setParams(httpParameters);

			try {
				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httpdelete);
				// According to the JAVA API, InputStream constructor do
				// nothing.
				// So we cant initialize InputStream although it is not an
				// interface
				InputStream inputStream = response.getEntity().getContent();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");

				// InputStreamReader inputStreamReader = new
				// InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				StringBuilder stringBuilder = new StringBuilder();
				String bufferedStrChunk = null;
				while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
					stringBuilder.append(bufferedStrChunk);
				}
				log_aplicacion.log_informacion("Entro al servicio");
				Respuesta = stringBuilder.toString();
				log_aplicacion.log_informacion("respuesta servicio " + Respuesta);
			} catch (ConnectTimeoutException e) {
				time_out = true;
			} catch (Exception e) {
				e.printStackTrace();
				log_aplicacion.log_error("Error servicio " + servicio + "  " + e.toString());
			}
		}
	}

	private void consultaPut() {
		if (prueba_internet.estadoConexion()) {
			envio = true;
			log_aplicacion.log_informacion(servicio + " ");

			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is
			// established.

			HttpConnectionParams.setConnectionTimeout(httpParameters, 60000);

			HttpConnectionParams.setSoTimeout(httpParameters, 60000);

			HttpClient httpclient = new DefaultHttpClient(httpParameters);
			HttpPut putRequest = new HttpPut(url_metodo);

			putRequest.setHeader("Authorization", "Basic YWRtaW46YWRtaW4=");

			putRequest.setParams(httpParameters);

			try {
				putRequest.setEntity(reqEntity);
				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(putRequest);
				// According to the JAVA API, InputStream constructor do
				// nothing.
				// So we cant initialize InputStream although it is not an
				// interface
				InputStream inputStream = response.getEntity().getContent();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");

				// InputStreamReader inputStreamReader = new
				// InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				StringBuilder stringBuilder = new StringBuilder();
				String bufferedStrChunk = null;
				while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
					stringBuilder.append(bufferedStrChunk);
				}
				log_aplicacion.log_informacion("Entro al servicio");
				Respuesta = stringBuilder.toString();
				log_aplicacion.log_informacion("respuesta servicio " + Respuesta);
			} catch (ConnectTimeoutException e) {
				time_out = true;
			} catch (Exception e) {
				e.printStackTrace();
				log_aplicacion.log_error("Error servicio " + servicio + "  " + e.toString());
			}
		}
	}

	@Override
	protected void onPreExecute() {
		// long timeout = 40;
		//
		// Timer timer = new Timer();
		// timer.schedule(new TimerTask() {
		// public void run() {
		// if (Respuesta.equals("") && !time_out) {
		// try {
		// cancelado = true;
		// ws.cancel(true);
		//// data_reservas.mensaje_error = new Mensaje(false,
		//// "Tiempo de espera al servidor agotado");
		// msg.what = 0;
		// puente_envio.sendMessage(msg);
		//
		// } catch (Exception e) {
		// // TODO: handle exception
		// }
		//
		// }
		// }
		// }, timeout * 1000);
		// log_aplicacion.log_informacion("onPreExecute");

	}

	@Override
	protected void onProgressUpdate(Void... values) {
		log_aplicacion.log_informacion("onProgressUpdate");
	}

	@Override
	protected void onPostExecute(Void result) {
		data_servicio.msj_error = "";
		data_servicio.msj = "";
		if (envio && !time_out) {
			try {

				log_aplicacion.log_informacion("onPostExecute " + servicio);
				Parseo_json parseo = new Parseo_json();

				if (servicio.equals(home)) {
					if (parseo.parseo_Consultar_home(Respuesta)) {
						log_aplicacion.log_informacion("parseo_home");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals(login)) {
					if (parseo.parseo_login(Respuesta)) {
						log_aplicacion.log_informacion("parseo_login");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals(pais)) {
					if (parseo.parseo_pais(Respuesta)) {
						log_aplicacion.log_informacion("parseo_pais");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals(crear_usuario)) {
					if (parseo.parseo_crear_usuario(Respuesta)) {
						log_aplicacion.log_informacion("parseo_pais");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals(crear_ben_ver)) {
					if (parseo.parseo_generico(Respuesta)) {
						log_aplicacion.log_informacion("crear_ben_ver");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals("set_role")) {
					if (parseo.parseo_set_role(Respuesta)) {
						log_aplicacion.log_informacion("set_role");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals(get_usuario)) {
					if (parseo.parseo_get_usuario(Respuesta)) {
						log_aplicacion.log_informacion("parseo_ get_usuario");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				} else if (servicio.equals("get_mensaje")) {
					if (parseo.parseo_get_mensaje(Respuesta)) {
						log_aplicacion.log_informacion("parseo_ get_usuario");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				}

				else if (servicio.equals("getfreespace")) {
					data_servicio.json_freespace = Respuesta;
					if (!Respuesta.equals("")) {

						log_aplicacion.log_informacion("parseo_ get_usuario");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				} else if (servicio.equals("getlistdocumento")) {
					data_servicio.json_list_documentos = Respuesta;
					if (!Respuesta.equals("")) {

						log_aplicacion.log_informacion("parseo_ getlistdocumento");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				} else if (servicio.equals("getnotificaciones")) {

					if (parseo.parseo_get_notificaciones(Respuesta)) {
						log_aplicacion.log_informacion("parseo_ getnotificaciones");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				} else if (servicio.equals("cancelarnotificaciones")) {

					if (parseo.parseo_generico(Respuesta)) {
						log_aplicacion.log_informacion("parseo_ cancelarnotificaciones");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				}

				else if (servicio.equals("getespacio_consejos")) {
					if (parseo.parseo_tipo(Respuesta, "getespacio_consejos")) {
						log_aplicacion.log_informacion("parseo_ get_usuario");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}
				}

				else if (servicio.equals("changePassword")) {
					if (parseo.parseo_changePassword(Respuesta)) {
						log_aplicacion.log_informacion("parseo_ get_usuario");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals("recuperar_contrasena")) {
					if (parseo.parseo_generico(Respuesta)) {
						log_aplicacion.log_informacion("parseo_recuperar_contrasena");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals("Update_Usuario")) {
					if (parseo.parseo_generico(Respuesta)) {
						log_aplicacion.log_informacion("parseo_ get_usuario");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals("get_genero")) {
					if (parseo.parseo_tipo(Respuesta, "get_genero")) {
						log_aplicacion.log_informacion("parseo_get_genero");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				}

				else if (servicio.equals("set_beneficiarios")) {
					if (parseo.parseo_set_beneficiarios(Respuesta)) {
						log_aplicacion.log_informacion("parseo_set_beneficiarios");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals("get_lista_relaciones")) {
					if (parseo.parseo_tipo_relacion(Respuesta)) {
						log_aplicacion.log_informacion("get_lista_relaciones");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				}

				else if (servicio.equals("subir_imagen")) {
					if (parseo.parseo_generico(Respuesta)) {
						log_aplicacion.log_informacion("subir_imagen");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals("get_imagen")) {

					if (parseo.parseo_imagen(Respuesta, id_consulta)) {
						log_aplicacion.log_informacion("get_imagen");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				}

				else if (servicio.equals("get_imagenperfil")) {

					if (parseo.parseo_imagen_perfil(Respuesta)) {
						log_aplicacion.log_informacion("get_imagenperfil");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				} else if (servicio.equals("getplanes")) {
					if (parseo.parseo_tipo(Respuesta, "getplanes")) {
						log_aplicacion.log_informacion("getplanes");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				}

				else if (servicio.equals("getsplash")) {
					if (parseo.parseo_tipo(Respuesta, "getsplash")) {
						log_aplicacion.log_informacion("getsplash");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				} else if (servicio.equals("get_lista_beneficiarios")) {
					if (parseo.parseo_lista_beneficiarios(Respuesta)) {
						log_aplicacion.log_informacion("get_lista_beneficiarios");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				}

				else if (servicio.equals("crear_mensaje") || servicio.equals("update_mensaje")) {
					if (parseo.parseo_crear_mensaje(Respuesta)) {
						log_aplicacion.log_informacion("crear_mensaje");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals("comprar")) {
					if (parseo.parseo_generico(Respuesta)) {
						log_aplicacion.log_informacion("crear_mensaje");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				} else if (servicio.equals("eliminarnotificaciones")) {
					try {
						JSONObject res = new JSONObject(Respuesta);
						log_aplicacion.log_informacion("getespacio_msg");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);

					} catch (Exception e) {
						// TODO: handle exception
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals("getespacio_msg")) {
					if (parseo.parseo_tipo(Respuesta, "getespacio_msg")) {
						log_aplicacion.log_informacion("getespacio_msg");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				}

				else if (servicio.equals("Delete_mensaje")) {
					if (parseo.parseo_tipo(Respuesta, "Delete_mensaje")) {
						log_aplicacion.log_informacion("Delete_mensaje");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				} else if (servicio.equals("get_historial")) {
					if (parseo.parseo_tipo(Respuesta, "get_historial")) {
						log_aplicacion.log_informacion("get_historial");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				} else if (servicio.equals("get_relacion")) {
					if (parseo.parseo_tipo(Respuesta, "get_relacion")) {
						log_aplicacion.log_informacion("get_relacion");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				}

				else if (servicio.equals("delete_ben_ver")) {
					if (parseo.parseo_crear_ben_ver(Respuesta)) {
						log_aplicacion.log_informacion("delete_ben_ver");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				} else if (servicio.equals("get_lista_verificadores")) {
					if (parseo.parseo_lista_verificadores(Respuesta)) {
						log_aplicacion.log_informacion("get_lista_verificadores");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				} else if (servicio.equals("get_usuario_beneficiario")
						|| servicio.equals("get_usuario_verificadores")) {

					if (parseo.parseo_get_usuario_lista(Respuesta)) {
						log_aplicacion.log_informacion("parseo_ get_usuario");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);

					}

				}

				else {
					if (parseo.parseo_generico(Respuesta)) {
						log_aplicacion.log_informacion("parseo generico");
						msg.what = 1;
						puente_envio.dispatchMessage(msg);
					} else {
						msg.what = 0;
						puente_envio.dispatchMessage(msg);
					}

				}

			} catch (Exception e) {
				// TODO: handle exception
				log_aplicacion.log_error(e.toString());
			}
		} else if (time_out) {
			try {
				// data_servicio.msj_error =
				// myContext.getResources().getString(R.string.verifique_la_conexion_a_internet);

				Toast.makeText(myContext, myContext.getResources().getString(R.string.verifique_la_conexion_a_internet),
						Toast.LENGTH_LONG).show();

			} catch (Exception e) {
				// TODO: handle exception
			}

			msg.what = 0;
			puente_envio.dispatchMessage(msg);

		}

		else if (!envio) {
			try {
				// data_servicio.msj_error =
				// myContext.getResources().getString(R.string.verifique_la_conexion_a_internet);

				Toast.makeText(myContext, myContext.getResources().getString(R.string.verifique_la_conexion_a_internet),
						Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				// TODO: handle exception
			}

			msg.what = 0;
			puente_envio.dispatchMessage(msg);
		}

	}

	public void descargar_imagen(String url_archivo) {

		try {
			// primero especificaremos el origen de nuestro archivo a descargar
			// utilizando
			// la ruta completa
			URL url = new URL(url_archivo);

			// establecemos la conexi�n con el destino
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			// establecemos el m�todo jet para nuestra conexi�n
			// el m�todo setdooutput es necesario para este tipo de conexiones
			urlConnection.setRequestMethod("GET");
			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Authorization", "Basic YWRtaW46YWRtaW4=");

			// por �ltimo establecemos nuestra conexi�n y cruzamos los dedos
			// <img
			// src="http://www.insdout.com/wp-includes/images/smilies/icon_razz.gif"
			// alt=":P" class="wp-smiley">
			urlConnection.connect();

			// vamos a establecer la ruta de destino para nuestra descarga
			// para hacerlo sencillo en este ejemplo he decidido descargar en
			// la ra�z de la tarjeta SD
			File SDCardRoot = Environment.getExternalStorageDirectory();

			File filecarpeta = new File(SDCardRoot + "/" + myContext.getResources().getString(R.string.app_name));
			if (!filecarpeta.exists()) {
				filecarpeta.mkdir();

			}
			// vamos a crear un objeto del tipo de fichero
			// donde descargaremos nuestro fichero, debemos darle el nombre que
			// queramos, si quisieramos hacer esto mas completo
			// coger�amos el nombre del origen
			File file = new File(SDCardRoot + "/" + myContext.getResources().getString(R.string.app_name) + "/perfil"
					+ url_archivo.substring(url_archivo.indexOf(".")));

			// utilizaremos un objeto del tipo fileoutputstream
			// para escribir el archivo que descargamos en el nuevo
			FileOutputStream fileOutput = new FileOutputStream(file);

			// leemos los datos desde la url
			InputStream inputStream = urlConnection.getInputStream();

			// obtendremos el tama�o del archivo y lo asociaremos a una
			// variable de tipo entero
			int totalSize = urlConnection.getContentLength();
			int downloadedSize = 0;

			// creamos un buffer y una variable para ir almacenando el
			// tama�o temporal de este
			byte[] buffer = new byte[1024];
			int bufferLength = 0;

			// ahora iremos recorriendo el buffer para escribir el archivo de
			// destino
			// siempre teniendo constancia de la cantidad descargada y el total
			// del tama�o
			// con esto podremos crear una barra de progreso
			while ((bufferLength = inputStream.read(buffer)) > 0) {

				fileOutput.write(buffer, 0, bufferLength);
				downloadedSize += bufferLength;
				// podr�amos utilizar una funci�n para ir actualizando el
				// progreso de lo
				// descargado

			}
			// cerramos
			fileOutput.close();

			// y gestionamos errores
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public byte[] FileToArrayOfBytes(String archivo) {
		File file = new File(archivo);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		try {
			FileInputStream fis = new FileInputStream(file);
			for (int readNum; (readNum = fis.read(buf)) != -1;) {
				bos.write(buf, 0, readNum); // no doubt here is 0
			}
			fis.close();
		} catch (Exception ex) {
			Log.d("Micrositios", ex.toString());
		}
		byte[] bytes = bos.toByteArray();
		return bytes;
	}



	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public boolean isAdjunto() {
		return Adjunto;
	}

	public void setAdjunto(boolean adjunto) {
		Adjunto = adjunto;
	}

	public Handler getPuente_envio() {
		return puente_envio;
	}

	public void setPuente_envio(Handler puente_envio) {
		this.puente_envio = puente_envio;
	}

}
