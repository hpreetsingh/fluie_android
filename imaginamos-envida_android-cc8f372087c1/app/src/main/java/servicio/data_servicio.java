package servicio;

import java.util.ArrayList;

import com.zonesoftware.fluie.db.Borrador;
import com.zonesoftware.fluie.db.Usuario_fluie;

import android.widget.FrameLayout;
import modelo.Home;
import modelo.Imagen;
import modelo.Item_lista;
import modelo.Mensaje;
import modelo.Notificacion;
import modelo.Usuario;

public class data_servicio {
	public final static String url_servicio = "http://fluie.com/fluie/api";
	public static Mensaje mensaje_error = new Mensaje(false, "");

	public static String jsonhome = "";
	public static String jsonsplash = "";
	public static ArrayList<String> espacion_mensajes = new ArrayList<String>();

	public static String jsonlogin = "";
	public static String json_paises = "";
	public static String json_generos = "";
	public static String json_espacio_msg = "";
	public static String json_usuario = "";
	public static String json_file = "";
	public static String json_subir_imagen = "";
	public static String json_planes = "";
	public static String json_compra = "";
	public static String json_historial = "";
	public static String json_tipo_relacion = "";
	public static String json_lista_beneficiario = "";
	public static String json_lista_verificadores = "";
	public static ArrayList<Usuario> lista_beneficiario = new ArrayList<Usuario>();
	public static ArrayList<Usuario> lista_verificadores = null;
	public static String tipo_msg = "";
	public static String password = "";
	public static ArrayList<Imagen> imagenes = new ArrayList<Imagen>();
	public static String archivo_texto = "";
	public static String archivo_voz = "";
	public static String archivo_video = "";
	public static ArrayList<Notificacion> Notificaciones = new ArrayList<Notificacion>();
	public static String tipo_historial = "";
	public static Usuario usuario_beneficiario = new Usuario();
	public static ArrayList<Item_lista> lista_seleccion_ben = null;

	public static String json_freespace = "[{\"iduser\":\"66\",\"space\":\"0\",\"size\":\"0\",\"free\":\"0\"}]";
	public static String json_list_documentos = "";
	public static Home home = new Home();
	public static Usuario usuario = new Usuario();
	public static String terms_conditions = "";
	public static String msj_error = "";
	public static String msj = "";
	public static String email_usuario = "";
	public static String url_perfil = "";
	///////////////////////////////////
	public static Usuario_fluie usuariofluie = null;
	public static String lista = "";
	public static String listaben = "";
	public static ArrayList<Item_lista> items = new ArrayList<Item_lista>();
	public static String editar = "";
	public static String opcion = "";
	public static String id_seleccion = "";
	public static int seleccion_edicion = 0;
	public static String seleccion_beneficiario = "";
	/////////////////// beneficiario////////////////
	public static String id_beneficiario = "";
	public static String tipo_mensaje = "";

	public static String opcion_mensaje = "";
	public static String mensaje_edicion = "";
	public static Borrador borradoredicion = null;

	public static Mensaje mensajeedicion = new Mensaje();
	public static ArrayList<Imagen> imagen_usuarios = new ArrayList<Imagen>();
	public static ArrayList<Usuario> lista_beneficiario_msg = new ArrayList<Usuario>();
	public static int heigth = 0;
	public static FrameLayout frame;
	// valor que indica si se esta subiendo borradores
	public static boolean subiendo_borradores = false;

}
