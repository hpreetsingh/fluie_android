package servicio;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.zonesoftware.fluie.BienvenidaActivity;
import com.zonesoftware.fluie.ComprarEspacioActivity;
import com.zonesoftware.fluie.CrearUsuarioActivity;
import com.zonesoftware.fluie.Descarga_Archivo;
import com.zonesoftware.fluie.Dialogo;
import com.zonesoftware.fluie.DialogoCarga;
import com.zonesoftware.fluie.EditarPersonaActivity;
import com.zonesoftware.fluie.NavigationDrawerFragment;
import com.zonesoftware.fluie.R;
import com.zonesoftware.fluie.UsuarioActivity;
import com.zonesoftware.fluie.db.Db_manager;
import com.zonesoftware.fluie.db.Usuario_fluie;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.widget.EditText;
import android.widget.Toast;
import modelo.Usuario;

public class consultar_servicios {
	Context micontext;
	Activity activity;
	FragmentActivity factivity;
	// Dialogo_carga cargar;
	String carnet = "";
	String documento = "";
	String fecha = "";
	NavigationDrawerFragment navegatio;
	EditText ident_susti;
	DialogoCarga cargar;
	Dialogo dialogo_modal;
	static String id = "";
	JSONArray jarraybeneficiarios = null;
	Db_manager db;

	public consultar_servicios(Context micontext, Activity activity) {
		super();
		this.micontext = micontext;
		this.activity = activity;
		dialogo_modal = new Dialogo(micontext, activity);
		db = new Db_manager(micontext);
		cargar = new DialogoCarga(activity, activity.getString(R.string.consultando),
				activity.getString(R.string.espere_un_momento_));
		// cargar = new Dialogo_carga(micontext, micontext.getResources()
		// .getString(R.string.cargando_informaci_n), micontext
		// .getResources().getString(R.string.espere_un_momento_));
		data_servicio.msj_error = "";

	}

	public void login(final String email, final String password, final boolean mostarscarga)
			throws JSONException, UnsupportedEncodingException {

		log_aplicacion.log_informacion("login");
		WS_cliente servicio = new WS_cliente(micontext);
		if (mostarscarga) {
			cargar.mostrar();
		}
		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {

					if (data_servicio.usuario.getRole_id().equals("1")) {

						if (db.getusuario() == null) {
							db.insertar_usuario(email, password);
						}
						WS_cliente servicio2;
						try {
							servicio2 = new WS_cliente(micontext);

							Handler puente = new Handler() {
								@SuppressLint("ShowToast")
								@Override
								public void handleMessage(Message msg) {
									if (msg.what == 1) {
										consultar_benficiarios();
										WS_cliente servicio3;

										try {
											servicio3 = new WS_cliente(micontext);

											Handler puente = new Handler() {
												@SuppressLint("ShowToast")
												@Override
												public void handleMessage(Message msg) {
													if (msg.what == 1) {

														log_aplicacion.log_informacion(
																"imagen" + data_servicio.usuario.getImage());
														if (data_servicio.usuario.getImage().equals("null")) {
															cargar.ocultar();
															Intent intent = new Intent(activity, UsuarioActivity.class);
															activity.startActivity(intent);
														} else {

															Handler puentedescarga = new Handler() {
																@SuppressLint("ShowToast")
																@Override
																public void handleMessage(Message msg) {
																	if (msg.what == 1) {
																		cargar.ocultar();

																		Intent intent = new Intent(activity,
																				UsuarioActivity.class);
																		activity.startActivity(intent);
//																		activity.finish();
																	} else if (msg.what == 0) {
																		cargar.ocultar();
																		Usuario_fluie usuario = null;
																		try {
																			usuario = db.getusuario();
																		} catch (Exception e) {
																			// TODO:
																			// handle
																			// exception
																		}
																		if (usuario != null) {
																			Intent intent = new Intent(activity,
																					BienvenidaActivity.class);
																			activity.startActivity(intent);

																			db.deleteusuario(usuario.getId());
																		}
																	}

																}
															};

															File dirImages = new File(
																	Environment.getExternalStorageDirectory()
																			+ "/Fluie/");

															File myPath = new File(dirImages, "perfil.png");
															if (myPath.exists()) {

																myPath.delete();

															}

															Descarga_Archivo descarga = new Descarga_Archivo(
																	data_servicio.usuario.getImage(), puentedescarga,
																	"perfil.png");
															data_servicio.usuario
																	.setImage(Environment.getExternalStorageDirectory()
																			+ "/Fluie/" + "perfil.png");
															descarga.execute();

														}

													} else if (msg.what == 0) {
														cargar.ocultar();
														if (!data_servicio.msj_error.equals("")) {
															dialogo_modal.dialogo(activity.getString(R.string.error),
																	data_servicio.msj_error, "",
																	activity.getString(R.string.aceptar), true, false);
															data_servicio.msj_error = "";
														}

													}

												}
											};
											servicio3.getfreespace();
											servicio3.setPuente_envio(puente);
											servicio3.execute();
										} catch (UnsupportedEncodingException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									} else if (msg.what == 0) {
										cargar.ocultar();
										if (!data_servicio.msj_error.equals("")) {
											dialogo_modal.dialogo(activity.getString(R.string.error),
													data_servicio.msj_error, "", activity.getString(R.string.aceptar),
													true, false);
											data_servicio.msj_error = "";
										}

									}

								}
							};
							servicio2.get_usuario(data_servicio.usuario.getUser_id());
							servicio2.setPuente_envio(puente);
							servicio2.execute();
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {
						dialogo_modal.dialogo(activity.getString(com.zonesoftware.fluie.R.string.informacion),
								activity.getString(R.string.el_usuario_debe_ser_titular_para_ingresar_), "",
								activity.getString(R.string.aceptar), true, false);

					}
				} else if (msg.what == 0) {
					cargar.ocultar();
					log_aplicacion.log_informacion("informacion " + data_servicio.msj_error);

					if (!data_servicio.msj_error.equals("")) {
						if (!mostarscarga) {

							try {
								db.deleteusuario(db.getusuario().getId());

							} catch (Exception e) {
								// TODO: handle exception
							}

							Intent intent = new Intent(activity, BienvenidaActivity.class);
							activity.startActivity(intent);
						} else {

							dialogo_modal.dialogo(activity.getString(R.string.error), data_servicio.msj_error, "",
									activity.getString(R.string.aceptar), true, false);
							data_servicio.msj_error = "";
						}
					}
				}

			}
		};
		servicio.login(email, password);
		servicio.setPuente_envio(puente);
		servicio.execute();

	}

	public void consultar_benficiarios() {
		data_servicio.lista_beneficiario = new ArrayList<Usuario>();
		data_servicio.lista_beneficiario.clear();
		WS_cliente servicio1;
		try {
			servicio1 = new WS_cliente(micontext);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {

					} else if (msg.what == 0) {

					}

				}
			};
			servicio1.get_lista_beneficiario(data_servicio.usuario.getUser_id());
			servicio1.setPuente_envio(puente);
			servicio1.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void obtener_lista_documentos() {

		WS_cliente servicio3;

		try {
			servicio3 = new WS_cliente(micontext);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {

					} else if (msg.what == 0) {

					}

				}
			};
			servicio3.getlistdocumento();
			servicio3.setPuente_envio(puente);
			servicio3.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void obtener_msgespacio() {

		WS_cliente servicio3;

		try {
			servicio3 = new WS_cliente(micontext);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {

					} else if (msg.what == 0) {

					}

				}
			};
			servicio3.getespacio_msg();
			servicio3.setPuente_envio(puente);
			servicio3.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void obtener_espacio() {

		WS_cliente servicio3;

		try {
			servicio3 = new WS_cliente(micontext);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						cargar.ocultar();

					} else if (msg.what == 0) {
						cargar.ocultar();
						if (!data_servicio.msj_error.equals("")) {

							dialogo_modal.dialogo(activity.getString(R.string.error), data_servicio.msj_error, "",
									activity.getString(R.string.aceptar), true,

									false);

							data_servicio.msj_error = "";
						}

					}

				}
			};
			servicio3.getfreespace();
			servicio3.setPuente_envio(puente);
			servicio3.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void obtener_planes() {

		WS_cliente servicio3;
		cargar.mostrar();
		try {
			servicio3 = new WS_cliente(micontext);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {

						cargar.ocultar();
						Intent intent = new Intent(activity, ComprarEspacioActivity.class);
						activity.startActivity(intent);

					} else if (msg.what == 0) {
						cargar.ocultar();
						if (!data_servicio.msj_error.equals("")) {
							dialogo_modal.dialogo(activity.getString(R.string.error), data_servicio.msj_error, "",
									activity.getString(R.string.cancelar), true, false);

						}

					}

				}
			};
			servicio3.getplanes();
			servicio3.setPuente_envio(puente);
			servicio3.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void consultarimg() {

		WS_cliente servicio;

		try {
			cargar.mostrar();
			servicio = new WS_cliente(micontext);

			Handler puente4 = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {

						Handler puentedescarga = new Handler() {
							@SuppressLint("ShowToast")
							@Override
							public void handleMessage(Message msg) {
								if (msg.what == 1) {
									cargar.ocultar();

									Intent intent = new Intent(activity, UsuarioActivity.class);
									activity.startActivity(intent);

								} else if (msg.what == 0) {
									cargar.ocultar();

									Intent intent = new Intent(activity, UsuarioActivity.class);
									activity.startActivity(intent);
								}

							}
						};

						File dirImages = new File(Environment.getExternalStorageDirectory() + "/Fluie/");

						File myPath = new File(dirImages, "perfil.png");
						if (myPath.exists()) {

							myPath.delete();

						}

						Descarga_Archivo descarga = new Descarga_Archivo(data_servicio.url_perfil, puentedescarga,
								"perfil.png");
						data_servicio.usuario
								.setImage(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
						descarga.execute();

					} else if (msg.what == 0) {

					}

				}
			};
			servicio.get_imagen_user(data_servicio.usuario.getUser_id());
			servicio.setPuente_envio(puente4);
			servicio.execute();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void subir_archivo(String file) {
		log_aplicacion.log_informacion("Archivo upload" + file);
		WS_cliente servicio3;

		try {
			servicio3 = new WS_cliente(micontext);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {

						cargar.ocultar();
						Usuario_fluie usuario = null;

						try {
							data_servicio.usuariofluie = usuario;
							Toast.makeText(micontext, data_servicio.msj, Toast.LENGTH_LONG).show();
							Intent intent = new Intent(activity, UsuarioActivity.class);
							activity.startActivity(intent);

						} catch (Exception e) {
							// TODO
							// Auto-generated
							// catch
							// block
							e.printStackTrace();
						}
					} else if (msg.what == 0) {
						cargar.ocultar();

						if (!data_servicio.msj_error.equals("")) {
							dialogo_modal.dialogo(activity.getString(R.string.error), data_servicio.msj_error, "",
									activity.getString(R.string.cancelar), true, false);
						}

					}

				}
			};
			servicio3.subir_imagen(file);
			servicio3.setPuente_envio(puente);
			servicio3.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void editarpersona() {

		if (data_servicio.json_generos.equals("")) {

			WS_cliente servicio;
			try {
				servicio = new WS_cliente(micontext);

				Handler puente = new Handler() {
					@SuppressLint("ShowToast")
					@Override
					public void handleMessage(Message msg) {
						if (msg.what == 1) {
							new Handler().postDelayed(new Runnable() {
								@Override
								public void run() {

									Intent intent = new Intent(activity, EditarPersonaActivity.class);
									activity.startActivity(intent);
								}
							}, 2000);

						} else if (msg.what == 0) {
							cargar.ocultar();
							if (!data_servicio.msj_error.equals("")) {
								dialogo_modal.dialogo(activity.getString(R.string.error), data_servicio.msj_error, "",
										activity.getString(R.string.cancelar), true, false);
								data_servicio.msj_error = "";
							}

						}

					}
				};
				servicio.getgenero();
				servicio.setPuente_envio(puente);
				servicio.execute();

			} catch (Exception e) {

			}
		}

		else {

			Intent intent = new Intent(activity, EditarPersonaActivity.class);
			activity.startActivity(intent);

		}

	}

	public void iniciar_home() throws JSONException, UnsupportedEncodingException {
		log_aplicacion.log_informacion("get_home");

		WS_cliente servicio = new WS_cliente(micontext);

		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {
					cargar.ocultar();
					obtener_lista_documentos();
					WS_cliente servicio2;
					try {
						servicio2 = new WS_cliente(micontext);

						Handler puente = new Handler() {
							@SuppressLint("ShowToast")
							@Override
							public void handleMessage(Message msg) {
								if (msg.what == 1) {
									WS_cliente servicio3;
									try {
										servicio3 = new WS_cliente(micontext);

										Handler puente = new Handler() {
											@SuppressLint("ShowToast")
											@Override
											public void handleMessage(Message msg) {
												if (msg.what == 1) {
													new Handler().postDelayed(new Runnable() {
														@Override
														public void run() {
															Usuario_fluie usuario = null;
															try {
																usuario = db.getusuario();
															} catch (Exception e) {
																// TODO: handle
																// exception
															}
															if (usuario == null) {
																Intent intent = new Intent(activity,
																		BienvenidaActivity.class);
																activity.startActivity(intent);
															} else {
																try {
																	data_servicio.usuariofluie = usuario;
																	login(usuario.getCorreo(), usuario.getPassword(),
																			false);
																} catch (UnsupportedEncodingException e) {
																	// TODO
																	// Auto-generated
																	// catch
																	// block
																	e.printStackTrace();
																} catch (JSONException e) {
																	// TODO
																	// Auto-generated
																	// catch
																	// block
																	e.printStackTrace();
																}

															}

														}
													}, 2000);

												} else if (msg.what == 0) {
													cargar.ocultar();
													if (!data_servicio.msj_error.equals("")) {
														dialogo_modal.dialogo(activity.getString(R.string.error),
																data_servicio.msj_error, "",
																activity.getString(R.string.cancelar), true, false);
														data_servicio.msj_error = "";
													}

												}

											}
										};
										servicio3.get_lista_relacion();
										servicio3.setPuente_envio(puente);
										servicio3.execute();
									} catch (UnsupportedEncodingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								} else if (msg.what == 0) {
									cargar.ocultar();
									if (!data_servicio.msj_error.equals("")) {
										dialogo_modal.dialogo(activity.getString(R.string.error),
												data_servicio.msj_error, "", activity.getString(R.string.cancelar),
												true, false);
										data_servicio.msj_error = "";
									}

								}

							}
						};
						servicio2.getcountry();
						servicio2.setPuente_envio(puente);
						servicio2.execute();
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (msg.what == 0) {

				}

			}
		};
		servicio.get_home();
		servicio.setPuente_envio(puente);
		servicio.execute();
	}

	public void get_list_beneficiarios() {

		data_servicio.lista_beneficiario = new ArrayList<Usuario>();
		WS_cliente servicio1;
		try {
			servicio1 = new WS_cliente(micontext);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						try {

							jarraybeneficiarios = new JSONArray(data_servicio.json_lista_beneficiario);

							for (int i = 0; i < jarraybeneficiarios.length(); i++) {

								id = jarraybeneficiarios.getJSONObject(i).getString("beneficiary_id");

								WS_cliente servicio3;
								try {
									servicio3 = new WS_cliente(micontext);

									Handler puente = new Handler() {
										@SuppressLint("ShowToast")
										@Override
										public void handleMessage(Message msg) {
											if (msg.what == 1) {
												if (jarraybeneficiarios.length() == data_servicio.lista_beneficiario
														.size()) {
													Intent intent = new Intent(activity, BienvenidaActivity.class);
													factivity.startActivity(intent);

												}

											} else if (msg.what == 0) {

											}

										}
									};
									servicio3.get_usuarios_lista(id);
									servicio3.setPuente_envio(puente);
									servicio3.execute();
								} catch (UnsupportedEncodingException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else if (msg.what == 0) {
						cargar.ocultar();
						if (!data_servicio.msj_error.equals("")) {
							dialogo_modal.dialogo("Error", data_servicio.msj_error, "", "cancelar", true, false);
							data_servicio.msj_error = "";
						}

					}

				}
			};
			servicio1.get_lista_beneficiario(data_servicio.usuario.getUser_id());
			servicio1.setPuente_envio(puente);
			servicio1.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void update_usuario(final String id, final String documento, final String email, final String email_opcional,
			final String name, final String name_last, final String country_id, final String phone,
			final String address, final String image, final String id_doc)
					throws UnsupportedEncodingException, JSONException {
		log_aplicacion.log_informacion("update");
		cargar.mostrar();
		WS_cliente servicio = new WS_cliente(micontext);
		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {

					data_servicio.usuario.setName(name);
					data_servicio.usuario.setName_last(name_last);
					data_servicio.usuario.setEmail(email);
					data_servicio.usuario.setAddress(address);
					data_servicio.usuario.setCountry_id(country_id);
					data_servicio.usuario.setTipo_documento(id_doc);
					data_servicio.usuario.setIdentification(documento);
					data_servicio.usuario.setEmail_optional(email_opcional);
					data_servicio.usuario.setIdentification(documento);
					Usuario_fluie usuario = null;
					try {
						usuario = db.getusuario();
					} catch (Exception e) {
						// TODO:
						// handle
						// exception
					}
					if (usuario != null) {
						try {
							usuario.setCorreo(email);
							db.updateusuario(usuario);
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					if (!image.equals("")) {
						subir_archivo(image);
					} else {
						cargar.ocultar();

						try {
							Toast.makeText(micontext, data_servicio.msj, Toast.LENGTH_LONG).show();
							Intent intent = new Intent(activity, UsuarioActivity.class);
							activity.startActivity(intent);

						} catch (Exception e) {
							// TODO
							// Auto-generated
							// catch
							// block
							e.printStackTrace();
						}

					}
				} else if (msg.what == 0) {
					cargar.ocultar();
					if (!data_servicio.msj_error.equals("")) {
						Toast.makeText(micontext, data_servicio.msj_error, Toast.LENGTH_LONG).show();
					}
				}

			}
		};
		servicio.Update_Usuario(data_servicio.usuario.getUser_id(), email, email_opcional, documento, name, name_last,
				country_id, phone, address, image, id_doc);

		servicio.setPuente_envio(puente);
		servicio.execute();

	}

	public void registar_usuario(final String email, final String password, String name, String name_last,
			String identification, String country_id, String phone, String address, final String image,
			String email_optional, String notification, final String pass, String confirm_password, String terms,
			String type_identification_id) throws UnsupportedEncodingException, JSONException {
		log_aplicacion.log_informacion("crear");
		cargar.mostrar();
		WS_cliente servicio = new WS_cliente(micontext);
		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {

					WS_cliente servicio;
					try {
						servicio = new WS_cliente(micontext);

						Handler puente = new Handler() {
							@SuppressLint("ShowToast")
							@Override
							public void handleMessage(Message msg) {
								if (msg.what == 1) {
									data_servicio.email_usuario = email;
									data_servicio.password = password;

									if (!image.equals("")) {
										subir_archivo(image);
									} else {
										cargar.ocultar();
										Usuario_fluie usuario = null;
										try {
											usuario = db.getusuario();
										} catch (Exception e) {
											// TODO: handle
											// exception
										}

										try {
											data_servicio.usuariofluie = usuario;

											login(data_servicio.email_usuario, data_servicio.password, true);

										} catch (UnsupportedEncodingException e) {
											// TODO
											// Auto-generated
											// catch
											// block
											e.printStackTrace();
										} catch (JSONException e) {
											// TODO
											// Auto-generated
											// catch
											// block
											e.printStackTrace();
										}

									}
									cargar.ocultar();
								} else if (msg.what == 0) {
									cargar.ocultar();
									if (!data_servicio.msj_error.equals("")) {
										dialogo_modal.dialogo(activity.getString(R.string.error),
												data_servicio.msj_error, "", activity.getString(R.string.aceptar), true,
												false);
										data_servicio.msj_error = "";
									}

								}

							}
						};
						servicio.set_role(data_servicio.usuario.getUser_id(), "1");
						servicio.setPuente_envio(puente);
						servicio.execute();
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (msg.what == 0) {
					cargar.ocultar();

				}

			}
		};
		servicio.crear_usuario(email, password, name, name_last, identification, country_id, phone, address,
				Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png", email_optional, notification,
				pass, confirm_password, terms, type_identification_id);

		data_servicio.usuario.setName(name);
		data_servicio.usuario.setName_last(name_last);
		data_servicio.usuario.setEmail(email);
		data_servicio.usuario.setIdentification(identification);
		data_servicio.usuario.setCountry_id(country_id);

		servicio.setPuente_envio(puente);
		servicio.execute();

	}

	public void crear_usuario() throws JSONException, UnsupportedEncodingException {
		WS_cliente servicio2;
		try {

			servicio2 = new WS_cliente(micontext);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						Intent intent = new Intent(activity, CrearUsuarioActivity.class);
						activity.startActivity(intent);
						cargar.ocultar();
					} else if (msg.what == 0) {
						cargar.ocultar();
						if (!data_servicio.msj_error.equals("")) {
							dialogo_modal.dialogo(activity.getString(R.string.error), data_servicio.msj_error, "",
									activity.getString(R.string.aceptar), true, false);
							data_servicio.msj_error = "";
						}

					}

				}
			};
			servicio2.getcountry();
			servicio2.setPuente_envio(puente);
			servicio2.execute();
		} catch (Exception e) {

		}

	}
}
