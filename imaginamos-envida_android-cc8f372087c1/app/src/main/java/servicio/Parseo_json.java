package servicio;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

import modelo.Home;
import modelo.Imagen;
import modelo.Mensaje;
import modelo.Notificacion;
import modelo.Usuario;

public class Parseo_json {

	public Parseo_json() {
	}

	public boolean parseo_Consultar_home(String str_json) {

		data_servicio.jsonhome = str_json;
		boolean parseo = false; //

		try {
			JSONArray jarray = new JSONArray(str_json);
			JSONObject j_data = jarray.getJSONObject(0);
			parseo = true;

			Home mhome = new Home();

			try {
				data_servicio.terms_conditions = j_data.getString("terms_conditions");

				mhome.setTerms_conditions(j_data.getString("terms_conditions"));
				log_aplicacion.log_informacion("informacion: " + j_data.getString("terms_conditions"));
			} catch (Exception e) {
				log_aplicacion.log_error("parseo: terms_conditions" + e.toString());
			}

			try {
				mhome.setAbout(j_data.getString("about"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setAbout_image(j_data.getString("about_image"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setAddress(j_data.getString("address"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}
			try {
				mhome.setTerms_conditions(j_data.getString("terms_conditions"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}
			try {
				mhome.setAlt_about_image(j_data.getString("alt_about_image"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setAlt_how_image(j_data.getString("alt_how_image"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setAlt_logo(j_data.getString("alt_logo"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setDescription(j_data.getString("description"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setDescription_beneficiary(j_data.getString("description_beneficiary"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setDescription_checker(j_data.getString("description_checker"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setDescription_user(j_data.getString("description_user"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setEmail(j_data.getString("email"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setFacebook(j_data.getString("facebook"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setGoogle(j_data.getString("google"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setHow_does_it_work(j_data.getString("how_does_it_work"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setHow_does_it_work_image(j_data.getString("how_does_it_work_image"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setIdhome(j_data.getString("idhome"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setPhone(j_data.getString("phone"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				mhome.setTitle(j_data.getString("title"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}
			try {
				mhome.setTwitter(j_data.getString("twitter"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}
			try {
				mhome.setVideo(j_data.getString("video"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			data_servicio.home = mhome;

		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

	public boolean parseo_tipo(String str_json, String tipo) {
		boolean parseo = false; //

		if (tipo.equals("get_genero")) {
			data_servicio.json_generos = str_json;
		} else if (tipo.equals("getespacio_msg")) {

			data_servicio.json_espacio_msg = str_json;

		} else if (tipo.equals("getplanes")) {

			data_servicio.json_planes = str_json;

		} else if (tipo.equals("comprar")) {

			data_servicio.json_compra = str_json;

		} else if (tipo.equals("get_historial")) {

			data_servicio.json_historial = str_json;

		} else if (tipo.equals("subir_imagen")) {

			data_servicio.json_subir_imagen = str_json;

		} else if (tipo.equals("subir_imagen")) {

			data_servicio.json_subir_imagen = str_json;

		} else if (tipo.equals("getsplash")) {

			data_servicio.jsonsplash = str_json;

		} else if (tipo.equals("getespacio_consejos")) {

			data_servicio.espacion_mensajes.add(str_json);

		}

		else if (tipo.equals("Delete_mensaje")) {
			try {
				JSONObject jobj = new JSONObject(str_json);
				parseo = true;
			} catch (Exception e) { //
				log_aplicacion.log_error("parseo: " + e.toString());
			}

		}

		try {
			JSONArray jarray = new JSONArray(str_json);
			parseo = true;
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

	public boolean parseo_pais(String str_json) {

		data_servicio.json_paises = str_json;
		boolean parseo = false; //

		try {
			JSONArray jarray = new JSONArray(str_json);
			parseo = true;
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

	public boolean parseo_tipo_relacion(String str_json) {

		data_servicio.json_tipo_relacion = str_json;
		boolean parseo = false; //

		try {
			JSONArray jarray = new JSONArray(str_json);
			parseo = true;
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

	public boolean parseo_lista_beneficiarios(String str_json) {

		data_servicio.json_lista_beneficiario = str_json;
		boolean parseo = false; //

		try {
			JSONArray jarray = new JSONArray(str_json);
			parseo = true;
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}
		log_aplicacion.log_informacion("parseo: " + parseo);
		//
		return parseo;
	}

	public boolean parseo_lista_verificadores(String str_json) {

		data_servicio.json_lista_verificadores = str_json;
		boolean parseo = false; //

		try {
			JSONArray jarray = new JSONArray(str_json);
			parseo = true;
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

	public boolean parseo_get_mensaje(String str_json) {

		boolean parseo = false; //

		/*
		 * "iduser": "3" "email": "" "name": null "name_last": null
		 * "identification": null "country_id": "1" "phone": null "address":
		 * null "image": null "email_optional": null "active": "1" "space":
		 * "20480" "notification": "1" "date_created": "2015-10-22 12:25:56"
		 * "date_updated": null
		 * 
		 */
		try {

			JSONArray jsonmensajes = new JSONArray(str_json);
			int j = 0;

			Mensaje msj = new Mensaje();

			try {
				msj.setIdmessage(jsonmensajes.getJSONObject(j).getString("idmessage"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				msj.setType_event_id(jsonmensajes.getJSONObject(j).getString("type_event_id"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				msj.setType_file_id(jsonmensajes.getJSONObject(j).getString("type_file_id"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				msj.setType_file_id(jsonmensajes.getJSONObject(j).getString("type_file_id"));
			} catch (Exception e) {
				// TODO: handle exception
			}

			try {
				msj.setTitle(jsonmensajes.getJSONObject(j).getString("title"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				msj.setDate_view(jsonmensajes.getJSONObject(j).getString("date_view"));
			} catch (Exception e) {
				// TODO: handle exception
			}

			try {
				msj.setDescription(jsonmensajes.getJSONObject(j).getString("description"));
			} catch (Exception e) {
				// TODO: handle exception
			}

			try {
				msj.setDescription(jsonmensajes.getJSONObject(j).getString("description"));

				JSONArray jsonfiles = new JSONArray(jsonmensajes.getJSONObject(j).getString("messageFiles"));

				data_servicio.json_file = jsonfiles.toString();

			} catch (Exception e) {
				// TODO: handle exception
			}

			data_servicio.mensajeedicion = msj;

			ArrayList<Usuario> beneficiarios = new ArrayList<Usuario>();

			JSONArray jsonbeneficiarios = new JSONArray(
					jsonmensajes.getJSONObject(j).getString("messageBeneficiaries"));
			log_aplicacion.log_informacion("id_ben" + jsonbeneficiarios.toString());

			JSONArray jsonbeneficiarios_completo = new JSONArray(data_servicio.json_lista_beneficiario);

			for (int i = 0; i < jsonbeneficiarios.length(); i++) {

				Usuario us = new Usuario();

				log_aplicacion
						.log_informacion("id_ben_mes" + jsonbeneficiarios.getJSONObject(j).getString("beneficiary_id"));

				for (int k = 0; k < jsonbeneficiarios_completo.length(); k++) {
					log_aplicacion.log_informacion(
							"id_ben_list" + jsonbeneficiarios_completo.getJSONObject(k).getString("beneficiary_id"));

					if (jsonbeneficiarios_completo.getJSONObject(k).getString("beneficiary_id")
							.equals(jsonbeneficiarios.getJSONObject(i).getString("beneficiary_id"))) {

						try {
							log_aplicacion.log_informacion(
									"id_ben_list" + jsonbeneficiarios_completo.getJSONObject(k).toString());

							us.setUser_id(jsonbeneficiarios_completo.getJSONObject(k).getJSONObject("beneficiary")
									.getString("iduser"));

							us.setName(jsonbeneficiarios_completo.getJSONObject(k).getJSONObject("beneficiary")
									.getString("name"));

						} catch (Exception e) {
							// TODO: handle exception
						}

						beneficiarios.add(us);
						break;
					}
				}

			}

			data_servicio.lista_beneficiario_msg = (ArrayList<Usuario>) beneficiarios.clone();

			parseo = true;

		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

	/*
	 * 02-24 15:17:06.550: I/fluied(20808): informacion fluie:
	 * [{"idbeneficiaryuser":"71","beneficiary_id":"173","user_id":"171","code":
	 * "2b1b05fcf49b6c33cee325fcca0b9356","relationship_id":"7","beneficiary":{
	 * "iduser":"173","password":"ca73ce86b2a8f51638f94f8db05f268b","token":
	 * "56c063688e638745aead5be1dd8a4e8f","country_id":null,"gender_id":"2",
	 * "type_identification_id":null,"image":"female-user.jpg","active":"1",
	 * "space":"0","notification":"1","folder":
	 * "dc2fb90ee6b985cf53159766860fba89","date_created":"2016-02-10 10:08:25"
	 * ,"date_updated":"2016-02-23 21:10:13","state":"1","name":"natalia "
	 * ,"email":"natalia.pruebita@imaginamos.com","seed":
	 * "dce7602f018f34d8aac5a50bd3d2d742","name_last":"prueba","identification":
	 * "","phone":"","mobile":"","address":"","email_optional":""}},{
	 * "idbeneficiaryuser":"72","beneficiary_id":"177","user_id":"171","code":
	 * "c74a86f13482b7ce3e4f6db15eeb8c53","relationship_id":"1","beneficiary":{
	 * "iduser":"177","password":"57908659963168598a7eefa71744b0cd","token":
	 * "c74a86f13482b7ce3e4f6db15eeb8c53","country_id":null,"gender_id":"2",
	 * "type_identification_id":null,"image":"female-user.jpg","active":"1",
	 * "space":"0","notification":"1","folder":
	 * "45094a6b5e297686b1245f4df86700c4","date_created":"2016-02-15 12:10:54"
	 * ,"date_updated":"2016-02-17 12:28:28"
	 * ,"state":"1","name":"tatiana.pinzon","email":
	 * "tatiana.pinzon@imaginamos.com","seed":"a5a31d71c2e8d3225eadbfa9d715d93b"
	 * ,"name_last":"","identification":"","phone":"","mobile":"","address":"",
	 * "email_optional":""}},{"idbeneficiaryuser":"87","beneficiary_id":"87",
	 * "user_id":"171","code":"beb6c9c485d1042bf8f790a4bc0bee35",
	 * "relationship_id":"2","beneficiary":{"iduser":"87","password":
	 * "edf232b0f48f8a0a4534802d751222d2","token":null,"country_id":"1",
	 * "gender_id":"1","type_identification_id":"0","image":
	 * "29_01_2016_09_01_58_image.jpg","active":"1","space":"4005",
	 * "notification":"1","folder":"e8515b0fd34460e333c7bce9bc6489ea",
	 * "date_created":"2016-01-22 15:07:49","date_updated":"2016-02-23 18:44:14"
	 * ,"state":"1","name":"carlos","email":"pepiperez@gmail.com","seed":
	 * "c78a6b986219d22f5b145268de64da34","name_last":"robinson lara"
	 * ,"identification":"123123","phone":"3570124","mobile":"","address":
	 * "calle 53 a bis num 21"
	 * ,"email_optional":""}},{"idbeneficiaryuser":"92","beneficiary_id":"195",
	 * "user_id":"171","code":"8a6daa1041e1c0907c857d2a7e5a18d0",
	 * "relationship_id":"3","beneficiary":{"iduser":"195","password":
	 * "0915a6d0fa54fafbca4aa68145f9f894","token":
	 * "8a6daa1041e1c0907c857d2a7e5a18d0","country_id":null,"gender_id":"1",
	 * "type_identification_id":null,"image":"male-user.svg","active":"0",
	 * "space":"0","notification":"1","folder":
	 * "02bee7b944e653fb208c33e475cf0789","date_created":"2016-02-17 11:58:53"
	 * ,"date_updated":null,"state":"1","name":"jjulian mmarin"
	 * ,"email":"maein@gmail.com","seed":"364fdd7cfd31024c5642f8290348ae21",
	 * "name_last":"","identification":"","phone":"","mobile":"","address":"",
	 * "email_optional":""}},{"idbeneficiaryuser":"95","beneficiary_id":"198",
	 * "user_id":"171","code":"39a5b5b621ef7dc339ca5dbdf70d7d83",
	 * "relationship_id":"1","beneficiary":{"iduser":"198","password":
	 * "edff7e2b39fb2b7d5bc27d35f9eb05f2","token":
	 * "39a5b5b621ef7dc339ca5dbdf70d7d83","country_id":null,"gender_id":"2",
	 * "type_identification_id":null,"image":"male-user.jpg","active":"0",
	 * "space":"0","notification":"1","folder":
	 * "a821c78e8642165697c21866d6fb1e7c","date_created":"2016-02-17 12:08:32"
	 * ,"date_updated":"2016-02-23 05:07:19","state":"1","name":"prueba hoy "
	 * ,"email":"pruebahoyhoy.hhh@gmail.com","seed":
	 * "89010479b4eb5d38bf0cb1eec652607a","name_last":"h","identification":"",
	 * "phone":"","mobile":"","address":"","email_optional":""}},{
	 * "idbeneficiaryuser":"96","beneficiary_id":"199","user_id":"171","code":
	 * "44bec33dc3ed639dced315d015ebbfa6","relationship_id":"2","beneficiary":{
	 * "iduser":"199","password":"7e24e07e8b0c9dd2ab3554b90f6db48d","token":
	 * "44bec33dc3ed639dced315d015ebbfa6","country_id":null,"gender_id":"1",
	 * "type_identification_id":null,"image":"male-user.svg","active":"0",
	 * "space":"0","notification":"1","folder":
	 * "aeb9192ae7ee79e02c36f55dddb6ff0e","date_created":"2016-02-17 12:09:46"
	 * ,"date_updated":null,"state":"1","name":"benpruebaa pru","
	 * 
	 */

	public boolean parseo_get_usuario(String str_json) {

		boolean parseo = false; //

		/*
		 * "iduser": "3" "email": "" "name": null "name_last": null
		 * "identification": null "country_id": "1" "phone": null "address":
		 * null "image": null "email_optional": null "active": "1" "space":
		 * "20480" "notification": "1" "date_created": "2015-10-22 12:25:56"
		 * "date_updated": null 03-01 23:27:03.480: I/fluied(17170): informacion
		 * fluie: [{"iduser":"171","email":"viviana.gomez@imaginamos.com",
		 * "name":"viviana","name_last":"gomez",
		 * "identification":"1033709160","country_id":"1","gender_id":"1",
		 * "type_identification_id":"1","phone":"123456","address":
		 * "cra 20 # 23.- 4"
		 * ,"image":"01_03_2016_11_27_02_UTF-8","email_optional":"","active":"1"
		 * ,"space":"1165","notification":"1","date_created":
		 * "2016-02-10 09:11:07","date_updated":"2016-03-01 23:27:02"}]
		 * 
		 */
		try {

			JSONArray jarray = new JSONArray(str_json);
			data_servicio.json_usuario = str_json;

			int i = 0;

			data_servicio.usuario.setUser_id(jarray.getJSONObject(i).getString("iduser"));
			data_servicio.usuario.setName(jarray.getJSONObject(i).getString("name"));
			data_servicio.usuario.setEmail(jarray.getJSONObject(i).getString("email"));

			data_servicio.usuario.setName_last(jarray.getJSONObject(i).getString("name_last"));
			data_servicio.usuario.setIdentification(jarray.getJSONObject(i).getString("identification"));
			data_servicio.usuario.setCountry_id(jarray.getJSONObject(i).getString("country_id"));
			data_servicio.usuario.setPhone(jarray.getJSONObject(i).getString("phone"));

			data_servicio.usuario.setAddress(jarray.getJSONObject(i).getString("address"));

			data_servicio.usuario.setEmail_optional(jarray.getJSONObject(i).getString("email_optional"));

			data_servicio.usuario.setTipo_documento(jarray.getJSONObject(i).getString("type_identification_id"));

			data_servicio.usuario.setActive(jarray.getJSONObject(i).getString("active"));
			data_servicio.usuario.setSpace(jarray.getJSONObject(i).getString("space"));
			data_servicio.usuario.setNotification(jarray.getJSONObject(i).getString("notification"));

			parseo = true;

		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

	public boolean parseo_get_usuario_lista(String str_json) {

		boolean parseo = false; //
		/*
		 * "iduser": "3" "email": "" "name": null "name_last": null
		 * "identification": null "country_id": "1" "phone": null "address":
		 * null "image": null "email_optional": null "active": "1" "space":
		 * "20480" "notification": "1" "date_created": "2015-10-22 12:25:56"
		 * "date_updated": null
		 * 
		 */
		try {

			JSONArray jarray = new JSONArray(str_json);

			int i = 0;

			data_servicio.usuario_beneficiario.setUser_id(jarray.getJSONObject(i).getString("iduser"));
			data_servicio.usuario_beneficiario.setName(jarray.getJSONObject(i).getString("name"));
			data_servicio.usuario_beneficiario.setEmail(jarray.getJSONObject(i).getString("email"));

			data_servicio.usuario_beneficiario.setName_last(jarray.getJSONObject(i).getString("name_last"));
			data_servicio.usuario_beneficiario.setIdentification(jarray.getJSONObject(i).getString("identification"));
			data_servicio.usuario_beneficiario.setCountry_id(jarray.getJSONObject(i).getString("country_id"));
			data_servicio.usuario_beneficiario.setPhone(jarray.getJSONObject(i).getString("phone"));
			try {
				data_servicio.usuario_beneficiario.setMovil(jarray.getJSONObject(i).getString("movil"));
			} catch (Exception e) {
				// TODO: handle exception
			}

			data_servicio.usuario_beneficiario.setAddress(jarray.getJSONObject(i).getString("address"));

			data_servicio.usuario_beneficiario.setImage(jarray.getJSONObject(i).getString("image"));
			data_servicio.usuario_beneficiario.setEmail_optional(jarray.getJSONObject(i).getString("email_optional"));
			data_servicio.usuario_beneficiario.setActive(jarray.getJSONObject(i).getString("active"));
			data_servicio.usuario_beneficiario.setSpace(jarray.getJSONObject(i).getString("space"));
			data_servicio.usuario_beneficiario.setNotification(jarray.getJSONObject(i).getString("notification"));
			data_servicio.usuario_beneficiario.setGenero(jarray.getJSONObject(i).getString("gender_id"));
			parseo = true;

			if (data_servicio.lista.equals("beneficiarios")) {
				try {

					JSONArray jsonbeneficiario = new JSONArray(data_servicio.json_lista_beneficiario);
					if (jsonbeneficiario.length() == 0) {

					} else {

						for (int j = 0; j < jsonbeneficiario.length(); j++) {

							String id = jsonbeneficiario.getJSONObject(j).getString("beneficiary_id");
							if (id.equals(data_servicio.usuario_beneficiario.getUser_id())) {
								data_servicio.usuario_beneficiario
										.setRelacion(jsonbeneficiario.getJSONObject(j).getString("relationship_id"));
								data_servicio.usuario_beneficiario.setId_beneficiario_user(
										jsonbeneficiario.getJSONObject(j).getString("idbeneficiaryuser"));

								break;
							}

						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}

				data_servicio.lista_beneficiario.add(data_servicio.usuario_beneficiario.clone());

			} else if (data_servicio.lista.equals("verificadores")) {

				try {

					JSONArray jsonverificadores = new JSONArray(data_servicio.json_lista_verificadores);
					if (jsonverificadores.length() == 0) {

					} else {

						for (int j = 0; i < jsonverificadores.length(); j++) {

							String id = jsonverificadores.getJSONObject(j).getString("checker_id");

							if (id.equals(data_servicio.usuario_beneficiario.getUser_id())) {

								data_servicio.usuario_beneficiario
										.setEstado(jsonverificadores.getJSONObject(j).getString("active"));
								break;
							}
						}
					}

				} catch (Exception e) {
					// TODO: handle exception
				}

				data_servicio.lista_verificadores.add(data_servicio.usuario_beneficiario.clone());
			}

		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

	// beneficiarios o verificadores
	public boolean parseo_lista(String str_json) {

		boolean parseo = false; //
		/*
		 * "iduser": "3" "email": "" "name": null "name_last": null
		 * "identification": null "country_id": "1" "phone": null "address":
		 * null "image": null "email_optional": null "active": "1" "space":
		 * "20480" "notification": "1" "date_created": "2015-10-22 12:25:56"
		 * "date_updated": null
		 * 
		 */
		try {
			JSONArray jarray = new JSONArray(str_json);

			for (int i = 0; i < jarray.length(); i++) {

				if (jarray.getJSONObject(i).getString("email").equals(data_servicio.email_usuario)) {

					data_servicio.usuario.setUser_id(jarray.getJSONObject(i).getString("iduser"));
					data_servicio.usuario.setName(jarray.getJSONObject(i).getString("name"));
					data_servicio.usuario.setEmail(jarray.getJSONObject(i).getString("email"));

					data_servicio.usuario.setName_last(jarray.getJSONObject(i).getString("name_last"));
					data_servicio.usuario.setIdentification(jarray.getJSONObject(i).getString("identification"));
					data_servicio.usuario.setCountry_id(jarray.getJSONObject(i).getString("country_id"));
					data_servicio.usuario.setPhone(jarray.getJSONObject(i).getString("phone"));
					try {
						data_servicio.usuario.setMovil(jarray.getJSONObject(i).getString("movil"));
					} catch (Exception e) {
						// TODO: handle exception
					}

					data_servicio.usuario.setAddress(jarray.getJSONObject(i).getString("address"));

					data_servicio.usuario.setImage(jarray.getJSONObject(i).getString("image"));
					data_servicio.usuario.setEmail_optional(jarray.getJSONObject(i).getString("email_optional"));
					data_servicio.usuario.setActive(jarray.getJSONObject(i).getString("active"));
					data_servicio.usuario.setSpace(jarray.getJSONObject(i).getString("space"));
					data_servicio.usuario.setNotification(jarray.getJSONObject(i).getString("notification"));

					break;
				}
				parseo = true;
			}

		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

	public boolean parseo_imagen_perfil(String str_json) {

		boolean parseo = false; //

		try {

			JSONObject j_data = new JSONObject(str_json);

			try {

				try {
					String result = j_data.getString("result");
					if (result.equals("success")) {
						parseo = true;
						String image = j_data.getString("image");

						data_servicio.url_perfil = image;

					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return parseo;
	}

	public boolean parseo_imagen(String str_json, String id) {

		boolean parseo = false; //

		try {

			JSONObject j_data = new JSONObject(str_json);

			try {

				try {
					String result = j_data.getString("result");
					if (result.equals("success")) {
						parseo = true;
						String image = j_data.getString("image");

						boolean agregar = true;
						for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
							if (data_servicio.imagen_usuarios.get(l).getId_user().equals(id)) {

								agregar = false;
								break;
							}
						}

						if (agregar) {
							Imagen img = new Imagen(id, image);
							img.setActualizar(true);
							data_servicio.imagen_usuarios.add(img);
						}
					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return parseo;
	}

	public boolean parseo_generico(String str_json) {

		boolean parseo = false; //

		try {

			JSONObject j_data = new JSONObject(str_json);

			try {

				/*
				 * 01-23 11:17:23.210: I/fluied(17145): informacion fluie:
				 * respuesta servicio {"result":"success","msg":
				 * "Mensaje creado exitosamente."}
				 */
				try {
					String result = j_data.getString("result");
					if (result.equals("success")) {
						parseo = true;
						data_servicio.msj = j_data.getString("msg");
					} else {
						data_servicio.msj_error = j_data.getString("msg");
						parseo = false;
					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return parseo;
	}

	public boolean parseo_crear_mensaje(String str_json) {

		boolean parseo = false; //

		try {

			JSONObject j_data = new JSONObject(str_json);

			try {

				/*
				 * 01-23 11:17:23.210: I/fluied(17145): informacion fluie:
				 * respuesta servicio {"result":"success","msg":
				 * "Mensaje creado exitosamente."}
				 */
				try {
					String result = j_data.getString("result");
					if (result.equals("success")) {
						parseo = true;
						data_servicio.msj = j_data.getString("msg");
					}

				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					String result = j_data.getString("result");
					if (result.equals("error")) {
						parseo = false;
						data_servicio.msj_error = j_data.getString("msg");
						data_servicio.msj = j_data.getString("msg");
					}

				} catch (Exception e) {
					// TODO: handle exception
				}

				parseo = true;

			} catch (Exception e) {
				// TODO: handle exception
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return parseo;
	}

	public boolean parseo_crear_usuario(String str_json) {

		data_servicio.usuario.setJson_usurio(str_json);
		boolean parseo = false; //

		try {

			JSONObject j_data = new JSONObject(str_json);

			try {
				j_data.getJSONObject("error");
				parseo = false;

				JSONObject msgerror = j_data.getJSONObject("message");

				data_servicio.msj_error = "";

				Iterator<?> keys = msgerror.keys();
				String msj_error = "";
				while (keys.hasNext()) {

					String key = (String) keys.next();
					log_aplicacion.log_informacion("info error" + key);

					JSONArray jarray = msgerror.getJSONArray(key);
					for (int i = 0; i < jarray.length(); i++) {
						msj_error = msj_error + "\n" + jarray.getString(i);
					}
				}
				data_servicio.msj_error = msj_error;
				data_servicio.msj_error = msj_error.replaceAll("[", "");
				data_servicio.msj_error = data_servicio.msj_error.replaceAll("]", "");

			} catch (Exception e) {
				log_aplicacion.log_error("parseo4: " + e.toString());
			}

			try {
				data_servicio.usuario.setActive(j_data.getString("active"));
				parseo = true;

			} catch (Exception e) {
				log_aplicacion.log_error("parseo3: " + e.toString());
			}

			try {
				data_servicio.usuario.setUser_id(j_data.getString("iduser"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo2: " + e.toString());
			}
			parseo = true;
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo1: " + e.toString());
		}

		return parseo;
	}

	public boolean parseo_crear_ben_ver(String str_json) {

		data_servicio.usuario.setJson_usurio(str_json);
		boolean parseo = false; //

		try {

			JSONObject j_data = new JSONObject(str_json);

			try {
				j_data.getJSONObject("error");
				parseo = false;

				JSONObject msgerror = j_data.getJSONObject("message");

				data_servicio.msj_error = "";

				Iterator<?> keys = msgerror.keys();
				String msj_error = "";
				while (keys.hasNext()) {

					String key = (String) keys.next();
					log_aplicacion.log_informacion("info error" + key);

					JSONArray jarray = msgerror.getJSONArray(key);
					for (int i = 0; i < jarray.length(); i++) {
						msj_error = msj_error + "\n" + jarray.getString(i);
					}
				}
				data_servicio.msj_error = msj_error;
				data_servicio.msj_error = msj_error.replaceAll("[", "");
				data_servicio.msj_error = data_servicio.msj_error.replaceAll("]", "");

			} catch (Exception e) {
				log_aplicacion.log_error("parseo4: " + e.toString());
			}

			try {
				j_data.getString("active");
				parseo = true;

			} catch (Exception e) {
				log_aplicacion.log_error("parseo3: " + e.toString());
			}

			try {
				data_servicio.id_beneficiario = j_data.getString("iduser");

			} catch (Exception e) {
				log_aplicacion.log_error("parseo2: " + e.toString());
			}
			parseo = true;
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo1: " + e.toString());
		}

		return parseo;
	}

	public boolean parseo_set_beneficiarios(String str_json) {

		boolean parseo = true; //

		try {

			JSONObject j_data = new JSONObject(str_json);
			parseo = true;
			try {
				j_data.getJSONObject("error");
				parseo = false;
				JSONObject msgerror = j_data.getJSONObject("message");
				data_servicio.msj_error = "";
				Iterator<?> keys = msgerror.keys();
				String msj_error = "";
				while (keys.hasNext()) {

					String key = (String) keys.next();
					log_aplicacion.log_informacion("info error" + key);

					JSONArray jarray = msgerror.getJSONArray(key);
					for (int i = 0; i < jarray.length(); i++) {
						msj_error = msj_error + "\n" + jarray.getString(i);
					}
				}
				data_servicio.msj_error = msj_error;
				data_servicio.msj_error = msj_error.replaceAll("[", "");
				data_servicio.msj_error = data_servicio.msj_error.replaceAll("]", "");

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				data_servicio.usuario.setActive(j_data.getString("active"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				data_servicio.usuario.setUser_id(j_data.getString("iduser"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}
			parseo = true;
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		return parseo;
	}

	public boolean parseo_set_role(String str_json) {

		boolean parseo = true; //

		try {

			JSONObject j_data = new JSONObject(str_json);
			parseo = true;
			try {
				j_data.getJSONObject("error");
				parseo = false;
				JSONObject msgerror = j_data.getJSONObject("message");
				data_servicio.msj_error = "";
				Iterator<?> keys = msgerror.keys();
				String msj_error = "";
				while (keys.hasNext()) {

					String key = (String) keys.next();
					log_aplicacion.log_informacion("info error" + key);

					JSONArray jarray = msgerror.getJSONArray(key);
					for (int i = 0; i < jarray.length(); i++) {
						msj_error = msj_error + "\n" + jarray.getString(i);
					}
				}
				data_servicio.msj_error = msj_error;
				data_servicio.msj_error = msj_error.replaceAll("[", "");
				data_servicio.msj_error = data_servicio.msj_error.replaceAll("]", "");

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				data_servicio.usuario.setActive(j_data.getString("active"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

			try {
				data_servicio.usuario.setUser_id(j_data.getString("iduser"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}
			parseo = true;
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		return parseo;
	}

	public boolean parseo_login(String str_json) {

		boolean parseo = false; //

		try {

			JSONObject j_data = new JSONObject(str_json);
			try {

				Usuario usuario = new Usuario();

				try {
					usuario.setImage(j_data.getString("image"));

				} catch (Exception e) {
					// TODO: handle exception
				}

				try {
					usuario.setActive(j_data.getString("active"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					usuario.setSpace(j_data.getString("space"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					usuario.setUser_id(j_data.getString("iduser"));
				} catch (Exception e) {
					// TODO: handle exception
				}

				try {
					usuario.setIdentification(j_data.getString("identification"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				try {
					usuario.setRole_id(j_data.getJSONArray("userRoles").getJSONObject(0).getJSONObject("role")
							.getString("idrole"));
				} catch (Exception e) {
					// TODO: handle exception

					usuario.setRole_id("1");
				}
				try {
					usuario.setCountry_id(j_data.getString("country_id"));
				} catch (Exception e) {
					// TODO: handle exception
				}

				usuario.setPhone(j_data.getString("phone"));

				try {
					usuario.setNotification(j_data.getString("notification"));
				} catch (Exception e) {
					// TODO: handle exception
				}

				usuario.setName(j_data.getString("name"));
				usuario.setEmail(j_data.getString("email"));

				usuario.setName_last(j_data.getString("name_last"));

				try {
					usuario.setEmail_optional(j_data.getString("email_optional"));

				} catch (Exception e) {
					// TODO: handle exception
				}

				data_servicio.usuario = usuario;

				parseo = true;

			} catch (Exception e) { //
				log_aplicacion.log_error("parseo: " + e.toString());
			}
			if (parseo == false) {

				try {
					if (j_data.getString("status").equals("ok")) {
						parseo = true;

					} else {
						JSONObject msgerror = j_data.getJSONObject("message");

						data_servicio.msj_error = msgerror.toString();
						parseo = false;
						Iterator<?> keys = msgerror.keys();
						String msj_error = "";
						while (keys.hasNext()) {

							String key = (String) keys.next();
							log_aplicacion.log_informacion("info error" + key);

							JSONArray jarray = msgerror.getJSONArray(key);
							for (int i = 0; i < jarray.length(); i++) {
								msj_error = msj_error + "\n" + jarray.getString(i);
							}
						}
						data_servicio.msj_error = msj_error;
						data_servicio.msj_error = msj_error.replaceAll("[", "");
						data_servicio.msj_error = data_servicio.msj_error.replaceAll("]", "");
						parseo = false;

					}

				} catch (Exception e) {
					log_aplicacion.log_error("parseo: " + e.toString());
				}
			}
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

	public boolean parseo_update(String str_json) {

		boolean parseo = false; //

		try {

			JSONObject j_data = new JSONObject(str_json);

			parseo = true;
			try {
				j_data.getJSONObject("error");
				parseo = false;

				data_servicio.msj_error = j_data.getString("msg");

			} catch (Exception e) {
				log_aplicacion.log_error("parseo4: " + e.toString());
			}

			try {
				data_servicio.usuario.setActive(j_data.getString("active"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo3: " + e.toString());
			}

			try {
				data_servicio.usuario.setUser_id(j_data.getString("iduser"));

			} catch (Exception e) {
				log_aplicacion.log_error("parseo2: " + e.toString());
			}
			parseo = true;
		} catch (Exception e) { //
			log_aplicacion.log_error("parseo1: " + e.toString());
		}

		return parseo;
	}

	public boolean parseo_get_notificaciones(String str_json) {

		boolean parseo = false; //
		data_servicio.Notificaciones.clear();
		try {

			JSONArray j_array = new JSONArray(str_json);

			parseo = true;
			for (int i = 0; i < j_array.length(); i++) {

				Notificacion notificacion = new Notificacion(j_array.getJSONObject(i).getString("idnotification"),
						j_array.getJSONObject(i).getString("type_notification_id"),
						j_array.getJSONObject(i).getString("user_id"),
						j_array.getJSONObject(i).getString("user_from_id"),
						j_array.getJSONObject(i).getString("notification"),
						j_array.getJSONObject(i).getString("date_created"),
						j_array.getJSONObject(i).getString("deleted"), j_array.getJSONObject(i).getString("reported"),
						j_array.getJSONObject(i).getString("guest"),
						j_array.getJSONObject(i).getJSONObject("userFrom").getString("name"),
						j_array.getJSONObject(i).getJSONObject("userFrom").getString("image"),
						j_array.getJSONObject(i).getString("view"));
				notificacion.setOther_id(j_array.getJSONObject(i).getString("other_id"));
				data_servicio.Notificaciones.add(notificacion);

			}

		} catch (Exception e) { //
			log_aplicacion.log_error("parseo1: " + e.toString());
		}

		return parseo;
	}

	public boolean parseo_changePassword(String str_json) {

		data_servicio.msj_error = "";
		boolean parseo = false; //

		try {

			JSONObject j_data = new JSONObject(str_json);

			try {
				if (j_data.getString("status").equals("ok")) {
					parseo = true;
					String msgerror = j_data.getString("message");

					data_servicio.msj_error = msgerror.toString();

				} else {
					JSONObject msgerror = j_data.getJSONObject("message");

					data_servicio.msj_error = msgerror.toString();
					parseo = false;
					Iterator<?> keys = msgerror.keys();
					String msj_error = "";
					while (keys.hasNext()) {

						String key = (String) keys.next();
						log_aplicacion.log_informacion("info error" + key);

						JSONArray jarray = msgerror.getJSONArray(key);
						for (int i = 0; i < jarray.length(); i++) {
							msj_error = msj_error + "\n" + jarray.getString(i);
						}
					}
					data_servicio.msj_error = msj_error;
					data_servicio.msj_error = msj_error.replaceAll("[", "");
					data_servicio.msj_error = data_servicio.msj_error.replaceAll("]", "");
					parseo = false;

				}

			} catch (Exception e) {
				log_aplicacion.log_error("parseo: " + e.toString());
			}

		} catch (Exception e) { //
			log_aplicacion.log_error("parseo: " + e.toString());
		}

		//
		return parseo;
	}

}
