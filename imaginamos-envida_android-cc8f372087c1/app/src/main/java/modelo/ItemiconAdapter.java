package modelo;

import java.util.ArrayList;

import com.zonesoftware.fluie.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemiconAdapter extends BaseAdapter {
	protected Activity activity;
	public ArrayList<String> items;

	final String BENEFICIARIOS = "beneficiarios";
	final String VERIFICADORES = "verificadores";
	ItemiconAdapter adapter;
	static Typeface tr;

	static class ViewHolder {
		public ImageView icon;

		public TextView titulo;

	}

	public ItemiconAdapter(Activity activity, ArrayList<String> items) {
		this.activity = activity;
		this.items = (ArrayList<String>) items.clone();
		adapter = this;
		tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");

	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		ViewHolder viewholder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			vi = inflater.inflate(R.layout.item_lista_icon, null);
			viewholder = new ViewHolder();
			viewholder.icon = (ImageView) vi.findViewById(R.id.img_item);
			viewholder.titulo = (TextView) vi.findViewById(R.id.textnombre);

			viewholder.titulo.setTypeface(tr);

			// viewholder.fecha_actual = (TextView) vi
			// .findViewById(R.id.textfecha_actual);
			//
			// viewholder.fecha_solicitud = (TextView) vi
			// .findViewById(R.id.textfecha_solicitud);

			vi.setTag(viewholder);
		}
		viewholder = (ViewHolder) vi.getTag();
		String titulo = items.get(position).toString();
		viewholder.titulo.setText(titulo);

		View view_linea = (View) vi.findViewById(R.id.view_linea);
		view_linea.setVisibility(View.VISIBLE);
		if (position == 0) {
			viewholder.icon.setImageResource(R.drawable.ic_beneficiario);
		}
		if (position == 1) {
			view_linea.setVisibility(View.INVISIBLE);

			viewholder.icon.setImageResource(R.drawable.ic_verificador);
		}

		return vi;
	}

}
