package modelo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.zonesoftware.fluie.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import servicio.data_servicio;

public class ItemplanAdapter extends BaseAdapter implements Filterable {
	protected Activity activity;

	boolean icon;
	JSONArray array;
	Typeface tm, tr;

	static class ViewHolder {

		public TextView titulo;
		public TextView text_tamano;
		public TextView precio;

	}

	public ItemplanAdapter(Activity activity) {
		this.activity = activity;
		tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Medium.ttf");
		tm = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");
		try {
			array = new JSONArray(data_servicio.json_planes);
		} catch (Exception e) {
			// TODO: handle exception
		}
		this.icon = icon;

	}

	@Override
	public int getCount() {
		return array.length();
	}

	@Override
	public Object getItem(int position) {
		try {
			return array.getJSONObject(position);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		ViewHolder viewholder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			vi = inflater.inflate(R.layout.item_lista2, null);
			viewholder = new ViewHolder();

			viewholder.text_tamano = (TextView) vi.findViewById(R.id.text_tamano);

			viewholder.titulo = (TextView) vi.findViewById(R.id.textnombre);

			viewholder.precio = (TextView) vi.findViewById(R.id.text_precio);
			// viewholder.fecha_actual = (TextView) vi
			// .findViewById(R.id.textfecha_actual);
			//
			// viewholder.fecha_solicitud = (TextView) vi
			// .findViewById(R.id.textfecha_solicitud);

			vi.setTag(viewholder);
		}

		TextView text_tamano = (TextView) vi.findViewById(R.id.text_tamano);

		JSONObject jobj;
		try {
			jobj = array.getJSONObject(position);

			viewholder = (ViewHolder) vi.getTag();

			BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

			Typeface tr;

			viewholder.text_tamano.setText(jobj.getString("space_name"));

			viewholder.titulo.setText(jobj.getString("name"));

			viewholder.precio.setText(jobj.getString("price"));

			viewholder.text_tamano.setTypeface(tm);

			viewholder.titulo.setTypeface(tm);
			viewholder.precio.setTypeface(tm);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vi;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return null;
	}

}
