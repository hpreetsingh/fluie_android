package modelo;

public class Home {

String idhome="";
String language_id="";
String title="";
String logo="";
String alt_logo="";
String description="";


String video="";
String description_user="";
String description_beneficiary="";
String description_checker="";
String address="";
String phone="";
String email="";
String terms_conditions="";
String facebook="";
String twitter="";
String google="";
String about_image="";
String alt_about_image="";
String about="";
String how_does_it_work_image="";
String alt_how_image="";
String how_does_it_work="";

public Home(){
	
}

public String getIdhome() {
	return idhome;
}
public void setIdhome(String idhome) {
	this.idhome = idhome;
}
public String getLanguage_id() {
	return language_id;
}
public void setLanguage_id(String language_id) {
	this.language_id = language_id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getLogo() {
	return logo;
}
public void setLogo(String logo) {
	this.logo = logo;
}
public String getAlt_logo() {
	return alt_logo;
}
public void setAlt_logo(String alt_logo) {
	this.alt_logo = alt_logo;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getVideo() {
	return video;
}
public void setVideo(String video) {
	this.video = video;
}
public String getDescription_user() {
	return description_user;
}
public void setDescription_user(String description_user) {
	this.description_user = description_user;
}
public String getDescription_beneficiary() {
	return description_beneficiary;
}
public void setDescription_beneficiary(String description_beneficiary) {
	this.description_beneficiary = description_beneficiary;
}
public String getDescription_checker() {
	return description_checker;
}
public void setDescription_checker(String description_checker) {
	this.description_checker = description_checker;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getTerms_conditions() {
	return terms_conditions;
}
public void setTerms_conditions(String terms_conditions) {
	this.terms_conditions = terms_conditions;
}
public String getFacebook() {
	return facebook;
}
public void setFacebook(String facebook) {
	this.facebook = facebook;
}
public String getTwitter() {
	return twitter;
}
public void setTwitter(String twitter) {
	this.twitter = twitter;
}
public String getGoogle() {
	return google;
}
public void setGoogle(String google) {
	this.google = google;
}
public String getAbout_image() {
	return about_image;
}
public void setAbout_image(String about_image) {
	this.about_image = about_image;
}
public String getAlt_about_image() {
	return alt_about_image;
}
public void setAlt_about_image(String alt_about_image) {
	this.alt_about_image = alt_about_image;
}
public String getAbout() {
	return about;
}
public void setAbout(String about) {
	this.about = about;
}
public String getHow_does_it_work_image() {
	return how_does_it_work_image;
}
public void setHow_does_it_work_image(String how_does_it_work_image) {
	this.how_does_it_work_image = how_does_it_work_image;
}
public String getAlt_how_image() {
	return alt_how_image;
}
public void setAlt_how_image(String alt_how_image) {
	this.alt_how_image = alt_how_image;
}
public String getHow_does_it_work() {
	return how_does_it_work;
}
public void setHow_does_it_work(String how_does_it_work) {
	this.how_does_it_work = how_does_it_work;
}
	

	
	
	
	
	
	
	
}
