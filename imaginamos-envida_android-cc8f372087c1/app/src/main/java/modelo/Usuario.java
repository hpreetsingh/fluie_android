package modelo;

public class Usuario {

	String user_id = "";
	String role_id = "";
	String json_usurio = "";
	String active = "";
	String email = "";
	String name = "";
	String name_last = "";
	String identification = "";
	String country_id = "";
	String phone = "";
	String address = "";
	String image = "";
	String image_local = "";
	String email_optional = "";
	String space = "";
	String notification = "";
	String date_updated = "";
	String movil = "";
	String relacion = "";
	String estado = "";
	String genero = "";
	String id_beneficiario_user = "";
	String tipo_documento = "";
	/*
	 * "iduser": "3" "email": "" "name": null "name_last": null
	 * "identification": null "country_id": "1" "phone": null "address": null
	 * "image": null "email_optional": null "active": "1" "space": "20480"
	 * "notification": "1" "date_created": "2015-10-22 12:25:56" "date_updated":
	 * null
	 */

	public Usuario(String user_id, String role_id, String json_usurio) {
		super();
		this.user_id = user_id;
		this.role_id = role_id;
		this.json_usurio = json_usurio;
	}

	public Usuario(String user_id) {
		super();
		this.user_id = user_id;
	}

	public Usuario() {

	}

	public Usuario(String user_id, String role_id, String json_usurio, String active, String email, String name,
			String name_last, String identification, String country_id, String phone, String address, String image,
			String email_optional, String space, String notification, String date_updated, String movil,
			String relacion) {
		super();
		this.user_id = user_id;
		this.role_id = role_id;
		this.json_usurio = json_usurio;
		this.active = active;
		this.email = email;
		this.name = name;
		this.name_last = name_last;
		this.identification = identification;
		this.country_id = country_id;
		this.phone = phone;
		this.address = address;
		this.image = image;
		this.email_optional = email_optional;
		this.space = space;
		this.notification = notification;
		this.date_updated = date_updated;
		this.movil = movil;
		this.relacion = relacion;
	}

	public Usuario(String user_id, String role_id, String json_usurio, String active, String email, String name,
			String name_last, String identification, String country_id, String phone, String address, String image,
			String email_optional, String space, String notification, String date_updated, String movil,
			String relacion, String estado) {
		super();
		this.user_id = user_id;
		this.role_id = role_id;
		this.json_usurio = json_usurio;
		this.active = active;
		this.email = email;
		this.name = name;
		this.name_last = name_last;
		this.identification = identification;
		this.country_id = country_id;
		this.phone = phone;
		this.address = address;
		this.image = image;
		this.email_optional = email_optional;
		this.space = space;
		this.notification = notification;
		this.date_updated = date_updated;
		this.movil = movil;
		this.relacion = relacion;
		this.estado = estado;
	}

	public Usuario(String user_id, String role_id, String json_usurio, String active, String email, String name,
			String name_last, String identification, String country_id, String phone, String address, String image,
			String email_optional, String space, String notification, String date_updated, String movil,
			String relacion, String estado, String genero) {
		super();
		this.user_id = user_id;
		this.role_id = role_id;
		this.json_usurio = json_usurio;
		this.active = active;
		this.email = email;
		this.name = name;
		this.name_last = name_last;
		this.identification = identification;
		this.country_id = country_id;
		this.phone = phone;
		this.address = address;
		this.image = image;
		this.email_optional = email_optional;
		this.space = space;
		this.notification = notification;
		this.date_updated = date_updated;
		this.movil = movil;
		this.relacion = relacion;
		this.estado = estado;
		this.genero = genero;
	}

	public Usuario clone() {

		return new Usuario(user_id, role_id, json_usurio, active, email, name, name_last, identification, country_id,
				phone, address, image, email_optional, space, notification, date_updated, movil, relacion, estado,
				genero, id_beneficiario_user);
	}

	public void clear() {
		user_id = "";
		role_id = "";
		json_usurio = "";
		active = "";
		email = "";
		name = "";
		name_last = "";
		identification = "";
		country_id = "";
		phone = "";
		address = "";
		image = "";
		email_optional = "";
		space = "";
		notification = "";
		date_updated = "";
		movil = "";
		relacion = "";
		estado = "";
		genero = "";

	}

	public Usuario(String user_id, String role_id, String json_usurio, String active, String email, String name,
			String name_last, String identification, String country_id, String phone, String address, String image,
			String email_optional, String space, String notification, String date_updated, String movil,
			String relacion, String estado, String genero, String id_beneficiario_user) {
		super();
		this.user_id = user_id;
		this.role_id = role_id;
		this.json_usurio = json_usurio;
		this.active = active;
		this.email = email;
		this.name = name;
		this.name_last = name_last;
		this.identification = identification;
		this.country_id = country_id;
		this.phone = phone;
		this.address = address;
		this.image = image;
		this.email_optional = email_optional;
		this.space = space;
		this.notification = notification;
		this.date_updated = date_updated;
		this.movil = movil;
		this.relacion = relacion;
		this.estado = estado;
		this.genero = genero;
		this.id_beneficiario_user = id_beneficiario_user;
	}

	public String getImage_local() {
		return image_local;
	}

	public void setImage_local(String image_local) {
		this.image_local = image_local;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getId_beneficiario_user() {
		return id_beneficiario_user;
	}

	public void setId_beneficiario_user(String id_beneficiario_user) {
		this.id_beneficiario_user = id_beneficiario_user;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public String getJson_usurio() {
		return json_usurio;
	}

	public String getTipo_documento() {
		return tipo_documento;
	}

	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

	public void setJson_usurio(String json_usurio) {
		this.json_usurio = json_usurio;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName_last() {
		return name_last;
	}

	public void setName_last(String name_last) {
		this.name_last = name_last;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getCountry_id() {
		return country_id;
	}

	public void setCountry_id(String country_id) {
		this.country_id = country_id;
	}

	public String getRelacion() {
		return relacion;
	}

	public void setRelacion(String relacion) {
		this.relacion = relacion;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getEmail_optional() {
		return email_optional;
	}

	public void setEmail_optional(String email_optional) {
		this.email_optional = email_optional;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public String getDate_updated() {
		return date_updated;
	}

	public void setDate_updated(String date_updated) {
		this.date_updated = date_updated;
	}

}
