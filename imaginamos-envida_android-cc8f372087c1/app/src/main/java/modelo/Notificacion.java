package modelo;

public class Notificacion {
	String idnotification = "";
	String type_notification_id = "";
	String user_id = "";
	String user_from_id = "";

	String notification = "";
	String date_created = "";
	String view = "";
	String deleted = "";
	String reported = "";
	String guest = "";
	String Nombre = "";
	String imagen = "";
	String other_id = "";

	public Notificacion(String user_from_id, String notification, String date_created, String nombre, String imagen) {
		super();
		this.user_from_id = user_from_id;
		this.notification = notification;
		this.date_created = date_created;
		Nombre = nombre;
		this.imagen = imagen;
	}

	public Notificacion(String idnotification, String type_notification_id, String user_id, String user_from_id,
			String notification, String date_created, String deleted, String reported, String guest, String nombre,
			String imagen, String view) {
		super();
		this.idnotification = idnotification;
		this.type_notification_id = type_notification_id;
		this.user_id = user_id;
		this.user_from_id = user_from_id;
		this.notification = notification;
		this.date_created = date_created;
		this.deleted = deleted;
		this.reported = reported;
		this.guest = guest;
		Nombre = nombre;
		this.imagen = imagen;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

	public String getOther_id() {
		return other_id;
	}

	public void setOther_id(String other_id) {
		this.other_id = other_id;
	}

	public String getDate_created() {
		return date_created;
	}

	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}

	public String getIdnotification() {
		return idnotification;
	}

	public void setIdnotification(String idnotification) {
		this.idnotification = idnotification;
	}

	public String getType_notification_id() {
		return type_notification_id;
	}

	public void setType_notification_id(String type_notification_id) {
		this.type_notification_id = type_notification_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_from_id() {
		return user_from_id;
	}

	public void setUser_from_id(String user_from_id) {
		this.user_from_id = user_from_id;
	}

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public String getReported() {
		return reported;
	}

	public void setReported(String reported) {
		this.reported = reported;
	}

	public String getGuest() {
		return guest;
	}

	public void setGuest(String guest) {
		this.guest = guest;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

}
