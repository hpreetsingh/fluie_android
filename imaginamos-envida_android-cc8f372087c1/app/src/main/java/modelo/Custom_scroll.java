package modelo;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

public class Custom_scroll extends HorizontalScrollView {
	private boolean scrollable = true;

	public void setScrollingEnabled(boolean scrollable) {
		this.scrollable = scrollable;
	}

	public boolean isScrollable() {
		return scrollable;
	}

	public Custom_scroll(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public Custom_scroll(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public Custom_scroll(Context context) {
		super(context);
		init(context);
	}

	void init(Context context) {

	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {

		// if we can scroll pass the event to the superclass
		if (scrollable) {
			return super.onTouchEvent(ev);
		}

		return false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (scrollable) {
			return super.onInterceptTouchEvent(ev);
		}
		return false;
	}
}