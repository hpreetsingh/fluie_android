package modelo;

public class Item_lista {
	String url_imagen = "";
	String titulo = "";
	String subtitulo = "";
	String estado = "";
	String id = "";
	String tipo = "msg";

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	String genero = "";

	public Item_lista(String url_imagen, String titulo, String subtitulo, String estado, String id,String genero) {
		super();
		this.url_imagen = url_imagen;
		this.titulo = titulo;
		this.subtitulo = subtitulo;
		this.estado = estado;
		this.id = id;
		this.genero=genero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl_imagen() {
		return url_imagen;
	}

	public void setUrl_imagen(String url_imagen) {
		this.url_imagen = url_imagen;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getSubtitulo() {
		return subtitulo;
	}

	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
