package modelo;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.zonesoftware.fluie.EditarPersonaActivity;
import com.zonesoftware.fluie.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class ItemImagenAdapter extends BaseAdapter implements Filterable {
	protected Activity activity;
	public ArrayList<Item_lista> items;
	private ArrayList<Item_lista> originalData = null;
	private ArrayList<Item_lista> filteredData = null;

	private ItemFilter mFilter = new ItemFilter();
	boolean icon;
	final String BENEFICIARIOS = "beneficiarios";
	final String VERIFICADORES = "verificadores";
	ItemImagenAdapter adapter;
	Typeface tm, tr;

	static class ViewHolder {
		public ImageView icon;

		public TextView titulo;
		public TextView subtitulo;
		public TextView estado;

	}

	public ItemImagenAdapter(Activity activity, ArrayList<Item_lista> items, boolean icon) {
		this.activity = activity;
		this.items = items;
		this.icon = icon;
		originalData = (ArrayList<Item_lista>) items.clone();
		adapter = this;
		tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();

	}

	public void setitems(ArrayList<Item_lista> list) {
		data_servicio.lista_seleccion_ben = (ArrayList<Item_lista>) list.clone();
		originalData = (ArrayList<Item_lista>) list.clone();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		ViewHolder viewholder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			vi = inflater.inflate(R.layout.item_lista, null);
			viewholder = new ViewHolder();
			viewholder.icon = (ImageView) vi.findViewById(R.id.img_item);

			viewholder.subtitulo = (TextView) vi.findViewById(R.id.textsubtitulo);

			viewholder.titulo = (TextView) vi.findViewById(R.id.textnombre);

			viewholder.estado = (TextView) vi.findViewById(R.id.textestado);
			viewholder.subtitulo.setTypeface(tr);
			viewholder.titulo.setTypeface(tr);
			viewholder.estado.setTypeface(tr);

			// viewholder.fecha_actual = (TextView) vi
			// .findViewById(R.id.textfecha_actual);
			//
			// viewholder.fecha_solicitud = (TextView) vi
			// .findViewById(R.id.textfecha_solicitud);

			vi.setTag(viewholder);
		}

		TextView editar = (TextView) vi.findViewById(R.id.texteditar);
		editar.setTypeface(tr);
		if (icon) {

			click c = new click(position);

			editar.setOnClickListener(c);
		} else {
			editar.setText(R.string.agregar);
			editar.setTextColor(activity.getResources().getColor(R.color.violeta_login));
			editar.setCompoundDrawables(null, null, null, null);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT);
			params.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
			params.weight = (float) 0.4;
			params.rightMargin = 25;
			// LayoutParams layout_params = new LayoutParams(Gravity.RIGHT);
			//
			editar.setLayoutParams(params);
			editar.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
			// editar.setVisibility(View.INVISIBLE);
		}
		Item_lista item = items.get(position);

		viewholder = (ViewHolder) vi.getTag();

		Typeface tr;

		viewholder.subtitulo.setText(item.getSubtitulo());

		if (data_servicio.lista.equals("verificadores")
				&& item.getSubtitulo().equals(activity.getResources().getString(R.string.confirmado))) {

			viewholder.subtitulo.setTextColor(activity.getResources().getColor(R.color.naraja_boton));
		} else if (data_servicio.lista.equals("verificadores")
				&& item.getSubtitulo().equals(activity.getResources().getString(R.string.sin_confirmar))) {

			viewholder.subtitulo.setTextColor(activity.getResources().getColor(R.color.violeta_login));

		}

		viewholder.titulo.setText(item.getTitulo());
		viewholder.estado.setText(item.getEstado());

		Imagen image_usuario = null;

		for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
			if (data_servicio.imagen_usuarios.get(l).getId_user().equals(item.getId())) {

				image_usuario = data_servicio.imagen_usuarios.get(l);
				break;
			}
		}
//		if (image_usuario != null) {
//
//			if ((image_usuario.getImagen() == null) || image_usuario.isActualizar()) {
//				guardarImagen(image_usuario);
//
//				// URL url;
//				// try {
//				// url = new URL(image_usuario.getUrl());
//				//
//				// Bitmap bmp;
//				// try {
//				// bmp =
//				// BitmapFactory.decodeStream(url.openConnection().getInputStream());
//				// viewholder.icon.setImageBitmap(bmp);
//				// image_usuario.setImagen(bmp);
//				//
//				// } catch (IOException e) {
//				// // TODO Auto-generated catch block
//				// e.printStackTrace();
//				// }
//				//
//				// } catch (MalformedURLException e) {
//				// // TODO Auto-generated catch block
//				// e.printStackTrace();
//				// }
//
//			} else if (image_usuario.getImagen() != null) {
//				viewholder.icon.setImageBitmap(image_usuario.getImagen());
//
//
//
//			} else {
//				viewholder.icon.setImageResource(R.drawable.doge);
//
//			}
//
//		} else {
			if(item.getGenero().equals("1")) {
				viewholder.icon.setImageResource(R.drawable.doge);
			}else {
				viewholder.icon.setImageResource(R.drawable.femaledogo);
			}

//		}
		return vi;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub

		return mFilter;

	}

	class click implements View.OnClickListener {

		int seleccion = 0;

		public click(int seleccion) {
			this.seleccion = seleccion;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			data_servicio.seleccion_beneficiario = items.get(seleccion).getId();

			data_servicio.opcion = "editar";

			Imagen image_usuario = null;

			for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
				if (data_servicio.imagen_usuarios.get(l).getId_user().equals(items.get(seleccion).getId())) {

					image_usuario = data_servicio.imagen_usuarios.get(l);
					image_usuario.setActualizar(true);
					break;
				}
			}

			Intent intent = new Intent(activity, EditarPersonaActivity.class);

			activity.startActivityForResult(intent, 200);
		}

	}

	private void guardarImagen(final Imagen image_usuario) {
		// File dirImages = cw.getDir("perfil", Context.MODE_PRIVATE);
		new AsyncTask<Void, Void, String>() {

			int height;
			String tiempo = "";
			Bitmap bitmap = null;

			@Override
			protected String doInBackground(Void... params) {

				String msg = "";
				URL url;
				try {
					url = new URL(image_usuario.getUrl());

					try {
						bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

						image_usuario.setImagen(bitmap);

						image_usuario.setActualizar(false);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						image_usuario.setActualizar(false);

					}

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					image_usuario.setActualizar(false);
				}
				return msg;
			}

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();

			}

			@Override
			protected void onProgressUpdate(Void... values) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void onPostExecute(String msg) {
				log_aplicacion.log_informacion("onpostexcecute");
				try {
					Imagen image = null;
					for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
						if (data_servicio.imagen_usuarios.get(l).getId_user().equals(image_usuario.getId_user())) {
							image = data_servicio.imagen_usuarios.get(l);
							break;
						}
					}
					image.setActualizar(false);
					if (bitmap != null) {
						image.setImagen(bitmap);
						adapter.notifyDataSetChanged();
					}

				} catch (Exception e) {
					// TODO: handle exception
					log_aplicacion.log_informacion("onpostexcecute error " + e.toString());

				}
			}

		}.execute(null, null, null);

	}

	private class ItemFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {

			String filterString = constraint.toString().toLowerCase();

			FilterResults results = new FilterResults();

			final ArrayList<Item_lista> list = (ArrayList<Item_lista>) originalData.clone();

			int count = list.size();
			final ArrayList<Item_lista> nlist = new ArrayList<Item_lista>(count);

			if (filterString.equals("")) {

				results.values = list;
				results.count = list.size();
			} else {

				String filterableString = "";

				for (int i = 0; i < count; i++) {
					filterableString = list.get(i).getTitulo();
					System.out.println("titulo: " + filterableString);
					System.out.println("titulo: " + filterString);

					if (filterableString.toLowerCase().contains(filterString)) {
						nlist.add(list.get(i));
						System.out.println("ingresa: " + list.get(i).getTitulo());

					}
				}

				results.values = nlist;
				results.count = nlist.size();
			}
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			items = (ArrayList<Item_lista>) results.values;
			data_servicio.lista_seleccion_ben = (ArrayList<Item_lista>) results.values;
			notifyDataSetChanged();
		}

	}

}
