package modelo;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONException;

import com.zonesoftware.fluie.DialogoCarga;
import com.zonesoftware.fluie.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import servicio.WS_cliente;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class ItemNotificacionesAdapter extends BaseAdapter {

	protected Activity activity;
	public ArrayList<Notificacion> items;
	boolean icon;
	DialogoCarga cargar;
	ItemNotificacionesAdapter adapter;
	static Typeface tr;

	static class ViewHolder {
		public LinearLayout layout;
		public ImageView icon;

		public TextView nombre;
		public TextView notificacion;
		public TextView fecha;
		public ImageView eliminar;
		public Button btn_cancelar;

	}

	public ItemNotificacionesAdapter(Activity activity, ArrayList<Notificacion> items) {
		this.activity = activity;
		this.items = items;
		this.icon = icon;
		this.items = (ArrayList<Notificacion>) items.clone();
		adapter = this;
		cargar = new DialogoCarga(activity, "Consultando", "Espere un momento...");
		tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");

	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		ViewHolder viewholder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			vi = inflater.inflate(R.layout.item_lista_notificaciones, null);
			viewholder = new ViewHolder();
			viewholder.icon = (ImageView) vi.findViewById(R.id.img_item);

			viewholder.nombre = (TextView) vi.findViewById(R.id.textnombre);
			viewholder.nombre.setTypeface(tr);

			viewholder.notificacion = (TextView) vi.findViewById(R.id.textnotificacion);
			viewholder.notificacion.setTypeface(tr);
			viewholder.fecha = (TextView) vi.findViewById(R.id.textfecha);
			viewholder.fecha.setTypeface(tr);
			viewholder.eliminar = (ImageView) vi.findViewById(R.id.image_eliminar);

			viewholder.btn_cancelar = (Button) vi.findViewById(R.id.button_cancelar);
			viewholder.btn_cancelar.setTypeface(tr);
			viewholder.layout = (LinearLayout) vi.findViewById(R.id.layout_notificacion);

			vi.setTag(viewholder);
		}

		Notificacion item = items.get(position);

		viewholder = (ViewHolder) vi.getTag();

		Typeface tr;
		LinearLayout layout = (LinearLayout) vi.findViewById(R.id.layout_notificacion);
		viewholder.nombre.setText(item.getNombre());

		viewholder.notificacion.setText(item.getNotification());
		viewholder.fecha.setText(item.getDate_created());

		Imagen image_usuario = null;

		if (item.getType_notification_id().equals("6")) {
			viewholder.btn_cancelar.setVisibility(View.VISIBLE);
			Click click_cancelar = new Click(item.getIdnotification(), "cancelar", item, position);
			viewholder.btn_cancelar.setOnClickListener(click_cancelar);

		} else {

			layout.removeView(viewholder.btn_cancelar);

		}

		if (item.getView().equals("1")) {

			viewholder.layout.setBackgroundColor(activity.getResources().getColor(R.color.white));

		} else if (item.getView().equals("0")) {

			viewholder.layout.setBackgroundColor(activity.getResources().getColor(R.color.gris_notificacion));

		}

		Click click_eliminar = new Click(item.getIdnotification(), "eliminar", item, position);

		viewholder.eliminar.setOnClickListener(click_eliminar);

		for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
			if (data_servicio.imagen_usuarios.get(l).getId_user().equals(item.getUser_from_id())) {

				image_usuario = data_servicio.imagen_usuarios.get(l);
				break;
			}
		}

		if (image_usuario != null) {

			if (image_usuario.getImagen() == null || image_usuario.isActualizar()) {

				guardarImagen(image_usuario);
			} else {
				viewholder.icon.setImageBitmap(image_usuario.getImagen());

			}

		} else {
			viewholder.icon.setImageResource(R.drawable.doge);

		}
		return vi;
	}

	class Click implements View.OnClickListener {

		String seleccion = "";
		String tipo = "";
		Notificacion notificacion;
		int position = 0;

		public Click(String seleccion, String tipo, Notificacion notificacion, int position) {

			this.seleccion = seleccion;
			this.tipo = tipo;
			this.notificacion = notificacion;
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stu
			if (tipo.equals("eliminar")) {
				eliminar(notificacion, position);

			} else if (tipo.equals("cancelar")) {

				cancelar(notificacion, position);
			}

		}

	}

	public void eliminar(Notificacion notificacion, final int position) {
		WS_cliente servicio;
		try {
			servicio = new WS_cliente(activity);
			cargar.mostrar();
			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						cargar.ocultar();
						items.remove(position);
						data_servicio.Notificaciones.remove(position);
						adapter.notifyDataSetChanged();
					} else if (msg.what == 0) {
						cargar.ocultar();
					}

				}
			};
			servicio.setPuente_envio(puente);
			servicio.eliminarnotificaciones(notificacion.getIdnotification());
			servicio.execute();

		} catch (UnsupportedEncodingException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void cancelar(Notificacion notificacion, final int position) {
		WS_cliente servicio;
		try {
			servicio = new WS_cliente(activity);
			cargar.mostrar();
			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						cargar.ocultar();
						items.remove(position);

						adapter.notifyDataSetChanged();
					} else if (msg.what == 0) {
						cargar.ocultar();
					}

				}
			};
			servicio.setPuente_envio(puente);
			servicio.cancelarnotificaciones(notificacion.getOther_id());
			servicio.execute();

		} catch (UnsupportedEncodingException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void guardarImagen(final Imagen image_usuario) {
		// File dirImages = cw.getDir("perfil", Context.MODE_PRIVATE);
		new AsyncTask<Void, Void, String>() {

			int height;
			String tiempo = "";
			Bitmap bitmap = null;

			@Override
			protected String doInBackground(Void... params) {

				String msg = "";
				URL url;
				try {
					url = new URL(image_usuario.getUrl());

					try {
						bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

						image_usuario.setImagen(bitmap);
						image_usuario.setActualizar(false);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return msg;
			}

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();

			}

			@Override
			protected void onProgressUpdate(Void... values) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void onPostExecute(String msg) {
				log_aplicacion.log_informacion("onpostexcecute");
				try {

					if (bitmap != null) {
						Imagen image = null;

						for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
							if (data_servicio.imagen_usuarios.get(l).getId_user().equals(image_usuario.getId_user())) {

								image = data_servicio.imagen_usuarios.get(l);
								break;
							}
						}
						image.setImagen(bitmap);
						image.setActualizar(false);
						adapter.notifyDataSetChanged();

					}
				} catch (Exception e) {
					// TODO: handle exception
					log_aplicacion.log_informacion("onpostexcecute error " + e.toString());

				}
			}

		}.execute(null, null, null);

	}

}
