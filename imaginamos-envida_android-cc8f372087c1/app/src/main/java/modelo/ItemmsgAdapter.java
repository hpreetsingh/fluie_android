package modelo;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.zonesoftware.fluie.Descarga_Archivo;
import com.zonesoftware.fluie.Dialogo;
import com.zonesoftware.fluie.DialogoCarga;
import com.zonesoftware.fluie.HistorialMensajesActivity;
import com.zonesoftware.fluie.MensajeActivity;
import com.zonesoftware.fluie.R;
import com.zonesoftware.fluie.db.Db_manager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import servicio.WS_cliente;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class ItemmsgAdapter extends BaseAdapter implements Filterable {
    protected Activity activity;
    public ArrayList<Item_lista> items;
    private ArrayList<Item_lista> originalData = null;
    private ArrayList<Item_lista> filteredData = null;
    DialogoCarga cargar;
    private ItemFilter mFilter = new ItemFilter();
    boolean icon;
    Typeface tm, tr;
    AlertDialog alertDialog;
    int descargas = 0;
    Dialogo dialogo_modal;
    String PATH = "";
    HistorialMensajesActivity activida_historial;

    static class ViewHolder {
        public TextView titulo;
        public TextView subtitulo;
        public TextView estado;
    }

    int consultas = 0;
    ItemmsgAdapter adapter;
    Db_manager db;

    public ItemmsgAdapter(Activity activity, ArrayList<Item_lista> items, boolean icon,
                          HistorialMensajesActivity activida_historia) {
        this.activity = activity;
        this.items = items;
        this.icon = icon;
        this.activida_historial = activida_historia;
        PATH = Environment.getExternalStorageDirectory() + "/Fluie/video";
        db = new Db_manager(activity.getApplicationContext());
        originalData = (ArrayList<Item_lista>) items.clone();
        dialogo_modal = new Dialogo(activity, activity);
        cargar = new DialogoCarga(activity, activity.getString(R.string.consultando),
                activity.getString(R.string.espere_un_momento_));
        adapter = this;
        tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void notifyDataSetChanged() {
        // TODO Auto-generated method stub
        super.notifyDataSetChanged();

    }

    public void setitems(ArrayList<Item_lista> list) {
        data_servicio.lista_seleccion_ben = (ArrayList<Item_lista>) list.clone();
        originalData = (ArrayList<Item_lista>) list.clone();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        Item_lista item = items.get(position);
        if (item.getTipo().equals("msg")) {
            ViewHolder viewholder;
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.item_lista_msg, null);
            viewholder = new ViewHolder();
            viewholder.subtitulo = (TextView) vi.findViewById(R.id.textsubtitulo);

            viewholder.titulo = (TextView) vi.findViewById(R.id.textnombre);

            viewholder.estado = (TextView) vi.findViewById(R.id.textestado);

            viewholder.subtitulo.setTypeface(tr);
            viewholder.titulo.setTypeface(tr);
            viewholder.estado.setTypeface(tr);

            TextView editar = (TextView) vi.findViewById(R.id.texteditar);

            editar.setTypeface(tr);

            click c = new click("editar", position);

            editar.setOnClickListener(c);

            TextView eliminar = (TextView) vi.findViewById(R.id.texteliminar);
            eliminar.setTypeface(tr);
            click c2 = new click("eliminar", position);
            eliminar.setOnClickListener(c2);

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

            viewholder.subtitulo.setText(item.getSubtitulo());

            if (item.getTitulo().length() > 40) {
                String titulo = item.getTitulo().substring(0, 40) + "...";

                if (item.getTitulo().contains("\n")) {
                    titulo = titulo.replaceAll("\n", " ");
                    viewholder.titulo.setText(titulo);

                } else {

                    viewholder.titulo.setText(item.getTitulo());
                }

            } else {
                if (item.getTitulo().contains("\n")) {
                    String titulo = item.getTitulo().replaceAll("\n", " ");
                    viewholder.titulo.setText(titulo);

                } else {

                    viewholder.titulo.setText(item.getTitulo());
                }
            }
            String tamano = item.getEstado();
            try {
                if (tamano.contains(".")) {
                    tamano = tamano.substring(0, tamano.indexOf(".") + 3);

                }
            } catch (Exception e) {
                // TODO: handle exception
            }
            if (tamano.contains("MB")) {

                viewholder.estado.setText(tamano);
            } else {
                viewholder.estado.setText(tamano + " MB");
            }
        } else {

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.item_lista_consejos, null);

            TextView titulo = (TextView) vi.findViewById(R.id.text1_lista_historial);
            titulo.setText(activity.getString(R.string.texto_historial) + " " + item.titulo);
            titulo.setTypeface(tr);
            TextView info2 = (TextView) vi.findViewById(R.id.text3_lista_historial);
            info2.setTypeface(tr);
            String tamano = item.getEstado();

            titulo.setText(activity.getString(R.string.texto_historial) + " " + tamano);

        }
        return vi;
    }

    @Override
    public Filter getFilter() {
        // TODO Auto-generated method stub
        return mFilter;
    }

    class click implements View.OnClickListener {

        int seleccion = 0;
        String tipo_click = "";

        public click(String tipo_click, int seleccion) {
            this.seleccion = seleccion;
            this.tipo_click = tipo_click;

        }

        @Override
        public void onClick(View v) {

            if (data_servicio.tipo_historial.equals("mensajes")) {
                if (tipo_click.equals("editar")) {
                    data_servicio.opcion_mensaje = "editar";
                    data_servicio.mensaje_edicion = "mensaje";

                    cargar_mensaje(items.get(seleccion));

                } else if (tipo_click.equals("eliminar")) {

                    dialogo_eliminar(activity.getString(R.string.informacion),
                            activity.getString(R.string.esta_seguro_de_eliminar_el_mensage),
                            activity.getString(R.string.no), activity.getString(R.string.si), false, true, seleccion);
                }

            } else {
                if (tipo_click.equals("editar")) {
                    data_servicio.opcion_mensaje = "editar";
                    data_servicio.mensaje_edicion = "borrador";
                    data_servicio.borradoredicion = db.getborrador(items.get(seleccion).getId());

                    Intent intent = new Intent(activity, MensajeActivity.class);
                    activity.startActivity(intent);

                } else if (tipo_click.equals("eliminar")) {

                    dialogo_eliminar(activity.getString(R.string.informacion), "Esta seguro de eliminar el borrador",
                            activity.getString(R.string.no), activity.getString(R.string.si), false, true, seleccion);

                }

            }

        }

    }

    public void eliminarborradores(int position) {

        db.deleteborrador(items.get(position).getId());
        items.remove(position);
        this.notifyDataSetChanged();
        alertDialog.cancel();
    }

    public void eliminarmesaje(final int posicion) {

        WS_cliente servicio;

        try {
            servicio = new WS_cliente(activity);
            cargar.mostrar();
            Handler puente = new Handler() {
                @SuppressLint("ShowToast")
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == 1) {

                        cargar.ocultar();
                        items.remove(posicion);
                        alertDialog.cancel();

                        activida_historial.getlist();

                    } else if (msg.what == 0) {
                        cargar.ocultar();
                        alertDialog.cancel();
                        activida_historial.getlist();
                    }

                }
            };
            servicio.Delete_mensaje(items.get(posicion).id);
            servicio.setPuente_envio(puente);
            servicio.execute();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void parseo_msg(final String id) {

        JSONArray jsonmensajes;
        try {
            jsonmensajes = new JSONArray(data_servicio.json_historial);

            for (int j = 0; j < jsonmensajes.length(); j++) {
                if (id.equals(jsonmensajes.getJSONObject(j).getString("idmessage"))) {
                    Mensaje msj = new Mensaje();

                    try {
                        msj.setIdmessage(jsonmensajes.getJSONObject(j).getString("idmessage"));
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    try {
                        msj.setType_event_id(jsonmensajes.getJSONObject(j).getString("type_event_id"));
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    try {
                        msj.setType_file_id(jsonmensajes.getJSONObject(j).getString("type_file_id"));
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    try {
                        msj.setType_file_id(jsonmensajes.getJSONObject(j).getString("type_file_id"));
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                    try {
                        msj.setTitle(jsonmensajes.getJSONObject(j).getString("title"));
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    try {
                        msj.setDate_view(jsonmensajes.getJSONObject(j).getString("date_view"));
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                    try {
                        msj.setDescription(jsonmensajes.getJSONObject(j).getString("description"));
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                    JSONArray jsonfiles = new JSONArray(jsonmensajes.getJSONObject(j).getString("messageFiles"));

                    ArrayList<Usuario> beneficiarios = new ArrayList<Usuario>();

                    JSONArray jsonbeneficiarios = new JSONArray(
                            jsonmensajes.getJSONObject(j).getString("messageBeneficiaries"));
                    for (int i = 0; i < jsonbeneficiarios.length(); i++) {

                        Usuario us = new Usuario();
                        us.setName(jsonbeneficiarios.getJSONObject(j).getString("beneficiary_id"));

                        us.setUser_id(jsonbeneficiarios.getJSONObject(j).getString("beneficiary_id"));

                        beneficiarios.add(us);

                    }
                    data_servicio.mensajeedicion = msj;
                    data_servicio.lista_beneficiario_msg = (ArrayList<Usuario>) beneficiarios.clone();

                    break;
                }
            }

            Intent intent = new Intent(activity, MensajeActivity.class);
            activity.startActivity(intent);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void cargar_archivo(final String id) {

		/*
         * {"idmessagefile":"117","message_id":"130","type_file_id":"2",
		 * "file":"11_02_2016_04_57_26_171.ogg","size":"1", "url":
		 * "http:\/\/mentesweb.com\/fluie\/programacion\/loadContent\/loadUrl\/MTE3\/46925793",
		 * "date_created":"2016-02-11 16:57:26"}
		 */
        consultas = 0;

        data_servicio.imagenes.clear();
        if (data_servicio.mensajeedicion.getType_file_id().equals("1")
                || data_servicio.mensajeedicion.getType_file_id().equals("2")) {

            cargar.ocultar();
            Intent intent = new Intent(activity, MensajeActivity.class);
            activity.startActivity(intent);

        } else {
            data_servicio.imagenes.clear();
            try {
                final JSONArray files = new JSONArray(data_servicio.json_file);
                if (files.length() == 0) {
                    cargar.ocultar();
                    Intent intent = new Intent(activity, MensajeActivity.class);
                    activity.startActivity(intent);

                } else {

                    for (int i = 0; i < files.length(); i++) {

                        files.getJSONObject(i).getString("url");
                        Handler puentedescarga = new Handler() {
                            @SuppressLint("ShowToast")
                            @Override
                            public void handleMessage(Message msg) {
                                if (msg.what == 1) {

                                    consultas++;

                                    log_aplicacion.log_informacion("consultas exitosas " + consultas);
                                    if (consultas == descargas) {
                                        log_aplicacion.log_informacion("inicia activity mensajes");
                                        Intent intent = new Intent(activity, MensajeActivity.class);
                                        activity.startActivity(intent);
                                        cargar.ocultar();
                                    }

                                } else if (msg.what == 0) {
                                    consultas++;
                                    if (consultas == files.length()) {

                                        cargar.ocultar();
                                    }

                                }

                            }
                        };

                        File dirImages = new File(Environment.getExternalStorageDirectory() + "/Fluie/"
                                + files.getJSONObject(i).getString("file"));

                        Descarga_Archivo descarga = new Descarga_Archivo(files.getJSONObject(i).getString("url"),
                                puentedescarga, files.getJSONObject(i).getString("file"));
                        String archivo = Environment.getExternalStorageDirectory() + "/Fluie/"
                                + files.getJSONObject(i).getString("file");

                        if (!dirImages.exists()) {
                            descarga.execute();
                            descargas++;
                        }

                        if (data_servicio.mensajeedicion.getType_file_id().equals("4")) {
                            if (dirImages.exists()) {
                                data_servicio.imagenes.add(new Imagen(archivo));
                            } else {

                                data_servicio.imagenes.add(new Imagen(descarga.getUrllocal()));
                            }
                            /*
							 * "idmessagefile":"117","message_id":"130",
							 * "type_file_id":"2",
							 * "file":"11_02_2016_04_57_26_171.ogg","size":"1",
							 * "url":
							 * "http:\/\/mentesweb.com\/fluie\/programacion\/loadContent\/loadUrl\/MTE3\/46925793",
							 * "date_created":"2016-02-11 16:57:26"
							 */
                        }
                        if (data_servicio.mensajeedicion.getType_file_id().equals("3")) {

                            data_servicio.archivo_texto = archivo;
                        }
                        if (data_servicio.mensajeedicion.getType_file_id().equals("2")) {

                            data_servicio.archivo_voz = archivo;
                        }

                        if (i == files.length() - 1 && (descargas == 0)) {
                            cargar.ocultar();

                            Intent intent = new Intent(activity, MensajeActivity.class);
                            activity.startActivity(intent);

                        }

                    }

                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    public void cargar_mensaje(final Item_lista item) {
        WS_cliente servicio;

        try {
            servicio = new WS_cliente(activity);
            cargar.mostrar();
            Handler puente = new Handler() {
                @SuppressLint("ShowToast")
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == 1) {
                        data_servicio.mensajeedicion.setSize(item.estado);
                        cargar_archivo(item.getId());
                    } else if (msg.what == 0) {
                        cargar.ocultar();
                        log_aplicacion.log_informacion("error obteniendo mensaje");
                    }

                }
            };
            servicio.get_mensaje(item.getId());
            servicio.setPuente_envio(puente);
            servicio.execute();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final ArrayList<Item_lista> list = (ArrayList<Item_lista>) originalData.clone();
            int count = list.size();
            final ArrayList<Item_lista> nlist = new ArrayList<Item_lista>(count);
            if (filterString.equals("")) {

                results.values = list;
                results.count = list.size();
            } else {

                String filterableString = "";

                for (int i = 0; i < count; i++) {
                    filterableString = list.get(i).getTitulo() + " " + list.get(i).getSubtitulo() + " "
                            + list.get(i).getEstado();
                    System.out.println("titulo: " + filterableString);
                    System.out.println("titulo: " + filterString);

                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(list.get(i));
                        System.out.println("ingresa: " + list.get(i).getTitulo());
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList<Item_lista>) results.values;
            data_servicio.lista_seleccion_ben = (ArrayList<Item_lista>) results.values;
            notifyDataSetChanged();
        }

    }

    public void dialogo_eliminar(String titulo, String mensaje, String boton1, String boton2,
                                 final boolean boton1estado, final boolean boton2estado, final int posicion) {
        tm = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Medium.ttf");
        tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");

        LayoutInflater li = LayoutInflater.from(activity);
        final View prompt = li.inflate(R.layout.mensaje, null);
        TextView txtitulo = (TextView) prompt.findViewById(R.id.textmsg_titulo);

        txtitulo.setText(titulo);
        TextView txtmsg = (TextView) prompt.findViewById(R.id.textmsg);
        txtmsg.setText(mensaje);

        Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setView(prompt);
        alertDialogBuilder.setCancelable(false);
        TextView txtb1 = (TextView) prompt.findViewById(R.id.textbtn1);
        txtb1.setText(boton1);
        TextView txtb2 = (TextView) prompt.findViewById(R.id.textbtn2);

        txtb2.setText(boton2);
        txtb1.setTypeface(tm);
        txtb2.setTypeface(tm);
        txtitulo.setTypeface(tm);
        txtmsg.setTypeface(tm);

        ListView lista_opciones = (ListView) prompt.findViewById(R.id.listopciones);

        LinearLayout contenedor = (LinearLayout) prompt.findViewById(R.id.layout_msg);

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        // android.R.layout.simple_list_item_1,
        // android.R.id.text1, options);

        // lista_opciones.setAdapter(adapter);

        // lista_opciones.setOnItemClickListener(new
        // AdapterView.OnItemClickListener() {
        //
        // @Override
        // public void onItemClick(AdapterView<?> parent, View view, int
        // position, long id) {
        // // TODO Auto-generated method stub
        // if (options[position].equals("Tomar foto")) {
        // } else if (options[position].equals("Elegir de galeria")) {
        // }
        // }
        //
        // });

        alertDialog = alertDialogBuilder.create();

        alertDialog.show();

        if (!boton1.equals("")) {

            txtb1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton1estado) {

                    } else {
                        alertDialog.cancel();
                    }

                }
            });

        }

        if (!boton2.equals("")) {

            txtb2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton2estado) {

                        if (data_servicio.tipo_historial.equals("mensajes")) {

                            eliminarmesaje(posicion);
                        } else {

                            eliminarborradores(posicion);
                        }

                    } else {
                        alertDialog.cancel();
                    }

                }
            });
        }

        // Creamos un AlertDialog y lo mostramos

    }

}
