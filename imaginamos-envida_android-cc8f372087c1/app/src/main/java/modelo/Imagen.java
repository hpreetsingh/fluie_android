package modelo;

import android.graphics.Bitmap;

public class Imagen {

	String id_user = "";
	String url = "";
	Bitmap imagen = null;
	boolean actualizar = false;

	public Imagen(String url) {
		super();
		this.url = url;
	}

	public Imagen(String url, Bitmap imagen) {
		super();
		this.url = url;
		this.imagen = imagen;
	}

	public Imagen(String id_user, String url, Bitmap imagen) {
		super();
		this.id_user = id_user;
		this.url = url;
		this.imagen = imagen;
	}

	public Imagen(String id_user, String url) {
		super();
		this.id_user = id_user;
		this.url = url;
	}

	public boolean isActualizar() {
		return actualizar;
	}

	public void setActualizar(boolean actualizar) {
		this.actualizar = actualizar;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Bitmap getImagen() {
		return imagen;
	}

	public void setImagen(Bitmap imagen) {
		this.imagen = imagen;
	}

	public String getId_user() {
		return id_user;
	}

	public void setId_user(String id_user) {
		this.id_user = id_user;
	}

	public Imagen(String id_user, String url, Bitmap imagen, boolean actualizar) {
		super();
		this.id_user = id_user;
		this.url = url;
		this.imagen = imagen;
		this.actualizar = actualizar;
	}

	public Imagen clone() {

		return new Imagen(id_user, url, imagen, actualizar);
	}

}
