package modelo;

import com.zonesoftware.fluie.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;

import java.io.InputStream;
import java.io.OutputStream;

public class Utils {

	public static void setBadgeCount(Context context, LayerDrawable icon, int count) {

		BadgeDrawable badge;

		// Reuse drawable if possible
		Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
//		Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
//// Scale it to 50 x 50
//		Drawable reuse = new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, 20, 20, true));
		if (reuse != null && reuse instanceof BadgeDrawable) {
			badge = (BadgeDrawable) reuse;
		} else {
			badge = new BadgeDrawable(context);
		}

		badge.setCount(count);
		icon.mutate();
		icon.setDrawableByLayerId(R.id.ic_badge, badge);
	}
}
