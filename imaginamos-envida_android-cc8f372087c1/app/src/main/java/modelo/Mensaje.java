package modelo;

import java.util.ArrayList;

public class Mensaje {

	boolean estado = false;
	String mensaje = "";

	String idmessage = "";
	String type_event_id = "";
	String type_file_id = "";
	String user_id = "";
	String title = "";
	String date_view = "";
	String description = "";
	String size = "";
	ArrayList<String> imagenes = new ArrayList<String>();

	public Mensaje(boolean estado, String mensaje) {

		this.estado = estado;
		this.mensaje = mensaje;
	}

	public Mensaje() {

	}

	public Mensaje(String idmessage, String type_event_id, String type_file_id, String user_id, String title,
			String date_view, String description) {
		super();
		this.idmessage = idmessage;
		this.type_event_id = type_event_id;
		this.type_file_id = type_file_id;
		this.user_id = user_id;
		this.title = title;
		this.date_view = date_view;
		this.description = description;
	}

	public Mensaje(boolean estado, String mensaje, String idmessage, String type_event_id, String type_file_id,
			String user_id, String title, String date_view, String description, ArrayList<String> imagenes) {
		super();
		this.estado = estado;
		this.mensaje = mensaje;
		this.idmessage = idmessage;
		this.type_event_id = type_event_id;
		this.type_file_id = type_file_id;
		this.user_id = user_id;
		this.title = title;
		this.date_view = date_view;
		this.description = description;
		this.imagenes = imagenes;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public ArrayList<String> getImagenes() {
		return imagenes;
	}

	public void setImagenes(ArrayList<String> imagenes) {
		this.imagenes = imagenes;
	}

	public String getIdmessage() {
		return idmessage;
	}

	public void setIdmessage(String idmessage) {
		this.idmessage = idmessage;
	}

	public String getType_event_id() {
		return type_event_id;
	}

	public void setType_event_id(String type_event_id) {
		this.type_event_id = type_event_id;
	}

	public String getType_file_id() {
		return type_file_id;
	}

	public void setType_file_id(String type_file_id) {
		this.type_file_id = type_file_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate_view() {
		return date_view;
	}

	public void setDate_view(String date_view) {
		this.date_view = date_view;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
