package com.zonesoftware.fluie;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import servicio.data_servicio;

public class AlmacenamientoActivity extends ActionBarActivity {
	private ViewPager mPager;
	ActionBar mActionbar;
	int posicion_tab = 0;
	View v_borrador, v_mensajes;
	Typeface tm, tr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_historial);
		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}
		mActionbar = getSupportActionBar();

		mActionbar.setDisplayShowTitleEnabled(true);
		mActionbar.setTitle("");
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.setIcon(R.drawable.ic_atras);
		mActionbar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_atras));
		mActionbar.setDisplayUseLogoEnabled(false);

		/** Getting a reference to ViewPager from the layout */
		mPager = (ViewPager) findViewById(R.id.pager);

		/** Getting a reference to FragmentManager */
		FragmentManager fm = getSupportFragmentManager();

		/** Defining a listener for pageChange */
		ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				super.onPageSelected(position);
				posicion_tab = position;

				if (position == 0) {
					v_borrador.setVisibility(View.INVISIBLE);
					v_mensajes.setVisibility(View.VISIBLE);
					mPager.setCurrentItem(0);
				} else if (position == 1) {
					v_borrador.setVisibility(View.VISIBLE);
					v_mensajes.setVisibility(View.INVISIBLE);
					mPager.setCurrentItem(1);
				}

			}
		};

		/** Setting the pageChange listener to the viewPager */
		mPager.setOnPageChangeListener(pageChangeListener);

		/** Creating an instance of FragmentPagerAdapter */
		MyFragmentPagerAdapter fragmentPagerAdapter = new MyFragmentPagerAdapter(fm);

		/** Setting the FragmentPagerAdapter object to the viewPager object */
		mPager.setAdapter(fragmentPagerAdapter);

		mActionbar.setDisplayShowTitleEnabled(true);

		v_borrador = (View) findViewById(R.id.view_borrador);

		v_mensajes = (View) findViewById(R.id.view_mensajes);
		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		TextView t_borrador = (TextView) findViewById(R.id.text_tab_borrador);
		TextView t_mensaje = (TextView) findViewById(R.id.text_tab_mensajes);

		t_borrador.setTypeface(tr);
		t_mensaje.setTypeface(tr);
		LinearLayout ll_mensajes = (LinearLayout) findViewById(R.id.ll_tab_mensajes);
		LinearLayout ll_borradores = (LinearLayout) findViewById(R.id.ll_tab_borrador);

		ll_mensajes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				v_borrador.setVisibility(View.INVISIBLE);
				v_mensajes.setVisibility(View.VISIBLE);
				mPager.setCurrentItem(0);
			}
		});
		ll_borradores.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				v_borrador.setVisibility(View.VISIBLE);
				v_mensajes.setVisibility(View.INVISIBLE);
				mPager.setCurrentItem(1);

			}
		});

		// /** Defining tab listener */
		// ActionBar.TabListener tabListener = new ActionBar.TabListener() {
		//
		// @Override
		// public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		//
		// }
		//
		// @Override
		// public void onTabSelected(Tab tab, FragmentTransaction ft) {
		//
		// posicion_tab = tab.getPosition();
		// mPager.setCurrentItem(tab.getPosition());
		//
		// }
		//
		// @Override
		// public void onTabReselected(Tab tab, FragmentTransaction ft) {
		//
		// }
		// };
		//
		// /** Creating fragment1 Tab */
		// Tab tab =
		// mActionbar.newTab().setText(getResources().getString(R.string.mis_mensajes))
		// .setTabListener(tabListener);
		//
		// mActionbar.addTab(tab, 0, true);
		//
		// /** Creating fragment2 Tab */
		// tab =
		// mActionbar.newTab().setText(getResources().getString(R.string.borradores)).setTabListener(tabListener);
		//
		// mActionbar.addTab(tab, 1, false);
		//
		// tab = mActionbar.newTab().setText("").setTabListener(tabListener);
		//
		// mActionbar.addTab(tab, 2, false);

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		GuardarInformacionApp.Guardar_info(outState);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in action bar clicked; goto parent activity.
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

		final int PAGE_COUNT = 2;

		/** Constructor of the class */
		public MyFragmentPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		/** This method will be invoked when a page is requested to create */
		@Override
		public Fragment getItem(int arg0) {
			Bundle data = new Bundle();
			switch (arg0) {

			/** tab1 is selected */
			case 0:
				data_servicio.tipo_historial = "mensajes";
				MismensajesFragment fragment1 = new MismensajesFragment();

				return fragment1;

			/** tab2 is selected */
			case 1:
				data_servicio.tipo_historial = "borradores";
				Borradoresfragment fragment2 = new Borradoresfragment();

				return fragment2;

			}

			return null;
		}

		/** Returns the number of pages */
		@Override
		public int getCount() {
			return PAGE_COUNT;
		}
	}
}
