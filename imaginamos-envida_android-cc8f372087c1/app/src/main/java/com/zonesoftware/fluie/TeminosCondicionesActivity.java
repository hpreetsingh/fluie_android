package com.zonesoftware.fluie;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class TeminosCondicionesActivity extends ActionBarActivity {
	WebView webview = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_teminos_condiciones);
		webview = (WebView) this.findViewById(R.id.webView_terminos);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
		log_aplicacion.log_informacion(data_servicio.home.getTerms_conditions());

		// webview.loadDataWithBaseURL("",
		// data_servicio.home.getTerms_conditions(), "text/html", "UTF-8",

		String str = "<html><style>body{ font-size:12px;}</style><body>" + data_servicio.terms_conditions
				+ "</body></html>";

		log_aplicacion.log_informacion("html " + str);

		webview.loadDataWithBaseURL(null, str, "text/html", "utf-8", null);

		Button btn_atras = (Button) findViewById(R.id.button_atras);

		btn_atras.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

	}

	
}
