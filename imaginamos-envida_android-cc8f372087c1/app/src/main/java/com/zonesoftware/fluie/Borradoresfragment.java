package com.zonesoftware.fluie;

import java.util.ArrayList;

import com.zonesoftware.fluie.db.Borrador;
import com.zonesoftware.fluie.db.Db_manager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import servicio.consultar_servicios;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class Borradoresfragment extends Fragment {
	View vi;
	consultar_servicios consul;
	Db_manager db;
	Double tamanio_video = 0.0;
	Double tamanio_voz = 0.0;
	Double tamanio_imagen = 0.0;
	Double tamanio_text = 0.0;

	ArrayList<Borrador> borradores_texto;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		vi = inflater.inflate(R.layout.fragment_borrador, null);
		init();
		return vi;

	}

	public void init() {
		tamanio_video = 0.0;
		tamanio_voz = 0.0;
		tamanio_imagen = 0.0;
		tamanio_text = 0.0;

		data_servicio.tipo_historial = "borradores";
		TextView textespacioborrador = (TextView) vi.findViewById(R.id.textespacioborrador);

		LinearLayout lay_audio, lay_video, lay_texto, layout_imagen;

		lay_audio = (LinearLayout) vi.findViewById(R.id.layout_audio);

		lay_texto = (LinearLayout) vi.findViewById(R.id.layout_texto);

		lay_video = (LinearLayout) vi.findViewById(R.id.layout_video);

		layout_imagen = (LinearLayout) vi.findViewById(R.id.layout_imagen);

		TextView audio = (TextView) vi.findViewById(R.id.text_audio_tamano);
		TextView video = (TextView) vi.findViewById(R.id.text_video_tamano);
		TextView imagen = (TextView) vi.findViewById(R.id.text_imagen_tamano);
		TextView texto = (TextView) vi.findViewById(R.id.text_texto_tamano);

		try {
			db = new Db_manager(getActivity());
			ArrayList<Borrador> borradores = db.getborradores("1");

			for (int j = 0; j < borradores.size(); j++) {

				tamanio_video += Double.valueOf(borradores.get(j).getTamano());

			}

			borradores = db.getborradores("2");

			for (int j = 0; j < borradores.size(); j++) {

				tamanio_voz += Double.valueOf(borradores.get(j).getTamano());

			}
			borradores = db.getborradores("3");
			borradores_texto = (ArrayList<Borrador>) borradores.clone();
			for (int j = 0; j < borradores.size(); j++) {

				tamanio_text += Double.valueOf(borradores.get(j).getTamano());

			}
			borradores = db.getborradores("4");

			for (int j = 0; j < borradores.size(); j++) {

				tamanio_imagen += Double.valueOf(borradores.get(j).getTamano());

			}

			String tvoz = String.valueOf(tamanio_voz);

			if (tvoz.contains(".") && tvoz.length() > 3) {
				tvoz = tvoz.substring(0, tvoz.indexOf(".") + 3);

			}

			String tvideo = String.valueOf(tamanio_video);
			if (tvideo.contains(".") && tvideo.length() > 3) {
				tvideo = tvideo.substring(0, tvideo.indexOf(".") + 3);

			}

			String timagen = String.valueOf(tamanio_imagen);
			if (timagen.contains(".") && timagen.length() > 3) {
				timagen = timagen.substring(0, timagen.indexOf(".") + 3);

			}

			String ttext = String.valueOf(tamanio_text);

			if (ttext.contains(".") && ttext.length() > 3) {
				ttext = ttext.substring(0, ttext.indexOf(".") + 3);

			}

			audio.setText(tvoz + "MB");
			video.setText(tvideo + "MB");
			imagen.setText(timagen + "MB");
			texto.setText(ttext + "MB");

			Double espacio_total = tamanio_video + tamanio_voz + tamanio_text + tamanio_imagen;
			String tespacio_total = String.valueOf(espacio_total);

			if (tespacio_total.contains(".") && tespacio_total.length() > 3) {
				tespacio_total = tespacio_total.substring(0, tespacio_total.indexOf(".") + 3);

			}

			textespacioborrador.setText(tespacio_total + "MB");
		} catch (Exception e) {
			// TODO: handle exception
			log_aplicacion.log_informacion("error " + e.toString());
		}
		layout_imagen.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (tamanio_imagen != 0.0) {
					data_servicio.tipo_msg = "4";

					Intent intent = new Intent(vi.getContext(), HistorialMensajesActivity.class);
					startActivity(intent);
				}
			}
		});

		lay_audio.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (tamanio_voz != 0.0) {
					data_servicio.tipo_msg = "2";
					Intent intent = new Intent(vi.getContext(), HistorialMensajesActivity.class);
					startActivity(intent);
				}
			}
		});
		lay_video.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (tamanio_video != 0.0) {
					data_servicio.tipo_msg = "1";
					Intent intent = new Intent(vi.getContext(), HistorialMensajesActivity.class);
					startActivity(intent);
				}
			}
		});
		lay_texto.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (tamanio_text != 0.0 || borradores_texto.size() > 0) {
					data_servicio.tipo_msg = "3";

					Intent intent = new Intent(vi.getContext(), HistorialMensajesActivity.class);
					startActivity(intent);
				}
			}
		});
		consul = new consultar_servicios(vi.getContext(), getActivity());
		Button buton = (Button) vi.findViewById(R.id.bn_seleccion_lista);

		buton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				consul.obtener_planes();
			}
		});

	}

	@Override
	public void onStart() { // TODO Auto-generated method stub
		super.onStart();
		init();
	}

}