package com.zonesoftware.fluie;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import modelo.Home;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class BienvenidaActivity extends YouTubeFailureRecoveryActivity {
	Home infohome = null;
	Typeface tm, tr;
	public static final String DEVELOPER_KEY = "AIzaSyAvnnFyYjVuLYTwGh0kpRd6FZ2Q9cA3qm0";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_bienvenida);
		tm = Typeface.createFromAsset(getAssets(), "font/Roboto_Medium.ttf");
		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		YouTubePlayerView youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
		youTubeView.initialize(DEVELOPER_KEY, this);
		infohome = data_servicio.home;
		TextView txtBienvenida = (TextView) findViewById(R.id.text_bienvenida);
		TextView txtBienvenida_titulo = (TextView) findViewById(R.id.text_bienvenida_titulo);
		txtBienvenida_titulo.setTypeface(tm);
		txtBienvenida_titulo.setText(infohome.getTitle());

		txtBienvenida.setTypeface(tr);
		txtBienvenida.setText(infohome.getDescription());
		Button bt_empezar = (Button) findViewById(R.id.boton_empezar);
		bt_empezar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startactivity();
			}
		});

	}

	protected void startactivity() {
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
	}

	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
		if (!wasRestored) {
			String video = data_servicio.home.getVideo();
			// https://www.youtube.com/watch?v=
			video = video.substring(video.indexOf("=") + 1);
			log_aplicacion.log_informacion(video);
			player.cueVideo(video);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		GuardarInformacionApp.Guardar_info(outState);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	@Override
	protected YouTubePlayer.Provider getYouTubePlayerProvider() {
		return (YouTubePlayerView) findViewById(R.id.youtube_view);
	}

}
