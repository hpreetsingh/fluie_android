package com.zonesoftware.fluie;

import org.json.JSONArray;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import servicio.consultar_servicios;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class MismensajesFragment extends Fragment {

	View vi;
	consultar_servicios consul;
	ProgressBar ps;
	static Typeface tr;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		vi = inflater.inflate(R.layout.mismensajes, null);
		init();
		return vi;

	}

	public void init() {
		LinearLayout lay_audio, lay_video, lay_texto, layout_imagen;

		lay_audio = (LinearLayout) vi.findViewById(R.id.layout_audio);

		lay_texto = (LinearLayout) vi.findViewById(R.id.layout_texto);

		lay_video = (LinearLayout) vi.findViewById(R.id.layout_video);

		layout_imagen = (LinearLayout) vi.findViewById(R.id.layout_imagen);
		data_servicio.tipo_historial = "mensajes";
		ps = (ProgressBar) vi.findViewById(R.id.progressBaralmacenamiento);
		ps.setAnimation(null);
		tr = Typeface.createFromAsset(getActivity().getAssets(), "font/Roboto_Regular.ttf");

		layout_imagen.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				data_servicio.tipo_msg = "4";
				data_servicio.tipo_historial = "mensajes";
				Intent intent = new Intent(vi.getContext(), HistorialMensajesActivity.class);
				startActivity(intent);
			}
		});

		lay_audio.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				data_servicio.tipo_msg = "2";
				data_servicio.tipo_historial = "mensajes";
				Intent intent = new Intent(vi.getContext(), HistorialMensajesActivity.class);
				startActivity(intent);
			}
		});
		lay_video.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				data_servicio.tipo_msg = "1";
				data_servicio.tipo_historial = "mensajes";
				Intent intent = new Intent(vi.getContext(), HistorialMensajesActivity.class);
				startActivity(intent);
			}
		});
		lay_texto.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				data_servicio.tipo_msg = "3";
				data_servicio.tipo_historial = "mensajes";
				Intent intent = new Intent(vi.getContext(), HistorialMensajesActivity.class);
				startActivity(intent);

			}
		});
		consul = new consultar_servicios(vi.getContext(), getActivity());
		Button buton = (Button) vi.findViewById(R.id.bn_seleccion_lista);

		buton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				consul.obtener_planes();
			}
		});

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		data_servicio.tipo_historial = "mensajes";
		try {

			ps = (ProgressBar) vi.findViewById(R.id.progressBaralmacenamiento);

			TextView porcentaje = (TextView) vi.findViewById(R.id.textporcentaja);
			ps.setProgress(100);

			ps.setIndeterminate(false);
			ps.getIndeterminateDrawable().setColorFilter(0xFF211061, android.graphics.PorterDuff.Mode.MULTIPLY);

			int porcentajeint = 0;
			int espacio = 0;

			TextView audio = (TextView) vi.findViewById(R.id.text_audio_tamano);
			TextView video = (TextView) vi.findViewById(R.id.text_video_tamano);
			TextView imagen = (TextView) vi.findViewById(R.id.text_imagen_tamano);
			TextView texto = (TextView) vi.findViewById(R.id.text_texto_tamano);

			try {

				audio.setText("0 MB ");
				video.setText("0 MB ");
				imagen.setText("0 MB ");
				texto.setText("0 MB ");

				audio.setTypeface(tr);
				video.setTypeface(tr);
				imagen.setTypeface(tr);
				texto.setTypeface(tr);

				TextView tamano_archivos = (TextView) vi.findViewById(R.id.tamano_archivos);
				tamano_archivos.setTypeface(tr);
				JSONArray jo = new JSONArray(data_servicio.json_espacio_msg);
				for (int i = 0; i < jo.length(); i++) {

					String idtypefile = jo.getJSONObject(i).getString("idtypefile");
					String space = jo.getJSONObject(i).getString("size");
					if (idtypefile.equals("1")) {

						video.setText(space + " MB ");
					}

					else if (idtypefile.equals("2")) {
						audio.setText(space + " MB ");

					} else if (idtypefile.equals("3")) {

						texto.setText(space + " MB ");
					} else if (idtypefile.equals("4")) {

						imagen.setText(space + " MB ");
					}

				}

			} catch (Exception e) {
				// TODO: handle exception
			}

			try {

				TextView tamano_archivos = (TextView) vi.findViewById(R.id.tamano_archivos);
				JSONArray jo = new JSONArray(data_servicio.json_freespace);

				String freespace = jo.getJSONObject(0).getString("free");
				String space = jo.getJSONObject(0).getString("space");
				String size = jo.getJSONObject(0).getString("size");

				tamano_archivos.setText((Integer.valueOf(size)) + "MB /" + space + "MB");

				try {
					int sizeint = Integer.parseInt(size);
					int spaceint = Integer.parseInt(space);
					log_aplicacion.log_informacion("espacio  " + sizeint);
					log_aplicacion.log_informacion("espacio  " + spaceint);

					espacio = (int) (((double) sizeint / (double) spaceint) * 100);

				} catch (Exception e) {
					// TODO: handle exception
				}

				porcentaje.setText(espacio + "%");
				porcentaje.setTypeface(tr);
				porcentajeint = espacio;

			} catch (Exception e) {
				// TODO: handle exception
			}

			ps.setProgress(espacio);
			ps.setEnabled(false);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}