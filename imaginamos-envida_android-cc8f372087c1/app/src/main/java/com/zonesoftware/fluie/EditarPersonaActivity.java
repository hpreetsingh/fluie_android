package com.zonesoftware.fluie;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import modelo.Imagen;
import modelo.Usuario;
import servicio.WS_cliente;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class EditarPersonaActivity extends ActionBarActivity {
	LinearLayout contenedor;
	EditText nombre, apellido, correo, telefono;
	TextView eliminar;
	Typeface tm, tr;
	Context micontext;
	DialogoCarga cargar;
	Dialogo dialogo_modal;
	Spinner sp_parentesco = null;
	String relation_id = "";
	JSONArray jarray;
	RadioButton radiobuton_mas, radiobuton_fem;
	Usuario us = null;
	AlertDialog.Builder alertDialogBuilder;
	AlertDialog alertDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editar_verificador);
		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setIcon(R.drawable.ic_x);
		actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_x));
		actionBar.setDisplayUseLogoEnabled(false);
		micontext = this;
		contenedor = (LinearLayout) findViewById(R.id.layout_contenedor);
		nombre = (EditText) findViewById(R.id.editTextnombre);
		apellido = (EditText) findViewById(R.id.editTextapellidos);
		correo = (EditText) findViewById(R.id.editcorreo);
		telefono = (EditText) findViewById(R.id.editTelefono);
		radiobuton_mas = (RadioButton) findViewById(R.id.radio0);
		radiobuton_fem = (RadioButton) findViewById(R.id.radio1);
		eliminar = (TextView) findViewById(R.id.texteliminar);

		/*
		 * [{"idrelationship":"1","relationship":"Familiar"},{"idrelationship":
		 * "2","relationship":"Amigo"},{"idrelationship":"3","relationship":
		 * "otro"},{"idrelationship":"4","relationship":"Hermano "}]
		 * 
		 */

		try {

			sp_parentesco = (Spinner) findViewById(R.id.spinner_parentesco);
			ArrayList<String> lista_parentesco = new ArrayList<String>();
			lista_parentesco.add(getString(R.string.seleccione_el_parentesco));
			try {

				jarray = new JSONArray(data_servicio.json_tipo_relacion);

				for (int i = 0; i < jarray.length(); i++) {
					try {
						lista_parentesco.add(jarray.getJSONObject(i).getString("relationship"));

					} catch (Exception e) {
						// TODO: handle exception
					}

				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner, lista_parentesco);
			dataAdapter.setDropDownViewResource(R.layout.spinner2);
			sp_parentesco.setAdapter(dataAdapter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		nombre.setTypeface(tr);
		apellido.setTypeface(tr);
		correo.setTypeface(tr);

		eliminar.setTypeface(tr);

		dialogo_modal = new Dialogo(micontext, this);
		Button btn = (Button) findViewById(R.id.button_guardar);
		cargar = new DialogoCarga(this, "Consultando", "Espere un momento...");
		if (data_servicio.lista.equals("beneficiarios")) {
			eliminar.setText("ELIMINAR BENEFICIARIO");

			eliminar.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					dialogo(getString(R.string.informacion),
							getString(R.string.esta_seguro_de_eliminar_el_beneficiario_), "",
							getString(R.string.aceptar), true, true);
				}
			});
			contenedor.removeView(telefono);
		} else if (data_servicio.lista.equals("verificadores")) {
			eliminar.setText("ELIMINAR VERIFICADOR");
			contenedor.removeView(sp_parentesco);
			eliminar.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					dialogo(getString(R.string.informacion),
							getString(R.string.esta_seguro_de_eliminar_el_verificador_), "",
							getString(R.string.aceptar), true, true);

				}
			});

		}

		if (data_servicio.opcion.equals("crear")) {

			contenedor.removeView(eliminar);

		} else if (data_servicio.lista.equals("verificadores")) {

		}

		if (data_servicio.opcion.equals("editar")) {

			if (data_servicio.lista.equals("beneficiarios")) {

				for (int i = 0; i < data_servicio.lista_beneficiario.size(); i++) {
					if (data_servicio.lista_beneficiario.get(i).getUser_id()
							.equals(data_servicio.seleccion_beneficiario)) {

						us = data_servicio.lista_beneficiario.get(i);
					}

				}

			} else if (data_servicio.lista.equals("verificadores")) {

				for (int i = 0; i < data_servicio.lista_verificadores.size(); i++) {
					if (data_servicio.lista_verificadores.get(i).getUser_id()
							.equals(data_servicio.seleccion_beneficiario)) {

						us = data_servicio.lista_verificadores.get(i);
					}

				}
				String estado = "";

				log_aplicacion.log_informacion("activo " + us.getActive());

			}
			try {
				nombre.setText(us.getName());
				apellido.setText(us.getName_last());
				correo.setText(us.getEmail());
				telefono.setText(us.getPhone());
				if (us.getGenero().equals("1")) {
					radiobuton_mas.setChecked(true);

				} else if (us.getGenero().equals("2")) {

					radiobuton_fem.setChecked(true);
				}
				if (data_servicio.lista.equals("beneficiarios")) {
					jarray = new JSONArray(data_servicio.json_tipo_relacion);

					for (int i = 0; i < jarray.length(); i++) {
						log_aplicacion.log_informacion("relacion " + us.getRelacion());
						log_aplicacion.log_informacion(
								"relacionarreglo " + jarray.getJSONObject(i).getString("idrelationship"));

						if (jarray.getJSONObject(i).getString("idrelationship").equals(us.getRelacion())) {
							sp_parentesco.setSelection(i + 1);
							break;
						}
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				log_aplicacion.log_informacion("click");
				if (validar()) {
					if (data_servicio.opcion.equals("crear")) {

						log_aplicacion.log_informacion("crear");
						try {

							String genero = "";
							if (radiobuton_mas.isChecked()) {
								genero = "1";
							} else {
								genero = "2";

							}

							if (data_servicio.lista.equals("beneficiarios")) {
								String parentesco = sp_parentesco.getSelectedItem().toString();
								for (int i = 0; i < jarray.length(); i++) {
									if (jarray.getJSONObject(i).getString("relationship").equals(parentesco)) {
										parentesco = jarray.getJSONObject(i).getString("idrelationship");
									}

								}

								registrar_usuario(correo.getText().toString(), nombre.getText().toString(),
										apellido.getText().toString(), parentesco, "", genero);
							} else if (data_servicio.lista.equals("verificadores")) {
								registrar_usuario(correo.getText().toString(), nombre.getText().toString(),
										apellido.getText().toString(), "1", telefono.getText().toString(), genero);
							}

						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else if (data_servicio.opcion.equals("editar")) {
						if (us.getActive().equals("1")) {
							if (data_servicio.lista.equals("beneficiarios")) {
								dialogo_modal.dialogo(getString(R.string.informacion),
										getString(R.string.el_beneficiario) + " " + us.getName() + " "
												+ us.getName_last() + " "
												+ getString(R.string.es_un_usuario_activo_y_no_puede_ser_editado_),
										"", getString(R.string.aceptar), false, false);
							} else {
								dialogo_modal.dialogo(getString(R.string.informacion),
										getString(R.string.el_verificador) + " " + us.getName() + " "
												+ us.getName_last() + " "
												+ getString(R.string.es_un_usuario_activo_y_no_puede_ser_editado_),
										"", getString(R.string.aceptar), false, false);
							}
						} else if (us.getActive().equals("0")) {

							log_aplicacion.log_informacion(data_servicio.id_seleccion);

							try {

								String genero = "";
								if (radiobuton_mas.isChecked()) {

									genero = "1";
								} else {
									genero = "2";

								}

								if (data_servicio.lista.equals("beneficiarios")) {
									String parentesco = sp_parentesco.getSelectedItem().toString();
									for (int i = 0; i < jarray.length(); i++) {
										if (jarray.getJSONObject(i).getString("relationship").equals(parentesco)) {
											parentesco = jarray.getJSONObject(i).getString("idrelationship");
										}

									}
									update_usuario(nombre.getText().toString(), us.getUser_id(),
											correo.getText().toString(), telefono.getText().toString(), parentesco,
											genero);
								} else if (data_servicio.lista.equals("verificadores")) {

									update_usuario(nombre.getText().toString(), us.getUser_id(),
											correo.getText().toString(), telefono.getText().toString(), "1", genero);
								}

							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}

				}

			}
		});

	}

	public void registrar_usuario(String email, String name, String apellido, String relacion, String str_telefono,
			String genero) throws UnsupportedEncodingException, JSONException {

		cargar.mostrar();
		WS_cliente servicio = new WS_cliente(micontext);
		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {
					cargar.ocultar();
					Intent returnIntent = new Intent();
					returnIntent.putExtra("result", "ok");
					setResult(Activity.RESULT_OK, returnIntent);
					if (!data_servicio.msj.equals("")) {
						dialogo_modal.dialogo_cerrar(getString(R.string.informacion), data_servicio.msj, "",
								getString(R.string.aceptar), true, false);
						data_servicio.msj = "";
					} else {
						dialogo_modal.dialogo_cerrar(getString(R.string.informacion),
								getString(R.string.usuario_creado_exitosamente), "", getString(R.string.aceptar), true,
								false);
					}

				} else if (msg.what == 0) {
					cargar.ocultar();

					if (!data_servicio.msj_error.equals("")) {
						dialogo_modal.dialogo(getString(R.string.informacion), data_servicio.msj_error, "",
								getString(R.string.aceptar), true, false);
						data_servicio.msj_error = "";
					} else {
						dialogo_modal.dialogo(getString(R.string.informacion), "El Usuario no se pudo crear", "",
								"Aceptar", true, false);
					}

				}

			}
		};

		if (data_servicio.lista.equals("beneficiarios")) {
			servicio.crear_ben_ver(data_servicio.usuario.getUser_id(), email, name, apellido, relacion, genero);
		} else if (data_servicio.lista.equals("verificadores")) {

			servicio.crear_ben_ver(data_servicio.usuario.getUser_id(), email, name, apellido, str_telefono, genero);
		}

		servicio.setPuente_envio(puente);
		servicio.execute();

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent returnIntent = new Intent();

		returnIntent.putExtra("requestCode", 400);

		setResult(Activity.RESULT_CANCELED, returnIntent);
		finish();
	}

	public void eliminar() {

		WS_cliente servicio;
		try {
			servicio = new WS_cliente(micontext);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						cargar.ocultar();
						Intent returnIntent = new Intent();

						returnIntent.putExtra("requestCode", 200);
						returnIntent.putExtra("result", "ok");
						setResult(Activity.RESULT_OK, returnIntent);

						if (!data_servicio.msj_error.equals("")) {

							dialogo_modal.dialogo_cerrar(getString(R.string.informacion), data_servicio.msj_error, "",
									"Aceptar", true, false);
							data_servicio.msj_error = "";
						} else {
							dialogo_modal.dialogo_cerrar(getString(R.string.informacion), getString(R.string.successful_elimination), "",
									"Aceptar", true, false);
						}

					} else if (msg.what == 0) {
						cargar.ocultar();
						Intent returnIntent = new Intent();
						returnIntent.putExtra("result", "ok");
						setResult(Activity.RESULT_OK, returnIntent);
						finish();
					}

				}
			};
			String id_beneficiario = "";

			String json = "";
			if (data_servicio.lista.equals("beneficiarios")) {
				servicio.delete_ben_ver(data_servicio.usuario.getUser_id(), us.getUser_id());

			} else if (data_servicio.lista.equals("verificadores")) {
				servicio.delete_ben_ver(data_servicio.usuario.getUser_id(), us.getUser_id());

			}

			servicio.setPuente_envio(puente);
			servicio.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void guardarImagen(final Imagen image_usuario) {
		// File dirImages = cw.getDir("perfil", Context.MODE_PRIVATE);
		new AsyncTask<Void, Void, String>() {

			int height;
			String tiempo = "";
			Bitmap bitmap = null;

			@Override
			protected String doInBackground(Void... params) {

				String msg = "";
				URL url;
				try {
					url = new URL(image_usuario.getUrl());

					try {
						bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

						image_usuario.setImagen(bitmap);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return msg;
			}

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();

			}

			@Override
			protected void onProgressUpdate(Void... values) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void onPostExecute(String msg) {
				log_aplicacion.log_informacion("onpostexcecute");
				try {

					if (bitmap != null) {
						Imagen image = null;

						for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
							if (data_servicio.imagen_usuarios.get(l).getId_user().equals(image_usuario.getId_user())) {

								image = data_servicio.imagen_usuarios.get(l);
								break;
							}
						}
						image.setImagen(bitmap);

					}
				} catch (Exception e) {
					// TODO: handle exception
					log_aplicacion.log_informacion("onpostexcecute error " + e.toString());

				}
			}

		}.execute(null, null, null);

	}

	public void update_usuario(String name, String id, String email, String telefono, String relacion, String genero)
			throws UnsupportedEncodingException, JSONException {
		log_aplicacion.log_informacion("crear");
		cargar.mostrar();
		WS_cliente servicio = new WS_cliente(micontext);
		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {
					cargar.ocultar();

					Intent returnIntent = new Intent();
					returnIntent.putExtra("result", "ok");
					setResult(Activity.RESULT_OK, returnIntent);
					if (!data_servicio.msj_error.equals("")) {
						dialogo_modal.dialogo_cerrar(getString(R.string.informacion), data_servicio.msj_error, "",
								"Aceptar", true, false);
						data_servicio.msj_error = "";
					} else {
						dialogo_modal.dialogo_cerrar(getString(R.string.informacion), "Datos actualizados", "",
								"Aceptar", true, false);
					}

				} else if (msg.what == 0) {
					cargar.ocultar();
					Intent returnIntent = new Intent();
					returnIntent.putExtra("result", "ok");
					setResult(Activity.RESULT_OK, returnIntent);
					finish();
				}

			}
		};
		String id_beneficiario = "";

		String json = "";
		if (data_servicio.lista.equals("beneficiarios")) {

			servicio.Update_ben_verif(id, us.getId_beneficiario_user(), nombre.getText().toString(),
					apellido.getText().toString(), email, relacion, genero, "");

		} else if (data_servicio.lista.equals("verificadores")) {
			servicio.Update_ben_verif(id, id, nombre.getText().toString(), apellido.getText().toString(), email,
					relacion, genero, telefono);

		}

		servicio.setPuente_envio(puente);
		servicio.execute();

	}

	public boolean validar() {
		boolean campos_validos = true;
		Validacion_campos validar = new Validacion_campos();

		if (validar.validar_text(nombre.getText().toString())) {

			nombre.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			nombre.setBackground(getResources().getDrawable(R.drawable.edit_text_error));
		}

		if (validar.validar_text(apellido.getText().toString())) {

			apellido.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			apellido.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}

		if (validar.validar_email(correo.getText().toString())) {

			correo.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			correo.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}

		if (!campos_validos) {
			Toast.makeText(this, getString(R.string.verifique_los_campos_existen_campos_erroneos_), Toast.LENGTH_LONG)
					.show();
		}
		if (data_servicio.lista.equals("beneficiarios")) {
			if (sp_parentesco.getSelectedItemPosition() != 0) {
				try {
					JSONArray jarray2 = new JSONArray(data_servicio.json_tipo_relacion);
					for (int i = 0; i < jarray2.length(); i++) {
						if (jarray2.getJSONObject(i).getString("relationship")
								.equals(sp_parentesco.getSelectedItem().toString())) {
							relation_id = jarray2.getJSONObject(i).getString("idrelationship");

						}
					}

				} catch (Exception e) {
					// TODO: handle exception
				}
			} else {
				if (campos_validos) {
					Toast.makeText(this, getString(R.string.seleccione_el_parentesco_), Toast.LENGTH_LONG).show();

				}

				campos_validos = false;
			}
		}
		return campos_validos;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in action bar clicked; goto parent activity.
			Intent returnIntent = new Intent();

			returnIntent.putExtra("requestCode", 400);

			setResult(Activity.RESULT_CANCELED, returnIntent);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void dialogo(String titulo, String mensaje, String boton1, String boton2, final boolean boton1estado,
			final boolean boton2estado) {

		LayoutInflater li = LayoutInflater.from(micontext);
		final View prompt = li.inflate(R.layout.mensaje, null);
		TextView txtitulo = (TextView) prompt.findViewById(R.id.textmsg_titulo);

		txtitulo.setText(titulo);
		TextView txtmsg = (TextView) prompt.findViewById(R.id.textmsg);
		txtmsg.setText(mensaje);

		Builder alertDialogBuilder = new AlertDialog.Builder(micontext);
		alertDialogBuilder.setView(prompt);
		alertDialogBuilder.setCancelable(false);
		TextView txtb1 = (TextView) prompt.findViewById(R.id.textbtn1);
		txtb1.setText(boton1);
		TextView txtb2 = (TextView) prompt.findViewById(R.id.textbtn2);

		txtb2.setText(boton2);
		txtb1.setTypeface(tm);
		txtb2.setTypeface(tm);
		txtitulo.setTypeface(tm);
		txtmsg.setTypeface(tm);

		ListView lista_opciones = (ListView) prompt.findViewById(R.id.listopciones);

		LinearLayout contenedor = (LinearLayout) prompt.findViewById(R.id.layout_msg);

		alertDialog = alertDialogBuilder.create();

		alertDialog.show();

		if (!boton1.equals("")) {

			txtb1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (boton1estado) {

					} else {
						alertDialog.cancel();
					}

				}
			});

		}

		if (!boton2.equals("")) {

			txtb2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (boton2estado) {

						eliminar();

					} else {
						alertDialog.cancel();
					}

				}
			});
		}

		// Creamos un AlertDialog y lo mostramos

	}

}
