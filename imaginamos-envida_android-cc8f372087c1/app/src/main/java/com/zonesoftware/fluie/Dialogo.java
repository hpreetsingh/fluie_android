package com.zonesoftware.fluie;

import java.io.File;

import com.zonesoftware.fluie.db.Db_manager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import servicio.log_aplicacion;

public class Dialogo {

    Context micontext;
    Activity activity;

    Typeface tm, tr;
    AlertDialog.Builder alertDialogBuilder;

    AlertDialog alertDialog;

    DialogoCarga cargar;
    Db_manager db = null;

    public Dialogo(Context micontext, Activity activity) {
        super();
        this.micontext = micontext;
        this.activity = activity;
        tm = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Medium.ttf");
        tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");
        cargar = new DialogoCarga(activity, micontext.getString(R.string.eliminando),
                micontext.getString(R.string.espere_un_momento_));
        db = new Db_manager(micontext);

    }

    public void dialogo(String titulo, String mensaje, String boton1, String boton2, final boolean boton1estado,
                        final boolean boton2estado) {
        tm = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Medium.ttf");
        tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");

        LayoutInflater li = LayoutInflater.from(micontext);
        final View prompt = li.inflate(R.layout.mensaje, null);
        TextView txtitulo = (TextView) prompt.findViewById(R.id.textmsg_titulo);

        txtitulo.setText(titulo);
        TextView txtmsg = (TextView) prompt.findViewById(R.id.textmsg);
        txtmsg.setText(mensaje);

        Builder alertDialogBuilder = new AlertDialog.Builder(micontext);
        alertDialogBuilder.setView(prompt);
        alertDialogBuilder.setCancelable(false);
        TextView txtb1 = (TextView) prompt.findViewById(R.id.textbtn1);
        txtb1.setText(boton1);
        TextView txtb2 = (TextView) prompt.findViewById(R.id.textbtn2);

        txtb2.setText(boton2);
        txtb1.setTypeface(tm);
        txtb2.setTypeface(tm);
        txtitulo.setTypeface(tm);
        txtmsg.setTypeface(tm);

        ListView lista_opciones = (ListView) prompt.findViewById(R.id.listopciones);

        LinearLayout contenedor = (LinearLayout) prompt.findViewById(R.id.layout_msg);

        final String[] options = {"Tomar foto", "Elegir de galeria"};

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        // android.R.layout.simple_list_item_1,
        // android.R.id.text1, options);

        // lista_opciones.setAdapter(adapter);

        // lista_opciones.setOnItemClickListener(new
        // AdapterView.OnItemClickListener() {
        //
        // @Override
        // public void onItemClick(AdapterView<?> parent, View view, int
        // position, long id) {
        // // TODO Auto-generated method stub
        // if (options[position].equals("Tomar foto")) {
        // } else if (options[position].equals("Elegir de galeria")) {
        // }
        // }
        //
        // });

        alertDialog = alertDialogBuilder.create();

        alertDialog.show();

        if (!boton1.equals("")) {

            txtb1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton1estado) {

                    } else {
                        alertDialog.cancel();
                    }

                }
            });

        }

        if (!boton2.equals("")) {

            txtb2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton2estado) {

                    } else {
                        alertDialog.cancel();
                    }

                }
            });
        }

        // Creamos un AlertDialog y lo mostramos

    }

    public void dialogo_cerrar(String titulo, String mensaje, String boton1, String boton2, final boolean boton1estado,
                               final boolean boton2estado) {
        tm = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Medium.ttf");
        tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");

        LayoutInflater li = LayoutInflater.from(micontext);
        final View prompt = li.inflate(R.layout.mensaje, null);
        TextView txtitulo = (TextView) prompt.findViewById(R.id.textmsg_titulo);

        txtitulo.setText(titulo);
        TextView txtmsg = (TextView) prompt.findViewById(R.id.textmsg);
        txtmsg.setText(mensaje);

        Builder alertDialogBuilder = new AlertDialog.Builder(micontext);
        alertDialogBuilder.setView(prompt);
        alertDialogBuilder.setCancelable(false);
        TextView txtb1 = (TextView) prompt.findViewById(R.id.textbtn1);
        txtb1.setText(boton1);
        TextView txtb2 = (TextView) prompt.findViewById(R.id.textbtn2);

        txtb2.setText(boton2);
        txtb1.setTypeface(tm);
        txtb2.setTypeface(tm);
        txtitulo.setTypeface(tm);
        txtmsg.setTypeface(tm);

        ListView lista_opciones = (ListView) prompt.findViewById(R.id.listopciones);

        LinearLayout contenedor = (LinearLayout) prompt.findViewById(R.id.layout_msg);

        final String[] options = {"Tomar foto", "Elegir de galeria"};

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        // android.R.layout.simple_list_item_1,
        // android.R.id.text1, options);

        // lista_opciones.setAdapter(adapter);

        // lista_opciones.setOnItemClickListener(new
        // AdapterView.OnItemClickListener() {
        //
        // @Override
        // public void onItemClick(AdapterView<?> parent, View view, int
        // position, long id) {
        // // TODO Auto-generated method stub
        // if (options[position].equals("Tomar foto")) {
        // } else if (options[position].equals("Elegir de galeria")) {
        // }
        // }
        //
        // });

        alertDialog = alertDialogBuilder.create();

        alertDialog.show();

        if (!boton1.equals("")) {

            txtb1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton1estado) {

                        Intent intent = new Intent(activity, HistorialMensajesActivity.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        activity.startActivity(intent);

                        activity.finish();
                    } else {
                        alertDialog.cancel();
                    }

                }
            });

        }

        if (!boton2.equals("")) {

            txtb2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton2estado) {

                        Intent intent = new Intent(activity, HistorialMensajesActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        activity.startActivity(intent);

                        activity.finish();

                    } else {
                        alertDialog.cancel();
                        activity.finish();
                    }

                }
            });
        }

        // Creamos un AlertDialog y lo mostramos

    }

    public void dialogo_cerrar0(String titulo, String mensaje, String boton1, String boton2, final boolean boton1estado,
                                final boolean boton2estado) {
        tm = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Medium.ttf");
        tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");

        LayoutInflater li = LayoutInflater.from(micontext);
        final View prompt = li.inflate(R.layout.mensaje, null);
        TextView txtitulo = (TextView) prompt.findViewById(R.id.textmsg_titulo);

        txtitulo.setText(titulo);
        TextView txtmsg = (TextView) prompt.findViewById(R.id.textmsg);
        txtmsg.setText(mensaje);

        Builder alertDialogBuilder = new AlertDialog.Builder(micontext);
        alertDialogBuilder.setView(prompt);
        alertDialogBuilder.setCancelable(false);
        TextView txtb1 = (TextView) prompt.findViewById(R.id.textbtn1);
        txtb1.setText(boton1);
        TextView txtb2 = (TextView) prompt.findViewById(R.id.textbtn2);

        txtb2.setText(boton2);
        txtb1.setTypeface(tm);
        txtb2.setTypeface(tm);
        txtitulo.setTypeface(tm);
        txtmsg.setTypeface(tm);

        ListView lista_opciones = (ListView) prompt.findViewById(R.id.listopciones);

        LinearLayout contenedor = (LinearLayout) prompt.findViewById(R.id.layout_msg);

        final String[] options = {"Tomar foto", "Elegir de galeria"};

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        // android.R.layout.simple_list_item_1,
        // android.R.id.text1, options);

        // lista_opciones.setAdapter(adapter);

        // lista_opciones.setOnItemClickListener(new
        // AdapterView.OnItemClickListener() {
        //
        // @Override
        // public void onItemClick(AdapterView<?> parent, View view, int
        // position, long id) {
        // // TODO Auto-generated method stub
        // if (options[position].equals("Tomar foto")) {
        // } else if (options[position].equals("Elegir de galeria")) {
        // }
        // }
        //
        // });

        alertDialog = alertDialogBuilder.create();

        alertDialog.show();

        if (!boton1.equals("")) {

            txtb1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton1estado) {
//                        UsuarioActivity.activi.finish();
//                        Intent intent = new Intent(activity, UsuarioActivity.class);
//
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                        activity.startActivity(intent);
                        activity.finish();

                    } else {
                        alertDialog.cancel();
                        activity.finish();
                    }

                }
            });

        }

        if (!boton2.equals("")) {

            txtb2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton2estado) {
//                        UsuarioActivity.activi.finish();
//                        Intent intent = new Intent(activity, UsuarioActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                        activity.startActivity(intent);
                        activity.finish();

                    } else {
                        alertDialog.cancel();
                        activity.finish();
                    }

                }
            });
        }

        // Creamos un AlertDialog y lo mostramos

    }

    public void dialogo_cerrar1(String titulo, String mensaje, String boton1, String boton2, final boolean boton1estado,
                                final boolean boton2estado) {
        tm = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Medium.ttf");
        tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");

        LayoutInflater li = LayoutInflater.from(micontext);
        final View prompt = li.inflate(R.layout.mensaje, null);
        TextView txtitulo = (TextView) prompt.findViewById(R.id.textmsg_titulo);

        txtitulo.setText(titulo);
        TextView txtmsg = (TextView) prompt.findViewById(R.id.textmsg);
        txtmsg.setText(mensaje);

        Builder alertDialogBuilder = new AlertDialog.Builder(micontext);
        alertDialogBuilder.setView(prompt);
        alertDialogBuilder.setCancelable(false);
        TextView txtb1 = (TextView) prompt.findViewById(R.id.textbtn1);
        txtb1.setText(boton1);
        TextView txtb2 = (TextView) prompt.findViewById(R.id.textbtn2);

        txtb2.setText(boton2);
        txtb1.setTypeface(tm);
        txtb2.setTypeface(tm);
        txtitulo.setTypeface(tm);
        txtmsg.setTypeface(tm);

        ListView lista_opciones = (ListView) prompt.findViewById(R.id.listopciones);

        LinearLayout contenedor = (LinearLayout) prompt.findViewById(R.id.layout_msg);

        final String[] options = {"Tomar foto", "Elegir de galeria"};

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        // android.R.layout.simple_list_item_1,
        // android.R.id.text1, options);

        // lista_opciones.setAdapter(adapter);

        // lista_opciones.setOnItemClickListener(new
        // AdapterView.OnItemClickListener() {
        //
        // @Override
        // public void onItemClick(AdapterView<?> parent, View view, int
        // position, long id) {
        // // TODO Auto-generated method stub
        // if (options[position].equals("Tomar foto")) {
        // } else if (options[position].equals("Elegir de galeria")) {
        // }
        // }
        //
        // });

        alertDialog = alertDialogBuilder.create();

        alertDialog.show();

        if (!boton1.equals("")) {

            txtb1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton1estado) {

//						Intent intent = new Intent(activity, HistorialMensajesActivity.class);
//
//						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//						activity.startActivity(intent);

                        activity.finish();
                    } else {
                        alertDialog.cancel();
                    }

                }
            });

        }

        if (!boton2.equals("")) {

            txtb2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton2estado) {

//						Intent intent = new Intent(activity, HistorialMensajesActivity.class);
//						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//						activity.startActivity(intent);

                        activity.finish();

                    } else {
                        alertDialog.cancel();
                        activity.finish();
                    }

                }
            });
        }

        // Creamos un AlertDialog y lo mostramos

    }

    public void dialogo_cerrar_sesion(String titulo, String mensaje, String boton1, String boton2,
                                      final boolean boton1estado, final boolean boton2estado) {
        tm = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Medium.ttf");
        tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");

        LayoutInflater li = LayoutInflater.from(micontext);
        final View prompt = li.inflate(R.layout.mensaje, null);
        TextView txtitulo = (TextView) prompt.findViewById(R.id.textmsg_titulo);

        txtitulo.setText(titulo);
        TextView txtmsg = (TextView) prompt.findViewById(R.id.textmsg);
        txtmsg.setText(mensaje);

        Builder alertDialogBuilder = new AlertDialog.Builder(micontext);
        alertDialogBuilder.setView(prompt);
        alertDialogBuilder.setCancelable(false);
        TextView txtb1 = (TextView) prompt.findViewById(R.id.textbtn1);
        txtb1.setText(boton1);
        TextView txtb2 = (TextView) prompt.findViewById(R.id.textbtn2);

        txtb2.setText(boton2);
        txtb1.setTypeface(tm);
        txtb2.setTypeface(tm);
        txtitulo.setTypeface(tm);
        txtmsg.setTypeface(tm);

        ListView lista_opciones = (ListView) prompt.findViewById(R.id.listopciones);

        LinearLayout contenedor = (LinearLayout) prompt.findViewById(R.id.layout_msg);

        final String[] options = {"Tomar foto", "Elegir de galeria"};

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        // android.R.layout.simple_list_item_1,
        // android.R.id.text1, options);

        // lista_opciones.setAdapter(adapter);

        // lista_opciones.setOnItemClickListener(new
        // AdapterView.OnItemClickListener() {
        //
        // @Override
        // public void onItemClick(AdapterView<?> parent, View view, int
        // position, long id) {
        // // TODO Auto-generated method stub
        // if (options[position].equals("Tomar foto")) {
        // } else if (options[position].equals("Elegir de galeria")) {
        // }
        // }
        //
        // });

        alertDialog = alertDialogBuilder.create();

        alertDialog.show();

        if (!boton1.equals("")) {

            txtb1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton1estado) {

                    } else {
                        alertDialog.cancel();
                    }

                }
            });

        }

        if (!boton2.equals("")) {

            txtb2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton2estado) {

                        try {

                            db = new Db_manager(micontext);
                            log_aplicacion.log_informacion("id usuario" + db.getusuario().getId());
                            db.deleteusuario(db.getusuario().getId());
                            try {
                                File f = new File(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
                                if (f.exists()) {
                                    f.delete();
                                }
                            } catch (Exception e) {
                                // TODO: handle exception
                            }

                            Intent intent = new Intent(activity, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            activity.startActivity(intent);
                            activity.finish();

                        } catch (Exception e) {
                            // TODO: handle exception
                        }

                    } else {
                        alertDialog.cancel();
                    }

                }
            });
        }

        // Creamos un AlertDialog y lo mostramos

    }

    public void dialogo_eliminar(String titulo, String mensaje, String boton1, String boton2,
                                 final boolean boton1estado, final boolean boton2estado) {
        tm = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Medium.ttf");
        tr = Typeface.createFromAsset(activity.getAssets(), "font/Roboto_Regular.ttf");

        LayoutInflater li = LayoutInflater.from(micontext);
        final View prompt = li.inflate(R.layout.mensaje, null);
        TextView txtitulo = (TextView) prompt.findViewById(R.id.textmsg_titulo);

        txtitulo.setText(titulo);
        TextView txtmsg = (TextView) prompt.findViewById(R.id.textmsg);
        txtmsg.setText(mensaje);

        Builder alertDialogBuilder = new AlertDialog.Builder(micontext);
        alertDialogBuilder.setView(prompt);
        alertDialogBuilder.setCancelable(false);
        TextView txtb1 = (TextView) prompt.findViewById(R.id.textbtn1);
        txtb1.setText(boton1);
        TextView txtb2 = (TextView) prompt.findViewById(R.id.textbtn2);

        txtb2.setText(boton2);
        txtb1.setTypeface(tm);
        txtb2.setTypeface(tm);
        txtitulo.setTypeface(tm);
        txtmsg.setTypeface(tm);

        ListView lista_opciones = (ListView) prompt.findViewById(R.id.listopciones);

        LinearLayout contenedor = (LinearLayout) prompt.findViewById(R.id.layout_msg);

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        // android.R.layout.simple_list_item_1,
        // android.R.id.text1, options);

        // lista_opciones.setAdapter(adapter);

        // lista_opciones.setOnItemClickListener(new
        // AdapterView.OnItemClickListener() {
        //
        // @Override
        // public void onItemClick(AdapterView<?> parent, View view, int
        // position, long id) {
        // // TODO Auto-generated method stub
        // if (options[position].equals("Tomar foto")) {
        // } else if (options[position].equals("Elegir de galeria")) {
        // }
        // }
        //
        // });

        alertDialog = alertDialogBuilder.create();

        alertDialog.show();

        if (!boton1.equals("")) {

            txtb1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton1estado) {

                    } else {
                        alertDialog.cancel();
                    }

                }
            });

        }

        if (!boton2.equals("")) {

            txtb2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (boton2estado) {

                    } else {
                        alertDialog.cancel();
                    }

                }
            });
        }

        // Creamos un AlertDialog y lo mostramos

    }

}
