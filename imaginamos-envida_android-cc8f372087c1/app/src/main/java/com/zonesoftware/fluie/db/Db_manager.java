package com.zonesoftware.fluie.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class Db_manager {
	public static final String STRING_TYPE = "text";
	public static final String INT_TYPE = "integer";

	public static final String TABLENAME_USER = "usuario";

	public static final String CN_ID_USUARIO = "id_usuario";
	public static final String CN_CORREO = "correo";
	public static final String CN_PASSWORD = "contrasena";

	// Parametros borradores
	public static final String TABLENAME_BORRADOR = "Borradores";
	public static final String CN_ID_BORRADOR = "id_borrador";
	public static final String CN_TYPE_FILE = "type_file_id";
	public static final String CN_TYPE_MESSAGE = "type_message_id";

	public static final String CN_TITLE = "title";
	public static final String CN_DESCRIPCION = "descripcion";
	public static final String CN_DATA_VIEW = "date_view";
	public static final String CN_FILE = "file";
	public static final String CN_USER_ID = "user_id";
	public static final String CN_BENEFICIARIOS = "beneficiaries";
	public static final String CN_TAMANO = "tamano";
	public static final String CREATE_TABLE_USER = "create table " + TABLENAME_USER + "(" + CN_ID_USUARIO + " "
			+ INT_TYPE + " primary key autoincrement," + CN_CORREO + " " + STRING_TYPE + " not null" + "," + CN_PASSWORD
			+ " " + STRING_TYPE + " not null" + ");";

	public static final String CREATE_TABLE_BORRADOR = "create table " + TABLENAME_BORRADOR + "(" + CN_ID_BORRADOR + " "
			+ INT_TYPE + " primary key autoincrement," + CN_TYPE_FILE + " " + STRING_TYPE + " not null" + ", "
			+ CN_TYPE_MESSAGE + " " + STRING_TYPE + " not null" + ", " + CN_TITLE + " " + STRING_TYPE + " not null"
			+ "," + CN_DATA_VIEW + " " + STRING_TYPE + " not null" + "," + CN_DESCRIPCION + " " + STRING_TYPE
			+ " not null" + "," + CN_FILE + " " + STRING_TYPE + " not null" + ", " + CN_ID_USUARIO + " " + STRING_TYPE
			+ " not null" + "," + CN_BENEFICIARIOS + " " + STRING_TYPE + " not null" + "," + CN_TAMANO + " "
			+ STRING_TYPE + " not null" + ");";

	public static final String[] cols_usuario = new String[] { CN_ID_USUARIO, CN_CORREO, CN_PASSWORD };

	public static final String[] cols_borrador = new String[] { CN_ID_BORRADOR, CN_TYPE_FILE, CN_TYPE_MESSAGE, CN_TITLE,
			CN_DATA_VIEW, CN_DESCRIPCION, CN_FILE, CN_ID_USUARIO, CN_BENEFICIARIOS, CN_TAMANO };

	Context context;
	DbHelper helper;
	SQLiteDatabase DB_write;
	Encriptar encripta;
	Desencriptar desencripta;
	String llavefija = "uytrenauiop5sdvu2uscj";

	public Db_manager(Context context) {
		this.context = context;
		helper = new DbHelper(context);
		DB_write = helper.getWritableDatabase();

		encripta = new Encriptar();
		desencripta = new Desencriptar();

	}

	public long insertar_usuario(String correo, String password) {

		ContentValues newValues = new ContentValues();
		newValues.put(CN_CORREO, correo);
		newValues.put(CN_PASSWORD, encripta.encriptar_frase(password, llavefija));

		return DB_write.insert(TABLENAME_USER, null, newValues);

	}

	public long insertar_borrador(String type_file_id, String type_message_id, String title, String date_view,
			String description, String file, String id_usuario, String beneficiaries, String tamano) {

		ContentValues newValues = new ContentValues();
		newValues.put(CN_TYPE_FILE, type_file_id);
		newValues.put(CN_TYPE_MESSAGE, type_message_id);
		newValues.put(CN_TITLE, title);
		newValues.put(CN_DATA_VIEW, date_view);
		newValues.put(CN_DESCRIPCION, description);
		newValues.put(CN_FILE, file);
		newValues.put(CN_ID_USUARIO, id_usuario);
		newValues.put(CN_BENEFICIARIOS, beneficiaries);
		newValues.put(CN_TAMANO, tamano);
		return DB_write.insert(TABLENAME_BORRADOR, null, newValues);

	}

	//
	public boolean updateusuario(Usuario_fluie usuario) {
		ContentValues newValues = new ContentValues();
		newValues.put(CN_CORREO, usuario.getCorreo());
		newValues.put(CN_PASSWORD, encripta.encriptar_frase(usuario.getPassword(), llavefija));
		return DB_write.update(TABLENAME_USER, newValues, CN_ID_USUARIO + "=" + String.valueOf(usuario.getId()),
				null) > 0;
	}

	//
	public Usuario_fluie getusuario() {
		Usuario_fluie usuario = null;
		Cursor result = DB_write.query(true, TABLENAME_USER, cols_usuario, null, null, null, null, null, null);
		if ((result.getCount() == 0) || !result.moveToFirst()) {
			usuario = null;
		} else {
			if (result.moveToFirst()) {
				usuario = new Usuario_fluie(result.getInt(result.getColumnIndex(CN_ID_USUARIO)),
						result.getString(result.getColumnIndex(CN_CORREO)), desencripta
								.desencriptar_frase(result.getString(result.getColumnIndex(CN_PASSWORD)), llavefija));
			}
		}
		return usuario;
	}

	public ArrayList<Borrador> getborradores(String tipo) {
		ArrayList<Borrador> borradores = new ArrayList<Borrador>();

		String where = CN_ID_USUARIO + "=" + data_servicio.usuario.getUser_id() + " AND " + CN_TYPE_FILE + "='" + tipo
				+ "'";
		log_aplicacion.log_informacion("where borradores " + where);

		Cursor result = DB_write.query(TABLENAME_BORRADOR, cols_borrador, where, null, null, null, null, null);

		result.moveToFirst();
		// String id_archivo, String nombre, String id_drive,
		// String id_padre, String estado, String url, String tipo_archivo,
		// String fecha_modificacion

		while (!result.isAfterLast()) {

			Borrador borrador_temp = new Borrador(result.getInt(result.getColumnIndex(CN_ID_BORRADOR)),
					result.getString(result.getColumnIndex(CN_TYPE_FILE)),
					result.getString(result.getColumnIndex(CN_TYPE_MESSAGE)),
					result.getString(result.getColumnIndex(CN_TITLE)),
					result.getString(result.getColumnIndex(CN_DATA_VIEW)),
					result.getString(result.getColumnIndex(CN_DESCRIPCION)),
					result.getString(result.getColumnIndex(CN_FILE)),
					result.getString(result.getColumnIndex(CN_ID_USUARIO)),

					result.getString(result.getColumnIndex(CN_BENEFICIARIOS)),
					result.getString(result.getColumnIndex(CN_TAMANO)));
			borradores.add(0, borrador_temp);
			result.moveToNext();
		}
		return borradores;
	}

	public ArrayList<Borrador> getborradores() {
		ArrayList<Borrador> borradores = new ArrayList<Borrador>();

		String where = CN_ID_USUARIO + "=" + data_servicio.usuario.getUser_id();

		log_aplicacion.log_informacion("where borradores " + where);

		Cursor result = DB_write.query(TABLENAME_BORRADOR, cols_borrador, where, null, null, null, null, null);

		result.moveToFirst();
		// String id_archivo, String nombre, String id_drive,
		// String id_padre, String estado, String url, String tipo_archivo,
		// String fecha_modificacion

		while (!result.isAfterLast()) {

			Borrador borrador_temp = new Borrador(result.getInt(result.getColumnIndex(CN_ID_BORRADOR)),
					result.getString(result.getColumnIndex(CN_TYPE_FILE)),
					result.getString(result.getColumnIndex(CN_TYPE_MESSAGE)),
					result.getString(result.getColumnIndex(CN_TITLE)),
					result.getString(result.getColumnIndex(CN_DATA_VIEW)),
					result.getString(result.getColumnIndex(CN_DESCRIPCION)),
					result.getString(result.getColumnIndex(CN_FILE)),
					result.getString(result.getColumnIndex(CN_ID_USUARIO)),

					result.getString(result.getColumnIndex(CN_BENEFICIARIOS)),
					result.getString(result.getColumnIndex(CN_TAMANO)));
			borradores.add(borrador_temp);
			result.moveToNext();
		}
		return borradores;
	}

	public Borrador getborrador(String id) {

		Borrador borrador = null;
		String where = CN_ID_BORRADOR + "=" + id;
		Cursor result = DB_write.query(TABLENAME_BORRADOR, cols_borrador, where, null, null, null, null, null);
		if ((result.getCount() == 0) || !result.moveToFirst()) {
			borrador = null;
		} else {
			if (result.moveToFirst()) {
				borrador = new Borrador(result.getInt(result.getColumnIndex(CN_ID_BORRADOR)),
						result.getString(result.getColumnIndex(CN_TYPE_FILE)),
						result.getString(result.getColumnIndex(CN_TYPE_MESSAGE)),
						result.getString(result.getColumnIndex(CN_TITLE)),
						result.getString(result.getColumnIndex(CN_DATA_VIEW)),
						result.getString(result.getColumnIndex(CN_DESCRIPCION)),
						result.getString(result.getColumnIndex(CN_FILE)),
						result.getString(result.getColumnIndex(CN_ID_USUARIO)),
						result.getString(result.getColumnIndex(CN_BENEFICIARIOS)),
						result.getString(result.getColumnIndex(CN_TAMANO)));
			}
		}
		return borrador;
	}

	public void deleteborrador(String i) {

		System.out.println("Comment deleted with id: " + i);
		DB_write.delete(TABLENAME_BORRADOR, CN_ID_BORRADOR + " = " + i, null);

	}

	public void deleteusuario(int i) {

		System.out.println("Comment deleted with id: " + i);
		DB_write.delete(TABLENAME_USER, CN_ID_USUARIO + " = " + i, null);

	}

	// public long insertar_archivo(String nombre, String id_drive, String
	// id_padre, String estado, String url,
	// String tipo_archivo, String fecha_modificacion) {
	// long inserta = 0;
	//
	// ContentValues newValues = new ContentValues();
	// newValues.put(CN_NOMBRE, nombre);
	// newValues.put(CN_IDDRIVE, id_drive);
	// newValues.put(CN_IDPADRE, id_padre);
	// newValues.put(CN_ESTADO, estado);
	// newValues.put(CN_URL, url);
	// newValues.put(CN_TIPO, tipo_archivo);
	// newValues.put(CN_FECHA_MODIFICACION, fecha_modificacion);
	//
	// inserta = DB_backup.insert(TABLENAME, null, newValues);
	//
	// return inserta;
	// }
	//
	// public boolean removeAppsForScan(int row) {
	// return DB_backup.delete(TABLENAME, CN_ID + "=" + row, null) > 0;
	// }
	//
	// public boolean updateRegist(Archivo archivo) {
	// ContentValues newValues = new ContentValues();
	// newValues.put(CN_NOMBRE, archivo.getNombre());
	// newValues.put(CN_IDDRIVE, archivo.getId_drive());
	// newValues.put(CN_IDPADRE, archivo.getId_padre());
	// newValues.put(CN_ESTADO, archivo.getEstado());
	// newValues.put(CN_URL, archivo.getUrl());
	// newValues.put(CN_TIPO, archivo.getTipo_archivo());
	// newValues.put(CN_FECHA_MODIFICACION, archivo.getFecha_modificacion());
	//
	// return DB_backup.update(TABLENAME, newValues, CN_ID + "=" +
	// archivo.getId_archivo(), null) > 0;
	// }
	//
	// public Archivo getarchivo(String id_drive) {
	//
	// Archivo myarchivo = new Archivo();
	//
	// Cursor result = DB_backup.query(true, TABLENAME, cols_archivo, CN_IDDRIVE
	// + "=" + id_drive, null, null, null,
	// null, null);
	//
	// if ((result.getCount() == 0) || !result.moveToFirst()) {
	// myarchivo = null;
	//
	// } else {
	// if (result.moveToFirst()) {
	// myarchivo = new Archivo(result.getInt(result.getColumnIndex(CN_ID)),
	// result.getString(result.getColumnIndex(CN_NOMBRE)),
	// result.getString(result.getColumnIndex(CN_IDDRIVE)),
	// result.getString(result.getColumnIndex(CN_IDPADRE)),
	// result.getString(result.getColumnIndex(CN_ESTADO)),
	// result.getString(result.getColumnIndex(CN_URL)),
	// result.getString(result.getColumnIndex(CN_TIPO)),
	// result.getString(result.getColumnIndex(CN_FECHA_MODIFICACION)));
	// }
	//
	// }
	// return myarchivo;
	// }
	//
	// public ArrayList<Archivo> getlista_carpetas() {
	//// ArrayList<Archivo> archivos = new ArrayList<Archivo>();
	//// String where = CN_TIPO + "='Carpeta'";
	//// Cursor result = DB_backup.query(TABLENAME, cols_archivo, where, null,
	//// null, null, null, null);
	////
	//// result.moveToFirst();
	//// // String id_archivo, String nombre, String id_drive,
	//// // String id_padre, String estado, String url, String tipo_archivo,
	//// // String fecha_modificacion
	////
	//// while (!result.isAfterLast()) {
	//// Log.i("safecandy", "carpeta:" +
	//// result.getString(result.getColumnIndex(CN_NOMBRE)));
	//// Archivo archivo = new
	//// Archivo(result.getInt(result.getColumnIndex(CN_ID)),
	//// result.getString(result.getColumnIndex(CN_NOMBRE)),
	//// result.getString(result.getColumnIndex(CN_IDDRIVE)),
	//// result.getString(result.getColumnIndex(CN_IDPADRE)),
	//// result.getString(result.getColumnIndex(CN_ESTADO)),
	//// result.getString(result.getColumnIndex(CN_URL)),
	//// result.getString(result.getColumnIndex(CN_TIPO)),
	//// result.getString(result.getColumnIndex(CN_FECHA_MODIFICACION)));
	//// archivos.add(archivo);
	//// result.moveToNext();
	// }
	//
	// return archivos;
	//
	// }
	//
	// public ArrayList<Archivo> getlista() {
	// ArrayList<Archivo> archivos = new ArrayList<Archivo>();
	//
	// Cursor result = DB_backup.query(TABLENAME, cols_archivo, null, null,
	// null, null, null, null);
	//
	// result.moveToFirst();
	// // String id_archivo, String nombre, String id_drive,
	// // String id_padre, String estado, String url, String tipo_archivo,
	// // String fecha_modificacion
	//
	// while (!result.isAfterLast()) {
	// Log.i("safecandy", "carpeta:" +
	// result.getString(result.getColumnIndex(CN_NOMBRE)));
	// Archivo archivo = new
	// Archivo(result.getInt(result.getColumnIndex(CN_ID)),
	// result.getString(result.getColumnIndex(CN_NOMBRE)),
	// result.getString(result.getColumnIndex(CN_IDDRIVE)),
	// result.getString(result.getColumnIndex(CN_IDPADRE)),
	// result.getString(result.getColumnIndex(CN_ESTADO)),
	// result.getString(result.getColumnIndex(CN_URL)),
	// result.getString(result.getColumnIndex(CN_TIPO)),
	// result.getString(result.getColumnIndex(CN_FECHA_MODIFICACION)));
	// archivos.add(archivo);
	// result.moveToNext();
	// }
	//
	// return archivos;
	//
	// }
	//
	// public ArrayList<Archivo> getlista_archivos(String id_padre) {
	// ArrayList<Archivo> archivos = new ArrayList<Archivo>();
	// String where = CN_IDPADRE + "='" + id_padre + "'";
	// Cursor result = DB_backup.query(TABLENAME, cols_archivo, where, null,
	// null, null, null, null);
	//
	// result.moveToFirst();
	// // String id_archivo, String nombre, String id_drive,
	// // String id_padre, String estado, String url, String tipo_archivo,
	// // String fecha_modificacion
	//
	// while (!result.isAfterLast()) {
	//
	// Log.i("safecandy", "archivos:" +
	// result.getString(result.getColumnIndex(CN_NOMBRE)));
	//
	// Archivo archivo = new
	// Archivo(result.getInt(result.getColumnIndex(CN_ID)),
	// result.getString(result.getColumnIndex(CN_NOMBRE)),
	// result.getString(result.getColumnIndex(CN_IDDRIVE)),
	// result.getString(result.getColumnIndex(CN_IDPADRE)),
	// result.getString(result.getColumnIndex(CN_ESTADO)),
	// result.getString(result.getColumnIndex(CN_URL)),
	// result.getString(result.getColumnIndex(CN_TIPO)),
	// result.getString(result.getColumnIndex(CN_FECHA_MODIFICACION)));
	// archivos.add(archivo);
	// result.moveToNext();
	// }
	//
	// return archivos;
	//
	// }

}
