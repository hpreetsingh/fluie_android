package com.zonesoftware.fluie;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.zonesoftware.fluie.db.Borrador;
import com.zonesoftware.fluie.db.Db_manager;
import com.zonesoftware.fluie.db.Usuario_fluie;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.PopupMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import modelo.BadgeDrawable;
import modelo.Imagen;
import modelo.ItemNotificacionesAdapter;
import modelo.ItemiconAdapter;
import modelo.ListPopupWindow;
import modelo.Notificacion;
import modelo.Usuario;
import modelo.Utils;
import servicio.PruebaInternet;
import servicio.WS_cliente;
import servicio.consultar_servicios;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class UsuarioActivity extends ActionBarActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	public NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;
	public static Context con;
	public static Activity activi;
	static ActionBarActivity act;
	Typeface tm;

	static Typeface tr;
	static ProgressBar ps;
	static TextView porcentaje;

	Dialogo dialogo_modal;
	static consultar_servicios consultar;
	Db_manager db;
	Usuario_fluie usuario = null;
	static TextView text_espacio;
	static View viewcontent;
	private static final String[] STRINGS = { "Option1", "Option2", "Option3", "Option4" };
	final String BENEFICIARIOS = "beneficiarios";
	final String VERIFICADORES = "verificadores";
	ListPopupWindow popup, popup2 = null;
	boolean popnotificaciones = false;
	boolean poppersonas = false;
	String freespace = "";
	static com.zonesoftware.fluie.CircularImageView imagen_perfil;
	com.zonesoftware.fluie.CircularImageView imagen_perfil_menu;
	static TextView text_nombre;
	static FrameLayout frame;
	static TextView text_pais;

	static TextView text_correo;
	int mNotificationsCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_usuario);







		con = this;
		act = this;
		activi=this;
		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}

		try {
			db = new Db_manager(con);
			data_servicio.usuariofluie = db.getusuario();

		} catch (Exception e) {
			// TODO: handle exception
		}
		android.view.Display display = getWindowManager().getDefaultDisplay();

		android.graphics.Point size = new android.graphics.Point();
		data_servicio.heigth = size.y;

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();
		dialogo_modal = new Dialogo(this, this);

		// Set up the drawer.navigation_drawer
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
		mNavigationDrawerFragment.close();
		LinearLayout buttonperfil = (LinearLayout) findViewById(R.id.buttonperfil);

		LinearLayout buttoncerrar = (LinearLayout) findViewById(R.id.cerrar_sesion);
		LinearLayout buttonalmacenamiento = (LinearLayout) findViewById(R.id.buttonalmacenamiento);

		consultar = new consultar_servicios(con, act);

		TextView text_perfil = (TextView) findViewById(R.id.username);

		log_aplicacion.log_informacion("url imagen" + data_servicio.usuario.getImage());

		imagen_perfil_menu = (com.zonesoftware.fluie.CircularImageView) findViewById(R.id.img_perfil);

		if (!data_servicio.usuario.getImage().equals("")) {
			File f = new File(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
			if (f.exists()) {
				Bitmap bitmap = null;
				try {

					bitmap = BitmapFactory
							.decodeFile(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
					bitmap.prepareToDraw();
					int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
					bitmap = Bitmap.createScaledBitmap(bitmap, 512, nh, true);

					imagen_perfil_menu.setImageBitmap(bitmap);
					imagen_perfil_menu.setScaleType(ImageView.ScaleType.CENTER_CROP);
				} catch (Exception e) {
					// TODO: handle exception
				}

			}
		}

		String nombre = data_servicio.usuario.getName() + " " + data_servicio.usuario.getName_last();

		text_perfil.setText(nombre);
		tm = Typeface.createFromAsset(getAssets(), "font/Roboto_Medium.ttf");
		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		text_perfil.setTypeface(tr);
		consultar.obtener_msgespacio();

		buttonperfil.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(con, PerfilActivity.class);
				startActivity(intent);
			}
		});

		buttoncerrar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialogo_modal.dialogo_cerrar_sesion(getString(R.string.cerrar_sesion),
						getString(R.string._esta_seguro_de_cerrar_la_sesion_), getString(R.string.no),
						getString(R.string.si), false, true);

			}
		});

		buttonalmacenamiento.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(con, AlmacenamientoActivity.class);
				startActivity(intent);

			}
		});
		LinearLayout buttonvideo = (LinearLayout) findViewById(R.id.buttonvideo);

		buttonvideo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				data_servicio.tipo_mensaje = "video";
				data_servicio.opcion_mensaje = "crear";
				Intent intent = new Intent(con, MensajeActivity.class);
				startActivity(intent);
			}
		});

		LinearLayout buttonvoz = (LinearLayout) findViewById(R.id.buttonvoz);

		buttonvoz.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				data_servicio.tipo_mensaje = "voz";
				data_servicio.opcion_mensaje = "crear";
				Intent intent = new Intent(con, MensajeActivity.class);
				startActivity(intent);
			}
		});
		LinearLayout buttontext = (LinearLayout) findViewById(R.id.buttontexto);

		buttontext.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				data_servicio.tipo_mensaje = "texto";
				data_servicio.opcion_mensaje = "crear";
				Intent intent = new Intent(con, MensajeActivity.class);
				startActivity(intent);
			}
		});

		LinearLayout buttonimagen = (LinearLayout) findViewById(R.id.buttonimagen);

		buttonimagen.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				data_servicio.tipo_mensaje = "imagen";
				data_servicio.opcion_mensaje = "crear";
				Intent intent = new Intent(con, MensajeActivity.class);
				startActivity(intent);
			}
		});

		updateNotificationsBadge();

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		GuardarInformacionApp.Guardar_info(outState);

	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
				.commit();
	}

	public void onSectionAttached(int number) {
		switch (number) {
		case 1:
			mTitle = getString(R.string.title_section1);
			break;
		case 2:
			mTitle = getString(R.string.title_section2);
			break;
		case 3:
			mTitle = getString(R.string.title_section3);
			break;
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setTitle("");
		actionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflator.inflate(R.layout.titulo_action_bar, null);

		actionBar.setCustomView(v);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.usuario, menu);
			MenuItem item = menu.findItem(R.id.action_notificacion);
			ImageView imageView = new ImageView(UsuarioActivity.this);
			imageView.setBackgroundResource(R.drawable.icon_noti);
			BadgeDrawable badge;
			badge = new BadgeDrawable(UsuarioActivity.this);
			badge.setCount(mNotificationsCount);
			imageView.setImageDrawable(badge);
			imageView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					updateNotificationsBadge();
					View v = findViewById(R.id.action_notificacion);
					if (poppersonas) {
						log_aplicacion.log_informacion("popup2 show");
						popup2.dismiss();
						poppersonas = false;
					}
					if (popnotificaciones) {
						frame.setVisibility(View.GONE);
						log_aplicacion.log_informacion("popup show");
						popup.dismiss();
						popnotificaciones = false;

					} else {
						showPopuplist(v);
						cambio_estado();

					}
				}
			});
			item.setActionView(imageView);
			//ll
//			LayerDrawable icon = (LayerDrawable) item.getIcon();

			// Update LayerDrawable's BadgeDrawable
			//ll


//			Utils.setBadgeCount(this, icon, mNotificationsCount);

			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	private void updateNotificationsBadge() {

		int count = 0;
		for (int i = 0; i < data_servicio.Notificaciones.size(); i++) {

			if (data_servicio.Notificaciones.get(i).getView().equals("0")) {
				count++;
			}
		}

		mNotificationsCount = count;

		// force the ActionBar to relayout its MenuItems.
		// onCreateOptionsMenu(Menu) will be called again.
		invalidateOptionsMenu();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		// Toast.makeText(con, "presiono " + item.getItemId(),
		// Toast.LENGTH_LONG).show();
		int id = item.getItemId();
		updateNotificationsBadge();
		if (item.getItemId() == R.id.action_notificacion) {
			View v = findViewById(R.id.action_notificacion);
			// initiatePopupWindow();

//			if (poppersonas) {
//				log_aplicacion.log_informacion("popup2 show");
//				popup2.dismiss();
//				poppersonas = false;
//
//			}
//			if (popnotificaciones) {
//				frame.setVisibility(View.GONE);
//				log_aplicacion.log_informacion("popup show");
//				popup.dismiss();
//				popnotificaciones = false;
//
//			} else {
//
//				showPopuplist(v);
//				cambio_estado();
//
//			}

			// showPopupMenu(R.id.action_notificacion);
			// initiatePopupWindow();
			// showPopup(v);

			return true;
		}

		if (item.getItemId() == R.id.action_usuarios) {
			if (popnotificaciones) {
				log_aplicacion.log_informacion("popup show");
				popup.dismiss();
				popnotificaciones = false;

			}
			data_servicio.listaben = "";
			View v = findViewById(R.id.action_usuarios);

			if (poppersonas) {
				log_aplicacion.log_informacion("popup2 show");

				frame.setVisibility(View.GONE);
				popup2.dismiss();
				poppersonas = false;

			} else {

				frame.setVisibility(View.VISIBLE);
				frame.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						frame.setVisibility(View.GONE);
						updateNotificationsBadge();
					}
				});
				showPopuplist_ben_ven(v);
			}

			// showPopupWindow(v);

			// ...

			return true;
		}

		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	void showPopupWindow(View view) {
		PopupMenu popup = new PopupMenu(con, view);

		try {
			Field[] fields = popup.getClass().getDeclaredFields();
			for (Field field : fields) {
				if ("mPopup".equals(field.getName())) {
					field.setAccessible(true);
					Object menuPopupHelper = field.get(popup);
					Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
					Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
					setForceIcons.invoke(menuPopupHelper, true);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		popup.getMenuInflater().inflate(R.menu.tipos, popup.getMenu());

		popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

			public boolean onMenuItemClick(MenuItem item) {
				frame.setVisibility(View.GONE);
				if (item.getItemId() == R.id.action_beneficiario) {
					data_servicio.lista = BENEFICIARIOS;

					Intent intent = new Intent(con, BeneficiarioActivity.class);

					startActivity(intent);

				} else if (item.getItemId() == R.id.action_verificado) {
					data_servicio.lista = VERIFICADORES;
					Intent intent = new Intent(con, BeneficiarioActivity.class);

					startActivity(intent);

				}
				return true;
			}
		});
		popup.show();

	}

	// public int measureContentWidth(ListAdapter adapter) {
	// int maxWidth = 0;
	// int count = adapter.getCount();
	// final int widthMeasureSpec = MeasureSpec.makeMeasureSpec(0,
	// MeasureSpec.UNSPECIFIED);
	// final int heightMeasureSpec = MeasureSpec.makeMeasureSpec(0,
	// MeasureSpec.UNSPECIFIED);
	// View itemView = null;
	// for (int i = 0; i < count; i++) {
	// itemView = adapter.getView(i, itemView, getActivity());
	// itemView.measure(widthMeasureSpec, heightMeasureSpec);
	// maxWidth = Math.max(maxWidth, itemView.getMeasuredWidth());
	// }
	// return maxWidth;
	// }

	private void showPopuplist(View anchorView) {

		popup = new ListPopupWindow(con);
		updateNotificationsBadge();

		if (data_servicio.Notificaciones.size() > 0) {
			ItemNotificacionesAdapter adapter = new ItemNotificacionesAdapter(this, data_servicio.Notificaciones);

			popup.setAdapter(adapter);

			ColorDrawable cd = new ColorDrawable(Color.TRANSPARENT);
			popup.setBackgroundDrawable(cd);

			popup.setAnchorView(anchorView);

			popup.setWidth(getSupportFragmentManager().getFragments().get(1).getView().getWidth() - 60);

			popup.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

					popup.dismiss();
					popnotificaciones = false;
					updateNotificationsBadge();

				}
			});
			popup.show1(1);
			popnotificaciones = true;

			frame.setVisibility(View.VISIBLE);

			frame.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					frame.setVisibility(View.GONE);
					updateNotificationsBadge();
				}
			});

		} else {

			Toast.makeText(con, getString(R.string.no_existen_notificaciones), Toast.LENGTH_SHORT).show();

		}
	}

	private void showPopuplist_ben_ven(View anchorView) {

		popup2 = new ListPopupWindow(con);

		ArrayList<String> lista = new ArrayList<String>();

		lista.add(getString(R.string.beneficiario));

		lista.add(getString(R.string.verificadores));
		ItemiconAdapter adapter = new ItemiconAdapter(this, lista);

		popup2.setAdapter(adapter);

		ColorDrawable cd = new ColorDrawable(Color.TRANSPARENT);
		popup2.setBackgroundDrawable(cd);

		popup2.setAnchorView(anchorView);

		popup2.setWidth(getSupportFragmentManager().getFragments().get(1).getView().getWidth() - 60);

		popup2.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				popup2.dismiss();
				poppersonas = false;
				if (position == 0) {
					data_servicio.lista = BENEFICIARIOS;
					Intent intent = new Intent(con, BeneficiarioActivity.class);

					startActivity(intent);

				}
				if (position == 1) {
					data_servicio.lista = VERIFICADORES;
					Intent intent = new Intent(con, BeneficiarioActivity.class);

					startActivity(intent);

				}
			}
		});
		popup2.show1(2);
		poppersonas = true;
	}

	private PopupWindow pwindo;

	private void initiatePopupWindow() {
		try {
			// We need to get the instance of the LayoutInflater
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.popup_layout, (ViewGroup) findViewById(R.id.popup_element));
			pwindo = new PopupWindow(layout,
					getSupportFragmentManager().getFragments().get(1).getView().getWidth() - 30, 300, true);
			pwindo.showAtLocation(layout, Gravity.TOP, 0, 0);
			pwindo.getContentView().setTop(getSupportActionBar().getHeight());
			ListView list = (ListView) layout.findViewById(R.id.listpopup);
			ItemNotificacionesAdapter adapter = new ItemNotificacionesAdapter(this, data_servicio.Notificaciones);

			list.setAdapter(adapter);
			list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					Toast.makeText(con, "Clicked item " + position, Toast.LENGTH_SHORT).show();
					pwindo.dismiss();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Usuario_fluie usuario = null;
		try {
			usuario = db.getusuario();
		} catch (Exception e) {
			// TODO: handle
			// exception
		}
		if (usuario != null) {

			try {
				data_servicio.usuariofluie = usuario;
				login(usuario.getCorreo(), usuario.getPassword());
			} catch (UnsupportedEncodingException e) {
				// TODO
				// Auto-generated
				// catch
				// block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO
				// Auto-generated
				// catch
				// block
				e.printStackTrace();
			}

		}

	}

	public void login(final String email, final String password) throws JSONException, UnsupportedEncodingException {

		log_aplicacion.log_informacion("login");
		WS_cliente servicio = new WS_cliente(con);

		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {

					if (data_servicio.usuario.getRole_id().equals("1")) {

						if (db.getusuario() == null) {
							db.insertar_usuario(email, password);
						}
						WS_cliente servicio2;
						try {
							servicio2 = new WS_cliente(con);

							Handler puente = new Handler() {
								@SuppressLint("ShowToast")
								@Override
								public void handleMessage(Message msg) {
									if (msg.what == 1) {
										setimagen();
										set_space();
										get_notificaciones();

									} else if (msg.what == 0) {

									}

								}
							};
							servicio2.get_usuario(data_servicio.usuario.getUser_id());
							servicio2.setPuente_envio(puente);
							servicio2.execute();
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				} else if (msg.what == 0) {

					log_aplicacion.log_informacion("informacion " + data_servicio.msj_error);

					if (!data_servicio.msj_error.equals("")) {

						dialogo_modal.dialogo_cerrar_sesion(con.getString(R.string.error), data_servicio.msj_error, "",
								con.getString(R.string.aceptar), false, true);
						data_servicio.msj_error = "";
					}

				}
			}

		};
		servicio.login(email, password);
		servicio.setPuente_envio(puente);
		servicio.execute();

	}

	int borradores_subidos = 0;
	boolean mostrar_msg = true;

	public void subir_borradores() {
		mostrar_msg = true;

		PruebaInternet internet = new PruebaInternet(con);
		borradores_subidos = 0;

		if (internet.estadoConexion()) {
			Db_manager db = new Db_manager(con);
			ArrayList<Borrador> borradores = db.getborradores();

			if (borradores.size() > 0) {

				for (int j = 0; j < borradores.size(); j++) {

					Borrador borrador = borradores.get(j);

					ArrayList<Imagen> imagenes = new ArrayList<Imagen>();

					if (borrador.getType_file_id().equals("4")) {
						if (!borrador.getFile().equals("")) {
							String[] array_ben = borrador.getFile().split(",");

							if (array_ben.length != 0) {
								for (int i = 0; i < array_ben.length; i++) {

									imagenes.add(new Imagen(array_ben[i]));
								}
							}
						}

					}

					String beneficiarios = borrador.getBeneficiaries();
					String beneficiarios_envio = "";
					String[] array_ben = beneficiarios.split(",");
					if (array_ben.length != 0) {
						for (int i = 0; i < array_ben.length; i++) {

							String[] array = array_ben[i].split(":");

							Usuario usu = new Usuario(array[0]);
							usu.setName(array[1]);
							if (i < array_ben.length - 1) {
								beneficiarios_envio = array[0] + ",";
							} else {

								beneficiarios_envio = array[0];
							}

						}
					} else if (!beneficiarios.equals("")) {
						String[] array = beneficiarios.split(":");

						Usuario usu = new Usuario(array[0]);
						usu.setName(array[1]);
						beneficiarios_envio = array[0];

					}

					WS_cliente servicio;
					try {

						log_aplicacion.log_informacion("Tama�o espacio" + Double.valueOf(freespace) + " borrador"
								+ (double) Double.valueOf(borrador.getTamano()));
						if ((double) Double.valueOf(borrador.getTamano()) < (double) Double.valueOf(freespace)
								&& !borrador.getBeneficiaries().equals("")) {
							data_servicio.subiendo_borradores = true;
							if (mostrar_msg) {

								Toast.makeText(con, con.getString(R.string.upload_drafts), Toast.LENGTH_LONG).show();
								mostrar_msg = false;

							}
							servicio = new WS_cliente(con);
							servicio.crear_mensaje(borrador.getType_file_id(), "1", borrador.getTitle(),
									borrador.getDate_view(), borrador.getDescription(), borrador.getFile(),
									beneficiarios_envio, (ArrayList<Imagen>) imagenes.clone());
							Handler_envio puente = new Handler_envio(borrador, servicio);
							borradores_subidos++;
						}

					} catch (Exception e) {

					}

				}

			}

		}
	}

	class Handler_envio {
		Borrador borrador;

		WS_cliente ws;

		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {
					borradores_subidos--;
					try {
						if (borrador != null) {
							db.deleteborrador(String.valueOf(borrador.getId_borrador()));

						}
					} catch (Exception e) {
						// TODO: handle exception
					}

					if (borradores_subidos == 0) {
						data_servicio.subiendo_borradores = false;
					}

					// dialogo_modal.dialogo_cerrar("Informacion",
					// data_servicio.msj, "", "Aceptar", true, true);

				} else if (msg.what == 0) {
					borradores_subidos--;
					// dialogo_modal.dialogo("Informacion",
					// data_servicio.msj_error, "", "Aceptar", true, true);
					if (borradores_subidos == 0) {
						data_servicio.subiendo_borradores = false;
					}
				}

			}
		};

		public Handler_envio(Borrador borrador, WS_cliente ws) {
			super();
			this.borrador = borrador;
			this.ws = ws;
			ws.setPuente_envio(puente);
			ws.execute();

		}

		public Borrador getBorrador() {
			return borrador;
		}

		public void setBorrador(Borrador borrador) {
			this.borrador = borrador;
		}

		public Handler getPuente() {
			return puente;
		}

		public void setPuente(Handler puente) {
			this.puente = puente;
		}

	}

	public void setimagen() {
		try {
			if (!data_servicio.usuario.getImage().equals("")) {
				File f = new File(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
				if (f.exists()) {

					try {
						Bitmap bitmap;
						log_aplicacion.log_informacion("imagen " + data_servicio.usuario.getImage());
						bitmap = BitmapFactory
								.decodeFile(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
						bitmap.prepareToDraw();
						int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
						bitmap = Bitmap.createScaledBitmap(bitmap, 512, nh, true);

						imagen_perfil.setImageBitmap(bitmap);
						imagen_perfil.setScaleType(ImageView.ScaleType.CENTER_CROP);
						imagen_perfil_menu.setImageBitmap(bitmap);
						imagen_perfil_menu.setScaleType(ImageView.ScaleType.CENTER_CROP);
						String email = data_servicio.usuario.getEmail().substring(0, 13) + "...";

						text_correo.setText(email);

						text_pais.setText("");

						text_nombre
								.setText(data_servicio.usuario.getName() + " " + data_servicio.usuario.getName_last());

						try {
							JSONArray jarray = new JSONArray(data_servicio.json_paises);

							for (int i = 0; i < jarray.length(); i++) {
								if (jarray.getJSONObject(i).getString("idcountry")
										.equals(data_servicio.usuario.getCountry_id())) {

									text_pais.setText(jarray.getJSONObject(i).getString("country"));
								}

							}

						} catch (Exception e) {
							// TODO: handle exception
						}

						text_pais.setTypeface(tr);
						text_correo.setTypeface(tr);

					} catch (Exception e) {
						// TODO: handle exception
					}

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void cambio_estado() {

		for (int i = 0; i < data_servicio.Notificaciones.size(); i++) {

			if (data_servicio.Notificaciones.get(i).getView().equals("0")) {
				Notificacion notificacion = data_servicio.Notificaciones.get(i);
				WS_cliente servicio;
				try {
					servicio = new WS_cliente(con);

					Handler puente = new Handler() {
						@SuppressLint("ShowToast")
						@Override
						public void handleMessage(Message msg) {
							if (msg.what == 1) {

							} else if (msg.what == 0) {

							}

						}
					};
					servicio.setPuente_envio(puente);
					servicio.setnotificaciones_vista(notificacion.getIdnotification());
					servicio.execute();

				} catch (UnsupportedEncodingException | JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void set_space() {

		try {
			consultar.obtener_msgespacio();

			WS_cliente servicio3;

			try {
				servicio3 = new WS_cliente(con);

				Handler puente = new Handler() {
					@SuppressLint("ShowToast")
					@Override
					public void handleMessage(Message msg) {
						if (msg.what == 1) {

							ps.setProgress(100);
							int porcentajeint = 0;
							int espacio = 0;
							try {

								JSONArray jo = new JSONArray(data_servicio.json_freespace);

								freespace = jo.getJSONObject(0).getString("free");
								String space = jo.getJSONObject(0).getString("space");
								String size = jo.getJSONObject(0).getString("size");
								log_aplicacion.log_informacion("espacio  " + freespace);
								log_aplicacion.log_informacion("espacio  " + space);
								log_aplicacion.log_informacion("espacio  " + size);

								try {
									int sizeint = Integer.parseInt(size);
									int spaceint = Integer.parseInt(space);
									log_aplicacion.log_informacion("espacio  " + sizeint);
									log_aplicacion.log_informacion("espacio  " + spaceint);

									espacio = (int) (((double) sizeint / (double) spaceint) * 100);

									log_aplicacion.log_informacion("espacio  " + espacio);
									text_espacio.setText(getResources().getString(R.string.actualmente_tiene) + " "
											+ freespace + "Mb" + " "
											+ getResources().getString(R.string.disponibles_para_crear_mensajes));
								} catch (Exception e) {
									// TODO: handle exception

									log_aplicacion.log_informacion("error " + e.toString());

								}

								porcentaje.setText(espacio + "%");

							} catch (Exception e) {
								// TODO: handle exception

								log_aplicacion.log_informacion("error 2 " + e.toString());

							}
							if (android.os.Build.VERSION.SDK_INT >= 11) {
								// will update the "progress" propriety of
								// seekbar until it
								// reaches
								// progress
								ObjectAnimator animation = ObjectAnimator.ofInt(ps, "progress", espacio);
								animation.setDuration(3000); // 0.5 second
								animation.setInterpolator(new DecelerateInterpolator());
								animation.start();
							} else {
								ps.setProgress(espacio);
							}
							if (!data_servicio.subiendo_borradores) {
								subir_borradores();
							}

						} else if (msg.what == 0) {

						}

					}
				};
				servicio3.getfreespace();
				servicio3.setPuente_envio(puente);
				servicio3.execute();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				WS_cliente servicio;
				data_servicio.espacion_mensajes.clear();
				servicio = new WS_cliente(con);

				Handler puente = new Handler() {
					@SuppressLint("ShowToast")
					@Override
					public void handleMessage(Message msg) {
						if (msg.what == 1) {
							WS_cliente servicio2;
							try {
								servicio2 = new WS_cliente(con);

								Handler puente = new Handler() {
									@SuppressLint("ShowToast")
									@Override
									public void handleMessage(Message msg) {
										if (msg.what == 1) {

										} else if (msg.what == 0) {

										}

									}
								};
								servicio2.getespacio_consejos(data_servicio.usuario.getUser_id(), "3");
								servicio2.setPuente_envio(puente);
								servicio2.execute();
							} catch (UnsupportedEncodingException | JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else if (msg.what == 0) {

						}

					}
				};
				servicio.getespacio_consejos(data_servicio.usuario.getUser_id(), "2");
				servicio.setPuente_envio(puente);
				servicio.execute();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
			log_aplicacion.log_informacion("Error set espacio" + e.toString());

		}

	}

	public void get_notificaciones() {

		try {

			WS_cliente servicio;

			try {
				servicio = new WS_cliente(con);

				Handler puente = new Handler() {
					@SuppressLint("ShowToast")
					@Override
					public void handleMessage(Message msg) {
						if (msg.what == 1) {

							updateNotificationsBadge();

						} else if (msg.what == 0) {

						}

					}
				};
				servicio.getnotificaciones();
				servicio.setPuente_envio(puente);
				servicio.execute();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
			log_aplicacion.log_informacion("Error set espacio" + e.toString());

		}

	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_usuario, container, false);
			try {
				text_nombre = (TextView) rootView.findViewById(R.id.textviewnombre);

				text_pais = (TextView) rootView.findViewById(R.id.textviewpais);
				text_espacio = (TextView) rootView.findViewById(R.id.textespacio);
				frame = (FrameLayout) rootView.findViewById(R.id.layout_framae);
				data_servicio.frame = frame;
				text_correo = (TextView) rootView.findViewById(R.id.textviewcorreo);

				try {

					ps = (ProgressBar) rootView.findViewById(R.id.progressBarspace);
					porcentaje = (TextView) rootView.findViewById(R.id.textporcentaje);

					JSONArray jo = new JSONArray(data_servicio.json_freespace);

					String freespace = jo.getJSONObject(0).getString("free");
					String space = jo.getJSONObject(0).getString("space");

					text_espacio.setText(getResources().getString(R.string.actualmente_tiene) + " " + freespace + "Mb"
							+ " " + getResources().getString(R.string.disponibles_para_crear_mensajes));

				} catch (Exception e) {
					// TODO: handle exception
				}

				String email = data_servicio.usuario.getEmail().substring(0, 13) + "...";

				text_correo.setText(email);

				text_pais.setText("");

				text_nombre.setText(data_servicio.usuario.getName() + " " + data_servicio.usuario.getName_last());

				try {
					JSONArray jarray = new JSONArray(data_servicio.json_paises);

					for (int i = 0; i < jarray.length(); i++) {
						if (jarray.getJSONObject(i).getString("idcountry")
								.equals(data_servicio.usuario.getCountry_id())) {

							text_pais.setText(jarray.getJSONObject(i).getString("country"));
						}

					}

				} catch (Exception e) {
					// TODO: handle exception
				}

				text_pais.setTypeface(tr);
				text_correo.setTypeface(tr);
				imagen_perfil = (com.zonesoftware.fluie.CircularImageView) rootView.findViewById(R.id.img_perfil);

				File f = new File(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
				if (f.exists()) {

					try {
						Bitmap bitmap;
						log_aplicacion.log_informacion("imagen " + data_servicio.usuario.getImage());
						bitmap = BitmapFactory
								.decodeFile(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
						bitmap.prepareToDraw();
						int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
						bitmap = Bitmap.createScaledBitmap(bitmap, 512, nh, true);

						imagen_perfil.setImageBitmap(bitmap);

					} catch (Exception e) {
						// TODO: handle exception
					}

				}

				Button bn_his = (Button) rootView.findViewById(R.id.buttonhistorial);
				Button bn_mensaje = (Button) rootView.findViewById(R.id.btn_crear_mensaje);
				bn_his.setTypeface(tr);
				bn_mensaje.setTypeface(tr);
				bn_mensaje.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						data_servicio.opcion_mensaje = "crear";
						Intent intent = new Intent(act, MensajeActivity.class);
						act.startActivity(intent);
					}
				});
				bn_his.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						data_servicio.tipo_msg = "";
						data_servicio.tipo_historial = "mensajes";

						Intent intent = new Intent(act, AlmacenamientoActivity.class);
						startActivity(intent);

					}
				});
				LinearLayout buttoncomprar = (LinearLayout) rootView.findViewById(R.id.buttoncomprar);

				buttoncomprar.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						consultar.obtener_planes();

					}
				});

			} catch (Exception e) {
				// TODO: handle exception
				log_aplicacion.log_error("error" + e);
			}
			String email = data_servicio.usuario.getEmail().substring(0, 13) + "...";

			text_correo.setText(email);
			return rootView;
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			((UsuarioActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
		}
	}

}
