package com.zonesoftware.fluie.db;

public class Borrador {
	int id_borrador = 0;
	String type_file_id = "";
	String type_message_id = "";
	String title = "";
	String date_view = "";
	String description = "";
	String file = "";
	String id_usuario = "";
	String beneficiaries = "";
	String tamano = "";

	public Borrador(int id_borrador, String type_file_id, String type_message_id, String title, String date_view,
			String description, String file, String id_usuario, String beneficiaries, String tamano) {
		super();
		this.id_borrador = id_borrador;
		this.type_file_id = type_file_id;
		this.type_message_id = type_message_id;
		this.title = title;
		this.date_view = date_view;
		this.description = description;
		this.file = file;
		this.id_usuario = id_usuario;
		this.beneficiaries = beneficiaries;
		this.tamano = tamano;
	}

	public String getTamano() {
		return tamano;
	}

	public void setTamano(String tamano) {
		this.tamano = tamano;
	}

	public int getId_borrador() {
		return id_borrador;
	}

	public void setId_borrador(int id_borrador) {
		this.id_borrador = id_borrador;
	}

	public String getType_file_id() {
		return type_file_id;
	}

	public void setType_file_id(String type_file_id) {
		this.type_file_id = type_file_id;
	}

	public String getType_message_id() {
		return type_message_id;
	}

	public void setType_message_id(String type_message_id) {
		this.type_message_id = type_message_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate_view() {
		return date_view;
	}

	public void setDate_view(String date_view) {
		this.date_view = date_view;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getBeneficiaries() {
		return beneficiaries;
	}

	public void setBeneficiaries(String beneficiaries) {
		this.beneficiaries = beneficiaries;
	}

}
