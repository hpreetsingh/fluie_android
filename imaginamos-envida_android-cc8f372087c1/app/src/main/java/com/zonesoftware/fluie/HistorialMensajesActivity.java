package com.zonesoftware.fluie;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.zonesoftware.fluie.db.Borrador;
import com.zonesoftware.fluie.db.Db_manager;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import modelo.Imagen;
import modelo.Item_lista;
import modelo.ItemmsgAdapter;
import modelo.Usuario;
import servicio.WS_cliente;
import servicio.consultar_servicios;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class HistorialMensajesActivity extends ActionBarActivity {
	ActionBar mActionbar;
	Button bn_seleccion;
	ArrayAdapter<String> myAdapter;
	ItemmsgAdapter adapter;
	ListView listView;
	String[] dataArray = new String[] { "India", "Androidhub4you", "Pakistan", "Srilanka", "Nepal", "Japan" };
	Context con;
	Typeface tm, tr;
	Context micontext;
	DialogoCarga cargar;
	JSONArray jsonverificadores;
	ArrayList<Item_lista> items;
	String estado = "";
	TextView textmsg;
	JSONArray jsonmensajes;
	Db_manager db;
	int cargaimg = 0;
	int cargaimgres = 0;
	consultar_servicios consultar;
	ArrayList<Borrador> borradores = new ArrayList<Borrador>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_historialmsg);

		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}

		mActionbar = getSupportActionBar();

		mActionbar.setDisplayShowTitleEnabled(true);
		mActionbar.setTitle("");
		mActionbar.setDisplayHomeAsUpEnabled(true);
		mActionbar.setIcon(R.drawable.ic_atras);
		mActionbar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_atras));
		mActionbar.setDisplayUseLogoEnabled(false);
		tm = Typeface.createFromAsset(getAssets(), "font/Roboto_Medium.ttf");
		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		con = this;
		micontext = this;
		consultar = new consultar_servicios(this, this);

		try {
			db = new Db_manager(con);
		} catch (Exception e) {
			// TODO: handle exception
		}

		bn_seleccion = (Button) findViewById(R.id.bn_seleccion_lista);
		SearchView searchView = (SearchView) findViewById(R.id.autoCompletebuscar);
		listView = (ListView) findViewById(R.id.lista_personas);
		textmsg = (TextView) findViewById(R.id.textV_msg);

		textmsg.setTypeface(tr);

		// TextView text1 = (TextView) findViewById(R.id.text1_lista_historial);
		//
		// TextView text3 = (TextView) findViewById(R.id.text3_lista_historial);
		// text1.setTypeface(tr);
		//
		// text3.setTypeface(tr);
		cargar = new DialogoCarga(this, getString(R.string.consultando), getString(R.string.espere_un_momento_));

		items = new ArrayList<Item_lista>();

		adapter = new ItemmsgAdapter(this, items, true, this);

		listView.setAdapter(adapter);

		listView.setTextFilterEnabled(true);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setIconifiedByDefault(false);
		searchView.setQueryHint(getResources().getString(R.string.buscar_msg));
		SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				// this is your adapter that will be filtered

				adapter.getFilter().filter(newText);
				System.out.println("on text chnge text: " + newText);
				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				// this is your adapter that will be filtered
				adapter.getFilter().filter(query);
				System.out.println("on query submit: " + query);
				return true;
			}
		};
		searchView.setOnQueryTextListener(textChangeListener);

		bn_seleccion.setTypeface(tr);

		bn_seleccion.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				consultar.obtener_planes();

			}
		});
		try {

			if (data_servicio.tipo_historial.equals("mensajes")) {

				getlist();

			} else if (data_servicio.tipo_historial.equals("borradores")) {
				getlistborradores();

			} else {

				getlistborradores();
			}
			get_list_beneficiarios();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		GuardarInformacionApp.Guardar_info(outState);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in action bar clicked; goto parent activity.
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		log_aplicacion.log_informacion("ingresa onstart");
		set_space();

		if (data_servicio.tipo_historial.equals("mensajes")) {

			getlist();

		} else if (data_servicio.tipo_historial.equals("borradores")) {
			getlistborradores();

		} else {

			getlistborradores();
		}

	}

	public void set_space() {

		try {

			WS_cliente servicio3;

			try {
				servicio3 = new WS_cliente(micontext);

				Handler puente = new Handler() {
					@SuppressLint("ShowToast")
					@Override
					public void handleMessage(Message msg) {
						if (msg.what == 1) {

						} else if (msg.what == 0) {

						}

					}
				};
				servicio3.getfreespace();
				servicio3.setPuente_envio(puente);
				servicio3.execute();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			WS_cliente servicio;

			try {
				if (data_servicio.espacion_mensajes.size() == 0) {
					data_servicio.espacion_mensajes.clear();
					servicio = new WS_cliente(micontext);

					Handler puente = new Handler() {
						@SuppressLint("ShowToast")
						@Override
						public void handleMessage(Message msg) {
							if (msg.what == 1) {
								WS_cliente servicio2;
								try {
									servicio2 = new WS_cliente(micontext);

									Handler puente = new Handler() {
										@SuppressLint("ShowToast")
										@Override
										public void handleMessage(Message msg) {
											if (msg.what == 1) {

											} else if (msg.what == 0) {

											}

										}
									};
									servicio2.getespacio_consejos(data_servicio.usuario.getUser_id(), "3");
									servicio2.setPuente_envio(puente);
									servicio2.execute();
								} catch (UnsupportedEncodingException | JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else if (msg.what == 0) {

							}

						}
					};
					servicio.getespacio_consejos(data_servicio.usuario.getUser_id(), "2");
					servicio.setPuente_envio(puente);
					servicio.execute();
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
			log_aplicacion.log_informacion("Error set espacio" + e.toString());

		}

	}

	public void getlist() {
		data_servicio.mensaje_edicion = "borrador";
		items.clear();
		WS_cliente servicio1;
		try {
			servicio1 = new WS_cliente(micontext);
			cargar.mostrar();

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						try {

							JSONArray jsonmensajes = new JSONArray(data_servicio.json_historial);
							items.clear();
							for (int j = 0; j < jsonmensajes.length(); j++) {

								JSONArray jsonfiles = new JSONArray(
										jsonmensajes.getJSONObject(j).getString("messageFiles"));
								String size = "";
								try {
									size = jsonfiles.getJSONObject(0).getString("size");
								} catch (Exception e) {
									// TODO: handle exception
								}

								items.add(new Item_lista("", jsonmensajes.getJSONObject(j).getString("title"),
										jsonmensajes.getJSONObject(j).getString("date_created"), size,
										jsonmensajes.getJSONObject(j).getString("idmessage"),""));

							}

							if (data_servicio.espacion_mensajes.size() > 0) {
								int size = 0;
								for (int i = 0; i < data_servicio.espacion_mensajes.size(); i++) {
									try {

										JSONArray arreglo = new JSONArray(
												data_servicio.espacion_mensajes.get(i).toString());

										JSONObject tamano_msg = arreglo.getJSONObject(0);
										size += Integer.valueOf(tamano_msg.getString("size"));

									} catch (Exception e) {
										// TODO: handle exception
									}

								}
								Item_lista item = new Item_lista("", "", "", String.valueOf(size) + "MB", "","");
								item.setTipo("consejos");
								items.add(item);
							} else {
								Item_lista item = new Item_lista("", "", "", "0MB", "","");
								item.setTipo("consejos");
								items.add(item);

							}

							adapter.setitems(items);

							adapter.notifyDataSetChanged();
							cargar.ocultar();
						} catch (Exception e) {
							// TODO: handle exception
						}

					} else if (msg.what == 0) {
						cargar.ocultar();
						if (!data_servicio.msj_error.equals("")) {

							data_servicio.msj_error = "";
						}

					}

				}
			};
			servicio1.get_historial();
			servicio1.setPuente_envio(puente);
			servicio1.execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void getlistborradores() {
		cargar.ocultar();
		log_aplicacion.log_informacion("borradores");
		items.clear();
		borradores.clear();
		borradores = db.getborradores(data_servicio.tipo_msg);

		for (int j = 0; j < borradores.size(); j++) {

			String tamanio = borradores.get(j).getTamano();
			if (tamanio.contains("MB")) {

				tamanio.substring(0, tamanio.indexOf("MB") - 2);
			} else {
				tamanio = tamanio + "MB";
			}

			items.add(new Item_lista("", borradores.get(j).getTitle(), borradores.get(j).getDate_view(), tamanio,
					String.valueOf(borradores.get(j).getId_borrador()),""));

		}

		adapter.setitems(items);

		adapter.notifyDataSetChanged();

	}

	public void get_list_beneficiarios() {

		if (data_servicio.lista_beneficiario.size() == 0) {

			data_servicio.lista_beneficiario = new ArrayList<Usuario>();
			data_servicio.lista_beneficiario.clear();
			WS_cliente servicio1;
			try {
				servicio1 = new WS_cliente(micontext);

				Handler puente = new Handler() {
					@SuppressLint("ShowToast")
					@Override
					public void handleMessage(Message msg) {
						if (msg.what == 1) {
							try {
								jsonverificadores = new JSONArray(data_servicio.json_lista_beneficiario);
								if (jsonverificadores.length() == 0) {

								} else {
									for (int i = 0; i < jsonverificadores.length(); i++) {

										data_servicio.usuario_beneficiario.clear();
										String id = jsonverificadores.getJSONObject(i).getString("beneficiary_id");
										data_servicio.usuario_beneficiario.setRelacion(
												jsonverificadores.getJSONObject(i).getString("relationship_id"));

										log_aplicacion.log_informacion("Relacion leida"
												+ jsonverificadores.getJSONObject(i).getString("relationship_id"));

										try {
											boolean imagen = true;

											Imagen image_usuario = null;
											for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
												if (data_servicio.imagen_usuarios.get(l).getId_user().equals(id)) {

													imagen = false;
													break;
												}
											}
											if (imagen) {
												WS_cliente servicio4;
												cargaimg++;
												try {

													servicio4 = new WS_cliente(micontext);

													Handler puente4 = new Handler() {
														@SuppressLint("ShowToast")
														@Override
														public void handleMessage(Message msg) {
															if (msg.what == 1) {

																cargaimgres++;
																if (cargaimg == cargaimgres) {

																	cargarimagenes();
																}

															} else if (msg.what == 0) {
																cargaimgres++;
																if (cargaimg == cargaimgres) {

																	cargarimagenes();
																}

															}

														}
													};
													servicio4.get_imagen_user(id);
													servicio4.setPuente_envio(puente4);
													servicio4.execute();
												} catch (Exception e) {
													// TODO: handle exception
												}

											}

										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} else if (msg.what == 0) {
							cargar.ocultar();
							if (!data_servicio.msj_error.equals("")) {

								data_servicio.msj_error = "";
							}

						}

					}
				};
				servicio1.get_lista_beneficiario(data_servicio.usuario.getUser_id());
				servicio1.setPuente_envio(puente);
				servicio1.execute();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void cargarimagenes() {
		Imagen image_usuario = null;
		for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {

			if (data_servicio.imagen_usuarios.get(l).getImagen() == null) {
				guardarImagen(data_servicio.imagen_usuarios.get(l));
			}
		}

	}

	private void guardarImagen(final Imagen image_usuario) {
		// File dirImages = cw.getDir("perfil", Context.MODE_PRIVATE);
		new AsyncTask<Void, Void, String>() {

			int height;
			String tiempo = "";
			Bitmap bitmap = null;

			@Override
			protected String doInBackground(Void... params) {

				String msg = "";
				URL url;
				try {
					url = new URL(image_usuario.getUrl());

					try {
						bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

						image_usuario.setImagen(bitmap);
						image_usuario.setActualizar(false);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return msg;
			}

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();

			}

			@Override
			protected void onProgressUpdate(Void... values) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void onPostExecute(String msg) {
				log_aplicacion.log_informacion("onpostexcecute");
				try {

					if (bitmap != null) {
						Imagen image = null;

						for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
							if (data_servicio.imagen_usuarios.get(l).getId_user().equals(image_usuario.getId_user())) {

								image = data_servicio.imagen_usuarios.get(l);
								break;
							}
						}
						image.setImagen(bitmap);
						image.setActualizar(false);
						adapter.notifyDataSetChanged();

					}
				} catch (Exception e) {
					// TODO: handle exception
					log_aplicacion.log_informacion("onpostexcecute error " + e.toString());

				}
			}

		}.execute(null, null, null);

	}
}
