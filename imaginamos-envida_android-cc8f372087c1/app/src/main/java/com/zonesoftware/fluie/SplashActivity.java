package com.zonesoftware.fluie;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.zonesoftware.fluie.db.DbHelper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import modelo.Imagen;
import servicio.WS_cliente;
import servicio.consultar_servicios;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class SplashActivity extends Activity {
	ImageView splashImageView;
	Context con;
	consultar_servicios init = null;
	AnimationDrawable animation;
	ArrayList<Imagen> imagenes = new ArrayList<Imagen>();
	JSONArray array;
	AnimationDrawable frameAnimation;
	int ik;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		init = new consultar_servicios(this, this);

		splashImageView = (ImageView) findViewById(R.id.imageV_splash);
		try {
			DbHelper dbhelp = new DbHelper(this);
			con = this;
			splash();
			String state = Environment.getExternalStorageState();

			if (Environment.MEDIA_MOUNTED.equals(state)) {
				// podemos escribir y leer en la memoria externa
				// Creo el directoio para guardar el fichero
				File dir = new File(Environment.getExternalStorageDirectory() + "/Fluie/");

				// si el direcctorio no existe, lo creo
				if (!dir.exists()) {

					dir.mkdir();
					File dir2 = new File(Environment.getExternalStorageDirectory() + "/Fluie/msg");
					if (!dir2.exists()) {

						dir2.mkdir();

					}

					File dir3 = new File(Environment.getExternalStorageDirectory() + "/Fluie/video");
					if (!dir3.exists()) {

						dir3.mkdir();

					}

				} else {
					File dir2 = new File(Environment.getExternalStorageDirectory() + "/Fluie/msg");
					if (!dir2.exists()) {

						dir2.mkdir();

					}
				}

			} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
				// solo podemos leer la memoria externa

			} else {
				// algo salio mal. no puedes leer ni escribir o no esta

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void splash() {

		WS_cliente servicio;
		try {
			servicio = new WS_cliente(con);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						try {
							animacion();

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else if (msg.what == 0) {

						Toast.makeText(con, con.getString(R.string.verifique_la_conexion_a_internet), Toast.LENGTH_LONG)
								.show();
					}

				}
			};
			servicio.setPuente_envio(puente);
			servicio.getsplash();
			servicio.execute();

		} catch (UnsupportedEncodingException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void animacion() {

		if (!data_servicio.jsonsplash.equals("")) {

			try {
				array = new JSONArray(data_servicio.jsonsplash);

				for (ik = 0; ik < array.length(); ik++) {
					Imagen imagen = new Imagen(array.getJSONObject(ik).getString("image"));
					if(array.length()==ik+1) {
						guardarImagen(imagen, true);
					}else{
						guardarImagen(imagen, false);
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	@SuppressLint("NewApi")
	public void cargar_animacion() {

		animation = new AnimationDrawable();

		for (int i = 0; i < imagenes.size(); i++) {
			Drawable drawable = new BitmapDrawable(imagenes.get(i).getImagen());


			animation.addFrame(drawable, 2000);

		}
//		Drawable myIcon = getResources().getDrawable( R.drawable.slider );
//		Drawable myIcon1 = getResources().getDrawable( R.drawable.slider_ );
//		animation.addFrame(myIcon, 2000);


		splashImageView.setImageDrawable(animation);
		splashImageView.setScaleType(ScaleType.CENTER_CROP);

		frameAnimation = (AnimationDrawable) splashImageView.getBackground();

		// splashImageView.post(new Runnable() {
		// @Override
		// public void run() {
		// animation.start();
		//
		// }
		// });

		splashImageView.post(new Runnable() {

			@Override
			public void run() {
				animation.start();
			}
		});

		try {
			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					try {
						init.iniciar_home();
					} catch (UnsupportedEncodingException | JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}, 1500);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void startactivity() {
		Intent intent = new Intent(this, BienvenidaActivity.class);

		startActivity(intent);
		finish();
	}

	private void guardarImagen(final Imagen image_splash, final boolean lastarray) {
		// File dirImages = cw.getDir("perfil", Context.MODE_PRIVATE);
		new AsyncTask<Void, Void, String>() {
			int height;
			String tiempo = "";
			Bitmap bitmap = null;

			@Override
			protected String doInBackground(Void... params) {

				String msg = "";
				URL url;
				try {
					url = new URL(image_splash.getUrl());
					try {
						bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
						image_splash.setImagen(bitmap);
						imagenes.add(image_splash);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return msg;
			}

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();

			}

			@Override
			protected void onProgressUpdate(Void... values) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void onPostExecute(String msg) {
				log_aplicacion.log_informacion("onpostexcecute");
				try {
					if (lastarray) {
						cargar_animacion();

					}

				} catch (Exception e) {
					// TODO: handle exception
					log_aplicacion.log_informacion("onpostexcecute error " + e.toString());

				}
			}

		}.execute(null, null, null);

	}


}
