package com.zonesoftware.fluie;

import android.os.Bundle;
import servicio.Parseo_json;
import servicio.data_servicio;

public class GuardarInformacionApp {

	public static void Guardar_info(Bundle outState) {
		outState.putString("jsonhome", data_servicio.jsonhome);

		outState.putString("json_list_documentos", data_servicio.json_list_documentos);
		outState.putString("jsonlogin", data_servicio.jsonlogin);
		outState.putString("json_paises", data_servicio.json_paises);
		outState.putString("json_generos", data_servicio.json_generos);
		outState.putString("json_espacio_msg", data_servicio.json_espacio_msg);
		outState.putString("json_planes", data_servicio.json_planes);
		outState.putString("json_compra", data_servicio.json_compra);
		outState.putString("json_historial", data_servicio.json_historial);
		outState.putString("json_tipo_relacion", data_servicio.json_tipo_relacion);
		outState.putString("json_lista_beneficiario", data_servicio.json_lista_beneficiario);
		outState.putString("json_usuario", data_servicio.json_usuario);

		outState.putString("tipo_msg", data_servicio.tipo_msg);
		outState.putString("tipo_historial", data_servicio.tipo_historial);
		outState.putString("json_freespace", data_servicio.json_freespace);
		outState.putString("email_usuario", data_servicio.email_usuario);

		outState.putString("terms_conditions", data_servicio.terms_conditions);
		outState.putString("lista", data_servicio.lista);
		outState.putString("editar", data_servicio.editar);

		outState.putString("opcion", data_servicio.opcion);
		outState.putString("id_seleccion", data_servicio.id_seleccion);
		outState.putInt("seleccion_edicion", data_servicio.seleccion_edicion);
		outState.putString("seleccion_beneficiario", data_servicio.seleccion_beneficiario);
		outState.putString("id_beneficiario", data_servicio.id_beneficiario);
		outState.putString("tipo_mensaje", data_servicio.tipo_mensaje);

	}

	public static void Cargar_info(Bundle savedInstanceState) {

		data_servicio.jsonhome = savedInstanceState.getString("jsonhome");
		data_servicio.jsonlogin = savedInstanceState.getString("jsonlogin");
		data_servicio.json_list_documentos = savedInstanceState.getString("json_list_documentos");

		data_servicio.json_paises = savedInstanceState.getString("json_paises");

		data_servicio.json_generos = savedInstanceState.getString("json_generos");
		data_servicio.json_espacio_msg = savedInstanceState.getString("json_espacio_msg");
		data_servicio.json_planes = savedInstanceState.getString("json_planes");
		data_servicio.json_compra = savedInstanceState.getString("json_compra");
		data_servicio.json_historial = savedInstanceState.getString("json_historial");
		data_servicio.json_tipo_relacion = savedInstanceState.getString("json_lista_beneficiario");
		data_servicio.json_lista_beneficiario = savedInstanceState.getString("json_lista_beneficiario");
		data_servicio.json_lista_verificadores = savedInstanceState.getString("json_lista_verificadores");

		data_servicio.json_usuario = savedInstanceState.getString("json_usuario");

		Parseo_json parse = new Parseo_json();
		parse.parseo_Consultar_home(data_servicio.jsonhome);
		parse.parseo_login(data_servicio.jsonlogin);

		parse.parseo_get_usuario(data_servicio.json_usuario);

		// public static ArrayList<Usuario> lista_beneficiario = new
		// ArrayList<Usuario>();
		// public static ArrayList<Usuario> lista_verificadores = null;
		data_servicio.tipo_msg = savedInstanceState.getString("tipo_msg");
		data_servicio.tipo_historial = savedInstanceState.getString("tipo_historial");

		// public static ArrayList<Item_lista> lista_seleccion_ben = null;

		data_servicio.json_freespace = savedInstanceState.getString("json_freespace");

		// public static Home home = new Home();
		// public static Usuario usuario = new Usuario();
		data_servicio.terms_conditions = savedInstanceState.getString("terms_conditions");
		data_servicio.msj_error = savedInstanceState.getString("msj_error");
		data_servicio.msj = savedInstanceState.getString("msj");
		data_servicio.email_usuario = savedInstanceState.getString("email_usuario");

		///////////////////////////////////

		data_servicio.lista = savedInstanceState.getString("lista");
		data_servicio.editar = savedInstanceState.getString("editar");
		data_servicio.opcion = savedInstanceState.getString("opcion");
		data_servicio.id_seleccion = savedInstanceState.getString("id_seleccion");
		data_servicio.seleccion_edicion = savedInstanceState.getInt("seleccion_edicion");
		data_servicio.seleccion_beneficiario = savedInstanceState.getString("seleccion_beneficiario");

		/////////////////// beneficiario////////////////
		data_servicio.id_beneficiario = savedInstanceState.getString("id_beneficiario");

		data_servicio.tipo_mensaje = savedInstanceState.getString("tipo_mensaje");

	}

}
