package com.zonesoftware.fluie;

import java.io.UnsupportedEncodingException;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import servicio.WS_cliente;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class RecuperaContrasenaActivity extends ActionBarActivity {

	EditText correo;
	Validacion_campos validar_campos;
	Context con;
	DialogoCarga cargar;
	Dialogo dialogo_modal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_recupera_contrasena);
		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setIcon(R.drawable.ic_atras);
		actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_atras));
		actionBar.setDisplayUseLogoEnabled(false);
		con = this;
		cargar = new DialogoCarga(this, getString(R.string.enviando), getString(R.string.espere_un_momento_));

		dialogo_modal = new Dialogo(this, this);
		Typeface tm, tr;
		tm = Typeface.createFromAsset(getAssets(), "font/Roboto_Medium.ttf");
		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		TextView text_recuperar = (TextView) findViewById(R.id.textrecuparcion);
		validar_campos = new Validacion_campos();
		text_recuperar.setTypeface(tr);
		correo = (EditText) findViewById(R.id.editemail);

		Button btn_recuperar = (Button) findViewById(R.id.buttonrecuperar);

		btn_recuperar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (validar()) {
					try {
						recuperarcontrasena(correo.getText().toString());
					} catch (UnsupportedEncodingException | JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		});

	}

	public void recuperarcontrasena(String email) throws JSONException, UnsupportedEncodingException {

		log_aplicacion.log_informacion("login");
		WS_cliente servicio = new WS_cliente(con);
		cargar.mostrar();
		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {
					cargar.ocultar();
					if (!data_servicio.msj.equals("")) {
						dialogo_modal.dialogo_cerrar(getString(R.string.informacion), data_servicio.msj, "",
								getString(R.string.aceptar), true, false);
						data_servicio.msj_error = "";
					}

				} else if (msg.what == 0) {
					cargar.ocultar();
					if (!data_servicio.msj.equals("")) {
						dialogo_modal.dialogo(getString(R.string.error), data_servicio.msj, "",
								getString(R.string.aceptar), true, false);
						data_servicio.msj_error = "";
					}

				}

			}
		};
		servicio.recuperar_contrasena(email);
		servicio.setPuente_envio(puente);
		servicio.execute();

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		GuardarInformacionApp.Guardar_info(outState);

	}

	public boolean validar() {
		boolean campos_validos = true;
		Validacion_campos validar = new Validacion_campos();

		if (validar.validar_email(correo.getText().toString())) {

			correo.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			correo.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}

		if (!campos_validos) {
			Toast.makeText(this, getString(R.string.verifique_el_correo), Toast.LENGTH_LONG).show();
		}

		return campos_validos;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in action bar clicked; goto parent activity.
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
