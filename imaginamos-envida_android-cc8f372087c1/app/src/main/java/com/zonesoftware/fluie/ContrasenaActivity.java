package com.zonesoftware.fluie;

import java.io.File;
import java.io.UnsupportedEncodingException;

import org.json.JSONException;

import com.zonesoftware.fluie.db.Db_manager;
import com.zonesoftware.fluie.db.Usuario_fluie;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import servicio.WS_cliente;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class ContrasenaActivity extends ActionBarActivity {
	DialogoCarga cargar;
	Dialogo dialogo_modal;
	Context micontext;
	EditText contrasena_ant, contrasena_nueva1, contrasena_nueva2;
	Typeface tm, tr;
	static CircularImageView imagen_perfil_menu;
	Db_manager db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_cambio_contrasena);
		ActionBar actionBar = getSupportActionBar();

		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setIcon(R.drawable.ic_x);
		actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_x));
		actionBar.setDisplayUseLogoEnabled(false);

		dialogo_modal = new Dialogo(this, this);
		micontext = this;
		db = new Db_manager(micontext);
		cargar = new DialogoCarga(this, getString(R.string.enviando), getString(R.string.espere_un_momento_));

		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Medium.ttf");
		tm = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		imagen_perfil_menu = (CircularImageView) findViewById(R.id.img_perfil);
		if (!data_servicio.usuario.getImage().equals("")) {
			File f = new File(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
			if (f.exists()) {
				decodeBitmapmenu(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
			}
		}
		contrasena_ant = (EditText) findViewById(R.id.editcontrasena_anterios);
		contrasena_nueva1 = (EditText) findViewById(R.id.editcontrasena1);
		contrasena_nueva2 = (EditText) findViewById(R.id.editcontrasena2);
		contrasena_ant.setTypeface(tm);
		contrasena_nueva2.setTypeface(tm);
		contrasena_nueva1.setTypeface(tm);
		Button guardar = (Button) findViewById(R.id.buttonguardar);

		guardar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
				if (validar()) {
					try {
						updatecontrasena(data_servicio.usuario.getEmail(), contrasena_nueva1.getText().toString());
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		});

	}

	public static Bitmap decodeBitmapmenu(String dir) {
		Bitmap bitmap;

		bitmap = BitmapFactory.decodeFile(dir);
		bitmap.prepareToDraw();
		int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
		bitmap = Bitmap.createScaledBitmap(bitmap, 512, nh, true);

		imagen_perfil_menu.setImageBitmap(bitmap);
		imagen_perfil_menu.setScaleType(ImageView.ScaleType.CENTER_CROP);

		// RectF drawableRect = new RectF ( 0 , 0 , 1080 ,1920 );
		// RectF viewRect = new RectF ( 0 , 0 , 100 , 100 );
		// Matrix matrix=new Matrix();
		// matrix.setRectToRect ( drawableRect , viewRect , Matrix . ScaleToFit
		// . CENTER );
		return bitmap;

	}

	public boolean validar() {
		boolean campos_validos = true;
		Validacion_campos validar = new Validacion_campos();

		if (validar.validar_pass(contrasena_ant.getText().toString())) {

			contrasena_ant.setBackground(getResources().getDrawable(R.drawable.edit_text));

		} else {

			campos_validos = false;
			contrasena_ant.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}

		if (validar.validar_pass(contrasena_nueva1.getText().toString())) {

			contrasena_nueva1.setBackground(getResources().getDrawable(R.drawable.edit_text));

		} else {

			campos_validos = false;
			contrasena_nueva1.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}

		if (contrasena_nueva1.getText().toString().equals(contrasena_nueva2.getText().toString())) {

			contrasena_nueva1.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			contrasena_nueva1.setBackground(getResources().getDrawable(R.drawable.edit_text_error));
			Toast.makeText(this, getString(R.string.las_contrase_as_no_coinsiden), Toast.LENGTH_LONG).show();
		}
		if (!campos_validos) {
			Toast.makeText(this, getString(R.string.verifique_los_campos_existen_campos_erroneos_), Toast.LENGTH_LONG)
					.show();
		}
		return campos_validos;
	}

	public void updatecontrasena(String email, final String password)
			throws JSONException, UnsupportedEncodingException {

		log_aplicacion.log_informacion("changePassword");
		WS_cliente servicio = new WS_cliente(micontext);
		cargar.mostrar();
		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {
					cargar.ocultar();
					Usuario_fluie usuario = null;
					try {
						usuario = db.getusuario();
					} catch (Exception e) {
						// TODO:
						// handle
						// exception
					}
					if (usuario != null) {
						try {
							usuario.setPassword(password);
							db.updateusuario(usuario);

						} catch (Exception e) {
							// TODO: handle exception
						}
					}

					if (!data_servicio.msj_error.equals("")) {
						dialogo_modal.dialogo(getString(R.string.informacion), data_servicio.msj_error, "",
								getString(R.string.aceptar), true, false);
						data_servicio.msj_error = "";
					}
					backscreen();
				} else if (msg.what == 0) {
					cargar.ocultar();
					if (!data_servicio.msj_error.equals("")) {
						dialogo_modal.dialogo(getString(R.string.error), data_servicio.msj_error, "",
								getString(R.string.aceptar), true, false);
						data_servicio.msj_error = "";
					}

				}

			}
		};
		servicio.changePassword(email, password);
		servicio.setPuente_envio(puente);
		servicio.execute();

	}

	private void backscreen() {
		this.finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in action bar clicked; goto parent activity.
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
