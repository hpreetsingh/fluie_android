package com.zonesoftware.fluie;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;

import com.github.kayvannj.permission_utils.Func2;
import com.github.kayvannj.permission_utils.PermissionUtil;
import com.zonesoftware.fluie.db.Borrador;
import com.zonesoftware.fluie.db.Db_manager;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import modelo.Custom_scroll;
import modelo.FileUtils;
import modelo.Imagen;
import modelo.Mensaje;
import modelo.Usuario;
import servicio.PruebaInternet;
import servicio.WS_cliente;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class MensajeActivity extends ActionBarActivity {

	private PermissionUtil.PermissionRequestObject mBothPermissionRequest;
	LinearLayout layout;
	LayoutInflater inflater;
	ImageButton bn_video;
	ImageButton bn_texto;
	ImageButton bn_img;
	ImageButton bn_voz;
	String archivo_texto = "";
	SeekBar barra_grabacion;
	String archivo_video = "";
	String archivo_grabacion = "";
	Uri grabacion = null;
	String seleccion = "";
	private MediaRecorder myRecorder;
	static MediaPlayer myPlayer = null;
	String url_grabacion_archivo = "";
	private String outputFile = null;
	final int PHOTO_CODE = 800;
	final int SELECT_PICTURE = 200;
	final int YOUR_RESULT_CODE = 300;
	final int ACTION_TAKE_VIDEO = 400;
	final int RECORD_REQUEST = 500;
	final int SELECT_VIDEO = 600;
	final int GRABACION_FILE = 900;

	String APP_DIRECTORY = "myPictureApp/";
	String MEDIA_DIRECTORY = APP_DIRECTORY + "media";
	String TEMPORAL_PICTURE_NAME = "temporal.jpg";
	AlertDialog alertDialog = null;
	Typeface tm, tr;
	Context context;
	TextView text_archivo;
	EditText edit_msg;
	ArrayList<Usuario> lista_beneficiario_msg = null;
	ArrayList<Imagen> imagenes = new ArrayList<Imagen>();
	LinearLayout layout_ben;
	ImageView btn_add;
	Spinner sp_programar;
	EditText asunto, fecha;
	DialogoCarga cargar;
	String url_capture = "";
	Dialogo dialogo_modal;
	View Vimagen, Vimagen_previa, Vtexto, Vvideo, Vaudio, Vreproductor, layoutvideo;
	Bitmap imagen;
	Handler timerHandler = new Handler();
	TextView tiempo;
	Runnable timerRunnable;
	Uri uriVideo;
	Db_manager db;
	int espacio_libre = 0;
	Borrador borrador = null;
	Mensaje mensaje = null;
	TextView triangulo_video, triangulo_audio, triangulo_texto, triangulo_imagen;
	DatePickerDialog midialogo_picker;
	boolean focus_fecha = false;
	PruebaInternet prueba_internet = null;
	int space = 0;
	LinearLayout.LayoutParams layoutparams_msg;
	Uri videoUri;
	MediaController mediacontroller;
	ProgressBar progress;
	TextView texto_agregar;
	String fecha_envio = "";
	Custom_scroll scroll_horizontal;

	static class Viewimagenes {

		public ImageView btn_adjuntar;
		public ImageView btn_foto;
		public LinearLayout lay_imagenes;

	}

	static class Viewvideo {

		public VideoView video = null;

	}

	Viewvideo viewvideo = null;
	Viewimagenes viewimagen = null;
	ProgressBar progressBar1;
	TextView txprogress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mensaje);

		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}

		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setIcon(R.drawable.ic_atras);
		actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_atras));
		actionBar.setDisplayUseLogoEnabled(false);
		context = this;
		prueba_internet = new PruebaInternet(this);
		try {

			if (data_servicio.lista_beneficiario.size() == 0) {

				data_servicio.items.clear();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		viewvideo = new Viewvideo();
		db = new Db_manager(context);
		dialogo_modal = new Dialogo(this, this);
		lista_beneficiario_msg = new ArrayList<Usuario>();
		cargar = new DialogoCarga(this, getString(R.string.enviando),
				getString(R.string.la_subida_de_archivos_puede_demorar_un_poco_));
		imagenes = new ArrayList<Imagen>();
		tm = Typeface.createFromAsset(getAssets(), "font/Roboto_Medium.ttf");
		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		layout = (LinearLayout) findViewById(R.id.layout_contenedor);
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		outputFile = Environment.getExternalStorageDirectory() + "/Fluie/msg";
		myRecorder = new MediaRecorder();
		myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		myRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		myRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
		myRecorder.setOutputFile(outputFile + "/tmpaudio.3gpp");
		myRecorder.setMaxDuration(120000);
		texto_agregar = (TextView) findViewById(R.id.textAgregar);
		texto_agregar.setTypeface(tr);
		asunto = (EditText) findViewById(R.id.editasunto);

		asunto.setTypeface(tr);

		fecha = (EditText) findViewById(R.id.editText2);
		fecha.setTypeface(tr);
		pickerfecha();
		layoutparams_msg = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		layoutparams_msg.gravity = Gravity.BOTTOM;

		fecha.setInputType(InputType.TYPE_NULL);
		fecha.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (focus_fecha) {

					mostrar_picker();
				}

			}
		});

		fecha.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				focus_fecha = hasFocus;
				if (hasFocus) {

					mostrar_picker();

				}
			}
		});

		Button bn_guardar = (Button) findViewById(R.id.button_guardar);

		bn_guardar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				log_aplicacion.log_informacion("tipo mensaje" + data_servicio.tipo_mensaje);

				if (!asunto.getText().toString().equals("")
						&& (lista_beneficiario_msg.size() > 0 || !prueba_internet.estadoConexion()
								|| data_servicio.mensaje_edicion.equals("borrador"))
						&& (((!archivo_grabacion.equals("") && seleccion.equals("voz"))
								|| (!archivo_video.equals("") && seleccion.equals("video"))
								|| (imagenes.size() > 0 && seleccion.equals("imagen"))) || (seleccion.equals("texto")))
						|| (data_servicio.opcion_mensaje.equals("editar"))) {

					if ((!fecha.getText().toString().equals("") && sp_programar.getSelectedItemPosition() == 1)
							|| sp_programar.getSelectedItemPosition() == 0) {

						log_aplicacion.log_informacion("enviar msg");
						Enviar_mensaje();

					} else {

						Toast.makeText(context,
								getResources().getString(R.string.verifique_la_programacion_del_mensaje),
								Toast.LENGTH_SHORT).show();

					}
				} else {

					Toast.makeText(context, getResources().getString(R.string.verifique_los_campos_y_el_archivo_),
							Toast.LENGTH_SHORT).show();
				}

			}
		});

		bn_video = (ImageButton) findViewById(R.id.imageBut_video);
		bn_voz = (ImageButton) findViewById(R.id.imageBut_voz);
		bn_texto = (ImageButton) findViewById(R.id.imageBut_texto);
		bn_img = (ImageButton) findViewById(R.id.imageBut_imagen);
		layout_ben = (LinearLayout) findViewById(R.id.layout_beneficiario);
		triangulo_video = (TextView) findViewById(R.id.triangulo_video);
		triangulo_audio = (TextView) findViewById(R.id.triangulo_audio);
		triangulo_texto = (TextView) findViewById(R.id.triangulo_texto);
		triangulo_imagen = (TextView) findViewById(R.id.triangulo_imagen);

		sp_programar = (Spinner) findViewById(R.id.spinner_programar);

		// ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
		// R.layout.spinner, lista_programar);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				if (position == getCount()) {
					((TextView) v.findViewById(android.R.id.text1)).setText("");
					((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); // "Hint
				}

				return v;
			}

			@Override
			public int getCount() {
				return super.getCount() - 1; // you dont display last item. It
												// is used as hint.
			}

		};

		adapter.setDropDownViewResource(R.layout.spinner2);

		adapter.add(getString(R.string.ausencia));

		adapter.add(getString(R.string.fecha));
		adapter.add(getString(R.string.programar));
		// ArrayList<String> lista_programar = new ArrayList<String>();
		// lista_programar.add(getString(R.string.programar));
		// lista_programar.add(getString(R.string.ausencia));
		//
		// lista_programar.add(getString(R.string.fecha));
		//

		sp_programar.setAdapter(adapter);
		sp_programar.setSelection(2);

		// dataAdapter.setDropDownViewResource(R.layout.spinner2);
		// sp_programar.setAdapter(dataAdapter);

		fecha.setVisibility(View.INVISIBLE);
		sp_programar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				// your code here
				if (position == 0) {
					fecha.setText("");
					fecha.setVisibility(View.INVISIBLE);
					fecha_envio = "";
				}
				if (position == 1) {

					fecha.setVisibility(View.VISIBLE);

				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});

		try {
			//
			// progress = (ProgressBar) findViewById(R.id.progressBar1);
			//
			JSONArray jo = new JSONArray(data_servicio.json_freespace);

			String freespace = jo.getJSONObject(0).getString("free");

			String size = jo.getJSONObject(0).getString("size");

			try {
				space = Integer.parseInt(freespace);

			} catch (Exception e) {
				// TODO: handle exception

				log_aplicacion.log_informacion("error " + e.toString());

			}
			// if (android.os.Build.VERSION.SDK_INT >= 11) {
			//
			// ObjectAnimator animation = ObjectAnimator.ofInt(progress,
			// "progress", espacio);
			// animation.setDuration(3000); // 0.5 second
			// animation.setInterpolator(new DecelerateInterpolator());
			// animation.start();
			// } else {
			// progress.setProgress(espacio);
			// }

		} catch (Exception e) {
			// TODO: handle exception
		}

		Vimagen = inflater.inflate(R.layout.msg_imagen, null);
		Vtexto = inflater.inflate(R.layout.escribir_msg, null);
		Vvideo = inflater.inflate(R.layout.grabar_video, null);
		Vaudio = inflater.inflate(R.layout.grabar_video, null);
		Vreproductor = inflater.inflate(R.layout.grabar_audio, null);
		Vimagen_previa = inflater.inflate(R.layout.vista_imagen, null);
		layoutvideo = inflater.inflate(R.layout.video, null);
		progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);
		txprogress = (TextView) findViewById(R.id.tvporcentaje);
		try {

			try {

				txprogress.setText("0 MB");

				progressBar1.setProgress(0);

			} catch (Exception e) {
				// TODO: handle exception
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

		if (data_servicio.opcion_mensaje.equals("crear")) {

			setlayout_video();

			if (data_servicio.tipo_mensaje.equals("video")) {

				setlayout_video();
			} else if (data_servicio.tipo_mensaje.equals("texto")) {

				setlayout_texto();
			} else if (data_servicio.tipo_mensaje.equals("voz")) {

				setlayout_voz();
			} else if (data_servicio.tipo_mensaje.equals("imagen")) {

				setlayout_imagen();
			}

			bn_video.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					setlayout_video();

				}
			});

			bn_voz.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					setlayout_voz();
				}
			});
			bn_texto.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					setlayout_texto();
				}
			});
			bn_img.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					setlayout_imagen();
				}
			});

			btn_add = (ImageView) findViewById(R.id.imageView1);

			btn_add.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (viewvideo != null) {
						if (viewvideo.video != null) {
							if (viewvideo.video.isPlaying()) {
								viewvideo.video.pause();
							}
						}
					}
					log_aplicacion.log_informacion("conexion " + prueba_internet.estadoConexion());
					if (prueba_internet.estadoConexion()
							|| (!prueba_internet.estadoConexion() && data_servicio.lista_beneficiario.size() > 0)) {

						data_servicio.lista = "beneficiarios";
						Intent i = new Intent(context, ListaBeneficiarioActivity.class);
						startActivityForResult(i, 110);
					} else {
						Toast.makeText(getApplicationContext(),
								getString(R.string.no_se_puede_consultar_los_beneficiarios), Toast.LENGTH_SHORT).show();
					}
				}
			});

		} else if (data_servicio.opcion_mensaje.equals("editar")) {
			btn_add = (ImageView) findViewById(R.id.imageView1);

			btn_add.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (viewvideo != null) {
						if (viewvideo.video != null) {
							if (viewvideo.video.isPlaying()) {
								viewvideo.video.pause();
							}
						}
					}
					log_aplicacion.log_informacion("conexion " + prueba_internet.estadoConexion());
					if (prueba_internet.estadoConexion()
							|| (!prueba_internet.estadoConexion() && data_servicio.lista_beneficiario.size() > 0)) {

						data_servicio.lista = "beneficiarios";
						Intent i = new Intent(context, ListaBeneficiarioActivity.class);
						startActivityForResult(i, 110);
					} else {
						Toast.makeText(getApplicationContext(),
								getString(R.string.no_se_puede_consultar_los_beneficiarios), Toast.LENGTH_SHORT).show();
					}
				}
			});

			if (data_servicio.mensaje_edicion.equals("mensaje")) {
				mensaje = data_servicio.mensajeedicion;

				log_aplicacion.log_informacion("tipo archivo" + mensaje.getType_file_id());
				log_aplicacion.log_informacion("archivo voz" + data_servicio.archivo_voz);
				log_aplicacion.log_informacion("archivo texto" + data_servicio.archivo_texto);

				if (mensaje.getType_file_id().equals("1")) {
					seleccion = "video";
					txprogress.setText(mensaje.getSize() + " MB");
					try {

						JSONArray jo = new JSONArray(data_servicio.json_freespace);

						String freespace = jo.getJSONObject(0).getString("free");
						String space = jo.getJSONObject(0).getString("space");
						String size = jo.getJSONObject(0).getString("size");
						log_aplicacion.log_informacion("espacio  " + freespace);
						log_aplicacion.log_informacion("espacio  " + space);
						log_aplicacion.log_informacion("espacio  " + size);

						try {

							int spaceint = Integer.parseInt(freespace);

							log_aplicacion.log_informacion("espacio  " + spaceint);

							int espa = (int) (((double) Double.valueOf(mensaje.getSize()) / (double) spaceint) * 100);
							progressBar1.setProgress(espa);

						} catch (Exception e) {
							// TODO: handle exception

							log_aplicacion.log_informacion("error " + e.toString());

						}

					} catch (Exception e) {
						// TODO: handle exception

						log_aplicacion.log_informacion("error 2 " + e.toString());

					}

					mostrarvideo(null);
				}
				if (mensaje.getType_file_id().equals("2")) {
					seleccion = "voz";
					archivo_grabacion = data_servicio.archivo_voz;
					reproducir_audio(null, archivo_grabacion);

				}
				if (mensaje.getType_file_id().equals("3")) {

					archivo_texto = data_servicio.archivo_texto;
					setlayout_texto();

					archivo_texto = data_servicio.archivo_texto;
					set_espacio(data_servicio.archivo_texto, imagenes, false);

				}
				if (mensaje.getType_file_id().equals("4")) {
					imagenes = (ArrayList<Imagen>) data_servicio.imagenes.clone();

					set_espacio("", imagenes, true);

					seleccion = "imagenes";
					setlayout_imagen();

				}

				lista_beneficiario_msg = (ArrayList<Usuario>) data_servicio.lista_beneficiario_msg.clone();

				asunto.setText(data_servicio.mensajeedicion.getTitle());

				if (data_servicio.mensajeedicion.getDate_view().equals("null")) {

					sp_programar.setSelection(0);
				} else {
					sp_programar.setSelection(1);
					fecha_envio = mensaje.getDate_view();
					log_aplicacion.log_informacion("fecha envio " + fecha_envio);
					try {

						String[] fecha_arreglo = fecha_envio.split("-");

						fecha.setText(fecha_arreglo[2] + "/" + fecha_arreglo[1] + "/" + fecha_arreglo[0]);

					} catch (Exception e) {
						// TODO: handle exception
						log_aplicacion.log_error("error fecha " + e.toString());
					}

				}

			} else if (data_servicio.mensaje_edicion.equals("borrador")) {
				borrador = data_servicio.borradoredicion;

				if (borrador.getDate_view().equals("")) {

					sp_programar.setSelection(0);
				} else {
					sp_programar.setSelection(1);
					fecha_envio = borrador.getDate_view();
					log_aplicacion.log_informacion("fecha envio " + fecha_envio);
					try {

						String[] fecha_arreglo = fecha_envio.split("-");

						fecha.setText(fecha_arreglo[2] + "/" + fecha_arreglo[1] + "/" + fecha_arreglo[0]);

					} catch (Exception e) {
						// TODO: handle exception
						log_aplicacion.log_error("error fecha " + e.toString());
					}

				}

				if (borrador.getType_file_id().equals("1")) {

					if (!borrador.getFile().equals("")) {

						archivo_video = borrador.getFile();
						seleccion = "video";
						set_espacio(archivo_video, imagenes, false);
						mostrarvideo(Uri.parse(archivo_video));

					} else {

						setlayout_video();

					}

				}

				if (borrador.getType_file_id().equals("2")) {
					setlayout_voz();
					if (!borrador.getFile().equals("")) {
						seleccion = "voz";
						archivo_grabacion = borrador.getFile();
						reproducir_audio(Uri.parse(archivo_grabacion), archivo_grabacion);
						set_espacio(archivo_grabacion, imagenes, false);
					} else {

						setlayout_voz();

					}

				}

				if (borrador.getType_file_id().equals("3")) {

					setlayout_texto();
					if (!borrador.getFile().equals("")) {

						archivo_texto = borrador.getFile();
						set_espacio(archivo_texto, imagenes, false);
					}

				}

				if (borrador.getType_file_id().equals("4")) {
					if (!borrador.getFile().equals("")) {
						String[] array_ben = borrador.getFile().split(",");
						if (array_ben.length != 0) {
							for (int i = 0; i < array_ben.length; i++) {

								imagenes.add(new Imagen(array_ben[i]));
							}
						}
					}
					setlayout_imagen();
					set_espacio("", imagenes, true);

				}

				asunto.setText(borrador.getTitle());

				String beneficiarios = borrador.getBeneficiaries();
				if (!beneficiarios.equals("")) {
					try {

						String[] array_ben = beneficiarios.split(",");
						if (array_ben.length != 0) {
							for (int i = 0; i < array_ben.length; i++) {

								String[] array = array_ben[i].split(":");

								Usuario usu = new Usuario(array[0]);
								usu.setName(array[1]);
								lista_beneficiario_msg.add(usu);
							}
						} else if (!beneficiarios.equals("")) {
							String[] array = beneficiarios.split(":");

							Usuario usu = new Usuario(array[0]);
							usu.setName(array[1]);
							lista_beneficiario_msg.add(usu);

						}

					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			}

		}
		handler.postDelayed(runnable, 1000);
	}

	public void set_espacio(String archivo, ArrayList<Imagen> arrayimagenes, boolean array) {

		String tamano = "0";
		double fileSizeInMB = 0;
		if (array) {
			for (int i = 0; i < arrayimagenes.size(); i++) {

				try {

					File f = new File(arrayimagenes.get(i).getUrl());

					if (f.exists()) {
						// Get length of file in bytes
						double fileSizeInBytes = f.length();
						// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
						double fileSizeInKB = fileSizeInBytes / 1024;
						// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
						fileSizeInMB += fileSizeInKB / 1024;

						log_aplicacion.log_informacion("size original " + fileSizeInBytes);

						log_aplicacion.log_informacion("fileSizemb original " + fileSizeInKB);

						log_aplicacion.log_informacion("size " + fileSizeInMB);
					}
				} catch (Exception e) {
					// TODO: handle exception
					log_aplicacion.log_informacion("error espacio imagen" + e.toString());
				}
			}

			tamano = String.valueOf(fileSizeInMB);
			if (tamano.contains(".") && tamano.length() > 4) {
				tamano = tamano.substring(0, tamano.indexOf(".") + 3);
			}
		} else {

			File f = new File(archivo);

			if (f.exists()) {
				// Get length of file in bytes
				double fileSizeInBytes = f.length();
				// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
				double fileSizeInKB = fileSizeInBytes / 1024;
				// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
				fileSizeInMB = fileSizeInKB / 1024;
				tamano = String.valueOf(fileSizeInMB);

			}
		}

		if (tamano.contains(".") && tamano.length() > 4) {
			tamano = tamano.substring(0, tamano.indexOf(".") + 3);
		}

		progressBar1.setProgress(0);
		int porcentajeint = 0;
		int espacio = 0;
		try {

			JSONArray jo = new JSONArray(data_servicio.json_freespace);

			String freespace = jo.getJSONObject(0).getString("free");
			String space = jo.getJSONObject(0).getString("space");
			String size = jo.getJSONObject(0).getString("size");
			log_aplicacion.log_informacion("espacio  " + freespace);
			log_aplicacion.log_informacion("espacio  " + space);
			log_aplicacion.log_informacion("espacio  " + size);

			try {

				int spaceint = Integer.parseInt(freespace);

				log_aplicacion.log_informacion("espacio  " + spaceint);

				espacio = (int) (((double) fileSizeInMB / (double) spaceint) * 100);

				log_aplicacion.log_informacion("espacio  " + espacio);

			} catch (Exception e) {
				// TODO: handle exception

				log_aplicacion.log_informacion("error " + e.toString());

			}

			txprogress.setText(tamano + " MB");

		} catch (Exception e) {
			// TODO: handle exception

			log_aplicacion.log_informacion("error 2 " + e.toString());

		}
		if (android.os.Build.VERSION.SDK_INT >= 11) {
			// will update the "progress" propriety of
			// seekbar until it
			// reaches
			// progress
			ObjectAnimator animation = ObjectAnimator.ofInt(progressBar1, "progress", espacio);
			animation.setDuration(3000); // 0.5 second
			animation.setInterpolator(new DecelerateInterpolator());
			animation.start();
		} else {
			progressBar1.setProgress(espacio);
		}

	}

	public static Drawable LoadImageFromWebOperations(String url) {

		try {
			InputStream is = (InputStream) new URL(url).getContent();
			Drawable d = Drawable.createFromStream(is, "src name");
			return d;
		} catch (Exception e) {
			return null;
		}

	}

	public void inicio_seleccion() {
		triangulo_video.setText("");
		triangulo_audio.setText("");
		triangulo_texto.setText("");
		triangulo_imagen.setText("");

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		GuardarInformacionApp.Guardar_info(outState);

	}

	public void Enviar_mensaje() {

		String tipo = "0";
		String descripcion = "Mensaje sin descripcion";
		String archivo = "";
		double fileSizeInMB = 0.0;
		String tamano = "0";
		stopPlay();

		// en caso de que el borrador sin beneficiarios sea editado y se agregue
		// el beneficiario y tenga
		// conexion a internet se debe subir

		if (data_servicio.mensaje_edicion.equals("borrador") && data_servicio.opcion_mensaje.equals("editar")
				&& prueba_internet.estadoConexion()) {
			data_servicio.opcion_mensaje = "crear";

		}

		if (seleccion.equals("imagen")) {

			tipo = "4";
			data_servicio.tipo_msg = "4";
			for (int i = 0; i < imagenes.size(); i++) {

				if (i == imagenes.size() - 1) {
					archivo += imagenes.get(i).getUrl();
				} else {
					archivo += imagenes.get(i).getUrl() + ",";
				}

			}

			for (int i = 0; i < imagenes.size(); i++) {

				try {

					File f = new File(imagenes.get(i).getUrl());

					if (f.exists()) {
						// Get length of file in bytes
						double fileSizeInBytes = f.length();
						// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
						double fileSizeInKB = fileSizeInBytes / 1024;
						// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
						fileSizeInMB += fileSizeInKB / 1024;

					}
				} catch (Exception e) {
					// TODO: handle exception
					log_aplicacion.log_informacion("error espacio imagen" + e.toString());
				}
			}
			tamano = String.valueOf(fileSizeInMB);

		} else if (seleccion.equals("texto")) {

			tipo = "3";
			data_servicio.tipo_msg = "3";
			archivo = archivo_texto;
			descripcion = edit_msg.getText().toString();

		} else if (seleccion.equals("voz")) {
			data_servicio.tipo_msg = "2";
			tipo = "2";
			archivo = archivo_grabacion;

		} else if (seleccion.equals("video")) {
			data_servicio.tipo_msg = "1";
			tipo = "1";
			archivo = archivo_video;

		}

		if (!seleccion.equals("imagen")) {
			File f = new File(archivo);

			if (f.exists()) {
				// Get length of file in bytes
				double fileSizeInBytes = f.length();
				// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
				double fileSizeInKB = fileSizeInBytes / 1024;
				// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
				fileSizeInMB = fileSizeInKB / 1024;
				tamano = String.valueOf(fileSizeInMB);

			}

		}

		String txtasunto = asunto.getText().toString();

		String txtfecha = fecha.getText().toString();
		txtfecha = fecha_envio;

		String beneficiarios = "";

		for (int i = 0; i < lista_beneficiario_msg.size(); i++) {
			if (i == lista_beneficiario_msg.size() - 1) {
				beneficiarios += lista_beneficiario_msg.get(i).getUser_id().toString();
			} else {
				beneficiarios += lista_beneficiario_msg.get(i).getUser_id().toString() + ",";
			}
		}

		cargar = new DialogoCarga(this, getString(R.string.enviando),
				getString(R.string.la_subida_de_archivos_puede_demorar_un_poco_));

		cargar.mostrar();
		WS_cliente servicio3;
		try {
			servicio3 = new WS_cliente(context);

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						cargar.ocultar();
						try {
							if (borrador != null) {
								db.deleteborrador(String.valueOf(borrador.getId_borrador()));

							}

						} catch (Exception e) {
							// TODO: handle exception
						}

						data_servicio.tipo_historial = "mensajes";
                        Log.e("love",""+data_servicio.msj);
						dialogo_modal.dialogo_cerrar(getString(R.string.informacion), data_servicio.msj, "",
								getString(R.string.aceptar), true, true);

					} else if (msg.what == 0) {
						if (!data_servicio.msj_error.equals("")) {
							dialogo_modal.dialogo(getString(R.string.informacion), data_servicio.msj_error, "",
									getString(R.string.aceptar), true, true);
						}
						cargar.ocultar();

					}

				}
			};

			if (!prueba_internet.estadoConexion() || (double) space < (double) fileSizeInMB
					|| (data_servicio.mensaje_edicion.equals("borrador") && lista_beneficiario_msg.size() == 0)) {

				log_aplicacion.log_informacion("borrador ");
				log_aplicacion.log_informacion("space " + space);
				log_aplicacion.log_informacion("peso " + fileSizeInMB);
				log_aplicacion.log_informacion("internet " + prueba_internet.estadoConexion());
				try {

					data_servicio.opcion_mensaje = "borradores";

					data_servicio.tipo_historial = "borradores";

					beneficiarios = "";

					for (int i = 0; i < lista_beneficiario_msg.size(); i++) {
						if (i == lista_beneficiario_msg.size() - 1) {
							beneficiarios += lista_beneficiario_msg.get(i).getUser_id().toString() + ":"
									+ lista_beneficiario_msg.get(i).getName();
						} else {
							beneficiarios += lista_beneficiario_msg.get(i).getUser_id().toString() + ":"
									+ lista_beneficiario_msg.get(i).getName() + ",";
						}
					}
					log_aplicacion.log_informacion(archivo);

					try {
						if (borrador != null) {
							db.deleteborrador(String.valueOf(borrador.getId_borrador()));

						}

					} catch (Exception e) {
						// TODO: handle exception
					}

					db.insertar_borrador(tipo, "1", txtasunto, txtfecha, descripcion, archivo,
							data_servicio.usuario.getUser_id(), beneficiarios, tamano);
					data_servicio.tipo_historial = "borradores";
					// edicion de un borrador sin beneficiarios

					if (data_servicio.mensaje_edicion.equals("borrador") && lista_beneficiario_msg.size() == 0) {

						dialogo_modal.dialogo_cerrar(getString(R.string.informacion),
								getString(R.string.borrador_editado), "", getString(R.string.aceptar), true, true);

					}

					else if (!prueba_internet.estadoConexion()) {
						dialogo_modal.dialogo_cerrar(getString(R.string.error_conexión),
								getString(
										R.string.no_posee_conexion_a_internet_en_el_momento_el_mensaje_fue_guardado_en_borradores_),
								"", getString(R.string.aceptar), true, true);
					} else if ((double) space < (double) fileSizeInMB) {

						dialogo_modal.dialogo_cerrar(getString(R.string.informacion),
								getString(R.string.no_posee_espacio_suficiente_el_mensaje_se_guardo_como_borrador_), "",
								getString(R.string.aceptar), true, true);
					}

					cargar.ocultar();

				} catch (Exception e) {
					// TODO: handle exception
					cargar.ocultar();
				}

			} else {
				log_aplicacion.log_informacion("Mensaje");
				txtasunto = asunto.getText().toString();
				if (data_servicio.opcion_mensaje.equals("editar")) {
					log_aplicacion.log_informacion("Actualizando msg");

					servicio3.update_mensaje(mensaje.getIdmessage(), tipo, "1", txtasunto, txtfecha, descripcion,
							archivo, beneficiarios, (ArrayList<Imagen>) imagenes.clone());

					// servicio3.crear_mensaje(tipo, "1", txtasunto, txtfecha,
					// descripcion, archivo, beneficiarios,
					// (ArrayList<Imagen>) imagenes.clone());
					//
					// eliminar();

				}

				if (data_servicio.opcion_mensaje.equals("crear")) {
					servicio3.crear_mensaje(tipo, "1", txtasunto, txtfecha, descripcion, archivo, beneficiarios,
							(ArrayList<Imagen>) imagenes.clone());
				}

				data_servicio.tipo_historial = "mensajes";

				servicio3.setPuente_envio(puente);
				servicio3.execute();
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getRealImagePathFromURI(Context context, Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public void iniciar() {
		setlayout_video();
		asunto.setText("");

	}

	public String getRealVideoPathFromURI(Context context, Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Video.Media.DATA };
			cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
			cursor.moveToFirst();
			log_aplicacion.log_informacion("archivo url" + cursor.getString(column_index));

			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public String getRealAudioPathFromURI(Context context, Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Audio.Media.DATA };
			cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}

		}
	}

	public void notification_change_imagen() {
		if (imagenes.size() > 0) {
			try {
				set_espacio("", (ArrayList<Imagen>) imagenes.clone(), true);
			} catch (Exception e) {
				// TODO: handle exception
			}

			int height = viewimagen.lay_imagenes.getHeight();
			int width = 0;

			try {
				viewimagen.lay_imagenes.removeAllViews();
			} catch (Exception e) {
				// TODO: handle exception
			}

			log_aplicacion.log_informacion("arreglo" + data_servicio.imagenes.size());

			for (int i = 0; i < imagenes.size(); i++) {

				int heigth_image = 0;
				log_aplicacion.log_informacion("onpostexecute");
				log_aplicacion.log_informacion(imagenes.get(i).getUrl());

				ImageView ima = new ImageView(context);
				ima.setScaleType(ImageView.ScaleType.CENTER_CROP);
				ima.setBackgroundColor(Color.WHITE);
				Bitmap bitmap = imagenes.get(i).getImagen();
				if (bitmap != null) {
					ima.setImageBitmap(bitmap);

				} else {

					bitmap = BitmapFactory.decodeFile(imagenes.get(i).getUrl());
					bitmap.prepareToDraw();
					int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
					bitmap = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
					imagenes.get(i).setImagen(bitmap);
					ima.setImageBitmap(bitmap);
				}

				LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(height, height);
				parm.leftMargin = 5;
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
						LayoutParams.WRAP_CONTENT);
				params.gravity = Gravity.BOTTOM;

				LinearLayout layoutsimag = new LinearLayout(context);
				layoutsimag.setOrientation(LinearLayout.VERTICAL);

				layoutsimag.addView(ima, parm);

				click_image click_imagen = new click_image(i);
				layoutsimag.setOnClickListener(click_imagen);

				viewimagen.lay_imagenes.addView(layoutsimag, 0, parm);

			}

			if (imagenes.size() < 5) {
				scroll_horizontal.setScrollingEnabled(false);

				for (int i = 0; i < 5 - imagenes.size(); i++) {

					LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(height, height);
					parm.leftMargin = 5;
					ImageView ima = new ImageView(context);
					ima.setScaleType(ImageView.ScaleType.CENTER_CROP);
					ima.setBackgroundColor(Color.WHITE);

					parm.leftMargin = 5;
					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT);
					params.gravity = Gravity.BOTTOM;

					LinearLayout layoutsimag = new LinearLayout(context);
					layoutsimag.setOrientation(LinearLayout.VERTICAL);

					layoutsimag.addView(ima, parm);
					viewimagen.lay_imagenes.addView(layoutsimag, parm);

				}

			} else {

				scroll_horizontal.setScrollingEnabled(true);

			}
		} else {
			int height = viewimagen.lay_imagenes.getHeight();
			int width = 0;
			scroll_horizontal.setScrollingEnabled(false);
			for (int i = 0; i < 5; i++) {

				LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(height, height);
				parm.rightMargin = 5;
				ImageView ima = new ImageView(context);
				ima.setScaleType(ImageView.ScaleType.CENTER_CROP);
				ima.setBackgroundColor(Color.WHITE);

				LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
						LayoutParams.WRAP_CONTENT);
				params2.gravity = Gravity.BOTTOM;

				LinearLayout layoutsimag = new LinearLayout(context);
				layoutsimag.setOrientation(LinearLayout.VERTICAL);

				layoutsimag.addView(ima, parm);

				viewimagen.lay_imagenes.addView(layoutsimag, 0, params2);

			}

		}
	}

	@SuppressLint("ResourceAsColor")
	private void guardarImagen(final Uri uri, final String url) {
		// File dirImages = cw.getDir("perfil", Context.MODE_PRIVATE);
		new AsyncTask<Void, Void, String>() {

			int height;
			String tiempo = "";
			Bitmap bitmap = null;

			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				log_aplicacion.log_informacion("doInBackground");

				bitmap = BitmapFactory.decodeFile(url);
				bitmap.prepareToDraw();
				int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
				bitmap = Bitmap.createScaledBitmap(bitmap, 512, nh, true);

				imagenes.add(new Imagen(url, bitmap));
				// Double relacion= (double) bitmap.getWidth()/ (double)
				// bitmap.getHeight();
				// int with_imagen=(int) (relacion*height);
				//
				// bitmap = bitmap.createBitmap(height, height,
				// Bitmap.Config.RGB_565);
				log_aplicacion.log_informacion(url);
				return msg;
			}

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();

				try {
					height = viewimagen.lay_imagenes.getHeight();

					log_aplicacion.log_informacion("onPreExecute");

				} catch (Exception e) {
					// TODO: handle exception
					log_aplicacion.log_informacion("onPreExecute error " + e.toString());
				}

			}

			@Override
			protected void onProgressUpdate(Void... values) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void onPostExecute(String msg) {
				log_aplicacion.log_informacion("onpostexcecute");
				try {

					if (bitmap != null) {
						notification_change_imagen();

						// int heigth_image = 0;
						// log_aplicacion.log_informacion("onpostexecute");
						// log_aplicacion.log_informacion(url);
						//
						// ImageView ima = new ImageView(context);
						// ima.setScaleType(ImageView.ScaleType.CENTER_CROP);
						// ima.setBackgroundColor(Color.WHITE);
						// ima.setImageBitmap(bitmap);
						//
						//
						// LinearLayout.LayoutParams parm = new
						// LinearLayout.LayoutParams(height, height);
						// parm.leftMargin = 5;
						// LinearLayout.LayoutParams params = new
						// LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
						// LayoutParams.WRAP_CONTENT);
						// params.gravity = Gravity.BOTTOM;
						//
						// LinearLayout layoutsimag = new LinearLayout(context);
						// layoutsimag.setOrientation(LinearLayout.VERTICAL);
						//
						// layoutsimag.addView(ima, parm);
						//
						// click_image click_imagen = new
						// click_image(imagenes.size() - 1);
						// layoutsimag.setOnClickListener(click_imagen);
						//
						// viewimagen.lay_imagenes.addView(layoutsimag, 0,
						// parm);

					}
				} catch (Exception e) {
					// TODO: handle exception
					log_aplicacion.log_informacion("onpostexcecute error " + e.toString());

				}
			}

		}.execute(null, null, null);

	}

	public void rec_voz() {

		try {
			Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
			startActivityForResult(intent, RECORD_REQUEST);
		}catch (ActivityNotFoundException e){
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.sonymobile.androidapp.audiorecorder&hl=en")));
		}

	}
	private void goToSettings() {
		Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
		myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
		myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivityForResult(myAppSettings, 12);
	}

	public static boolean isAvailable(Context ctx, Intent intent) {

		final PackageManager mgr = ctx.getPackageManager();

		List<ResolveInfo> list = mgr.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

		return list.size() > 0;

	}

	public void reiniciaricon() {

		bn_video.setImageResource(R.drawable.ic_video);
		bn_texto.setImageResource(R.drawable.ic_texto);
		bn_img.setImageResource(R.drawable.ic_imagen);
		bn_voz.setImageResource(R.drawable.ic_audio);

	}

	public void setlayout_video() {
		if (!seleccion.equals("video")) {
			seleccion = "video";

			archivo_texto = "";
			archivo_video = "";
			archivo_grabacion = "";
			imagenes.clear();
			inicio_seleccion();
			triangulo_video.setText("▲");

			View vi = Vvideo;
			try {
				layout.removeAllViews();
			} catch (Exception e) {
				// TODO: handle exception
			}
			ImageView img = (ImageView) vi.findViewById(R.id.imagen_grabar);
			img.setImageResource(R.drawable.boton_grabar_video);
			layout.addView(vi, layoutparams_msg);
			reiniciaricon();
			bn_video.setImageResource(R.drawable.ic_video_select);

			img.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					capturar_video();

				}
			});

			ImageView btn_adjuntar = (ImageView) vi.findViewById(R.id.imageVideo);

			btn_adjuntar.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					abrir_archivo();
				}
			});

		} else if (!archivo_video.equals("") && uriVideo != null) {

			mostrarvideo(uriVideo);
		}
	}

	public void mostrarvideo(Uri uri) {

		try {
			layout.removeAllViews();

		} catch (Exception e) {
			// TODO: handle exception
		}

		View vi = layoutvideo;

		viewvideo = new Viewvideo();

		viewvideo.video = (VideoView) vi.findViewById(R.id.videoView1);

		layout.addView(vi);

		if (uri != null) {
			try {
				set_espacio(archivo_video, imagenes, false);

			} catch (Exception e) {
				// TODO: handle exception
			}
			log_aplicacion.log_informacion("video " + archivo_video);

			viewvideo.video.setVideoURI(Uri.parse(archivo_video));
			viewvideo.video.setEnabled(true);
			viewvideo.video.bringToFront();

			SurfaceHolder holder = viewvideo.video.getHolder();

			holder.setFixedSize(400, 400);

			mediacontroller = new MediaController(this);
			viewvideo.video.setMediaController(mediacontroller);
			viewvideo.video.requestFocus();
			// we also set an setOnPreparedListener in order to know when the
			// video file is ready for playback
			viewvideo.video.setOnPreparedListener(new OnPreparedListener() {

				public void onPrepared(MediaPlayer mediaPlayer) {

					viewvideo.video.start();

				}
			});

			LinearLayout layout_eliminar = (LinearLayout) vi.findViewById(R.id.layout_eliminar);
			ImageView imageplay = (ImageView) vi.findViewById(R.id.imageplay);

			imageplay.setVisibility(View.GONE);
			ImageView imageEliminar = (ImageView) vi.findViewById(R.id.imageEliminar);
			imageEliminar.setImageResource(R.drawable.eliminar);
			imageEliminar.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						log_aplicacion.log_informacion("click eliminar");
						archivo_video = "";
						seleccion = "";
						setlayout_video();
					} catch (Exception e) {
						// TODO: handle exception
					}

				}
			});
			layout_eliminar.bringToFront();
			imageEliminar.bringToFront();
		} else {
			ImageView imageEliminar = (ImageView) vi.findViewById(R.id.imageEliminar);

			ImageView imageplay = (ImageView) vi.findViewById(R.id.imageplay);

			imageEliminar.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						log_aplicacion.log_informacion("click eliminar");
						archivo_video = "";
						seleccion = "";
						setlayout_video();
					} catch (Exception e) {
						// TODO: handle exception
					}

				}
			});

			imageplay.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					log_aplicacion.log_informacion("click cargar video");
					cargar_archivo(Descarga_video);

				}
			});

		}
	}

	final String Descarga_video = "Descarga_video";
	final String Descarga_audio = "Descarga_audio";

	public void cargar_archivo(final String tipo) {

		try {
			final JSONArray files = new JSONArray(data_servicio.json_file);
			cargar = new DialogoCarga(this, getString(R.string.descargando), getString(R.string.espere_un_momento_));

			for (int i = 0; i < files.length(); i++) {
				cargar.mostrar();
				files.getJSONObject(i).getString("url");
				Handler puentedescarga = new Handler() {
					@SuppressLint("ShowToast")
					@Override
					public void handleMessage(Message msg) {
						if (msg.what == 1) {
							if (tipo.equals(Descarga_video)) {
								mostrarvideo(Uri.parse(archivo_video));

							} else if (tipo.equals(Descarga_audio)) {
								reproducir_audio(Uri.parse(archivo_grabacion), archivo_grabacion);

							}

							cargar.ocultar();
						} else if (msg.what == 0) {
							cargar.ocultar();

						}

					}
				};
				File f = new File(Environment.getExternalStorageDirectory() + "/Fluie/"
						+ files.getJSONObject(i).getString("file"));

				if (!f.exists() || (f.exists() && f.length() > 0)) {
					if ((f.exists() && f.length() > 0)) {
						try {
							f.delete();
						} catch (Exception e) {
							// TODO: handle exception
						}

					}

					Descarga_Archivo descarga = new Descarga_Archivo(files.getJSONObject(i).getString("url"),
							puentedescarga, files.getJSONObject(i).getString("file"));

					descarga.execute();

					if (tipo.equals(Descarga_video)) {
						archivo_video = descarga.getUrllocal();

					} else if (tipo.equals(Descarga_audio)) {
						archivo_grabacion = descarga.getUrllocal();

					}

				} else {
					cargar.ocultar();

					if (tipo.equals(Descarga_video)) {
						archivo_video = Environment.getExternalStorageDirectory() + "/Fluie/"
								+ files.getJSONObject(i).getString("file");
						mostrarvideo(Uri.parse(archivo_video));
					} else if (tipo.equals(Descarga_audio)) {
						archivo_grabacion = Environment.getExternalStorageDirectory() + "/Fluie/"
								+ files.getJSONObject(i).getString("file");
						reproducir_audio(Uri.parse(archivo_grabacion), archivo_grabacion);
					}
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setlayout_voz() {
		if (!seleccion.equals("voz")) {
			inicio_seleccion();
			triangulo_audio.setText("▲");

			archivo_texto = "";
			archivo_video = "";
			archivo_grabacion = "";
			imagenes.clear();
			View vi = Vaudio;
			seleccion = "voz";
			try {
				layout.removeAllViews();
			} catch (Exception e) {
				// TODO: handle exception
			}
			ImageView img = (ImageView) vi.findViewById(R.id.imagen_grabar);
			img.setImageResource(R.drawable.boton_grabar_audio);
			layout.addView(vi, layoutparams_msg);
			reiniciaricon();
			bn_voz.setImageResource(R.drawable.ic_audio_select);

			ImageView btn_adjuntar = (ImageView) vi.findViewById(R.id.imageVideo);

			btn_adjuntar.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					openaudiofile();
				}
			});

			img.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					rec_voz();
					//
					//
					// try {
					// layout.removeAllViews();
					// } catch (Exception e) {
					// // TODO: handle exception
					// }
					//
					// layout.addView(vi2);
					//
					// start();
					// barra_grabacion = (SeekBar)
					// vi2.findViewById(R.id.seekBar2);
					//
					// barra_grabacion.setOnSeekBarChangeListener(new
					// SeekChanged(vi2.getContext()));

					// start();
				}
			});
		}
	}

	public class SeekChanged implements OnSeekBarChangeListener {

		private Context context;

		public SeekChanged(Context context) {
			this.context = context;
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			if (myPlayer != null && fromUser) {
				myPlayer.seekTo(progress);
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {

		}

	}

	public void capturar_video() {

		Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);

		// takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);

		String tiempo = String.valueOf(System.currentTimeMillis());

		url_grabacion_archivo = Environment.getExternalStorageDirectory().getPath() + "/fluie/" + tiempo + ".mp4";

		ContentValues value = new ContentValues();
		value.put(MediaStore.Video.Media.TITLE, tiempo);
		value.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
		value.put(MediaStore.Video.Media.DATA, url_grabacion_archivo);

		videoUri = context.getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, value);

		takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, url_grabacion_archivo);

		startActivityForResult(takeVideoIntent, ACTION_TAKE_VIDEO);

	}

	public void setlayout_imagen() {

		if (!seleccion.equals("imagen")) {
			archivo_texto = "";
			if (text_archivo != null) {
				text_archivo.setText("");

			}

			archivo_video = "";
			archivo_grabacion = "";

			inicio_seleccion();
			triangulo_imagen.setText("▲");

			View vi = Vimagen;

			scroll_horizontal = (Custom_scroll) vi.findViewById(R.id.horizontalScrollView1);

			if (viewimagen == null) {
				viewimagen = new Viewimagenes();
				viewimagen.btn_foto = (ImageView) vi.findViewById(R.id.imagen_bn_capturar);

				viewimagen.btn_adjuntar = (ImageView) vi.findViewById(R.id.imageadjuntar);

				viewimagen.lay_imagenes = (LinearLayout) vi.findViewById(R.id.layout_imagenes);

			}

			seleccion = "imagen";
			try {
				layout.removeAllViews();
			} catch (Exception e) {
				// TODO: handle exception
			}

			layout.addView(vi, layoutparams_msg);
			reiniciaricon();
			bn_img.setImageResource(R.drawable.ic_imagen_select);

			viewimagen.btn_foto.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					openCamera();
				}
			});

			viewimagen.btn_adjuntar.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					abrir_archivo();
				}
			});

			notification_change_imagen();

		}
	}

	boolean presion = false;
	int seleccion_img_prev = 0;

	public void setlayout_imagen_previa(int seleccion_) {
		Vimagen_previa = inflater.inflate(R.layout.vista_imagen, null);
		seleccion_img_prev = seleccion_;
		View vi = Vimagen_previa;
		presion = false;
		try {
			layout.removeAllViews();
		} catch (Exception e) {
			// TODO: handle exception
		}
		layout.addView(vi, layoutparams_msg);

		ImageView eliminar = (ImageView) vi.findViewById(R.id.imageneliminar);

		ImageView atras = (ImageView) vi.findViewById(R.id.imagenatras);

		ImageView imagen_previa = (ImageView) vi.findViewById(R.id.image_previa);

		imagen_previa.setImageBitmap(imagenes.get(seleccion_img_prev).getImagen());

		atras.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				seleccion = "";
				setlayout_imagen();

			}
		});

		eliminar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!presion) {
					seleccion = "";
					imagenes.remove(seleccion_img_prev);
					presion = true;
					setlayout_imagen();

					notification_change_imagen();
				}
			}
		});

	}

	public void abrir_archivo() {

		if (seleccion.equals("imagen")) {
			Intent intent = new Intent(Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			intent.setType("image/*");
			startActivityForResult(intent.createChooser(intent, "Selecciona app de imagen"), SELECT_PICTURE);

		}
		if (seleccion.equals("video")) {
			Intent intent = new Intent(Intent.ACTION_PICK,
					android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
			intent.setType("video/*");
			startActivityForResult(intent.createChooser(intent, "Selecciona app de video"), SELECT_VIDEO);

		}

	}

	public void setlayout_texto() {

		if (!seleccion.equals("texto")) {
			archivo_texto = "";

			archivo_video = "";
			archivo_grabacion = "";

			imagenes.clear();
			inicio_seleccion();
			triangulo_texto.setText("▲");

			View vi = Vtexto;
			seleccion = "texto";
			try {
				layout.removeAllViews();
			} catch (Exception e) {
				// TODO: handle exception
			}

			layout.addView(vi, layoutparams_msg);
			reiniciaricon();
			bn_texto.setImageResource(R.drawable.ic_text_select);

			Button bn1 = (Button) vi.findViewById(R.id.button1);

			text_archivo = (TextView) vi.findViewById(R.id.textdocumento);

			File f = new File(archivo_texto);
			if (f.exists()) {
				text_archivo.setText(f.getName());
			}

			edit_msg = (EditText) vi.findViewById(R.id.editText1);
			if (text_archivo != null) {
				text_archivo.setText("");

			}
			if (mensaje != null) {

				edit_msg.setText(mensaje.getDescription());
				try {
					File file = new File(data_servicio.archivo_texto);
					if (file.exists()) {
						text_archivo.setText(file.getName());
						set_espacio(archivo_texto, imagenes, false);
					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			}

			if (borrador != null) {

				edit_msg.setText(borrador.getDescription());

				try {
					File file = new File(data_servicio.archivo_texto);
					if (file.exists()) {
						text_archivo.setText(file.getName());
						set_espacio(archivo_texto, imagenes, false);
					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			}

			bn1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					// PackageManager packageManager = getPackageManager();
					// Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
					// intent.setType("text/plain");
					//
					// try {
					// log_aplicacion.log_informacion("ingresa a seleccion");
					// startActivityForResult(intent, YOUR_RESULT_CODE);
					// } catch (ActivityNotFoundException e) {
					// // The reason for the existence of aFileChooser
					// }

					Intent target = new Intent(Intent.ACTION_GET_CONTENT);

					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
						target.setAction(Intent.ACTION_OPEN_DOCUMENT);
						target.setType("*/*");
						String[] ACCEPT_MIME_TYPES = { "application/pdf", "text/plain",
								"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
								"application/msword"

						};

						target.putExtra(Intent.EXTRA_MIME_TYPES, ACCEPT_MIME_TYPES);

					} else {
						target.setAction(Intent.ACTION_GET_CONTENT);
						target.setType(
								"application/pdf|text/plain|application/msword|application/vnd.openxmlformats-officedocument.wordprocessingml.document");
					}

					// Create the chooser Intent

					target.addCategory(Intent.CATEGORY_OPENABLE);

					Intent intent2 = Intent.createChooser(target, getString(R.string.chooser_title));

					try {
						startActivityForResult(intent2, YOUR_RESULT_CODE);
					} catch (ActivityNotFoundException e) {
						// The reason for the existence of aFileChooser
					}

					// Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
					// Uri uri =
					// Uri.parse(Environment.getExternalStorageDirectory().getPath()
					// + "/");
					// intent.setDataAndType(uri, "file/*");
					// startActivityForResult(intent, YOUR_RESULT_CODE);
					// Intent intent = new Intent();
					// intent.setAction(Intent.ACTION_GET_CONTENT);
					// intent.setType("file/*");
					// startActivityForResult(intent, YOUR_RESULT_CODE);

				}
			});
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* grabacion de audio */
	public void start() {
		try {
			myRecorder.prepare();
			myRecorder.start();
		} catch (IllegalStateException e) {
			// start:it is called before prepare()
			// prepare: it is called after start() or before setOutputFormat()
			e.printStackTrace();
		} catch (IOException e) {
			// prepare() fails
			e.printStackTrace();
		}

		Toast.makeText(getApplicationContext(), "Start recording...", Toast.LENGTH_SHORT).show();
	}

	public void stop(View view) {
		try {
			myRecorder.stop();
			myRecorder.release();
			myRecorder = null;

			Toast.makeText(getApplicationContext(), "Stop recording...", Toast.LENGTH_SHORT).show();
		} catch (IllegalStateException e) {
			// it is called before start()
			e.printStackTrace();
		} catch (RuntimeException e) {
			// no valid audio/video data has been received
			e.printStackTrace();
		}
	}

	public void play() {
		try {
			myPlayer = new MediaPlayer();

			Toast.makeText(getApplicationContext(), "Start play the recording...", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void stopPlay() {
		try {
			if (myPlayer != null) {
				myPlayer.pause();

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		stopPlay();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		log_aplicacion.log_informacion("ingresa requestcode" + requestCode);

		log_aplicacion.log_informacion("ingresa result" + resultCode);
		try {

			switch (requestCode) {
			case PHOTO_CODE:
				if (resultCode == RESULT_OK) {
					try {

						guardarImagen(Uri.parse(url_capture), url_capture);

					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				break;

			case SELECT_PICTURE:
				if (resultCode == RESULT_OK) {
					try {

						Uri selectedImage = data.getData();
						String[] filePathColumn = { MediaStore.Images.Media.DATA };

						Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
						cursor.moveToFirst();

						int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
						String filePath = cursor.getString(columnIndex);
						cursor.close();

						guardarImagen(selectedImage, filePath);

						// Log.d(TAG, String.valueOf(bitmap));
					} catch (Exception e) {
						// TODO: handle exception
					}
					alertDialog.cancel();

				}
				break;
			case SELECT_VIDEO:
				if (resultCode == RESULT_OK) {
					try {

						uriVideo = data.getData();
						log_aplicacion.log_informacion(uriVideo.getPath());

						archivo_video = getRealVideoPathFromURI(context, uriVideo);
						log_aplicacion.log_informacion("video: " + archivo_video);
						mostrarvideo(uriVideo);
						// Log.d(TAG, String.valueOf(bitmap));
					} catch (Exception e) {
						// TODO: handle exception
					}
					alertDialog.cancel();

				}
				break;

			case YOUR_RESULT_CODE:
				if (resultCode == RESULT_OK) {

					final Uri uri = data.getData();

					String path = FileUtils.getPath(this, uri);

					log_aplicacion.log_informacion("archivo " + path);

					// Alternatively, use FileUtils.getFile(Context, Uri)
					if (path != null && FileUtils.isLocal(path)) {
						File file = new File(path);
						if (file.exists()) {
							if (file.getName().contains(".doc") || file.getName().contains(".docx")
									|| file.getName().contains(".pdf") || file.getName().contains(".txt")) {

								text_archivo.setText(file.getName());
								archivo_texto = path;

								set_espacio(path, imagenes, false);
							} else {

								Toast.makeText(context, getString(R.string.las_extensionas_debe_ser_txt_doc_docx_o_pdf),
										Toast.LENGTH_LONG).show();
							}

						} else {
							Toast.makeText(context, getString(R.string.no_se_puede_leer_el_archivo), Toast.LENGTH_LONG)
									.show();
							text_archivo.setText("");

							archivo_texto = "";

						}
					} else {
						Toast.makeText(context, getString(R.string.no_se_puede_leer_el_archivo), Toast.LENGTH_LONG)
								.show();

						archivo_texto = "";
						text_archivo.setText("");

					}

				}
				break;

			case GRABACION_FILE:
				if (resultCode == RESULT_OK) {

					final Uri uri = data.getData();

					archivo_grabacion = FileUtils.getPath(this, uri);

					String path = FileUtils.getPath(this, uri);

					log_aplicacion.log_informacion("archivo " + path);

					Uri ur = Uri.parse(path);

					File file = new File(archivo_grabacion);
					if (file.exists()) {
						if (file.getName().contains(".ogg") || file.getName().contains(".mp3")
								|| file.getName().contains(".aac") || file.getName().contains(".amr")) {

							reproducir_audio(ur, path);

						} else {
							Toast.makeText(context, getString(R.string.la_extension_pude_ser_acc_ogg_o_mp3),
									Toast.LENGTH_LONG).show();
							archivo_grabacion = "";
						}
					} else {
						Toast.makeText(context, getString(R.string.no_se_puede_leer_el_archivo), Toast.LENGTH_LONG)
								.show();
						archivo_grabacion = "";
					}

				}
				break;

			case 110:
				if (data_servicio.lista_beneficiario.size() != 0 || !data_servicio.seleccion_beneficiario.equals("")) {
					log_aplicacion.log_informacion("ingresa setResult(Activity.RESULT_OK, returnIntent);");
					log_aplicacion.log_informacion("seleccion " + data_servicio.seleccion_beneficiario);
					if (lista_beneficiario_msg.size() == 0) {

						for (int i = 0; i < data_servicio.lista_beneficiario.size(); i++) {
							if (data_servicio.lista_beneficiario.get(i).getUser_id()
									.equals(data_servicio.seleccion_beneficiario)) {

								Usuario us = data_servicio.lista_beneficiario.get(i);
								lista_beneficiario_msg.add(us);

								break;
							}

						}

						notification_change();
					} else {

						boolean agregar = true;
						for (int i = 0; i < lista_beneficiario_msg.size(); i++) {
							log_aplicacion.log_informacion("seleccion " + lista_beneficiario_msg.get(i).getUser_id());

							if (lista_beneficiario_msg.get(i).getUser_id()
									.equals(data_servicio.seleccion_beneficiario)) {
								agregar = false;
								break;

							}

						}
						if (agregar) {
							Usuario us = new Usuario();
							for (int j = 0; j < data_servicio.lista_beneficiario.size(); j++) {
								if (data_servicio.lista_beneficiario.get(j).getUser_id()
										.equals(data_servicio.seleccion_beneficiario)) {

									us = data_servicio.lista_beneficiario.get(j);
									lista_beneficiario_msg.add(us);

									notification_change();
									break;

								}

							}
						}
					}

				}
				if (seleccion.equals("video")) {
					if (viewvideo != null) {
						if (viewvideo.video != null) {
							viewvideo.video.start();
						}
					}
				}

				break;

			case ACTION_TAKE_VIDEO:

				uriVideo = data.getData();
				archivo_video = "";

				try {
					AssetFileDescriptor videoAsset = getContentResolver().openAssetFileDescriptor(data.getData(), "r");
					FileInputStream fis = videoAsset.createInputStream();
					String tiempo = String.valueOf(System.currentTimeMillis());

					url_grabacion_archivo = Environment.getExternalStorageDirectory().getPath() + "/fluie/" + tiempo
							+ ".mp4";

					File tmpFile = new File(Environment.getExternalStorageDirectory() + "/fluie/", tiempo + ".mp4");
					FileOutputStream fos = new FileOutputStream(tmpFile);

					byte[] buf = new byte[1024];
					int len;
					while ((len = fis.read(buf)) > 0) {
						fos.write(buf, 0, len);
					}
					fis.close();
					fos.close();
				} catch (IOException io_e) {
					// TODO: handle error
				}
				File f = new File(url_grabacion_archivo);
				if (f.exists()) {
					archivo_video = url_grabacion_archivo;
				} else {
					File f2 = new File(getRealVideoPathFromURI(context, uriVideo));
					if (f2.exists()) {
						archivo_video = getRealVideoPathFromURI(context, uriVideo);
					}

				}

				log_aplicacion.log_informacion("video: " + archivo_video);
				if (!archivo_video.equals("")) {
					mostrarvideo(Uri.parse(archivo_video));
				} else {
					Toast.makeText(context, context.getString(R.string.error_adjuntar_el_archivo), Toast.LENGTH_LONG)
							.show();
				}

				break;
			case RECORD_REQUEST:

				Uri audioFileUri = data.getData();

				String url = getRealAudioPathFromURI(context, audioFileUri);

				reproducir_audio(audioFileUri, url);
				break;
			}
		} catch (Exception e) {
			// TODO: handle exception
			log_aplicacion.log_error("error onactivityresult " + e.toString());

		}
	}

	ImageView play;

	public void reproducir_audio(Uri audioFileUri, String path) {
		try {
			if (audioFileUri != null) {

				grabacion = audioFileUri;

				if (path != null && FileUtils.isLocal(path)) {
					File file = new File(path);
					set_espacio(path, imagenes, false);

					archivo_grabacion = path;

				} else {
					archivo_grabacion = getRealAudioPathFromURI(context, audioFileUri);
					set_espacio(archivo_grabacion, imagenes, false);

				}

				try {
					layout.removeAllViews();
				} catch (Exception e) {
					// TODO: handle exception
				}

				layout.addView(Vreproductor, layoutparams_msg);

				barra_grabacion = (SeekBar) Vreproductor.findViewById(R.id.seekBar2);

				barra_grabacion.setOnSeekBarChangeListener(new SeekChanged(Vreproductor.getContext()));
				tiempo = (TextView) Vreproductor.findViewById(R.id.texttiempo);
				try {

					myPlayer = new MediaPlayer();

					if (path != null && FileUtils.isLocal(path)) {
						File file = new File(path);

						myPlayer.setDataSource(path);

					} else {
						try {

							myPlayer.setDataSource(context, grabacion);
						} catch (Exception e) {
							// TODO: handle exception
							myPlayer.setDataSource(grabacion.getPath());
						}

					}

					myPlayer.prepare();

					barra_grabacion.setMax((int) (myPlayer.getDuration()));

					int tiempomin = myPlayer.getDuration() / 60000;

					int tiemposeg = (myPlayer.getDuration() / 1000) - (60 * tiempomin);
					tiempo.setText("00:00");
					play = (ImageView) Vreproductor.findViewById(R.id.image_play);

					play.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							log_aplicacion.log_informacion("click play");
							log_aplicacion.log_informacion(archivo_grabacion);

							if (!myPlayer.isPlaying()) {

								try {

									myPlayer.start();
									play.setImageResource(R.drawable.pausa);
									final int duration = myPlayer.getDuration();
									final int amoungToupdate = duration / 1000;

									timerRunnable = new Runnable() {

										@Override
										public void run() {
											if (myPlayer != null) {
												int mCurrentPosition = myPlayer.getCurrentPosition() / 1000;

												if (mCurrentPosition < 60) {
													String stseg = "";
													if (mCurrentPosition < 10) {
														stseg = "0" + mCurrentPosition;
													} else {
														stseg = String.valueOf(mCurrentPosition);
													}

													tiempo.setText("00:" + stseg);
												} else {
													int n = mCurrentPosition / 60;
													int seg = mCurrentPosition - (60 * n);
													String stseg = "";
													if (seg < 10) {
														stseg = "0" + seg;
													} else {
														stseg = String.valueOf(seg);
													}
													tiempo.setText("0" + n + ":" + stseg);
												}
												barra_grabacion.setProgress(myPlayer.getCurrentPosition());
											}
											timerHandler.postDelayed(this, 1000);
										}
									};
									timerHandler.postDelayed(timerRunnable, 0);

								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else {
								play.setImageResource(R.drawable.ic_play);

								stopPlay();
							}
						}
					});

				} catch (Exception e) {
					// TODO: handle exception
				}
				ImageView btneliminar = (ImageView) Vreproductor.findViewById(R.id.imageneliminar);
				// start();
				btneliminar.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try {
							seleccion = "";
							setlayout_voz();

							archivo_grabacion = "";
							if (myPlayer != null) {
								myPlayer.stop();
								myPlayer.release();
								myPlayer = null;

								timerHandler.removeCallbacks(timerRunnable);

							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				});
			} else {
				layout.addView(Vreproductor, layoutparams_msg);
				play = (ImageView) Vreproductor.findViewById(R.id.image_play);

				play.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						cargar_archivo(Descarga_audio);
					}
				});

				ImageView btneliminar = (ImageView) Vreproductor.findViewById(R.id.imageneliminar);
				// start();
				btneliminar.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try {
							seleccion = "";
							setlayout_voz();

							archivo_grabacion = "";

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				});

			}
		} catch (Exception e) {
			// TODO: handle exception
			log_aplicacion.log_informacion("Error " + e.toString());
		}

	}

	public void eliminar() {

		WS_cliente servicio;

		try {
			servicio = new WS_cliente(context);
			cargar.mostrar();
			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {

					} else if (msg.what == 0) {

					}

				}
			};
			servicio.Delete_mensaje(data_servicio.mensajeedicion.getIdmessage());
			servicio.setPuente_envio(puente);
			servicio.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void pickerfecha() {

		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

			public void onDateSet(DatePicker view, int yearOf, int monthOfYear, int dayOfMonth) {
				int year = yearOf;
				int month = monthOfYear;
				int day = dayOfMonth;

				String dia = String.valueOf(day);
				String mes = String.valueOf(month + 1);

				if (dia.length() == 1) {
					dia = "0" + dia;

				}
				if (mes.length() == 1) {
					mes = "0" + mes;

				}

				String idioma = Locale.getDefault().getLanguage();
				log_aplicacion.log_informacion(idioma);
				fecha.setText(dia + "/" + mes + "/" + year);

				fecha_envio = year + "-" + mes + "-" + dia;

			}
		};

		midialogo_picker = new DatePickerDialog(context, mDateSetListener, year, month, day);
		midialogo_picker.setTitle("");

		midialogo_picker.setButton(DatePickerDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.aceptar),
				midialogo_picker);

	}

	public void mostrar_picker() {

		midialogo_picker.show();
		try {

			Button aceptar = (Button) midialogo_picker.getButton(DatePickerDialog.BUTTON_POSITIVE);

			int margen = (int) ((100) / context.getResources().getDisplayMetrics().density);
			int sdk = android.os.Build.VERSION.SDK_INT;

			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT);

			params.rightMargin = margen;

			params.gravity = Gravity.BOTTOM;

			aceptar.setLayoutParams(params);

			aceptar.setTextColor(getResources().getColor(R.color.violeta_login));
			Button cancelar = (Button) midialogo_picker.getButton(DatePickerDialog.BUTTON_NEGATIVE);

			cancelar.setTextColor(getResources().getColor(R.color.violeta_login));

		} catch (Exception e) {
			// TODO: handle exception

		}

	}

	public void agregar_imagen() {

	}

	private Handler handler = new Handler();
	private Runnable runnable = new Runnable() {
		public void run() {
			//
			// Do the stuff
			// \type name = new type();

			notification_change();
			if (mensaje != null) {
				if (mensaje.getType_file_id().equals("4")) {

					notification_change_imagen();
				}
			}

			if (borrador != null) {
				if (borrador.getType_file_id().equals("4")) {

					notification_change_imagen();
				}

			}

		}
	};

	public void notification_change() {
		try {
			layout_ben.removeAllViews();
		} catch (Exception e) {
			// TODO: handle exception

		}

		// layout_ben.addView(btn_add);

		log_aplicacion.log_informacion("ingresa lista2");

		int height = layout_ben.getHeight();

		if (lista_beneficiario_msg.size() == 0) {

			layout_ben.addView(texto_agregar);

		} else {

			for (int i = 0; i < lista_beneficiario_msg.size(); i++) {

				com.zonesoftware.fluie.CircularImageView circulo = new com.zonesoftware.fluie.CircularImageView(
						context);
//				circulo.setImageResource(R.drawable.doge);

				Imagen image_usuario = null;

				for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
					if (data_servicio.imagen_usuarios.get(l).getId_user()
							.equals(lista_beneficiario_msg.get(i).getUser_id())) {

						if(lista_beneficiario_msg.get(i).getGenero().equals("1")){
							circulo.setBackgroundResource(R.drawable.doge);
						}else{
							circulo.setBackgroundResource(R.drawable.femaledogo);
						}
//						if (data_servicio.imagen_usuarios.get(l).getImagen() != null) {
//							try {
//								circulo.setImageBitmap(data_servicio.imagen_usuarios.get(l).getImagen());
//							} catch (Exception e) {
//								// TODO: handle exception
//							}
//						} else {
//							circulo.setImageResource(R.drawable.doge);
//
//						}
						break;
					}
				}

				LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
						(int) (((double) (height * 2)) / ((double) 3)), (int) (((double) (height * 2)) / ((double) 3)));
				layoutParams.gravity = Gravity.BOTTOM;
				layoutParams.leftMargin = 15;
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
						LayoutParams.WRAP_CONTENT);
				params.gravity = Gravity.BOTTOM;

				LinearLayout layoutsbeneficiario = new LinearLayout(context);
				layoutsbeneficiario.setOrientation(LinearLayout.VERTICAL);

				layoutsbeneficiario.addView(circulo, layoutParams);
				LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT);
				paramstext.gravity = Gravity.CENTER_HORIZONTAL;
				paramstext.leftMargin = 5;
				TextView textnombre = new TextView(context);
				if (lista_beneficiario_msg.get(i).getName().length() > 8) {
					textnombre.setText(lista_beneficiario_msg.get(i).getName().substring(0, 8));
				} else {
					textnombre.setText(lista_beneficiario_msg.get(i).getName());

				}
				textnombre.setTypeface(tr);
				textnombre.setGravity(Gravity.CENTER_HORIZONTAL);

				Drawable img = getResources().getDrawable(R.drawable.x);

				img.setBounds(0, 0, 25, 25);
				// textnombre.setCompoundDrawables(null, null, img, null);
				layoutsbeneficiario.addView(textnombre, paramstext);
				click_benficiario click_beneficiario = new click_benficiario(i);
				layoutsbeneficiario.setOnClickListener(click_beneficiario);
				layout_ben.addView(layoutsbeneficiario, params);

			}
		}
		/*
		 * btn_add.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub if (viewvideo != null) { if (viewvideo.video != null) { if
		 * (viewvideo.video.isPlaying()) { viewvideo.video.pause(); } } }
		 * log_aplicacion.log_informacion("conexion " +
		 * prueba_internet.estadoConexion()); if
		 * (prueba_internet.estadoConexion() ||
		 * (!prueba_internet.estadoConexion() &&
		 * data_servicio.lista_beneficiario.size() > 0)) {
		 * 
		 * data_servicio.lista = "beneficiarios";
		 * 
		 * data_servicio.listaben = "mensaje"; Intent i = new Intent(context,
		 * ListaBeneficiarioActivity.class); startActivityForResult(i, 110); }
		 * else { Toast.makeText(getApplicationContext(),
		 * getString(R.string.no_se_puede_consultar_los_beneficiarios),
		 * Toast.LENGTH_SHORT).show(); }
		 * 
		 * } });
		 */
	}

	class click_benficiario implements View.OnClickListener {

		int posicion = 0;

		public click_benficiario(int posicion) {
			this.posicion = posicion;

		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			dialogo(getString(R.string.informacion),
					getString(R.string.desea_remover_a_)+" "+ lista_beneficiario_msg.get(posicion).getName()
							+ getString(R.string._del_mensaje_),
					getString(R.string.no), getString(R.string.si), false, true, posicion);

		}

	}

	class click_image implements View.OnClickListener {

		int posicion = 0;

		public click_image(int posicion) {
			this.posicion = posicion;

		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			log_aplicacion.log_informacion("click imagen id " + posicion);
			if (imagenes.size() > 0) {
				setlayout_imagen_previa(posicion);
			} else {
				notification_change_imagen();
			}
		}

	}

	private Bitmap decodeBitmap(String dir) {
		Bitmap bitmap = null;
		try {

			bitmap = BitmapFactory.decodeFile(dir);
		} catch (Exception e) {
			// TODO: handle exception
			log_aplicacion.log_informacion("decodeBitmap error " + e.toString());

		}
		return bitmap;

	}

	public void dialogo(String titulo, String mensaje, String boton1, String boton2, final boolean boton1estado,
			final boolean boton2estado, final int id_seleccion) {

		LayoutInflater li = LayoutInflater.from(context);
		final View prompt = li.inflate(R.layout.mensaje, null);
		TextView txtitulo = (TextView) prompt.findViewById(R.id.textmsg_titulo);

		txtitulo.setText(titulo);
		TextView txtmsg = (TextView) prompt.findViewById(R.id.textmsg);
		txtmsg.setText(mensaje);

		Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setView(prompt);
		alertDialogBuilder.setCancelable(false);
		TextView txtb1 = (TextView) prompt.findViewById(R.id.textbtn1);
		txtb1.setText(boton1);
		TextView txtb2 = (TextView) prompt.findViewById(R.id.textbtn2);

		txtb2.setText(boton2);
		txtb1.setTypeface(tm);
		txtb2.setTypeface(tm);
		txtitulo.setTypeface(tm);
		txtmsg.setTypeface(tm);

		ListView lista_opciones = (ListView) prompt.findViewById(R.id.listopciones);

		LinearLayout contenedor = (LinearLayout) prompt.findViewById(R.id.layout_msg);

		alertDialog = alertDialogBuilder.create();

		alertDialog.show();

		if (!boton1.equals("")) {

			txtb1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (boton1estado) {

					} else {
						alertDialog.cancel();
					}

				}
			});

		}

		if (!boton2.equals("")) {

			txtb2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (boton2estado) {
						lista_beneficiario_msg.remove(id_seleccion);
						notification_change();
						alertDialog.cancel();
					} else {
						alertDialog.cancel();
					}

				}
			});
		}

		// Creamos un AlertDialog y lo mostramos

	}

	private void openCamera() {
		File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
		file.mkdirs();
		TEMPORAL_PICTURE_NAME = System.currentTimeMillis() + ".jpg";
		url_capture = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator
				+ TEMPORAL_PICTURE_NAME;

		File newFile = new File(url_capture);

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(newFile));
		startActivityForResult(intent, PHOTO_CODE);
	}

	private void openaudiofile() {

		// PackageManager packageManager = getPackageManager();
		// Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		//
		//
		// intent.setType("audio/*");
		//
		// try {
		// log_aplicacion.log_informacion("ingresa a seleccion");
		// startActivityForResult(intent, GRABACION_FILE);
		// } catch (ActivityNotFoundException e) {
		// // The reason for the existence of aFileChooser
		// }

		Intent target = FileUtils.createGetContentIntent();
		// Create the chooser Intent
		Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath());
		target.setDataAndType(uri, "audio/*");

		Intent intent2 = Intent.createChooser(target, getString(R.string.chooser_title));
		try {

			startActivityForResult(intent2, GRABACION_FILE);
		} catch (ActivityNotFoundException e) {
			// The reason for the existence of aFileChooser
		}

	}

}
