package com.zonesoftware.fluie.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import servicio.log_aplicacion;

public class DbHelper extends SQLiteOpenHelper {

	private static final String DBName = "Bd_fluie.sqlite";
	private static final int DB_SCHEME_VERSION = 4;
	Context context;

	public DbHelper(Context mcontext) {
		super(mcontext, DBName, null, DB_SCHEME_VERSION);
		context = mcontext;
		log_aplicacion.log_informacion(Db_manager.CREATE_TABLE_BORRADOR);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		try {
			db.execSQL(Db_manager.CREATE_TABLE_USER);
			log_aplicacion.log_informacion(Db_manager.CREATE_TABLE_USER);

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			log_aplicacion.log_informacion(Db_manager.CREATE_TABLE_BORRADOR);
			db.execSQL(Db_manager.CREATE_TABLE_BORRADOR);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

		try {
			// Se Elimina la versi � n anterior de la tabla
			db.execSQL("DROP TABLE IF EXISTS " + Db_manager.TABLENAME_USER);

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			db.execSQL("DROP TABLE IF EXISTS " + Db_manager.TABLENAME_BORRADOR);
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			db.execSQL(Db_manager.CREATE_TABLE_USER);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			db.execSQL(Db_manager.CREATE_TABLE_BORRADOR);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
