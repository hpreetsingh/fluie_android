package com.zonesoftware.fluie;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import modelo.ItemImagenAdapter;
import modelo.Item_lista;
import modelo.Usuario;
import servicio.WS_cliente;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class BeneficiarioActivity extends ActionBarActivity {
	Button bn_seleccion;
	ArrayAdapter<String> myAdapter;
	ItemImagenAdapter adapter;
	ListView listView;
	String[] dataArray = new String[] { "India", "Androidhub4you", "Pakistan", "Srilanka", "Nepal", "Japan" };
	Context con;
	Typeface tm, tr;
	Context micontext;
	DialogoCarga cargar;
	JSONArray jsonverificadores;
	ArrayList<Item_lista> items;
	String estado = "";
	TextView textmsg;
	final String BENEFICIARIOS = "beneficiarios";
	final String VERIFICADORES = "verificadores";
	SearchView searchView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_beneficiario);
		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setIcon(R.drawable.ic_atras);
		actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_atras));
		actionBar.setDisplayUseLogoEnabled(false);
		tm = Typeface.createFromAsset(getAssets(), "font/Roboto_Medium.ttf");
		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		con = this;
		micontext = this;
		bn_seleccion = (Button) findViewById(R.id.bn_seleccion_lista);
		searchView = (SearchView) findViewById(R.id.autoCompletebuscar);
		listView = (ListView) findViewById(R.id.lista_personas);
		textmsg = (TextView) findViewById(R.id.textV_msg);
		if (!data_servicio.lista.equals("beneficiarios")) {
			estado = "Estado";

		}

		cargar = new DialogoCarga(this, getString(R.string.consultando), getString(R.string.espere_un_momento_));

		items = new ArrayList<Item_lista>();

		adapter = new ItemImagenAdapter(this, items, true);

		listView.setAdapter(adapter);

		listView.setTextFilterEnabled(true);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				data_servicio.opcion = "editar";

			}
		});

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setIconifiedByDefault(false);

		SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				// this is your adapter that will be filtered

				adapter.getFilter().filter(newText);
				System.out.println("on text chnge text: " + newText);
				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				// this is your adapter that will be filtered
				adapter.getFilter().filter(query);
				System.out.println("on query submit: " + query);
				return true;
			}
		};
		searchView.setOnQueryTextListener(textChangeListener);

		if (data_servicio.lista.equals(BENEFICIARIOS)) {
			bn_seleccion.setText(getResources().getString(R.string.agregar_beneficiario));
			searchView.setQueryHint(getResources().getString(R.string.buscar_beneficiario));

		} else if (data_servicio.lista.equals(VERIFICADORES)) {
			bn_seleccion.setText(getResources().getString(R.string.agregar_verificadores));
			searchView.setQueryHint(getResources().getString(R.string.buscar_verificador));

		}

		bn_seleccion.setTypeface(tr);

		bn_seleccion.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				data_servicio.opcion = "crear";

				Intent intent = new Intent(con, EditarPersonaActivity.class);
				startActivityForResult(intent, 200);

			}
		});

		if (data_servicio.lista.equals(BENEFICIARIOS)) {
			get_list_beneficiarios();

		} else if (data_servicio.lista.equals(VERIFICADORES)) {
			get_list_verificadores();
		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		GuardarInformacionApp.Guardar_info(outState);

	}

	public void get_list_beneficiarios() {

		data_servicio.lista_beneficiario = new ArrayList<Usuario>();
		data_servicio.lista_beneficiario.clear();
		WS_cliente servicio1;
		try {
			servicio1 = new WS_cliente(micontext);
			cargar.mostrar();

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						try {
							jsonverificadores = new JSONArray(data_servicio.json_lista_beneficiario);
							if (jsonverificadores.length() == 0) {
								textmsg.setText(con.getString(R.string.no_tiene_beneficiarios_asignados_));
								items.clear();
								adapter.notifyDataSetChanged();
								cargar.ocultar();

							} else {
								textmsg.setText("");
								textmsg.setVisibility(View.GONE);
								for (int i = 0; i < jsonverificadores.length(); i++) {

									data_servicio.usuario_beneficiario.clear();
									String id = jsonverificadores.getJSONObject(i).getString("beneficiary_id");
									data_servicio.usuario_beneficiario.setRelacion(
											jsonverificadores.getJSONObject(i).getString("relationship_id"));

									log_aplicacion.log_informacion("Relacion leida"
											+ jsonverificadores.getJSONObject(i).getString("relationship_id"));

									WS_cliente servicio3;
									try {
										servicio3 = new WS_cliente(micontext);

										Handler puente = new Handler() {
											@SuppressLint("ShowToast")
											@Override
											public void handleMessage(Message msg) {
												if (msg.what == 1) {
													if (jsonverificadores.length() == data_servicio.lista_beneficiario
															.size()) {
														cargar.ocultar();
														items.clear();

														for (int j = 0; j < data_servicio.lista_beneficiario
																.size(); j++) {
															String relacion = "";
															JSONArray jsonrelacion = null;
															try {
																jsonrelacion = new JSONArray(
																		data_servicio.json_tipo_relacion);
															} catch (JSONException e) {
																// TODO
																// Auto-generated
																// catch block
																e.printStackTrace();
															}

															for (int k = 0; k < jsonrelacion.length(); k++) {
																try {
																	log_aplicacion.log_informacion(
																			"ingresa a parseo relacion "
																					+ data_servicio.lista_beneficiario
																							.get(j).getRelacion());

																	if (jsonrelacion.getJSONObject(k)
																			.getString("idrelationship")
																			.equals(data_servicio.lista_beneficiario
																					.get(j).getRelacion())) {

																		relacion = jsonrelacion.getJSONObject(k)
																				.getString("relationship");
																		break;
																	}
																} catch (JSONException e) {
																	// TODO
																	// Auto-generated
																	// catch
																	// block
																	e.printStackTrace();
																}

															}

															log_aplicacion.log_informacion("ingresa a parseo relacion");

															log_aplicacion.log_informacion(relacion);
															items.add(new Item_lista("",
																	data_servicio.lista_beneficiario.get(j).getName()
																			+ " "
																			+ data_servicio.lista_beneficiario.get(j)
																					.getName_last(),
																	relacion, estado, data_servicio.lista_beneficiario
																			.get(j).getUser_id(),data_servicio.lista_beneficiario.get(j).getGenero()));

														}

														adapter.setitems(items);

														adapter.notifyDataSetChanged();

													}

												} else if (msg.what == 0) {

													cargar.ocultar();
												}

											}
										};
										servicio3.get_usuarios_lista(id);
										servicio3.setPuente_envio(puente);
										servicio3.execute();
										boolean imagen = true;
										for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
											if (data_servicio.imagen_usuarios.get(l).getId_user().equals(id)) {

												imagen = false;
												break;
											}
										}
										if (imagen) {
											WS_cliente servicio4;
											try {

												servicio4 = new WS_cliente(micontext);

												Handler puente4 = new Handler() {
													@SuppressLint("ShowToast")
													@Override
													public void handleMessage(Message msg) {
														if (msg.what == 1) {

														} else if (msg.what == 0) {
														}

													}
												};
												servicio4.get_imagen_user(id);
												servicio4.setPuente_envio(puente4);
												servicio4.execute();
											} catch (Exception e) {
												// TODO: handle exception
											}

										}

									} catch (UnsupportedEncodingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else if (msg.what == 0) {
						cargar.ocultar();
						if (!data_servicio.msj_error.equals("")) {

							data_servicio.msj_error = "";
						}

					}

				}
			};
			servicio1.get_lista_beneficiario(data_servicio.usuario.getUser_id());
			servicio1.setPuente_envio(puente);
			servicio1.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void get_list_verificadores() {

		data_servicio.lista_verificadores = new ArrayList<Usuario>();
		WS_cliente servicio1;
		try {
			servicio1 = new WS_cliente(micontext);
			cargar.mostrar();

			Handler puente = new Handler() {
				@SuppressLint("ShowToast")
				@Override
				public void handleMessage(Message msg) {
					if (msg.what == 1) {
						try {
							jsonverificadores = new JSONArray(data_servicio.json_lista_verificadores);
							if (jsonverificadores.length() == 0) {
								textmsg.setText(getResources().getString(R.string.no_tiene_verificadores_asignados_));

								cargar.ocultar();
								items.clear();
								adapter.notifyDataSetChanged();
							} else {
								textmsg.setText("");
								for (int i = 0; i < jsonverificadores.length(); i++) {

									String id = jsonverificadores.getJSONObject(i).getString("checker_id");
									data_servicio.usuario_beneficiario.clear();
									data_servicio.usuario_beneficiario
											.setEstado(jsonverificadores.getJSONObject(i).getString("active"));

									WS_cliente servicio3;
									try {
										servicio3 = new WS_cliente(micontext);

										Handler puente = new Handler() {
											@SuppressLint("ShowToast")
											@Override
											public void handleMessage(Message msg) {
												if (msg.what == 1) {
													if (jsonverificadores.length() == data_servicio.lista_verificadores
															.size()) {
														cargar.ocultar();
														items.clear();

														for (int j = 0; j < data_servicio.lista_verificadores
																.size(); j++) {
															String estado = "";
															if (data_servicio.lista_verificadores.get(j).getEstado()
																	.equals("1")) {

																estado = getString(R.string.confirmado);

															} else if (data_servicio.lista_verificadores.get(j)
																	.getEstado().equals("0")) {

																estado = getString(R.string.sin_confirmar);

															}

															items.add(new Item_lista("",
																	data_servicio.lista_verificadores.get(j).getName()
																			+ " "
																			+ data_servicio.lista_verificadores.get(j)
																					.getName_last(),
																	estado, "", data_servicio.lista_verificadores.get(j)
																			.getUser_id(),data_servicio.lista_verificadores.get(j).getGenero()));

														}

														adapter.setitems(items);

														adapter.notifyDataSetChanged();

													}

												} else if (msg.what == 0) {

													cargar.ocultar();
												}

											}
										};
										servicio3.get_usuarios_lista(id);
										servicio3.setPuente_envio(puente);
										servicio3.execute();

										boolean imagen = true;
										for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
											if (data_servicio.imagen_usuarios.get(l).getId_user().equals(id)) {
												imagen = false;
												break;
											}
										}
										if (imagen) {
											WS_cliente servicio4;
											try {

												servicio4 = new WS_cliente(micontext);

												Handler puente4 = new Handler() {
													@SuppressLint("ShowToast")
													@Override
													public void handleMessage(Message msg) {
														if (msg.what == 1) {

														} else if (msg.what == 0) {
														}

													}
												};
												servicio4.get_imagen_user(id);
												servicio4.setPuente_envio(puente4);
												servicio4.execute();
											} catch (Exception e) {
												// TODO: handle exception
											}

										}

									} catch (UnsupportedEncodingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else if (msg.what == 0) {
						cargar.ocultar();
						if (!data_servicio.msj_error.equals("")) {

							data_servicio.msj_error = "";
						}

					}

				}
			};
			servicio1.get_lista_verificador(data_servicio.usuario.getUser_id());
			servicio1.setPuente_envio(puente);
			servicio1.execute();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void traer_lista() {

		if (data_servicio.lista.equals("beneficiarios")) {

		} else if (data_servicio.lista.equals("verificadores")) {

		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		log_aplicacion.log_informacion("ingresa requestcode" + requestCode);

		log_aplicacion.log_informacion("ingresa result" + resultCode);
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case 200:
				searchView.setTag("");

				if (data_servicio.lista.equals("beneficiarios")) {
					get_list_beneficiarios();

				} else if (data_servicio.lista.equals("verificadores")) {
					get_list_verificadores();
				}
				break;

			}
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in action bar clicked; goto parent activity.
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
