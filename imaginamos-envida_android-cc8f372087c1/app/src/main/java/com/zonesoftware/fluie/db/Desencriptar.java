package com.zonesoftware.fluie.db;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.UnsupportedEncodingException;
// EXCEPTIONS
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
// KEY SPECIFICATIONS
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
// CIPHER / GENERATORS
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import Decoder.BASE64Decoder;

/**
 *
 * @author Manolo
 */
public class Desencriptar {

	public Desencriptar() {
	}

	Cipher dcipher;

	Desencriptar(SecretKey key, String algorithm) {
		try {

			dcipher = Cipher.getInstance(algorithm);
			dcipher.init(Cipher.DECRYPT_MODE, key);
		} catch (NoSuchPaddingException e) {
			System.out.println("EXCEPTION: NoSuchPaddingException");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("EXCEPTION: NoSuchAlgorithmException");
		} catch (InvalidKeyException e) {
			System.out.println("EXCEPTION: InvalidKeyException");
		}
	}

	Desencriptar(String passPhrase) {

		// 8-bytes Salt
		byte[] salt = { (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32, (byte) 0x56, (byte) 0x34, (byte) 0xE3,
				(byte) 0x03 };

		// Iteration count
		int iterationCount = 19;

		try {

			KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
			SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

			dcipher = Cipher.getInstance(key.getAlgorithm());

			// Prepare the parameters to the cipthers
			AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

			dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

		} catch (InvalidAlgorithmParameterException e) {
			System.out.println("EXCEPTION: InvalidAlgorithmParameterException");
		} catch (InvalidKeySpecException e) {
			System.out.println("EXCEPTION: InvalidKeySpecException");
		} catch (NoSuchPaddingException e) {
			System.out.println("EXCEPTION: NoSuchPaddingException");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("EXCEPTION: NoSuchAlgorithmException");
		} catch (InvalidKeyException e) {
			System.out.println("EXCEPTION: InvalidKeyException");
		}
	}

	public String desencriptar_seleccion(String palabra, int opcion, SecretKey clave) {

		String palabra_encriptada = "";

		if (opcion == 1) {
			// SecretKey desKey = KeyGenerator.getInstance("DES").generateKey();

			Desencriptar desEncrypter = new Desencriptar(clave, clave.getAlgorithm());
			// Encriptar la frase
			String desDecrypted = desEncrypter.decrypt(palabra);

			// Print out values

			palabra_encriptada = desDecrypted;

		}

		if (opcion == 2) {

			Desencriptar blowfishEncrypter = new Desencriptar(clave, clave.getAlgorithm());
			String blowfishDecrypted = blowfishEncrypter.decrypt(palabra);

			palabra_encriptada = blowfishDecrypted;
		}

		if (opcion == 3) {

			Desencriptar desedeEncrypter = new Desencriptar(clave, clave.getAlgorithm());
			String desedeDecrypted = desedeEncrypter.decrypt(palabra);

			palabra_encriptada = desedeDecrypted;
		}

		return palabra_encriptada;
	}

	public String desencriptar_frase(String palabra, String frase) {
		String palabra_encriptada = "";

		// Create encrypter/decrypter class
		Desencriptar desEncrypter = new Desencriptar(frase);

		// Encrypt the string
		String desDecrypted = desEncrypter.decrypt(palabra);

		// Print out values

		palabra_encriptada = desDecrypted;

		return palabra_encriptada;
	}

	public String decrypt(String str) {

		try {

			// Decode base64 to get bytes
			byte[] dec = new BASE64Decoder().decodeBuffer(str);

			// Decrypt
			byte[] utf8 = dcipher.doFinal(dec);

			// Decode using utf-8
			return new String(utf8, "UTF8");

		} catch (BadPaddingException e) {
		} catch (IllegalBlockSizeException e) {
		} catch (UnsupportedEncodingException e) {
		} catch (IOException e) {
		}
		return null;
	}

}