package com.zonesoftware.fluie.db;

public class Usuario_fluie {
	int id = 0;
	String correo = "";
	String password = "";

	public Usuario_fluie(int id, String correo, String password) {
		super();
		this.id = id;
		this.correo = correo;
		this.password = password;

	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

}
