package com.zonesoftware.fluie;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.zonesoftware.fluie.db.Borrador;
import com.zonesoftware.fluie.db.Db_manager;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import modelo.Imagen;
import modelo.Usuario;
import servicio.PruebaInternet;
import servicio.WS_cliente;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class UpdateReceiver extends BroadcastReceiver {

	String freespace = "";
	Context con;
	Db_manager db;
	ArrayList<Borrador> borradores;
	// numero de boradores subidos
	int borradores_subidos = 0;

	@Override
	public void onReceive(Context context, Intent intent) {

		con = context;
		borradores_subidos = 0;
		if (isOnline(con) && !data_servicio.subiendo_borradores) {
			get_space();
		}

	}

	public boolean isOnline(Context context) {

		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		// should check null because in air plan mode it will be null
		return (netInfo != null && netInfo.isConnected());

	}

	public void get_space() {

		try {
			WS_cliente servicio3;

			try {
				servicio3 = new WS_cliente(con);

				Handler puente = new Handler() {
					@SuppressLint("ShowToast")
					@Override
					public void handleMessage(Message msg) {
						if (msg.what == 1) {

							int porcentajeint = 0;
							int espacio = 0;
							try {

								JSONArray jo = new JSONArray(data_servicio.json_freespace);

								freespace = jo.getJSONObject(0).getString("free");
								String space = jo.getJSONObject(0).getString("space");
								String size = jo.getJSONObject(0).getString("size");
								log_aplicacion.log_informacion("espacio  " + freespace);
								log_aplicacion.log_informacion("espacio  " + space);
								log_aplicacion.log_informacion("espacio  " + size);

								try {
									int sizeint = Integer.parseInt(size);
									int spaceint = Integer.parseInt(space);
									log_aplicacion.log_informacion("espacio  " + sizeint);
									log_aplicacion.log_informacion("espacio  " + spaceint);

									espacio = (int) (((double) sizeint / (double) spaceint) * 100);

									log_aplicacion.log_informacion("espacio  " + espacio);
								} catch (Exception e) {
									// TODO: handle exception

									log_aplicacion.log_informacion("error " + e.toString());

								}

							} catch (Exception e) {
								// TODO: handle exception

								log_aplicacion.log_informacion("error 2 " + e.toString());

							}

							subir_borradores();

						} else if (msg.what == 0) {

						}

					}
				};
				servicio3.getfreespace();
				servicio3.setPuente_envio(puente);
				servicio3.execute();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
			log_aplicacion.log_informacion("Error set espacio" + e.toString());

		}

	}

	boolean mostrar_msg = true;

	public void subir_borradores() {
		mostrar_msg = true;
		PruebaInternet internet = new PruebaInternet(con);
		if (internet.estadoConexion()) {
			db = new Db_manager(con);
			borradores = db.getborradores();

			if (borradores.size() > 0) {

				for (int j = 0; j < borradores.size(); j++) {

					Borrador borrador = borradores.get(j);

					ArrayList<Imagen> imagenes = new ArrayList<Imagen>();

					if (borrador.getType_file_id().equals("4")) {
						if (!borrador.getFile().equals("")) {
							String[] array_ben = borrador.getFile().split(",");

							if (array_ben.length != 0) {
								for (int i = 0; i < array_ben.length; i++) {

									imagenes.add(new Imagen(array_ben[i]));
								}
							}
						}

					}

					String beneficiarios = borrador.getBeneficiaries();
					String beneficiarios_envio = "";
					String[] array_ben = beneficiarios.split(",");
					if (array_ben.length != 0) {
						for (int i = 0; i < array_ben.length; i++) {

							String[] array = array_ben[i].split(":");

							Usuario usu = new Usuario(array[0]);
							usu.setName(array[1]);
							if (i < array_ben.length - 1) {
								beneficiarios_envio = array[0] + ",";
							} else {

								beneficiarios_envio = array[0];
							}

						}
					} else if (!beneficiarios.equals("")) {
						String[] array = beneficiarios.split(":");

						Usuario usu = new Usuario(array[0]);
						usu.setName(array[1]);
						beneficiarios_envio = array[0];

					}

					WS_cliente servicio;
					try {
						data_servicio.subiendo_borradores = true;
						log_aplicacion.log_informacion("Tamano espacio" + Double.valueOf(freespace) + " borrador"
								+ (double) Double.valueOf(borrador.getTamano()));
						if ((double) Double.valueOf(borrador.getTamano()) < (double) Double.valueOf(freespace)
								&& !borrador.getBeneficiaries().equals("")) {
							if (mostrar_msg) {

								Toast.makeText(con, con.getString(R.string.upload_drafts), Toast.LENGTH_LONG).show();
								mostrar_msg = false;

							}

							servicio = new WS_cliente(con);
							servicio.crear_mensaje(borrador.getType_file_id(), "1", borrador.getTitle(),
									borrador.getDate_view(), borrador.getDescription(), borrador.getFile(),
									beneficiarios_envio, (ArrayList<Imagen>) imagenes.clone());
							Handler_envio puente = new Handler_envio(borrador, servicio);
							borradores_subidos++;
						}

					} catch (Exception e) {

					}

				}

			}

		}
	}

	class Handler_envio {
		Borrador borrador;

		WS_cliente ws;

		Handler puente = new Handler() {
			@SuppressLint("ShowToast")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 1) {
					borradores_subidos--;
					try {
						if (borrador != null) {
							db.deleteborrador(String.valueOf(borrador.getId_borrador()));

						}
					} catch (Exception e) {
						// TODO: handle exception
					}
					if (borradores_subidos == 0) {
						data_servicio.subiendo_borradores = false;
					}

					// dialogo_modal.dialogo_cerrar("Informacion",
					// data_servicio.msj, "", "Aceptar", true, true);

				} else if (msg.what == 0) {
					borradores_subidos--;

					if (borradores_subidos == 0) {
						data_servicio.subiendo_borradores = false;
					}
					// dialogo_modal.dialogo("Informacion",
					// data_servicio.msj_error, "", "Aceptar", true, true);

				}

			}
		};

		public Handler_envio(Borrador borrador, WS_cliente ws) {
			super();
			this.borrador = borrador;
			this.ws = ws;
			ws.setPuente_envio(puente);
			ws.execute();

		}

		public Borrador getBorrador() {
			return borrador;
		}

		public void setBorrador(Borrador borrador) {
			this.borrador = borrador;
		}

		public Handler getPuente() {
			return puente;
		}

		public void setPuente(Handler puente) {
			this.puente = puente;
		}

	}

}
