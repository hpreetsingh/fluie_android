package com.zonesoftware.fluie;

import java.util.StringTokenizer;

import android.util.Log;
import servicio.log_aplicacion;

public class Validacion_campos {

	private static String TEXTOTIPO1 = "^[a-zA-Záéíóú�?É�?ÓÚñÑ]*$";// Texto
																				// sin
	// espacios
	private static String TEXTOTIPO2 = "[A-Za-záéíóú�?É�?ÓÚñÑ������\\s]*";// Texto
	// con
	// espacios

	private static String TEXTOTIPOPASS = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
	private static String TEXTOTIPODIR = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$)$";

	private static String TEXTOTIPO3 = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";// email

	private static String TEXTOTIPO4 = "[-_.,:;¡!¿?'\"#A-Za-záéíóú�?É�?ÓÚ�Ñ������Ñ0-9\\s]*";// Texto
	// y
	// caracteres
	// especiales
	private static String TEXTOTIPO8 = "[A-Z ������]*";// Textocon tildes

	private static String TEXTOTIPO5 = "^[A-Z0-9]*$";// Texto y numeros
	private static String TEXTOTIPO6 = "^\\d*$";// Telefono
	private static String TEXTOTIPO7 = "\\d+";

	public Validacion_campos() {

	}

	public static boolean validar_numeros(String numero) {

		return numero.matches(TEXTOTIPO7);

	}

	public static boolean validar_email(String numero) {
		boolean email = false;
		email = numero.matches(TEXTOTIPO3);
		log_aplicacion.log_depueracion("validacion email " + email);

		return email;

	}

	public static boolean validar_text(String numero) {
		boolean text_espacio = false;
		text_espacio = numero.matches(TEXTOTIPO2);
		log_aplicacion.log_depueracion("validacion text " + text_espacio);

		return text_espacio;

	}

	public static boolean validar_pass(String texto) {
		boolean text_espacio = true;
		// text_espacio = texto.matches(TEXTOTIPOPASS);
		// log_aplicacion.log_depueracion("validacion text " + text_espacio);

		return text_espacio;

	}

	public static boolean validar_telefono(String numero) {
		boolean telefono = false;
		telefono = numero.matches(TEXTOTIPO6);
		log_aplicacion.log_depueracion("validacion telefono " + telefono);

		return telefono;

	}

	public static boolean validar_direccion(String numero) {
		boolean direccion = true;

		// direccion = numero.matches(TEXTOTIPODIR);

		log_aplicacion.log_depueracion("validacion direccion " + direccion);
		return direccion;
	}

	public static boolean validar_fecha(String fecha) {
		boolean esfecha = false;
		if (9 <= fecha.length() && fecha.length() <= 10) {

			Log.i("Policia", fecha);
			fecha = fecha.replace(" ", "");
			StringTokenizer st = new StringTokenizer(fecha, "/");

			if (st.countTokens() == 3) {

				String dia = st.nextToken();
				String mes = st.nextToken();
				String aniof = st.nextToken();

				if (Integer.valueOf(dia) != 0 && Integer.valueOf(dia) <= 31 && Integer.valueOf(mes) <= 12
						&& aniof.length() == 4) {
					Log.i("Policia", "valido");

					esfecha = true;
				}

			}
		}

		return esfecha;

	}

}
