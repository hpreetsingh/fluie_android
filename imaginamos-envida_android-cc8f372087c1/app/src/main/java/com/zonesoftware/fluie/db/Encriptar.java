package com.zonesoftware.fluie.db;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import Decoder.BASE64Encoder;

// EXCEPTIONS
// KEY SPECIFICATIONS
// CIPHER / GENERATORS

/**
 *
 * @author Manolo
 */
public class Encriptar {

	public Encriptar() {
	}

	Cipher ecipher;
	private SecureRandom random = new SecureRandom();

	Encriptar(SecretKey key, String algorithm) {
		try {
			ecipher = Cipher.getInstance(algorithm);
			ecipher.init(Cipher.ENCRYPT_MODE, key);
		} catch (NoSuchPaddingException e) {
			System.out.println("EXCEPTION: NoSuchPaddingException");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("EXCEPTION: NoSuchAlgorithmException");
		} catch (InvalidKeyException e) {
			System.out.println("EXCEPTION: InvalidKeyException");
		}
	}

	Encriptar(String passPhrase) {

		// 8-bytes Salt
		byte[] salt = { (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32, (byte) 0x56, (byte) 0x34, (byte) 0xE3,
				(byte) 0x03 };

		// Iteration count
		int iterationCount = 19;

		try {

			KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);

			SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

			ecipher = Cipher.getInstance(key.getAlgorithm());

			// Prepare the parameters to the cipthers
			AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

			ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);

		} catch (InvalidAlgorithmParameterException e) {
			System.out.println("EXCEPTION: InvalidAlgorithmParameterException");
		} catch (InvalidKeySpecException e) {
			System.out.println("EXCEPTION: InvalidKeySpecException");
		} catch (NoSuchPaddingException e) {
			System.out.println("EXCEPTION: NoSuchPaddingException");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("EXCEPTION: NoSuchAlgorithmException");
		} catch (InvalidKeyException e) {
			System.out.println("EXCEPTION: InvalidKeyException");
		}
	}

	public String fraserandom() {
		return new BigInteger(130, random).toString(32);

	}

	public String encriptar_seleccion(String palabra, int opcion, SecretKey clave) {

		String palabra_encriptada = "";

		if (opcion == 1) {
			Encriptar desEncrypter = new Encriptar(clave, clave.getAlgorithm());
			// Encriptar la frase
			String desEncrypted = desEncrypter.encrypt(palabra);

			// Print out values

			palabra_encriptada = desEncrypted;

		}

		if (opcion == 2) {

			Encriptar blowfishEncrypter = new Encriptar(clave, clave.getAlgorithm());
			String blowfishEncrypted = blowfishEncrypter.encrypt(palabra);

			palabra_encriptada = blowfishEncrypted;
		}

		if (opcion == 3) {

			Encriptar desedeEncrypter = new Encriptar(clave, clave.getAlgorithm());
			String desedeEncrypted = desedeEncrypter.encrypt(palabra);

			palabra_encriptada = desedeEncrypted;
		}

		return palabra_encriptada;
	}

	public String encriptar_frase(String palabra, String frase) {
		String palabra_encriptada = "";

		// Create encrypter/decrypter class
		Encriptar desEncrypter = new Encriptar(frase);

		// Encrypt the string
		String desEncrypted = desEncrypter.encrypt(palabra);

		// Print out values

		palabra_encriptada = desEncrypted;

		return palabra_encriptada;
	}

	public String encrypt(String str) {
		try {
			// Encode the string into bytes using utf-8
			byte[] utf8 = str.getBytes("UTF8");

			// Encrypt
			byte[] enc = ecipher.doFinal(utf8);

			// Encode bytes to base64 to get a string
			return new BASE64Encoder().encode(enc);

		} catch (BadPaddingException | IOException | IllegalBlockSizeException ignored) {
		}
		return null;
	}

}