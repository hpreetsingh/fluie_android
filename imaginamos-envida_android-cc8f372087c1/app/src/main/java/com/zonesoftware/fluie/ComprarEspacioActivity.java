package com.zonesoftware.fluie;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import org.json.JSONException;
import org.json.JSONObject;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import modelo.ItemplanAdapter;
import servicio.WS_cliente;
import servicio.data_servicio;

public class ComprarEspacioActivity extends ActionBarActivity {
	Typeface tr;
	private static PayPalConfiguration config = new PayPalConfiguration()

			// Start with mock environment. When ready, switch to sandbox
			// (ENVIRONMENT_SANDBOX)
			// or live (ENVIRONMENT_PRODUCTION)
			.environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK)

			.clientId("Ad7HtxqkmOC4EOvrZHbJppfyuGqKdums3AqT1NoFIdETN1OvdV-O2fSxtFvrBBBP3VK4hXk2h3QLbvM4");
	ItemplanAdapter adapter;
	Context con;
	int seleccion = 0;
	DialogoCarga cargar;
	Dialogo dialogo_modal;
	boolean openprofile=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comprar_espacio_);
		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}

		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setIcon(R.drawable.ic_atras);
		actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_atras));
		actionBar.setDisplayUseLogoEnabled(false);
		openprofile=false;
		con = this;
		dialogo_modal = new Dialogo(this, this);
		cargar = new DialogoCarga(this, "Consultando", "Espere un momento...");
		TextView text_comprar = (TextView) findViewById(R.id.text_comprar_espacio);
		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		text_comprar.setTypeface(tr);
		ListView list = (ListView) findViewById(R.id.listViewplanes);
		adapter = new ItemplanAdapter(this);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				seleccion = arg2;

				JSONObject json = (JSONObject) adapter.getItem(arg2);
				PayPalPayment payment;
				try {
					payment = new PayPalPayment(new BigDecimal(json.getString("price")), "USD",
							json.getString("name") + " " + json.getString("space_name"),
							PayPalPayment.PAYMENT_INTENT_SALE);

					Intent intent = new Intent(con, PaymentActivity.class);

					// send the same configuration for restart resiliency
					intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

					intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

					startActivityForResult(intent, 0);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		GuardarInformacionApp.Guardar_info(outState);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {

			PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
			if (confirm != null) {
				try {
					Log.i("paymentExample", confirm.toJSONObject().toString(4));

					JSONObject jsonobj = new JSONObject(confirm.toJSONObject().toString(4));

					JSONObject jsonres = jsonobj.getJSONObject("response");

					String id = jsonres.getString("id");
					String state = jsonres.getString("state");

					cargar.mostrar();
					WS_cliente servicio = new WS_cliente(con);
					Handler puente = new Handler() {
						@SuppressLint("ShowToast")
						@Override
						public void handleMessage(Message msg) {
							if (msg.what == 1) {

								WS_cliente servicio3;

								try {
									servicio3 = new WS_cliente(con);

									Handler puente = new Handler() {
										@SuppressLint("ShowToast")
										@Override
										public void handleMessage(Message msg) {
											if (msg.what == 1) {

												cargar.ocultar();

												if (!data_servicio.msj_error.equals("")) {
													dialogo_modal.dialogo_cerrar0(getString(R.string.informacion),
															data_servicio.msj_error, "", getString(R.string.aceptar),
															true, false);
													data_servicio.msj_error = "";
												} else {
													dialogo_modal.dialogo_cerrar0(getString(R.string.informacion),
															getString(R.string.compra_exitosa), "",
															getString(R.string.aceptar), true, false);

												}

											} else if (msg.what == 0) {
												cargar.ocultar();
												if (!data_servicio.msj_error.equals("")) {
													dialogo_modal.dialogo_cerrar0(getString(R.string.informacion),
															data_servicio.msj_error, "", getString(R.string.aceptar),
															true, false);
													data_servicio.msj_error = "";
												} else {
													dialogo_modal.dialogo_cerrar0(getString(R.string.informacion),
															getString(R.string.compra_exitosa), "",
															getString(R.string.aceptar), true, false);
												}
											}


										}
									};
									servicio3.getfreespace();
									servicio3.setPuente_envio(puente);
									servicio3.execute();
								} catch (UnsupportedEncodingException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else if (msg.what == 0) {
								cargar.ocultar();

							}

						}
					};
					try {

						JSONObject json = (JSONObject) adapter.getItem(seleccion);
						// comprar(String user_id, String price, String
						// payments_platform_id, String space_id, String space,
						// String reference) {
						String idbuy = "";
						Toast.makeText(this, state, Toast.LENGTH_LONG).show();
						if (state.equals("approved")) {

							idbuy = "1";
						}

						else if (state.equals("failed")) {
							idbuy = "3";

						} else if (state.equals("pending")) {
							idbuy = "2";

						} else {
							idbuy = "0";

						}

						/*
						 * 02-19 18:11:57.960: I/fluied(31052): informacion
						 * fluie: respuesta servicio
						 * {"confirm_buy":0,"user_id":"171","price":"25000",
						 * "payments_platform_id":"2",
						 * "space_id":"3","space":"1000","name":
						 * "viviana maria gomez","description":null,
						 * "log":null,"idbuy":"50","date_created":null}
						 */

						servicio.comprar(data_servicio.usuario.getUser_id(), json.getString("price"), "2",
								json.getString("idspace"), json.getString("space"), id, idbuy);
						servicio.setPuente_envio(puente);
						servicio.execute();

					} catch (Exception e) {
						// TODO: handle exception
					}
					// TODO: send 'confirm' to your server for verification.
					// see
					// https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
					// for more details.

				} catch (JSONException e) {
					Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		} else if (resultCode == Activity.RESULT_CANCELED) {
			Log.i("paymentExample", "The user canceled.");
		} else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
			Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
		}
	}

	@Override
	public void onDestroy() {
		stopService(new Intent(this, PayPalService.class));
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in action bar clicked; goto parent activity.
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
