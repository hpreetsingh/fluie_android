package com.zonesoftware.fluie;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import modelo.BadgeDrawable;
import modelo.ListPopupWindow;
import modelo.Utils;
import servicio.data_servicio;

/**
 * Fragment used for managing interactions for and presentation of a navigation
 * drawer. See the <a href=
 * "https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented
 * here.
 */
public class NavigationDrawerFragment extends Fragment {

	/**
	 * Remember the position of the selected item.
	 */
	private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

	/**
	 * Per the design guidelines, you should show the drawer on launch until the
	 * user manually expands it. This shared preference tracks this.
	 */
	private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

	/**
	 * A pointer to the current callbacks instance (the Activity).
	 */
	private NavigationDrawerCallbacks mCallbacks;

	/**
	 * Helper component that ties the action bar to the navigation drawer.
	 */
	private ActionBarDrawerToggle mDrawerToggle;

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerListView;
	private View mFragmentContainerView;
	private static final String[] STRINGS = { "Option1", "Option2", "Option3", "Option4" };
	private View anchorView;
	private int mCurrentSelectedPosition = 0;
	private boolean mFromSavedInstanceState;
	private boolean mUserLearnedDrawer;
	final String BENEFICIARIOS = "beneficiarios";
	final String VERIFICADORES = "verificadores";

	public NavigationDrawerFragment() {
	}

	NavigationDrawerFragment con;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		con = this;
		// Read in the flag indicating whether or not the user has demonstrated
		// awareness of the
		// drawer. See PREF_USER_LEARNED_DRAWER for details.
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

		if (savedInstanceState != null) {
			mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
			mFromSavedInstanceState = true;
		}

		// Select either the default item (0) or the last selected item.
		selectItem(mCurrentSelectedPosition);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Indicate that this fragment would like to influence the set of
		// actions in the action bar.
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		mDrawerListView = (ListView) inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
		mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				selectItem(position);
			}
		});
		mDrawerListView.setAdapter(new ArrayAdapter<String>(getActionBar().getThemedContext(),
				android.R.layout.simple_list_item_1, android.R.id.text1, new String[] { "", "", "", }));
		mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
		return mDrawerListView;
	}

	public boolean isDrawerOpen() {
		return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
	}

	/**
	 * Users of this fragment must call this method to set up the navigation
	 * drawer interactions.
	 *
	 * @param fragmentId
	 *            The android:id of this fragment in its activity's layout.
	 * @param drawerLayout
	 *            The DrawerLayout containing this fragment's UI.
	 */
	public void setUp(int fragmentId, DrawerLayout drawerLayout) {
		mFragmentContainerView = getActivity().findViewById(fragmentId);
		mDrawerLayout = drawerLayout;

		// set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// set up the drawer's list view with items and click listener

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		// ActionBarDrawerToggle ties together the the proper interactions
		// between the navigation drawer and the action bar app icon.
		mDrawerToggle = new ActionBarDrawerToggle(
				getActivity(), /* host Activity */
				mDrawerLayout, /* DrawerLayout object */
				R.drawable.icon_menu, /*
										 * nav drawer image to replace 'Up'
										 * caret
										 */
				R.string.navigation_drawer_open, /*
													 * "open drawer" description
													 * for accessibility
													 */
				R.string.navigation_drawer_close /*
													 * "close drawer"
													 * description for
													 * accessibility
													 */
		) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				if (!isAdded()) {
					return;
				}

				getActivity().supportInvalidateOptionsMenu(); // calls
																// onPrepareOptionsMenu()
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if (!isAdded()) {
					return;
				}

				if (!mUserLearnedDrawer) {
					// The user manually opened the drawer; store this flag to
					// prevent auto-showing
					// the navigation drawer automatically in the future.
					mUserLearnedDrawer = true;
					SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
					sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).commit();
				}

				getActivity().supportInvalidateOptionsMenu(); // calls
																// onPrepareOptionsMenu()
			}
		};

		// If the user hasn't 'learned' about the drawer, open it to introduce
		// them to the drawer,
		// per the navigation drawer design guidelines.
		if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
			mDrawerLayout.openDrawer(mFragmentContainerView);
		}

		// Defer code dependent on restoration of previous instance state.
		mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});

		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	private void selectItem(int position) {
		mCurrentSelectedPosition = position;
		if (mDrawerListView != null) {
			mDrawerListView.setItemChecked(position, true);
		}
		if (mDrawerLayout != null) {
			mDrawerLayout.closeDrawer(mFragmentContainerView);
		}
		if (mCallbacks != null) {
			mCallbacks.onNavigationDrawerItemSelected(position);
		}
	}

	public void close() {

		mDrawerLayout.closeDrawer(mFragmentContainerView);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallbacks = (NavigationDrawerCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Forward the new configuration the drawer toggle component.
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// If the drawer is open, show the global app actions in the action bar.
		// See also
		// showGlobalContextActionBar, which controls the top-left area of the
		// action bar.
		if (mDrawerLayout != null && isDrawerOpen()) {

			inflater.inflate(R.menu.usuario, menu);
			MenuItem item = menu.findItem(R.id.action_notificacion);

			ImageView imageView = new ImageView(con.getActivity());
			imageView.setBackgroundResource(R.drawable.icon_noti);
			BadgeDrawable badge;
			badge = new BadgeDrawable(con.getActivity());
			badge.setCount(0);
			imageView.setImageDrawable(badge);
			imageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

				}
			});
			item.setActionView(imageView);
//			MenuItem item = menu.findItem(R.id.action_notificacion);
//			LayerDrawable icon = (LayerDrawable) item.getIcon();
//
//			// Update LayerDrawable's BadgeDrawable
//			Utils.setBadgeCount(con.getActivity(), icon, 0);

			showGlobalContextActionBar();
		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		// if (item.getItemId() == R.id.action_notificacion) {
		// // ast.makeText(getActivity(), "Example action.",
		// // Toast.LENGTH_SHORT).show();
		//
		// final Activity activity = con.getActivity();
		// View v = activity.findViewById(R.id.action_notificacion);
		//
		// showPopuplist(viewcontent);
		// // showPopupMenu(R.id.action_notificacion);
		//
		// // showPopup(v);
		//
		// return true;
		// }
		//
		// if (item.getItemId() == R.id.action_usuarios) {
		// // Toast.makeText(getActivity(), "Example action.",
		// // Toast.LENGTH_SHORT).show();
		// // View menuItemView = findViewById(R.menu.global); // SAME ID AS
		// // MENU ID
		// // PopupMenu popupMenu = new PopupMenu(this, menuItemView);
		// // popupMenu.inflate(R.menu.counters_overflow);
		// // // ...
		// // popupMenu.show();
		//
		// final Activity activity = con.getActivity();
		// data_servicio.listaben = "";
		// View v = activity.findViewById(R.id.action_usuarios);
		//
		// // showPopup(v);
		// showPopupMenu(R.id.action_usuarios);
		// // ...
		//
		// return true;
		// }

		return super.onOptionsItemSelected(item);
	}

	private void showPopup(View v) {
		ListPopupWindow popup = new ListPopupWindow(con.getActivity());
		popup.setAdapter(new ArrayAdapter<String>(con.getActivity(), android.R.layout.simple_list_item_1, STRINGS));
		anchorView = con.getActivity().findViewById(android.R.id.home);
		popup.setAnchorView(anchorView);

		popup.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			}
		});
		popup.show();
	}

	private void showPopupMenu(int id) {

		final Activity activity = con.getActivity();

		View v = activity.findViewById(id);

		showPopupWindow(v);

	}

	void showPopupWindow(View view) {
		PopupMenu popup = new PopupMenu(con.getActivity(), view);

		try {
			Field[] fields = popup.getClass().getDeclaredFields();
			for (Field field : fields) {
				if ("mPopup".equals(field.getName())) {
					field.setAccessible(true);
					Object menuPopupHelper = field.get(popup);
					Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
					Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
					setForceIcons.invoke(menuPopupHelper, true);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		popup.getMenuInflater().inflate(R.menu.tipos, popup.getMenu());

		popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

			public boolean onMenuItemClick(MenuItem item) {
				if (item.getItemId() == R.id.action_beneficiario) {
					data_servicio.lista = BENEFICIARIOS;

					Intent intent = new Intent(con.getActivity(), BeneficiarioActivity.class);

					startActivity(intent);

				} else if (item.getItemId() == R.id.action_verificado) {
					data_servicio.lista = VERIFICADORES;
					Intent intent = new Intent(con.getActivity(), BeneficiarioActivity.class);

					startActivity(intent);

				}
				return true;
			}
		});
		popup.show();

	}

	// public int measureContentWidth(ListAdapter adapter) {
	// int maxWidth = 0;
	// int count = adapter.getCount();
	// final int widthMeasureSpec = MeasureSpec.makeMeasureSpec(0,
	// MeasureSpec.UNSPECIFIED);
	// final int heightMeasureSpec = MeasureSpec.makeMeasureSpec(0,
	// MeasureSpec.UNSPECIFIED);
	// View itemView = null;
	// for (int i = 0; i < count; i++) {
	// itemView = adapter.getView(i, itemView, getActivity());
	// itemView.measure(widthMeasureSpec, heightMeasureSpec);
	// maxWidth = Math.max(maxWidth, itemView.getMeasuredWidth());
	// }
	// return maxWidth;
	// }

	private void showPopuplist(View anchorView) {
		final ListPopupWindow popup = new ListPopupWindow(con.getActivity());

		// ItemNotificacionesAdapter adapter =new
		// ItemNotificacionesAdapter(con.getActivity(), items);

		popup.setAdapter(new ArrayAdapter<String>(con.getActivity(), android.R.layout.simple_list_item_1, STRINGS));

		ColorDrawable cd = new ColorDrawable(Color.WHITE);
		popup.setBackgroundDrawable(cd);
		popup.setAnchorView(anchorView);

		popup.setWidth(ListPopupWindow.MATCH_PARENT);

		popup.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Toast.makeText(con.getActivity(), "Clicked item " + position, Toast.LENGTH_SHORT).show();
				popup.dismiss();
			}
		});
		popup.show();
	}

	void showPopupWindownotificaction(View view) {
		PopupMenu popup = new PopupMenu(con.getActivity(), view);

		try {
			Field[] fields = popup.getClass().getDeclaredFields();
			for (Field field : fields) {
				if ("mPopup".equals(field.getName())) {
					field.setAccessible(true);
					Object menuPopupHelper = field.get(popup);
					Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
					Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
					setForceIcons.invoke(menuPopupHelper, true);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		popup.getMenuInflater().inflate(R.menu.mensaje, popup.getMenu());
		popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

			public boolean onMenuItemClick(MenuItem item) {

				return true;
			}
		});
		popup.show();

	}

	/**
	 * Per the navigation drawer design guidelines, updates the action bar to
	 * show the global app 'context', rather than just what's in the current
	 * screen.
	 */
	private void showGlobalContextActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setTitle("");
		LayoutInflater inflator = (LayoutInflater) ((ActionBarActivity) getActivity())
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflator.inflate(R.layout.titulo_action_bar, null);

		actionBar.setCustomView(v);
	}

	private ActionBar getActionBar() {
		return ((ActionBarActivity) getActivity()).getSupportActionBar();
	}

	/**
	 * Callbacks interface that all activities using this fragment must
	 * implement.
	 */
	public static interface NavigationDrawerCallbacks {
		/**
		 * Called when an item in the navigation drawer is selected.
		 */
		void onNavigationDrawerItemSelected(int position);
	}
}
