package com.zonesoftware.fluie;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import modelo.ItemImagenAdapter;
import modelo.Item_lista;
import modelo.Usuario;
import servicio.WS_cliente;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class ListaBeneficiarioActivity extends ActionBarActivity {

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        Intent returnIntent = new Intent();
        data_servicio.seleccion_beneficiario = "";

        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    ArrayAdapter<String> myAdapter;
    ItemImagenAdapter adapter;
    ListView listView;
    String[] dataArray = new String[]{"India", "Androidhub4you", "Pakistan", "Srilanka", "Nepal", "Japan"};
    Context con;
    Typeface tm, tr;
    Context micontext;
    DialogoCarga cargar;
    JSONArray jsonverificadores;
    Dialogo dialogo_modal;
    String estado = "";
    TextView textmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beneficiario2);
        dialogo_modal = new Dialogo(this, this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(R.drawable.ic_atras);
        actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_atras));
        actionBar.setDisplayUseLogoEnabled(false);
        tm = Typeface.createFromAsset(getAssets(), "font/Roboto_Medium.ttf");
        tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
        con = this;
        micontext = this;

        SearchView searchView = (SearchView) findViewById(R.id.autoCompletebuscar);
        listView = (ListView) findViewById(R.id.lista_personas);
        textmsg = (TextView) findViewById(R.id.textV_msg);
        if (!data_servicio.lista.equals("beneficiarios")) {
            estado = "Estado";

        }

        cargar = new DialogoCarga(this, getString(R.string.consultando), getString(R.string.espere_un_momento_));

        adapter = new ItemImagenAdapter(this, data_servicio.items, false);

        myAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dataArray);
        listView.setAdapter(adapter);
        listView.getLayoutParams().height = LayoutParams.WRAP_CONTENT;
        listView.setTextFilterEnabled(true);

        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                data_servicio.opcion = "editar";
                data_servicio.seleccion_beneficiario = adapter.items.get(arg2).getId();

                JSONArray jsonverificadore;
                try {
                    jsonverificadore = new JSONArray(data_servicio.json_lista_beneficiario);
                    jsonverificadore.getJSONObject(arg2).getString("beneficiary_id");

                    Intent returnIntent = new Intent();

                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                adapter.getFilter().filter(newText);
                System.out.println("on text chnge text: " + newText);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                // this is your adapter that will be filtered
                adapter.getFilter().filter(query);
                System.out.println("on query submit: " + query);
                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);

        if (data_servicio.lista.equals("beneficiarios")) {

            searchView.setQueryHint(getResources().getString(R.string.buscar_beneficiario));

        } else if (data_servicio.lista.equals("verificadores")) {

            searchView.setQueryHint(getResources().getString(R.string.buscar_verificador));

        }

        get_list_beneficiarios();

    }

    public void get_list_beneficiarios() {

        log_aplicacion.log_informacion("opcion " + data_servicio.listaben);
        log_aplicacion.log_informacion("tamano " + data_servicio.items.size());

        if (data_servicio.items.size() > 0 && data_servicio.lista_beneficiario.size() > 0) {
            adapter.setitems(data_servicio.items);

            adapter.notifyDataSetChanged();

        } else {
            data_servicio.lista_beneficiario = new ArrayList<Usuario>();
            WS_cliente servicio1;
            try {

                if (data_servicio.lista_beneficiario.size() == 0) {
                    servicio1 = new WS_cliente(micontext);
                    cargar.mostrar();

                    Handler puente = new Handler() {
                        @SuppressLint("ShowToast")
                        @Override
                        public void handleMessage(Message msg) {
                            if (msg.what == 1) {
                                try {
                                    jsonverificadores = new JSONArray(data_servicio.json_lista_beneficiario);
                                    if (jsonverificadores.length() == 0) {

                                        dialogo_modal.dialogo_cerrar1(getString(R.string.notificación),
                                                getString(R.string.tiene_beneficiarios), "", getString(R.string.acceptok), true, true);
                                        textmsg.setText("");
                                    } else {
                                        textmsg.setVisibility(View.GONE);
                                        for (int i = 0; i < jsonverificadores.length(); i++) {

                                            String id = jsonverificadores.getJSONObject(i).getString("beneficiary_id");

                                            WS_cliente servicio3;
                                            try {
                                                servicio3 = new WS_cliente(micontext);

                                                Handler puente = new Handler() {
                                                    @SuppressLint("ShowToast")
                                                    @Override
                                                    public void handleMessage(Message msg) {
                                                        if (msg.what == 1) {
                                                            if (jsonverificadores
                                                                    .length() == data_servicio.lista_beneficiario
                                                                    .size()) {
                                                                cargar.ocultar();
                                                                data_servicio.items.clear();
                                                                for (int j = 0; j < data_servicio.lista_beneficiario
                                                                        .size(); j++) {
                                                                    String relacion = "";
                                                                    JSONArray jsonrelacion = null;
                                                                    try {
                                                                        jsonrelacion = new JSONArray(
                                                                                data_servicio.json_tipo_relacion);
                                                                    } catch (JSONException e) {
                                                                        // TODO
                                                                        // Auto-generated
                                                                        // catch
                                                                        // block
                                                                        e.printStackTrace();
                                                                    }

                                                                    for (int k = 0; k < jsonrelacion.length(); k++) {
                                                                        try {
                                                                            log_aplicacion.log_informacion(
                                                                                    "ingresa a parseo relacion "
                                                                                            + data_servicio.lista_beneficiario
                                                                                            .get(j)
                                                                                            .getRelacion());

                                                                            if (jsonrelacion.getJSONObject(k)
                                                                                    .getString("idrelationship")
                                                                                    .equals(data_servicio.lista_beneficiario
                                                                                            .get(j).getRelacion())) {

                                                                                relacion = jsonrelacion.getJSONObject(k)
                                                                                        .getString("relationship");
                                                                                break;
                                                                            }
                                                                        } catch (JSONException e) {
                                                                            // TODO
                                                                            // Auto-generated
                                                                            // catch
                                                                            // block
                                                                            e.printStackTrace();
                                                                        }

                                                                    }

                                                                    data_servicio.items.add(new Item_lista("",
                                                                            data_servicio.lista_beneficiario.get(j)
                                                                                    .getName()
                                                                                    + " "
                                                                                    + data_servicio.lista_beneficiario
                                                                                    .get(j).getName_last(),
                                                                            relacion, estado,
                                                                            data_servicio.lista_beneficiario.get(j)
                                                                                    .getUser_id(),data_servicio.lista_beneficiario.get(j)
                                                                            .getGenero()));
                                                                }
                                                                adapter.setitems(data_servicio.items);
                                                                adapter.notifyDataSetChanged();

                                                            }

                                                        } else if (msg.what == 0) {

                                                            cargar.ocultar();
                                                        }

                                                    }
                                                };
                                                servicio3.get_usuarios_lista(id);
                                                servicio3.setPuente_envio(puente);
                                                servicio3.execute();

                                                boolean imagen = true;
                                                for (int l = 0; l < data_servicio.imagen_usuarios.size(); l++) {
                                                    if (data_servicio.imagen_usuarios.get(l).getId_user().equals(id)) {

                                                        imagen = false;
                                                        break;
                                                    }
                                                }
                                                if (imagen) {
                                                    WS_cliente servicio4;
                                                    try {

                                                        servicio4 = new WS_cliente(micontext);

                                                        Handler puente4 = new Handler() {
                                                            @SuppressLint("ShowToast")
                                                            @Override
                                                            public void handleMessage(Message msg) {
                                                                if (msg.what == 1) {

                                                                } else if (msg.what == 0) {
                                                                }

                                                            }
                                                        };
                                                        servicio4.get_imagen_user(id);
                                                        servicio4.setPuente_envio(puente4);
                                                        servicio4.execute();
                                                    } catch (Exception e) {
                                                        // TODO: handle
                                                        // exception
                                                    }

                                                }

                                            } catch (UnsupportedEncodingException e) {
                                                // TODO Auto-generated catch
                                                // block
                                                e.printStackTrace();
                                            } catch (JSONException e) {
                                                // TODO Auto-generated catch
                                                // block
                                                e.printStackTrace();
                                            }

                                        }
                                    }
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                            } else if (msg.what == 0) {
                                cargar.ocultar();
                                if (!data_servicio.msj_error.equals("")) {

                                    data_servicio.msj_error = "";
                                }

                            }

                        }
                    };
                    servicio1.get_lista_beneficiario(data_servicio.usuario.getUser_id());
                    servicio1.setPuente_envio(puente);
                    servicio1.execute();
                } else {
                    data_servicio.items.clear();
                    for (int j = 0; j < data_servicio.lista_beneficiario.size(); j++) {

                        data_servicio.items.add(new Item_lista("",
                                data_servicio.lista_beneficiario.get(j).getName() + " "
                                        + data_servicio.lista_beneficiario.get(j).getName_last(),
                                "", estado, data_servicio.lista_beneficiario.get(j).getUser_id(),data_servicio.lista_beneficiario.get(j).getGenero()));

                    }
                    adapter.notifyDataSetChanged();

                }

            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public void traer_lista() {

        if (data_servicio.lista.equals("beneficiarios")) {

        } else if (data_servicio.lista.equals("verificadores")) {

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
