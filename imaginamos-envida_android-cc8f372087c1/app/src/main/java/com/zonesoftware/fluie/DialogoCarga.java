package com.zonesoftware.fluie;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public class DialogoCarga {

	private Context micontext;
	private ProgressDialog dialog;

	String titulo, msg;
	boolean visible = false;

	public DialogoCarga(Context micontext, String titulo, String msg) {
		super();
		this.micontext = micontext;
		this.dialog = dialog;
		this.titulo = titulo;
		this.msg = msg;

	}

	public void mostrar() {
		if (!visible) {
			dialogo_carga(titulo, msg);
			visible = true;
		}
	}

	public void ocultar() {
		if (visible) {
			dialog.dismiss();
			visible = false;
		}
	}

	public Context getMicontext() {
		return micontext;
	}

	public void setMicontext(Context micontext) {
		this.micontext = micontext;
	}

	public ProgressDialog getDialog() {
		return dialog;
	}

	public void setDialog(ProgressDialog dialog) {
		this.dialog = dialog;
	}

	public void dialogo_carga(String titulo, String msg) {

		dialog = ProgressDialog.show(micontext, titulo, msg);
		
		try {
			
		
		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(micontext.getResources().getColor(R.color.transparent));
		int textViewId = dialog.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
		TextView tv = (TextView) dialog.findViewById(textViewId);
		tv.setTextColor(micontext.getResources().getColor(R.color.black));
		} catch (Exception e) {
			// TODO: handle exception
		}
		dialog.setOnKeyListener(new Dialog.OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					// finish();
					dialog.dismiss();
				}
				return true;
			}
		});

	}

}
