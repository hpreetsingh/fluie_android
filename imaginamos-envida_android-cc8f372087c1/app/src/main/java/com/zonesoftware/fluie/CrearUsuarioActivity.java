package com.zonesoftware.fluie;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import servicio.consultar_servicios;
import servicio.data_servicio;
import servicio.log_aplicacion;

public class CrearUsuarioActivity extends ActionBarActivity {
	EditText nombre;
	EditText apellido;

	EditText movil;
	EditText direccion;
	EditText correo;
	EditText correo_opcional;
	EditText contrsena;
	EditText contrasena2;
	EditText identificacion;
	consultar_servicios init;
	String APP_DIRECTORY = "myPictureApp/";
	String MEDIA_DIRECTORY = APP_DIRECTORY + "media";
	String TEMPORAL_PICTURE_NAME = "temporal.jpg";
	String dir = "";
	final int PHOTO_CODE = 100;
	final int SELECT_PICTURE = 200;

	CircularImageView imagen_perfil;
	Context context;
	Typeface tm, tr;
	Spinner paises, spinnertipo_documento = null;
	AlertDialog alertDialog = null;
	CheckBox ch;
	Dialogo dialogo = null;
	JSONArray jarray_documentos = null;
	JSONArray jarray = null;
	String image_url = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_crear_usuario_);
		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}
		context = this;
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setIcon(R.drawable.ic_atras);
		actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_atras));
		actionBar.setDisplayUseLogoEnabled(false);
		dialogo = new Dialogo(this, this);
		tm = Typeface.createFromAsset(getAssets(), "font/Roboto_Medium.ttf");
		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");
		nombre = (EditText) findViewById(R.id.editTextnombre);
		apellido = (EditText) findViewById(R.id.editTextapellidos);

		movil = (EditText) findViewById(R.id.editmovil);
		direccion = (EditText) findViewById(R.id.editdireccion);
		correo = (EditText) findViewById(R.id.editcorreo);
		correo_opcional = (EditText) findViewById(R.id.editcorreo2);
		contrsena = (EditText) findViewById(R.id.editcontrasena1);
		contrasena2 = (EditText) findViewById(R.id.editcontrasena2);
		identificacion = (EditText) findViewById(R.id.editidentificacion);
		nombre.setTypeface(tr);
		apellido.setTypeface(tr);

		movil.setTypeface(tr);
		direccion.setTypeface(tr);
		correo.setTypeface(tr);
		correo_opcional.setTypeface(tr);
		contrsena.setTypeface(tr);
		contrasena2.setTypeface(tr);
		ch = (CheckBox) findViewById(R.id.checkBoxterminos);
		TextView teminos = (TextView) findViewById(R.id.textvterminos);
		teminos.setText(Html.fromHtml(getString(R.string.acepto) + " <font color=\"blue\"><u> "
				+ getResources().getString(R.string.terminos_y_condiciones) + "</u></font>"));

		teminos.setTypeface(tm);
		teminos.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startactivity();
			}
		});

		init = new consultar_servicios(this, this);

		imagen_perfil = (CircularImageView) findViewById(R.id.img_perfil);
		FrameLayout frame_perfil = (FrameLayout) findViewById(R.id.frame_imagen);
		frame_perfil.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogo(getString(R.string.a_adir_imagen), "", "", getString(R.string.cancelar), true, false);
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
				// // TODO Auto-generated method stub
				// final CharSequence[] options = { "Tomar foto", "Elegir de
				// galeria" };
				//
				// final AlertDialog.Builder builder = new
				// AlertDialog.Builder(context);
				// builder.setTitle("Elige una opcion :D");
				// builder.setItems(options, new
				// DialogInterface.OnClickListener() {
				// @Override
				// public void onClick(DialogInterface dialog, int seleccion) {
				// if (options[seleccion] == "Tomar foto") {
				// openCamera();
				// } else if (options[seleccion] == "Elegir de galeria") {
				// Intent intent = new Intent(Intent.ACTION_PICK,
				// android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				// intent.setType("image/*");
				// startActivityForResult(intent.createChooser(intent,
				// "Selecciona app de imagen"),
				// SELECT_PICTURE);
				// } else if (options[seleccion] == "Cancelar") {
				// dialog.dismiss();
				// }
				// }
				// });
				// builder.show();
			}
		});

		try {
			jarray = new JSONArray(data_servicio.json_paises);

			paises = (Spinner) findViewById(R.id.spinnerpais);
			ArrayList<String> lista_paises = new ArrayList<String>();
			lista_paises.add(getString(R.string.seleccione));
			for (int i = 0; i < jarray.length(); i++) {
				lista_paises.add(jarray.getJSONObject(i).getString("country"));

			}

			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner, lista_paises);
			dataAdapter.setDropDownViewResource(R.layout.spinner2);
			paises.setAdapter(dataAdapter);

			jarray_documentos = new JSONArray(data_servicio.json_list_documentos);

			spinnertipo_documento = (Spinner) findViewById(R.id.spinnertipo_documento);
			ArrayList<String> lista_documentos = new ArrayList<String>();
			lista_documentos.add(getString(R.string.documento));
			for (int i = 0; i < jarray_documentos.length(); i++) {
				lista_documentos.add(jarray_documentos.getJSONObject(i).getString("name"));

			}

			ArrayAdapter<String> dataAdapterdoc = new ArrayAdapter<String>(this, R.layout.spinner, lista_documentos);
			dataAdapterdoc.setDropDownViewResource(R.layout.spinner2);
			spinnertipo_documento.setAdapter(dataAdapterdoc);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Button btnguardar = (Button) findViewById(R.id.buttonguardar);
		btnguardar.setTypeface(tm);
		btnguardar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
				if (validar()) {
					if (!ch.isChecked()) {

						dialogo.dialogo(getString(R.string.campo_obligatorio), "Debe aceptar los teminos y condiciones",
								"", "Aceptar", true, false);

					} else {
						int selecion_pais = paises.getSelectedItemPosition();
						String id_pais = "";
						if (selecion_pais != 0) {
							try {
								id_pais = jarray.getJSONObject(selecion_pais - 1).getString("idcountry");
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

						int seleccion = spinnertipo_documento.getSelectedItemPosition();

						String id_doc = "0";
						if (seleccion != 0) {
							try {
								id_doc = jarray_documentos.getJSONObject(seleccion - 1)
										.getString("idtype_identification");
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

						try {
							init.registar_usuario(correo.getText().toString(), contrsena.getText().toString(),
									nombre.getText().toString(), apellido.getText().toString(),
									identificacion.getText().toString(), String.valueOf(id_pais),
									movil.getText().toString(), direccion.getText().toString(), image_url,
									correo_opcional.getText().toString(), "0", contrsena.getText().toString(),
									contrasena2.getText().toString(), "1", id_doc);

						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}
			}
		});

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		GuardarInformacionApp.Guardar_info(outState);

	}

	public void startactivity() {
		Intent intent = new Intent(this, TeminosCondicionesActivity.class);
		startActivity(intent);

	}

	public boolean validar() {
		boolean campos_validos = true;
		Validacion_campos validar = new Validacion_campos();

		if (validar.validar_text(nombre.getText().toString())) {

			nombre.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			nombre.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}

		if (validar.validar_text(apellido.getText().toString())) {

			apellido.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			apellido.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}
		if (validar.validar_numeros(identificacion.getText().toString())) {

			identificacion.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			identificacion.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}

		if (validar.validar_telefono(movil.getText().toString())) {

			movil.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			movil.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}

		if (validar.validar_direccion(direccion.getText().toString())) {

			direccion.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			direccion.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}

		if (validar.validar_email(correo.getText().toString())) {

			correo.setBackground(getResources().getDrawable(R.drawable.edit_text));
		} else {
			campos_validos = false;
			correo.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}
		if (!correo_opcional.getText().toString().equals("")) {
			if (validar.validar_email(correo_opcional.getText().toString())) {

				correo_opcional.setBackground(getResources().getDrawable(R.drawable.edit_text));
			} else {
				campos_validos = false;
				correo_opcional.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

			}
		}

		if (!campos_validos) {
			Toast.makeText(this, getString(R.string.verifique_los_campos_existen_campos_erroneos_), Toast.LENGTH_LONG)
					.show();
		}

		if (validar.validar_pass(contrsena.getText().toString())) {

			contrsena.setBackground(getResources().getDrawable(R.drawable.edit_text));

		} else {

			campos_validos = false;
			contrsena.setBackground(getResources().getDrawable(R.drawable.edit_text_error));

		}

		if (contrsena.getText().toString().equals(contrasena2.getText().toString())) {

			contrasena2.setBackground(getResources().getDrawable(R.drawable.edit_text));

		} else {

			contrasena2.setBackground(getResources().getDrawable(R.drawable.edit_text_error));
			if (campos_validos) {

				Toast.makeText(this, getString(R.string.las_contrase_as_no_coinsiden), Toast.LENGTH_LONG).show();

			}
			campos_validos = false;
		}

		if (paises.getSelectedItemPosition() == 0) {

			if (campos_validos) {
				Toast.makeText(this, getString(R.string.selecciones_el_pais), Toast.LENGTH_LONG).show();
			}
			campos_validos = false;

		}

		if (spinnertipo_documento.getSelectedItemPosition() == 0) {

			if (campos_validos) {
				Toast.makeText(this, getString(R.string.seleccion_el_tipo_de_documento), Toast.LENGTH_LONG).show();
			}
			campos_validos = false;

		}

		return campos_validos;
	}

	public void dialogo(String titulo, String mensaje, String boton1, String boton2, final boolean boton1estado,
			final boolean boton2estado) {

		LayoutInflater li = LayoutInflater.from(context);
		final View prompt = li.inflate(R.layout.mensaje, null);
		TextView txtitulo = (TextView) prompt.findViewById(R.id.textmsg_titulo);

		txtitulo.setText(titulo);
		TextView txtmsg = (TextView) prompt.findViewById(R.id.textmsg);
		txtmsg.setText(mensaje);

		Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setView(prompt);
		alertDialogBuilder.setCancelable(false);
		TextView txtb1 = (TextView) prompt.findViewById(R.id.textbtn1);
		txtb1.setText(boton1);
		TextView txtb2 = (TextView) prompt.findViewById(R.id.textbtn2);

		txtb2.setText(boton2);
		txtb1.setTypeface(tm);
		txtb2.setTypeface(tm);
		txtitulo.setTypeface(tm);
		txtmsg.setTypeface(tm);

		ListView lista_opciones = (ListView) prompt.findViewById(R.id.listopciones);

		LinearLayout contenedor = (LinearLayout) prompt.findViewById(R.id.layout_msg);

		final String[] options = { getString(R.string.tomar_foto), getString(R.string.elegir_de_galeria) };

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				android.R.id.text1, options);

		lista_opciones.setAdapter(adapter);

		lista_opciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				if (options[position].equals(context.getString(R.string.tomar_foto))) {
					openCamera();
				} else if (options[position].equals(context.getString(R.string.elegir_de_galeria))) {
					Intent intent = new Intent(Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					startActivityForResult(
							intent.createChooser(intent, context.getString(R.string.selecciona_app_de_imagen)),
							SELECT_PICTURE);
				}
			}

		});

		alertDialog = alertDialogBuilder.create();

		alertDialog.show();

		if (!boton1.equals("")) {

			txtb1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (boton1estado) {

					} else {
						alertDialog.cancel();
					}

				}
			});

		}

		if (!boton2.equals("")) {

			txtb2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (boton2estado) {

					} else {
						alertDialog.cancel();
					}

				}
			});
		}

		// Creamos un AlertDialog y lo mostramos

	}

	private void openCamera() {
		File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
		file.mkdirs();

		String path = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator
				+ TEMPORAL_PICTURE_NAME;

		File newFile = new File(path);

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(newFile));
		startActivityForResult(intent, PHOTO_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try {

			switch (requestCode) {
			case PHOTO_CODE:
				if (resultCode == RESULT_OK) {
					dir = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator
							+ TEMPORAL_PICTURE_NAME;

					try {
						Bitmap bitmap = decodeBitmap(dir);
						guardarImagen(bitmap);
						bitmap.prepareToDraw();
						int nh = (int) (bitmap.getHeight() * (256.0 / bitmap.getWidth()));
						bitmap = Bitmap.createScaledBitmap(bitmap, 256, nh, true);

						imagen_perfil.setImageBitmap(bitmap);
						imagen_perfil.setScaleType(ImageView.ScaleType.FIT_CENTER);

					} catch (Exception e) {
						// TODO: handle exception
					}

					alertDialog.cancel();
					log_aplicacion.log_informacion("url: " + dir);
				}
				break;

			case SELECT_PICTURE:
				if (resultCode == RESULT_OK) {
					Uri path = data.getData();

					imagen_perfil.setImageURI(path);

					Uri uri = data.getData();

					try {
						Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
						guardarImagen(bitmap);
						bitmap.prepareToDraw();
						int nh = (int) (bitmap.getHeight() * (256.0 / bitmap.getWidth()));
						bitmap = Bitmap.createScaledBitmap(bitmap, 256, nh, true);

						imagen_perfil.setImageBitmap(bitmap);
						imagen_perfil.setScaleType(ImageView.ScaleType.CENTER_CROP);

						// Log.d(TAG, String.valueOf(bitmap));
					} catch (Exception e) {
						// TODO: handle exception
					}

					dir = path.getPath().toString();

					alertDialog.cancel();
					log_aplicacion.log_informacion("url: " + path);

				}
				break;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private Bitmap descargarImagen(String imageHttpAddress) {
		URL imageUrl = null;
		Bitmap imagen = null;
		try {
			imageUrl = new URL(imageHttpAddress);
			HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
			conn.connect();
			imagen = BitmapFactory.decodeStream(conn.getInputStream());
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return imagen;
	}

	private String guardarImagen(Bitmap imagen) {

		// File dirImages = cw.getDir("perfil", Context.MODE_PRIVATE);

		File dirImages = new File(Environment.getExternalStorageDirectory() + "/Fluie/");
		File myPath = new File(dirImages, "perfil.png");

		FileOutputStream fos = null;

		data_servicio.usuario.setImage(Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png");
		image_url = Environment.getExternalStorageDirectory() + "/Fluie/" + "perfil.png";
		try {
			fos = new FileOutputStream(myPath);
			imagen.compress(Bitmap.CompressFormat.JPEG, 100, fos);
			fos.flush();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return myPath.getAbsolutePath();
	}

	private Bitmap decodeBitmap(String dir) {
		Bitmap bitmap;
		bitmap = BitmapFactory.decodeFile(dir);

		// RectF drawableRect = new RectF ( 0 , 0 , 1080 ,1920 );
		// RectF viewRect = new RectF ( 0 , 0 , 100 , 100 );
		// Matrix matrix=new Matrix();
		// matrix.setRectToRect ( drawableRect , viewRect , Matrix . ScaleToFit
		// . CENTER );
		return bitmap;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in action bar clicked; goto parent activity.
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public static Bitmap getRoundedRectBitmap(Bitmap bitmap, int pixels) {
		Bitmap result = null;
		try {
			result = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
			Canvas canvas = new Canvas(result);

			int color = 0xff424242;
			Paint paint = new Paint();
			Rect rect = new Rect(0, 0, 200, 200);

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawCircle(50, 50, 50, paint);
			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			canvas.drawBitmap(bitmap, rect, rect, paint);

		} catch (NullPointerException e) {
		} catch (OutOfMemoryError o) {
		}
		return result;
	}

}
