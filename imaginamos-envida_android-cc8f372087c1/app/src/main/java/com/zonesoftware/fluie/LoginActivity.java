package com.zonesoftware.fluie;

import java.io.UnsupportedEncodingException;

import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import servicio.consultar_servicios;
import servicio.data_servicio;

public class LoginActivity extends Activity {
	consultar_servicios init;
	EditText elogin;
	EditText epassword;
	Context con;
	Dialogo dialogo = null;
	Dialogo dialogo_modal;
	Typeface tm, tr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			GuardarInformacionApp.Cargar_info(savedInstanceState);
		}

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);
		init = new consultar_servicios(this, this);
		elogin = (EditText) findViewById(R.id.editlogin);
		epassword = (EditText) findViewById(R.id.editpassword);
		con = this;
		TextView tvrestabece = (TextView) findViewById(R.id.text_restablecer);
		tvrestabece.setText(Html.fromHtml((String) getResources().getString(R.string.restablecer)));
		tm = Typeface.createFromAsset(getAssets(), "font/Roboto_Medium.ttf");
		tr = Typeface.createFromAsset(getAssets(), "font/Roboto_Regular.ttf");

		elogin.setTypeface(tr);
		epassword.setTypeface(tr);

		Button btnlogin = (Button) findViewById(R.id.buttonlogin);

		Button btncrear_usuario = (Button) findViewById(R.id.buttoncrear);
		dialogo_modal = new Dialogo(this, this);
		tvrestabece.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(con, RecuperaContrasenaActivity.class);
				startActivity(intent);
			}
		});

		btnlogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String login = elogin.getText().toString();
				String contrasena = epassword.getText().toString();
				try {

					if (!login.equals("") && !contrasena.equals("")) {
						data_servicio.email_usuario = login;

						init.login(login, contrasena, true);
						InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
					} else {
						if (!login.equals("") && contrasena.equals("")) {

							// Toast.makeText(con, "Ingrese la contrase�a",
							// Toast.LENGTH_SHORT).show();
							dialogo_modal.dialogo("Error", getString(R.string.ingrese_la_contrase_a), "",
									getString(R.string.aceptar), true, false);
						}
					}
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		btncrear_usuario.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try {
					init.crear_usuario();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		btncrear_usuario.setTypeface(tr);
		btnlogin.setTypeface(tr);
		tvrestabece.setTypeface(tr);

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		GuardarInformacionApp.Guardar_info(outState);

	}
}
